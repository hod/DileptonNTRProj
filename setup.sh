#!/bin/sh

### -------> OBSOLETE
### check the release number here:
### https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease
### OBSOLETE -------->

### see some instructions
### https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/

setupATLAS

mkdir -p source build run run/plots

cd source

#asetup AnalysisBase,2.6.3,here
#asetup 21.2,AnalysisBase,latest
#asetup 21.2.0,AnalysisBase
asetup 21.2,AnalysisBase,latest

cd -

source build/*/setup.sh
