// ROOT include(s):
#include <TChain.h>
#include <TSystem.h>

// Local include(s):
// //#include "DileptonNTReader/ElectronCalibAlg.h"
// //#include "DileptonNTReader/ElectronSelectionAlg.h"

#include "DileptonNTReader/DileptonNTReader.h"

// Tool include(s):
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/LocalDriver.h"
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>


int main(int argc, char* argv[])
{
   // Check the number of parameters
    if(argc<3)
    {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << "  full_path_to_sample   submitDir" << std::endl;
        return 1;
    }
    TString samplepath = argv[1];
    TString submitDir  = argv[2];
    TString histpath   = samplepath;
    histpath = histpath.ReplaceAll("minitrees.root","hist");
    std::cout << argv[0] << " running with sample    : " << samplepath << std::endl;
    std::cout << argv[0] << " running with submitDir : " << submitDir  << std::endl;
    std::cout << argv[0] << " running with hist      : " << histpath   << std::endl;


   // Set up a TChain on some local files:
   ::TChain chain( "nominal" );
   chain.Add( samplepath+"/*.root*" );

   // Use it to set up a sample with:
   SH::SampleHandler sh;
   sh.add( SH::makeFromTChain( "sample", chain ) );
   sh.print();

   // Create an EventLoop job:
   EL::Job job;
   job.sampleHandler( sh );

   // my algorithm
   DNR::DileptonNTReader* alg = new DNR::DileptonNTReader();

   // define an output and an ntuple associated to that output
   EL::OutputStream output("tinytrees");
   job.outputAdd(output);
   EL::NTupleSvc *ntuple = new EL::NTupleSvc("tinytrees");
   job.algsAdd(ntuple);
   alg->outputName = "tinytrees"; // give the name of the output to the algorithm
   alg->nominalmet = "wmuon_cst"; // give the name of the nominal MET collection to the algorithm
   alg->gridhistpath = histpath;  // give the name of the grid hist path to the algorithm
   alg->otree_sel    = "preOR";   // give the name of the selection to use for the output tree to the algorithm

   // Set up the analysis algorithms:
   //job.algsAdd( new DNR::ElectronCalibAlg() );
   //job.algsAdd( new DNR::ElectronSelectionAlg() );
   job.algsAdd( alg );

   // Run the job using the direct driver:
   EL::DirectDriver driver;
   driver.submit( job, submitDir.Data() );
   
   // Run the job using the local driver:
   //EL::LocalDriver driver;
   //driver.submit( job, "mc16_13TeV" );

   return 0;
}
