
// System include(s):
#include <algorithm>
#include <memory>

// ROOT include(s):
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

// Tool include(s):
#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "CxxUtils/fpcompare.h"

// Local include(s):
#include "DileptonNTReader/DileptonNTReader.h"
#include "check.h"

// Declare the class to ROOT:
ClassImp( DNR::DileptonNTReader )



namespace DNR
{	
   //// constructor
   DileptonNTReader::DileptonNTReader() : EL::Algorithm() {}

   EL::StatusCode DileptonNTReader::setupJob( EL::Job& job )
   {
      std::cout << "setupJob" << std::endl;

      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode DileptonNTReader::changeInput( bool firstFile )
   {
      std::cout << "changeInput" << std::endl;

      //// declare all branches
      DecalreInputBranches();

      //// set all branches
      SetInputBranches();

      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::histInitialize()
   {
      std::cout << "histInitialize" << std::endl;

      /// firstoff, get the information from the grid processing
      GetInfoFromGridHist();

      //// the nominal MET to use
      std::cout << "nominal met collection used: " << nominalmet << std::endl; // from the steering

      //// the grid hist path to use
      std::cout << "grid hist path used: " << gridhistpath << std::endl; // from the steering

      // add here as many selections you like...
      // it has to correspond to the assignments in InitSelection()
      selections.push_back("preOR");
      selections.push_back("pstOR");

      //////////////////////////////////////////////////////////

      // these are the basic channels we have (the inclusive ones)
      leptonchannels.push_back("ee");
      leptonchannels.push_back("uu");
      leptonchannels.push_back("eu");

      //// add here as many channels (inclusive and exclusive) you like...
      for(unsigned int i=0 ; i<leptonchannels.size() ; ++i)
      {
         TString lchn = leptonchannels[i];
         channels.push_back(lchn);
         channels.push_back(lchn+"0b"); // b-veto
         channels.push_back(lchn+"1b");
         channels.push_back(lchn+"2b");
         channels.push_back(lchn+"met");
      }

      //// add here the channels for the cutflow
      vector<TString> vts;
      for(unsigned int i=0 ; i<channels.size() ; ++i)
      {
         TString chn = channels[i];
         cuts.insert(make_pair(chn,vts));
      }
      //// add here the preselection, common to all channels
      for(unsigned int i=0 ; i<channels.size() ; ++i)
      {
         TString chn = channels[i];
         cuts[chn].push_back("evt:xAOD");
         cuts[chn].push_back("evt:DxAOD");
         cuts[chn].push_back("evt:GRL");
         cuts[chn].push_back("evt:cleaning");
         // cuts[chn].push_back("evt:vertex");
         // cuts[chn].push_back("evt:MET_cleaning");

         if(chn.BeginsWith("ee"))
         {
            //// dielectron specific baseline cuts -> need to match the content and order in ApplySelection -> can be taken from config file
            cuts[chn].push_back("e:all");
            cuts[chn].push_back("e:2any");
            cuts[chn].push_back("e:LHID");
            cuts[chn].push_back("e:eta");
            cuts[chn].push_back("e:pt");
            // cuts[chn].push_back("e:ChID");
            cuts[chn].push_back("e:OQ");
            cuts[chn].push_back("e:d0sig");
            cuts[chn].push_back("e:z0delta");
			cuts[chn].push_back("e:iso");  /*---*/ last_e_cut = "e:iso"; /*---*/
            cuts[chn].push_back("ee:2e");
            cuts[chn].push_back("ee:trig");
            // cuts[chn].push_back("ee:OS");
            cuts[chn].push_back("ee:m");
         }
         if(chn.BeginsWith("uu"))
         {
            //// dimuon specific baseline cuts -> need to match the content and order in ApplySelection -> can be taken from config file
            cuts[chn].push_back("u:all");
            cuts[chn].push_back("u:2any");
            cuts[chn].push_back("u:eta");
            cuts[chn].push_back("u:pt");
            cuts[chn].push_back("u:CB");
            cuts[chn].push_back("u:HighPt");
            cuts[chn].push_back("u:NonBad");
            cuts[chn].push_back("u:d0sig");
            cuts[chn].push_back("u:z0delta");
            cuts[chn].push_back("u:iso");  /*---*/ last_u_cut = "u:iso"; /*---*/
            cuts[chn].push_back("uu:2u");
            cuts[chn].push_back("uu:trig");
            cuts[chn].push_back("uu:OS");
            cuts[chn].push_back("uu:m");
         }
         if(chn.BeginsWith("eu"))
         {
            //// electron+muon specific baseline cuts -> need to match the content and order in ApplySelection -> can be taken from config file
            cuts[chn].push_back("l:all");
            cuts[chn].push_back("l:2any");
            // cuts[chn].push_back("l:eta");
            // cuts[chn].push_back("l:pt");
            // cuts[chn].push_back("l:ID");
            // cuts[chn].push_back("l:quality");
            // // cuts[chn].push_back("l:ChID");
            // cuts[chn].push_back("l:d0sig");
            // cuts[chn].push_back("l:z0delta");
            // cuts[chn].push_back("l:iso");
            cuts[chn].push_back("l:allgood");
            cuts[chn].push_back("eu:1e1u");
            cuts[chn].push_back("eu:trig");
            cuts[chn].push_back("eu:OS");
            cuts[chn].push_back("eu:m");
         }
      }
      
      //// add here the exclusive selections, common to all channels
      for(unsigned int i=0 ; i<leptonchannels.size() ; ++i)
      {
         TString lchn = leptonchannels[i];
         cuts[lchn+"0b"].push_back(lchn+":0b");
         cuts[lchn+"1b"].push_back(lchn+":1b");
         cuts[lchn+"2b"].push_back(lchn+":2b");
         cuts[lchn+"met"].push_back(lchn+":met");
         cuts[lchn+"met"].push_back(lchn+":dphi");
      }
      std::cout << "booked cuts" << std::endl;

      //////////////////////////////////////////////////////////


      // add here all met collections
      metcollections.push_back("eleterm");
      metcollections.push_back("jetterm");
      metcollections.push_back("muonterm");
      metcollections.push_back("phterm");
      metcollections.push_back("muonterm_cst");
      metcollections.push_back("muonterm_tst");
      metcollections.push_back("softerm_cst");
      metcollections.push_back("softerm_tst");
      metcollections.push_back("noelectron_cst");
      metcollections.push_back("noelectron_tst");
      metcollections.push_back("nomuon_cst");
      metcollections.push_back("nomuon_tst");
      metcollections.push_back("wmuon_cst");
      metcollections.push_back("wmuon_tst");
      metcollections.push_back("nophcalib_nomuon_cst");
      metcollections.push_back("nophcalib_nomuon_tst");
      metcollections.push_back("nophcalib_wmuon_cst");
      metcollections.push_back("nophcalib_wmuon_tst");
      metcollections.push_back("nophoton_cst");
      metcollections.push_back("nophoton_tst");
      metcollections.push_back("track");
      metcollections.push_back("truth");

      /// add here all triggers
      hlttriggers.push_back("2e17_lhloose");
      hlttriggers.push_back("2e17_lhvloose");
      hlttriggers.push_back("mu26_imedium");
      hlttriggers.push_back("mu26_ivarmedium");
      hlttriggers.push_back("mu50");
      hlttriggers.push_back("2mu14");
      hlttriggers.push_back("e60_lhmedium");
      hlttriggers.push_back("e120_lhloose");
      hlttriggers.push_back("e24_lhmedium_L1EM20VH");
      hlttriggers.push_back("e24_lhmedium_L1EM20VHI");

      //// book histograms
      BookHistograms();

      return EL::StatusCode::SUCCESS;
   }




   EL::StatusCode DileptonNTReader::initialize()
   {
      std::cout << "initialize" << std::endl;

      MeV2GeV = 1.e-3;
      GeV2MeV = 1.e3;
      m_eventCounter = 0;

      TFile* outputFile = wk()->getOutputFile(outputName);
      otree = new TTree("nominal","nominal");
      otree->SetDirectory(outputFile);
     SetOutBranches();
      
      vector<bool>           vbtmp;
      vector<TLorentzVector> vtlvtmp;
      for(int i=0; i<(int)selections.size() ; ++i)
      {
         TString sel = selections[i];
         good_el.insert(make_pair(sel,vbtmp));
         good_mu.insert(make_pair(sel,vbtmp));
         good_ph.insert(make_pair(sel,vbtmp));
         good_jet.insert(make_pair(sel,vbtmp));
         good_bjet.insert(make_pair(sel,vbtmp));
         good_elp.insert(make_pair(sel,vtlvtmp));
         good_mup.insert(make_pair(sel,vtlvtmp));
         good_php.insert(make_pair(sel,vtlvtmp));
         good_jetp.insert(make_pair(sel,vtlvtmp));
         good_bjetp.insert(make_pair(sel,vtlvtmp));
         ngoodel.insert(make_pair(sel,0));
         ngoodmu.insert(make_pair(sel,0));
         ngoodph.insert(make_pair(sel,0));
         ngoodjet.insert(make_pair(sel,0));
         ngoodbjet.insert(make_pair(sel,0));
         el1.insert(make_pair(sel,-1));
         el2.insert(make_pair(sel,-1));
         mu1.insert(make_pair(sel,-1));
         mu2.insert(make_pair(sel,-1));

         /// initialise vars, flags, counters and cutflow
         TMapTSf tmpmapf;
         fvars.insert(make_pair(sel,tmpmapf));
         TMapTSi tmpmapi;
         counters.insert(make_pair(sel,tmpmapi));
         TMapTSb tmpmapb;
         flags.insert(make_pair(sel,tmpmapb));
         T2MapTSi t2mpmapi;
         cutflow.insert(make_pair(sel,t2mpmapi));
         for(unsigned int i=0 ; i<channels.size() ; ++i)
         {
            TString chn = channels[i];

            fvars[sel].insert(make_pair("pt1_"+chn,      -999));
            fvars[sel].insert(make_pair("pt2_"+chn,      -999));
            fvars[sel].insert(make_pair("m_"+chn,        -999));
            fvars[sel].insert(make_pair("mt2_"+chn,      -999));
            fvars[sel].insert(make_pair("htmlt_"+chn,    -999));
            fvars[sel].insert(make_pair("lt_"+chn,       -999));
            fvars[sel].insert(make_pair("ht_"+chn,       -999));
            fvars[sel].insert(make_pair("st_"+chn,       -999));
            fvars[sel].insert(make_pair("meff_"+chn,     -999));
            fvars[sel].insert(make_pair("metOmll_"+chn,  -999));
            fvars[sel].insert(make_pair("pllOmll_"+chn,  -999));
            fvars[sel].insert(make_pair("ptllOmll_"+chn, -999));
            for(unsigned int i=0 ; i<metcollections.size() ; ++i)
            {
               TString colname = metcollections[i];
               fvars[sel].insert(make_pair("dphi1_"+colname+"_"+chn, -999));
               fvars[sel].insert(make_pair("dphi2_"+colname+"_"+chn, -999));
               fvars[sel].insert(make_pair("dphi12_"+colname+"_"+chn, -999));
            }

            counters[sel].insert(make_pair("eventCounter_pass_"+chn,0));

            flags[sel].insert(make_pair("pass_"+chn,false));
            flags[sel].insert(make_pair("pass_only_"+chn,false));

            cutflow[sel].insert(make_pair(chn,tmpmapi));
            for(unsigned int c=0 ; c<cuts[chn].size() ; ++c)
            {
               TString cut = cuts[chn][c];
               cutflow[sel][chn].insert(make_pair(cut, cut.Contains("evt:") ? 2 : 0 ));
            }
         }

         /// initialise leptons, jets, etc (once)
         Electrons tmp_el;  els.insert(make_pair(sel,tmp_el));
         Muons tmp_mu;      mus.insert(make_pair(sel,tmp_mu));
         Photons tmp_ph;    phs.insert(make_pair(sel,tmp_ph));
         Jets tmp_jet;      jets.insert(make_pair(sel,tmp_jet));
         Bjets tmp_bjet;    bjets.insert(make_pair(sel,tmp_bjet));
      }
      
      /// initialise mets
      for(unsigned int i=0 ; i<metcollections.size() ; ++i)
      {
         TString colname = metcollections[i];
         mets.et.insert(make_pair(    colname,-999));
	     mets.etx.insert(make_pair(   colname,-999));
	     mets.ety.insert(make_pair(   colname,-999));
	     mets.sumet.insert(make_pair( colname,-999));
	     mets.phi.insert(make_pair(   colname,-999));
      }

      /// initialise triggers
      for(unsigned int i=0 ; i<hlttriggers.size() ; ++i)
	  {
         TString trig = hlttriggers[i];
         trigs.pass.insert(make_pair(trig,0));
      }

      return EL::StatusCode::SUCCESS;
   }



   EL::StatusCode DileptonNTReader::execute()
   {
      // std::cout << "execute, wk()->treeEntry()=" << wk()->treeEntry() << std::endl;

      wk()->tree()->GetEntry(wk()->treeEntry());
      // actual event processing happens below

      m_eventCounter++;

      //// clear the output branches
      ClearOutBranches();
      
      //// this is independent of the selection
      SetTriggers();
      SetMets();

      for(int i=0; i<(int)selections.size() ; ++i)
      {
         TString sel = selections[i];

         //// apply the selection
         ApplySelection(sel);
         // std::cout << "after ApplySelection, sel=" << sel << std::endl;

         //// calculate some variables
         CalculateVars(sel);

         //// fill the histograms
         FillHistograms(sel);
         // std::cout << "after FillHistograms, sel=" << sel << std::endl;

         //// dump some info
         DumpInfo(sel);
      }

      //// fill the output tree
      FillOutTree();
      // std::cout << "after FillOutTree" << std::endl;

      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode DileptonNTReader::finalize()
   {
      std::cout << "finalize" << std::endl;
      std::cout << "Number of processed events = " << m_eventCounter << std::endl;
      std::cout << "------------------------------------------------------------" << std::endl;
      for(int i=0; i<(int)selections.size() ; ++i)
      {
         TString sel = selections[i];
         std::cout << "Number of ee passed (no uu,eu veto) events[" << sel << "] = " << counters[sel]["eventCounter_pass_ee"] << std::endl;
         std::cout << "Number of uu passed (no ee,eu veto) events[" << sel << "] = " << counters[sel]["eventCounter_pass_uu"] << std::endl;
         std::cout << "Number of eu passed (no ee,uu veto) events[" << sel << "] = " << counters[sel]["eventCounter_pass_eu"] << std::endl;
         std::cout << "------------------------------------------------------------" << std::endl;
 
         //// post processing histograms
         for(unsigned int i=0 ; i<channels.size() ; ++i)
         {
            TString chn = channels[i];
            histos1[sel]["eff_pt_mu_selmat_"+chn]->Divide(histos1[sel]["effn_pt_mu_selmat_"+chn],histos1[sel]["effd_pt_mu_selmat_"+chn]);
            histos1[sel]["eff_pt_mu_selec_"+chn]->Divide(histos1[sel]["effn_pt_mu_selec_"+chn],histos1[sel]["effd_pt_mu_selec_"+chn]);
         }

      }
      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::SetTriggers()
   {
      for(unsigned int i=0 ; i<hlttriggers.size() ; ++i)
	  {
         TString trig = hlttriggers[i];
         trigs.pass[trig]  = in_i["trigger_HLT_"+trig];
      }
      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode DileptonNTReader::SetMets()
   {
      for(unsigned int i=0 ; i<metcollections.size() ; ++i)
      {
         TString colname = metcollections[i];
         mets.et[    colname] = in_f["met_"+colname+"_et"];
         mets.etx[   colname] = in_f["met_"+colname+"_etx"];
         mets.ety[   colname] = in_f["met_"+colname+"_ety"];
         mets.sumet[ colname] = in_f["met_"+colname+"_sumet"];
         mets.phi[   colname] = in_f["met_"+colname+"_phi"];
      }

      return EL::StatusCode::SUCCESS;
   }



   EL::StatusCode DileptonNTReader::ResetVars(TString sel)
   {
      for(TMapTSf::iterator it=fvars[sel].begin() ; it!=fvars[sel].end() ; ++it) it->second = -999;
      for(TMapTSb::iterator it=flags[sel].begin() ; it!=flags[sel].end() ; ++it) it->second = false;

      for(T2MapTSi::iterator it1=cutflow[sel].begin() ; it1!=cutflow[sel].end() ; ++it1)
      {
         TString chn = it1->first;
         for(TMapTSi::iterator it2=cutflow[sel][chn].begin() ; it2!=cutflow[sel][chn].end() ; ++it2)
         {
            it2->second = it2->first.Contains("evt:") ? 2 : 0;
         }
      }

      return EL::StatusCode::SUCCESS;
   }



   EL::StatusCode DileptonNTReader::InitSelection(TString sel)
   {
      // std::cout << "InitSelection" << std::endl;
	
      good_el[sel].clear();
      good_mu[sel].clear();
      good_ph[sel].clear();
      good_jet[sel].clear();
      good_bjet[sel].clear();
      good_elp[sel].clear();
      good_mup[sel].clear(); 
      good_php[sel].clear(); 
      good_jetp[sel].clear(); 
      good_bjetp[sel].clear(); 
      ngoodel[sel] = 0;
      ngoodmu[sel] = 0;
      ngoodph[sel] = 0;
      ngoodjet[sel] = 0;
      ngoodbjet[sel] = 0;
      el1[sel] = -1;
      el2[sel] = -1;
      mu1[sel] = -1;
      mu2[sel] = -1;
      
      // initialise the lepton structures
      TString x = (sel=="preOR") ? "_preor_" : "_";

      els[sel].SF                 = in_vf["el"+x+"SF"];
      els[sel].SFiso              = in_vf["el"+x+"SF_iso"];
      els[sel].cl_etaBE2          = in_vf["el"+x+"cl_etaBE2"];
	  els[sel].pt                 = in_vf["el"+x+"pt"];
	  els[sel].eta                = in_vf["el"+x+"eta"];
	  els[sel].phi                = in_vf["el"+x+"phi"];
	  els[sel].m                  = in_vf["el"+x+"m"];
	  els[sel].e                  = in_vf["el"+x+"e"];
	  els[sel].charge             = in_vf["el"+x+"charge"];
      els[sel].d0sig              = in_vf["el"+x+"d0sig"];
      els[sel].z0sig              = in_vf["el"+x+"z0sig"];
      els[sel].isGoodOQ           = in_vi["el"+x+"isGoodOQ"];
      els[sel].passLHID           = in_vi["el"+x+"passLHID"];
      els[sel].passChID           = in_vi["el"+x+"passChID"];
      els[sel].isotool_pass_loose = in_vi["el"+x+"isotool_pass_loose"];
      // std::cout << "after el " << sel << std::endl;
      
      mus[sel].SF                          = in_vf["mu"+x+"SF"];
      mus[sel].SFiso                       = in_vf["mu"+x+"SF_iso"];
      mus[sel].pt                          = in_vf["mu"+x+"pt"];
      mus[sel].ptErr                       = in_vf["mu"+x+"ptErr"];
      mus[sel].eta                         = in_vf["mu"+x+"eta"];
      mus[sel].phi                         = in_vf["mu"+x+"phi"];
      mus[sel].m                           = in_vf["mu"+x+"m"];
      mus[sel].e                           = in_vf["mu"+x+"e"];
      mus[sel].charge                      = in_vf["mu"+x+"charge"];
      mus[sel].d0sig                       = in_vf["mu"+x+"d0sig"];
      mus[sel].z0sig                       = in_vf["mu"+x+"z0sig"];
      mus[sel].isCB                        = in_vi["mu"+x+"isCB"];
      mus[sel].isBad                       = in_vi["mu"+x+"isBad"];
      mus[sel].isBadHighPt                 = in_vi["mu"+x+"isBadHighPt"];
      mus[sel].isHighPt                    = in_vi["mu"+x+"isHighPt"];
      mus[sel].isotool_pass_loosetrackonly = in_vi["mu"+x+"isotool_pass_loosetrackonly"];
      // std::cout << "after mu " << sel << std::endl;
      
      phs[sel].pt                          = in_vf["ph"+x+"pt"];
      phs[sel].eta                         = in_vf["ph"+x+"eta"];
      phs[sel].phi                         = in_vf["ph"+x+"phi"];
      phs[sel].m                           = in_vf["ph"+x+"m"];
      // std::cout << "after ph " << sel << std::endl;
      
      jets[sel].pt                         = in_vf["jet"+x+"pt"];
      jets[sel].eta                        = in_vf["jet"+x+"eta"];
      jets[sel].phi                        = in_vf["jet"+x+"phi"];
      jets[sel].m                          = in_vf["jet"+x+"m"];
      jets[sel].fch                        = in_vf["jet"+x+"fch"];
      jets[sel].fmax                       = in_vf["jet"+x+"fmax"];
      jets[sel].cleaning                   = in_vi["jet"+x+"cleaning"];
      // std::cout << "after jet " << sel << std::endl;
      
      bjets[sel].pt                        = in_vf["jet"+x+"pt"];
      bjets[sel].eta                       = in_vf["jet"+x+"eta"];
      bjets[sel].phi                       = in_vf["jet"+x+"phi"];
      bjets[sel].m                         = in_vf["jet"+x+"m"];
      bjets[sel].cleaning                  = in_vi["jet"+x+"cleaning"];
      bjets[sel].isbjet                    = in_vi["jet"+x+"isbjet"];
      bjets[sel].isbjet_loose              = in_vi["jet"+x+"isbjet_loose"];
      // std::cout << "after bjet " << sel << std::endl;

      // initialise the good object counters
      for(unsigned int i=0 ; i<els[sel].pt->size() ; ++i)
      {
         good_el[sel].push_back(true);
         good_elp[sel].push_back(TLorentzVector());
      }
      // std::cout << "after set el vectors " << sel << std::endl;
      for(unsigned int i=0 ; i<mus[sel].pt->size() ; ++i)
      {
         good_mu[sel].push_back(true);
         good_mup[sel].push_back(TLorentzVector());
      }
      // std::cout << "after set mu vectors " << sel << std::endl;
      for(unsigned int i=0 ; i<phs[sel].pt->size() ; ++i)
      {
         good_ph[sel].push_back(true);
         good_php[sel].push_back(TLorentzVector());
      }
      // std::cout << "after set ph vectors " << sel << std::endl;
      for(unsigned int i=0 ; i<jets[sel].pt->size() ; ++i)
      {
         good_jet[sel].push_back(true);
         good_jetp[sel].push_back(TLorentzVector());
      }
      // std::cout << "after set jet vectors " << sel << std::endl;
      for(unsigned int i=0 ; i<bjets[sel].pt->size() ; ++i)
      {
         good_bjet[sel].push_back(true);
         good_bjetp[sel].push_back(TLorentzVector());
      }
      // std::cout << "after set bjet vectors " << sel << std::endl;

      return EL::StatusCode::SUCCESS;
   }




   EL::StatusCode DileptonNTReader::ApplySelection(TString sel)
   {
      // std::cout << "selection: " << sel << std::endl;

      /// initialise these upon every event
      InitSelection(sel);

      /// reset the flags, fvars, cutflow, etc
      ResetVars(sel);

      /// tight cleaning on leading jet (event level)
      // bool pass_jetcleaning = (jets[sel].pt->size()==0 || (!(TMath::Abs(jets[sel].eta->at(0))<=2.4 && jets[sel].fch->at(0)/jets[sel].fmax->at(0)<0.1)));

      /// electron selection
      /// single electrons
      cutflow[sel]["ee"]["e:all"] = 2;
      cutflow[sel]["ee"]["e:2any"] = (good_el[sel].size()>1) ? 2 : 0;
      for(unsigned int i=0 ; i<good_el[sel].size() ; ++i)
      {
         //std::cout << "i="<< i << std::endl;
         if(!good_el[sel][i]) continue; // no point to check it

         bool pass_lhid = ( els[sel].passLHID->at(i) );
         if(!pass_lhid) { good_el[sel][i] = false; continue; }
         else cutflow[sel]["ee"]["e:LHID"]++;
         // std::cout << "pass_lhid="<< pass_lhid << std::endl;

         bool pass_eta = ( TMath::Abs(els[sel].cl_etaBE2->at(i))<2.47 && !(TMath::Abs(els[sel].cl_etaBE2->at(i))>1.37 && TMath::Abs(els[sel].cl_etaBE2->at(i))<1.52) );
         if(!pass_eta) { good_el[sel][i] = false; continue; }
         else cutflow[sel]["ee"]["e:eta"]++;
         // std::cout << "pass_eta="<< pass_eta << std::endl;

         bool pass_pt = ( els[sel].pt->at(i)*MeV2GeV>30. );
         if(!pass_pt) { good_el[sel][i] = false; continue; }
         else cutflow[sel]["ee"]["e:pt"]++;
         // std::cout << "pass_pt="<< pass_pt << std::endl;
         
         // bool pass_chid = ( els[sel].passChID->at(i) );
         // if(!pass_chid) { good_el[sel][i] = false; continue; }
         // else cutflow[sel]["ee"]["e:ChID"]++;
         // // std::cout << "pass_chid="<< pass_chid << std::endl;
         
         bool pass_OQ = ( els[sel].isGoodOQ->at(i) );
         if(!pass_OQ) { good_el[sel][i] = false; continue; }
         else cutflow[sel]["ee"]["e:OQ"]++;
         // std::cout << "pass_OQ="<< pass_OQ << std::endl;
         
         bool pass_d0sig = ( TMath::Abs(els[sel].d0sig->at(i))<5. ); // check if this is really the variable we are using
         if(!pass_d0sig) { good_el[sel][i] = false; continue; }
         else cutflow[sel]["ee"]["e:d0sig"]++;
         // std::cout << "pass_d0sig="<< pass_d0sig << std::endl;
         
         bool pass_z0sig = ( TMath::Abs(els[sel].z0sig->at(i))<0.5 ); // check if this is really the variable we are using
         if(!pass_z0sig) { good_el[sel][i] = false; continue; }
         else cutflow[sel]["ee"]["e:z0delta"]++;
         // std::cout << "pass_z0sig="<< pass_z0sig << std::endl;
         
         bool pass_liso = ( els[sel].isotool_pass_loose->at(i) );
         if(!pass_liso) { good_el[sel][i] = false; continue; }
         else cutflow[sel]["ee"]["e:iso"]++;
         // std::cout << "pass_liso="<< pass_liso << std::endl;
      }
      /// count good electrons and check highest pT (assuming pT vector is sorted)
      for(int i=0 ; i<(int)good_el[sel].size() ; ++i)
      {
         if(!good_el[sel][i]) continue;
         good_elp[sel][i].SetPtEtaPhiE(els[sel].pt->at(i),els[sel].eta->at(i),els[sel].phi->at(i),els[sel].e->at(i));
         if(el1[sel]<0)                el1[sel] = i;
         if(el2[sel]<0 && el1[sel]!=i) el2[sel] = i;
         ngoodel[sel]++;
      }

      bool pass_2el = (ngoodel[sel]>1); // at least 2 good electrons
      if(pass_2el) cutflow[sel]["ee"]["ee:2e"]++;
      // std::cout << "pass_2el="<< pass_2el << std::endl;
      bool pass_ee_trig = (pass_2el && trigs.pass["2e17_lhloose"]==1); // electrons trigger
      if(pass_ee_trig) cutflow[sel]["ee"]["ee:trig"]++;
      // std::cout << "pass_ee_trig="<< pass_ee_trig << std::endl;
      // bool pass_eeos = (pass_ee_trig && els[sel].charge->at(el1[sel])*els[sel].charge->at(el2[sel])<0); // oposite charge
      // if(pass_eeos) cutflow[sel]["ee"]["ee:OS"]++;
      // // std::cout << "pass_eeos="<< pass_eeos << std::endl;
      // float mee = (pass_2el) ? (good_elp[sel][el1[sel]]+good_elp[sel][el2[sel]]).M()*MeV2GeV : -999;
      // bool pass_mee = (pass_eeos && mee>80.);  // dielectron mass >80 GeV
      float mee = (pass_2el) ? (good_elp[sel][el1[sel]]+good_elp[sel][el2[sel]]).M()*MeV2GeV : -999;
      bool pass_mee = (pass_ee_trig && mee>80.);  // dielectron mass >80 GeV
      if(pass_mee) cutflow[sel]["ee"]["ee:m"]++;
      // std::cout << "pass_mee="<< pass_mee << std::endl;
      // flags[sel]["pass_ee"] = (pass_jetcleaning && pass_2el && pass_eeos && pass_mee);
      flags[sel]["pass_ee"] = pass_mee;
      if(pass_mee) counters[sel]["eventCounter_pass_ee"]++;


      // std::cout << "after ee" << std::endl;


      /// muon selection
      /// single muons
      cutflow[sel]["uu"]["u:all"] = 2;
      cutflow[sel]["uu"]["u:2any"] = (good_mu[sel].size()>1) ? 2 : 0;
      for(unsigned int i=0 ; i<good_mu[sel].size() ; ++i)
      {
         // std::cout << "i="<< i << std::endl;
         if(!good_mu[sel][i]) continue; // no point to check it
	
         bool pass_eta = ( TMath::Abs(mus[sel].eta->at(i))<2.5 );
         if(!pass_eta) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:eta"]++;
         // std::cout << "pass_eta="<< pass_eta << std::endl;

         bool pass_pt = ( mus[sel].pt->at(i)*MeV2GeV>30. );
         if(!pass_pt) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:pt"]++;
         // std::cout << "pass_pt="<< pass_pt << std::endl;

         bool pass_cb = ( mus[sel].isCB->at(i)==1 );
         if(!pass_cb) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:CB"]++;
         // std::cout << "pass_cb="<< pass_cb << std::endl;

         bool pass_hpt = ( mus[sel].isHighPt->at(i)==1 );
         if(!pass_hpt) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:HighPt"]++;
         // std::cout << "pass_hpt="<< pass_hpt << std::endl;

         bool pass_nonbad = ( mus[sel].isBadHighPt->at(i)==0 );
         if(!pass_nonbad) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:NonBad"]++;
         // std::cout << "pass_nonbad="<< pass_nonbad << std::endl;
         
         bool pass_d0sig = ( TMath::Abs(mus[sel].d0sig->at(i))<3. ); // check if this is really the variable we are using
         if(!pass_d0sig) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:d0sig"]++;
         // std::cout << "pass_d0sig="<< pass_d0sig << std::endl;
         
         bool pass_z0sig = ( TMath::Abs(mus[sel].z0sig->at(i))<0.5 ); // check if this is really the variable we are using
         if(!pass_z0sig) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:z0delta"]++;
         // std::cout << "pass_z0sig="<< pass_z0sig << std::endl;
         
         bool pass_liso = ( mus[sel].isotool_pass_loosetrackonly->at(i) );
         if(!pass_liso) { good_mu[sel][i] = false; continue; }
         else cutflow[sel]["uu"]["u:iso"]++;
         // std::cout << "pass_liso="<< pass_liso << std::endl;
      }
      /// count good muons and check highest pT (assuming pT vector is sorted)
      for(int i=0 ; i<(int)good_mu[sel].size() ; ++i)
      {
         if(!good_mu[sel][i]) continue;
         good_mup[sel][i].SetPtEtaPhiM(mus[sel].pt->at(i),mus[sel].eta->at(i),mus[sel].phi->at(i),mus[sel].m->at(i));
         if(mu1[sel]<0)                mu1[sel] = i;
         if(mu2[sel]<0 && mu1[sel]!=i) mu2[sel] = i;
         ngoodmu[sel]++;
      }
      bool pass_2mu = (ngoodmu[sel]>1); // at least 2 good muons
      if(pass_2mu) cutflow[sel]["uu"]["uu:2u"]++;
      // std::cout << "pass_2mu="<< pass_2mu << std::endl;
      bool pass_uu_trig = (pass_2mu && 
                           ((trigs.pass["mu26_imedium"]==1   || trigs.pass["mu50"]==1)/*2015*/ ||
                           (trigs.pass["mu26_ivarmedium"]==1 || trigs.pass["mu50"]==1)/*2016*/)); // muon trigger
      if(pass_uu_trig) cutflow[sel]["uu"]["uu:trig"]++;
      // std::cout << "pass_uu_trig="<< pass_uu_trig << std::endl;
      bool pass_uuos = (pass_uu_trig && mus[sel].charge->at(mu1[sel])*mus[sel].charge->at(mu2[sel])<0); // oposite charge
      if(pass_uuos) cutflow[sel]["uu"]["uu:OS"]++;
      // std::cout << "pass_uuos="<< pass_uuos << std::endl;
      float muu = (pass_2mu) ? (good_mup[sel][mu1[sel]]+good_mup[sel][mu2[sel]]).M()*MeV2GeV : -999;
      bool pass_muu = (pass_uuos && muu>80.);  // dimuon mass >80 GeV
      if(pass_muu) cutflow[sel]["uu"]["uu:m"]++;
      // std::cout << "pass_muu="<< pass_muu << std::endl;
      // flags[sel]["pass_uu"] = (pass_jetcleaning && pass_muu);
      flags[sel]["pass_uu"] = pass_muu;
      if(pass_muu) counters[sel]["eventCounter_pass_uu"]++;



      // std::cout << "after uu" << std::endl;


      /// e+mu selection
      cutflow[sel]["eu"]["l:all"] = 2;
      cutflow[sel]["eu"]["l:2any"] = (good_el[sel].size()>0 && good_mu[sel].size()>0) ? 2 : 0;
      cutflow[sel]["eu"]["l:allgood"] = (cutflow[sel]["ee"][last_e_cut]>0 && cutflow[sel]["uu"][last_u_cut]>0) ? 2 : 0;
      bool pass_1e1u = (ngoodmu[sel]>0 && ngoodel[sel]>0);
      if(pass_1e1u) cutflow[sel]["eu"]["eu:1e1u"]++;
      // std::cout << "pass_1e1u="<< pass_1e1u << std::endl;
      bool pass_eu_trig = (pass_1e1u &&
                           ((trigs.pass["mu26_imedium"]==1           || trigs.pass["mu50"]==1)/*2015*/ ||
                            (trigs.pass["mu26_ivarmedium"]==1        || trigs.pass["mu50"]==1)/*2016*/ ||
                            (trigs.pass["e24_lhmedium_L1EM20VHI"]==1 || trigs.pass["e60_lhmedium"]==1  || trigs.pass["e120_lhloose"]==1))); // electron+muon trigger
      if(pass_eu_trig) cutflow[sel]["eu"]["eu:trig"]++;
      // std::cout << "pass_eu_trig="<< pass_eu_trig << std::endl;
      bool pass_euos = (pass_eu_trig && els[sel].charge->at(el1[sel])*mus[sel].charge->at(mu1[sel])<0); // oposite charge
      if(pass_euos) cutflow[sel]["eu"]["eu:OS"]++;
      // std::cout << "pass_euos="<< pass_euos << std::endl;
      float meu = (pass_1e1u) ? (good_elp[sel][el1[sel]]+good_mup[sel][mu1[sel]]).M()*MeV2GeV : -999;
      bool pass_meu = (pass_euos && meu>80.);
      if(pass_meu) cutflow[sel]["eu"]["eu:m"]++;
      // std::cout << "pass_meu="<< pass_meu << std::endl;
      // flags[sel]["pass_eu"] = (pass_jetcleaning && pass_meu && pass_os);
      flags[sel]["pass_eu"] = pass_meu;
      if(pass_meu) counters[sel]["eventCounter_pass_eu"]++;


      // std::cout << "after eu" << std::endl;


      // ================== other objects ================== //


      /// single photon selection
      for(unsigned int i=0 ; i<good_ph[sel].size() ; ++i)
      {
         //std::cout << "i="<< i << std::endl;
         if(!good_ph[sel][i]) continue; // no point to check it
	
	     /// check variables sanity first since missing links end up as "-999"
         bool fail_sanity = (phs[sel].eta->at(i)<-998.    ||
                             phs[sel].phi->at(i)<-998.    ||
                             phs[sel].pt->at(i)<0         ||
                             phs[sel].m->at(i)<0);
         if(fail_sanity) { good_ph[sel][i] = false; continue; }
         // std::cout << "fail_sanity="<< fail_sanity << std::endl;
	
         bool pass_pt = (phs[sel].pt->at(i)*MeV2GeV>30.);
         if(!pass_pt) { good_ph[sel][i] = false; continue; }
         // std::cout << "pass_pt="<< pass_pt << std::endl;

         int pass_eta = ( TMath::Abs(phs[sel].eta->at(i))<2.47 && !(TMath::Abs(phs[sel].eta->at(i))>1.37 && TMath::Abs(phs[sel].eta->at(i))<1.52) );
         if(!pass_eta) { good_ph[sel][i] = false; continue; }
         // std::cout << "pass_eta="<< pass_eta << std::endl;
      }
      /// count good jets and check highest pT (assuming pT vector is sorted)
      for(int i=0 ; i<(int)good_ph[sel].size() ; ++i)
      {
         if(!good_ph[sel][i]) continue;
         good_php[sel][i].SetPtEtaPhiM(phs[sel].pt->at(i),phs[sel].eta->at(i),phs[sel].phi->at(i),phs[sel].m->at(i));
         ngoodph[sel]++;
      }


      // std::cout << "after photons" << std::endl;



      /// single jet selection
      for(unsigned int i=0 ; i<good_jet[sel].size() ; ++i)
      {
         // std::cout << "i="<< i << std::endl;
         if(!good_jet[sel][i]) continue; // no point to check it      

         /// check variables sanity first since missing links end up as "-999"
         bool fail_sanity = (jets[sel].eta->at(i)<-998. ||
                             jets[sel].phi->at(i)<-998. ||
                             jets[sel].pt->at(i)<0      ||
                             jets[sel].m->at(i)<0       ||
                             jets[sel].cleaning->at(i)<0 );
         if(fail_sanity) { good_jet[sel][i] = false; continue; }
         // std::cout << "fail_sanity="<< fail_sanity << std::endl;

         bool pass_clean = (jets[sel].cleaning->at(i)>0); // i.e. not BAD
         if(!pass_clean) { good_jet[sel][i] = false; continue; }
         // std::cout << "pass_clean="<< pass_clean << std::endl;

         bool pass_pt = (jets[sel].pt->at(i)*MeV2GeV>50.); // this is the default recommended threshold for analyses not interested in the low-pt regime no JVT correction/cut needed
         if(!pass_pt) { good_jet[sel][i] = false; continue; }
         // std::cout << "pass_pt="<< pass_pt << std::endl;

         bool pass_eta = (TMath::Abs(jets[sel].eta->at(i))<2.8);
         if(!pass_eta) { good_jet[sel][i] = false; continue; }
         // std::cout << "pass_eta="<< pass_eta << std::endl;
      }
      /// count good jets and check highest pT (assuming pT vector is sorted)
      for(int i=0 ; i<(int)good_jet[sel].size() ; ++i)
      {
         if(!good_jet[sel][i]) continue;
         good_jetp[sel][i].SetPtEtaPhiM(jets[sel].pt->at(i),jets[sel].eta->at(i),jets[sel].phi->at(i),jets[sel].m->at(i));
         ngoodjet[sel]++;
      }

      
      // std::cout << "after jets" << std::endl;
      

      /// single b-jet selection
      for(unsigned int i=0 ; i<good_bjet[sel].size() ; ++i)
      {
         //std::cout << "i="<< i << std::endl;
         if(!good_bjet[sel][i]) continue; // no point to check it

         /// check variables sanity first since missing links end up as "-999"
         bool fail_sanity = (bjets[sel].eta->at(i)<-998.    ||
                             bjets[sel].phi->at(i)<-998.    ||
                             bjets[sel].pt->at(i)<0         ||
                             bjets[sel].m->at(i)<0          ||
                             bjets[sel].cleaning->at(i)<0   ||
                             bjets[sel].isbjet->at(i)<0 );
         if(fail_sanity) { good_bjet[sel][i] = false; continue; }
         // std::cout << "fail_sanity="<< fail_sanity << std::endl;

         bool pass_clean = (bjets[sel].cleaning->at(i)>0); // i.e. not BAD
         if(!pass_clean) { good_bjet[sel][i] = false; continue; }
         // std::cout << "pass_clean="<< pass_clean << std::endl;

         bool pass_pt = (bjets[sel].pt->at(i)*MeV2GeV>50.); // this is the default recommended threshold for analyses not interested in the low-pt regime no JVT correction/cut needed
         if(!pass_pt) { good_bjet[sel][i] = false; continue; }
         // std::cout << "pass_pt="<< pass_pt << std::endl;

         bool pass_eta = (TMath::Abs(bjets[sel].eta->at(i))<2.8);
         if(!pass_eta) { good_bjet[sel][i] = false; continue; }
         // std::cout << "pass_eta="<< pass_eta << std::endl;

         bool pass_btag = ( bjets[sel].isbjet->at(i) );
         if(!pass_btag) { good_bjet[sel][i] = false; continue; }
         // std::cout << "pass_btag="<< pass_btag << std::endl;

         TLorentzVector thisb;
         thisb.SetPtEtaPhiM(bjets[sel].pt->at(i),bjets[sel].eta->at(i),bjets[sel].phi->at(i),bjets[sel].m->at(i));

         bool pass_elOR = true;
         for(int i=0 ; i<(int)good_el[sel].size() ; ++i)
         {
	         if(!good_el[sel][i]) continue;
             float dR = good_elp[sel][i].DeltaR(thisb);
             if(dR<0.2) { pass_elOR = false; break; }
	     }
         if(!pass_elOR) { good_bjet[sel][i] = false; continue; }
         // std::cout << "pass_elOR="<< pass_elOR << std::endl;

         bool pass_muOR = true;
         for(int i=0 ; i<(int)good_mu[sel].size() ; ++i)
         {
	         if(!good_mu[sel][i]) continue;
             float dR = good_mup[sel][i].DeltaR(thisb);
             if(dR<0.2) { pass_muOR = false; break; }
	     }
         if(!pass_muOR) { good_bjet[sel][i] = false; continue; }
         // std::cout << "pass_muOR="<< pass_muOR << std::endl;
      }
      /// count good jets and check highest pT (assuming pT vector is sorted)
      for(int i=0 ; i<(int)good_bjet[sel].size() ; ++i)
      {
         if(!good_bjet[sel][i]) continue;
         good_bjetp[sel][i].SetPtEtaPhiM(bjets[sel].pt->at(i),bjets[sel].eta->at(i),bjets[sel].phi->at(i),bjets[sel].m->at(i));
         ngoodbjet[sel]++;
      }

      // std::cout << "after bjets" << std::endl;


   
      // ================== summary of inclusive selections ================== //
      /// fill "pass only" flags
      flags[sel]["pass_only_ee"] = (flags[sel]["pass_ee"] && !flags[sel]["pass_uu"] && !flags[sel]["pass_eu"]);
      flags[sel]["pass_only_uu"] = (flags[sel]["pass_uu"] && !flags[sel]["pass_ee"] && !flags[sel]["pass_eu"]);
      flags[sel]["pass_only_eu"] = (flags[sel]["pass_eu"] && !flags[sel]["pass_ee"] && !flags[sel]["pass_uu"]);
      


      // ================== summary of exclusive selections ================== //
      for(unsigned int i=0 ; i<leptonchannels.size() ; ++i)
      {
         TString lchn = leptonchannels[i];

         //// first fill up the baseline cutflows for the exclusive selections
         for(TMapTSi::iterator it=cutflow[sel][lchn].begin() ; it!=cutflow[sel][lchn].end() ; ++it)
         {
			TString cut = it->first;
			int N = it->second;
            cutflow[sel][lchn+"0b"][cut]  = N;
            cutflow[sel][lchn+"1b"][cut]  = N;
            cutflow[sel][lchn+"2b"][cut]  = N;
            cutflow[sel][lchn+"met"][cut] = N;
         }

         //// fill the exclusive flags
         if(flags[sel]["pass_"+lchn])
         {
            //// for b-jets selections
            if(ngoodbjet[sel]==0) { cutflow[sel][lchn+"0b"][lchn+":0b"]=1; flags[sel]["pass_"+lchn+"0b"]=true; }
            if(ngoodbjet[sel]>0)  { cutflow[sel][lchn+"1b"][lchn+":1b"]=1; flags[sel]["pass_"+lchn+"1b"]=true; }
            if(ngoodbjet[sel]>1)  { cutflow[sel][lchn+"2b"][lchn+":2b"]=1; flags[sel]["pass_"+lchn+"2b"]=true; }
          
            //// met selection
            TLorentzVector pll;
            float dphi = -999.;
            if(lchn=="ee" && flags[sel]["pass_ee"]) dphi = TMath::Abs(TVector2::Phi_mpi_pi(mets.phi[nominalmet]-(good_elp[sel][el1[sel]]+good_elp[sel][el2[sel]]).Phi()));
            if(lchn=="uu" && flags[sel]["pass_uu"]) dphi = TMath::Abs(TVector2::Phi_mpi_pi(mets.phi[nominalmet]-(good_mup[sel][mu1[sel]]+good_mup[sel][mu2[sel]]).Phi()));
            if(lchn=="eu" && flags[sel]["pass_eu"]) dphi = TMath::Abs(TVector2::Phi_mpi_pi(mets.phi[nominalmet]-(good_elp[sel][el1[sel]]+good_mup[sel][mu1[sel]]).Phi()));
            float met = mets.et[nominalmet]*MeV2GeV;
            bool passmet  = (met>100.);           // at least 100 GeV met
            bool passdphi = (passmet && dphi>2.); // Z' is back to back with the met
            if(passmet)    cutflow[sel][lchn+"met"][lchn+":met"]=1;
            if(passdphi) { cutflow[sel][lchn+"met"][lchn+":dphi"]=1; flags[sel]["pass_"+lchn+"met"]=true; }
         }
      }

      return EL::StatusCode::SUCCESS;
   }


} // namespace DNR
