// event loop
#include <EventLoop/Worker.h>

#include "DileptonNTReader/DileptonNTReader.h"
#include "check.h"

namespace DNR
{
   EL::StatusCode DileptonNTReader::SetLogBins(Int_t nbins, Double_t min, Double_t max, Double_t* xpoints)
   {
      Double_t logmin  = log10(min);
      Double_t logmax  = log10(max);
      Double_t logbinwidth = (Double_t)( (logmax-logmin)/(Double_t)nbins );
      xpoints[0] = min;
      for(Int_t i=1 ; i<=nbins ; i++) xpoints[i] = TMath::Power( 10,(logmin + i*logbinwidth) );
      return EL::StatusCode::SUCCESS;
   }
   
   EL::StatusCode DileptonNTReader::SetSqrtBins(Int_t nbins, Double_t min, Double_t max, Double_t* xpoints)
   {
      Double_t sqrtmin = sqrt(min);
      Double_t sqrtmax = sqrt(max);
      Double_t sqrtbinwidth = (Double_t)((sqrtmax-sqrtmin)/(Double_t)nbins);
      xpoints[0] = min;
      for(int i=1 ; i<=nbins ; i++) xpoints[i] = pow(sqrtmin+i*sqrtbinwidth,2);
      return EL::StatusCode::SUCCESS;
   }
	
	
   EL::StatusCode DileptonNTReader::GetInfoFromGridHist()
   {
      std::cout << "getting grid histograms" << std::endl;
      TFile* f = new TFile(gridhistpath+"/hist-grid.root","READ");
      TH1D* hCFraw = (TH1D*)f->Get("cutflowhisto_raw_SignalRegion");
      TH1D* hCFwgt = (TH1D*)f->Get("cutflowhisto_SignalRegion");
      TH1D* hEvtCount = (TH1D*)f->Get("histoEventCount");
      std::cout << "<--- Getting cutflowhisto(_raw)_SignalRegion --->" << std::endl;
	  for(Int_t b=1 ; b<=hCFraw->GetNbinsX() ; ++b)
	  {
         TString cut = hCFraw->GetXaxis()->GetBinLabel(b);
         float Nraw = hCFraw->GetBinContent(b);
         float Nwgt = hCFwgt->GetBinContent(b);
         gridCF.push_back(cut);
         gridCFraw.insert(make_pair(cut,Nraw));
         gridCFwgt.insert(make_pair(cut,Nwgt));
         std::cout << cut << ": Nraw=" << Nraw << ", Nwgt=" << Nwgt <<  std::endl;
      }
      std::cout << "<--- Getting histoEventCount --->" << std::endl;
      for(Int_t b=1 ; b<=hEvtCount->GetNbinsX() ; ++b)
	  {
         TString cut = hEvtCount->GetXaxis()->GetBinLabel(b);
         float N = hEvtCount->GetBinContent(b);
         if(N==0) continue;
         gridEC.insert(make_pair(cut,N));
         std::cout << cut << ": N=" << N <<  std::endl;
      }
      std::cout << "---------------------------------" << std::endl;

      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::BookHistograms()
   {
      std::cout << "booking histograms" << std::endl;

      // Make sure that statistical uncertainties are kept:
      TH1::SetDefaultSumw2();

      //// some binning
      const Int_t    nlogptbins = 15;
      const Double_t logptmin   = 30.;
      const Double_t logptmax   = 1000.;
      Double_t logptbins[nlogptbins+1];
      SetLogBins(nlogptbins,logptmin,logptmax,logptbins);

      // histograms:
      for(int i=0; i<(int)selections.size() ; ++i)
      {
         TString sel = selections[i];

         TMapTSP2TH1 tmpmaph1;
         TMapTSP2TH2 tmpmaph2;
         histos1.insert(make_pair(sel,tmpmaph1));
         histos2.insert(make_pair(sel,tmpmaph2));
         for(unsigned int i=0 ; i<channels.size() ; ++i)
         {
            TString chn = channels[i];
            TString clabel = chn;
            clabel.ReplaceAll("u","#mu"); // this is a bit dangerous

            //// cutflow TH1 histos
            unsigned int ncuts = cuts[chn].size();
            histos1[sel].insert(make_pair("cutflow_raw_"+chn, new TH1D("cutflow_raw_"+chn+"_"+sel, ";;Events passing (raw)",      ncuts,0,ncuts) ));
            histos1[sel].insert(make_pair("cutflow_wgt_"+chn, new TH1D("cutflow_wgt_"+chn+"_"+sel, ";;Events passing (weighted)", ncuts,0,ncuts) ));
            for(unsigned int i=0 ; i<cuts[chn].size() ; ++i)
            {
               Int_t bin = i+1;
			   TString cut = cuts[chn][i];
               histos1[sel]["cutflow_raw_"+chn]->GetXaxis()->SetBinLabel(bin,cut);
               histos1[sel]["cutflow_wgt_"+chn]->GetXaxis()->SetBinLabel(bin,cut);
            }
            GridCutFlow(sel,chn);

            //// other TH1 histos
            histos1[sel].insert(make_pair("eff_pt_mu_selmat_"+chn,  new TH1D("eff_pt_mu_selmat_"+chn+"_"+sel,  ";p_{T} [GeV];Efficiency", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("effn_pt_mu_selmat_"+chn, new TH1D("effn_pt_mu_selmat_"+chn+"_"+sel, ";p_{T} [GeV];Events", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("eff_pt_mu_selec_"+chn,   new TH1D("eff_pt_mu_selec_"+chn+"_"+sel,   ";p_{T} [GeV];Efficiency", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("effn_pt_mu_selec_"+chn,  new TH1D("effn_pt_mu_selec_"+chn+"_"+sel,  ";p_{T} [GeV];Events", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("effd_pt_mu_inacc_"+chn,  new TH1D("effd_pt_mu_inacc_"+chn+"_"+sel,  ";p_{T} [GeV];Events", nlogptbins,logptbins) ));

            histos1[sel].insert(make_pair("eff_pt_el_selmat_"+chn,  new TH1D("eff_pt_el_selmat_"+chn+"_"+sel,  ";p_{T} [GeV];Efficiency", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("effn_pt_el_selmat_"+chn, new TH1D("effn_pt_el_selmat_"+chn+"_"+sel, ";p_{T} [GeV];Events", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("eff_pt_el_selec_"+chn,   new TH1D("eff_pt_el_selec_"+chn+"_"+sel,   ";p_{T} [GeV];Efficiency", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("effn_pt_el_selec_"+chn,  new TH1D("effn_pt_el_selec_"+chn+"_"+sel,  ";p_{T} [GeV];Events", nlogptbins,logptbins) ));
            histos1[sel].insert(make_pair("effd_pt_el_inacc_"+chn,  new TH1D("effd_pt_el_inacc_"+chn+"_"+sel,  ";p_{T} [GeV];Events", nlogptbins,logptbins) ));

            histos1[sel].insert(make_pair("n_el_"+chn,    new TH1D("n_el_"+chn+"_"+sel,    ";Electron multiplicity;Events", 5,0,5) ));
            histos1[sel].insert(make_pair("n_mu_"+chn,    new TH1D("n_mu_"+chn+"_"+sel,    ";Muon multiplicity;Events",     5,0,5) ));
            histos1[sel].insert(make_pair("n_ph_"+chn,    new TH1D("n_ph_"+chn+"_"+sel,    ";Photon multiplicity;Events",   10,0,10) ));
            histos1[sel].insert(make_pair("n_jet_"+chn,   new TH1D("n_jet_"+chn+"_"+sel,   ";Jet multiplicity;Events",      20,0,20) ));
            histos1[sel].insert(make_pair("n_bjet_"+chn,  new TH1D("n_bjet_"+chn+"_"+sel,  ";Bjet multiplicity;Events",     5,0,5) ));

            histos1[sel].insert(make_pair("pt_l1_"+chn,    new TH1D("pt_l1_"+chn+"_"+sel,    ";p_{T} (leading lepton) [GeV];Events",   200,0,2000) ));
            histos1[sel].insert(make_pair("pt_l2_"+chn,    new TH1D("pt_l2_"+chn+"_"+sel,    ";p_{T} (subleading lepton) [GeV];Events",200,0,2000) ));
            histos1[sel].insert(make_pair("m_"+chn,        new TH1D("m_"+chn+"_"+sel,        ";m "+clabel+" [GeV];Events",             600,0,6000) ));
            histos1[sel].insert(make_pair("mt2_"+chn,      new TH1D("mt2_"+chn+"_"+sel,      ";m_{T2} "+clabel+" [GeV];Events",        250,0,500) ));
            histos1[sel].insert(make_pair("htmlt_"+chn,    new TH1D("htmlt_"+chn+"_"+sel,    ";H_{T}-L_{T} "+clabel+" [GeV];Events",   800,-4000,4000) ));
            histos1[sel].insert(make_pair("lt_"+chn,       new TH1D("lt_"+chn+"_"+sel,       ";L_{T} "+clabel+" [GeV];Events",         500,0,5000) ));
            histos1[sel].insert(make_pair("ht_"+chn,       new TH1D("ht_"+chn+"_"+sel,       ";H_{T} "+clabel+" [GeV];Events",         400,0,4000) ));
            histos1[sel].insert(make_pair("st_"+chn,       new TH1D("st_"+chn+"_"+sel,       ";S_{T} "+clabel+" [GeV];Events",         600,0,6000) ));
            histos1[sel].insert(make_pair("meff_"+chn,     new TH1D("meff_"+chn+"_"+sel,     ";m_{eff} "+clabel+" [GeV];Events",       600,0,6000) ));
            histos1[sel].insert(make_pair("metOmll_"+chn,  new TH1D("metOmll_"+chn+"_"+sel,  ";#it{E}_{T}^{miss}/m "+clabel+";Events", 100,0,5) ));
            histos1[sel].insert(make_pair("pllOmll_"+chn,  new TH1D("pllOmll_"+chn+"_"+sel,  ";p/m "+clabel+";Events",                 100,0,5) ));
            histos1[sel].insert(make_pair("ptllOmll_"+chn, new TH1D("ptllOmll_"+chn+"_"+sel, ";p_{T}/m "+clabel+";Events",             100,0,5) ));
            for(unsigned int i=0 ; i<metcollections.size() ; ++i)
            {
               TString colname = metcollections[i];
               histos1[sel].insert(make_pair("met_"+colname+"_dphi1_"+chn,  new TH1D("met_"+colname+"_dphi1_"+chn+"_"+sel,  ";"+colname+" |#Delta#phi(#it{E}_{T}^{miss},l_{1})|;Events", 64,0,TMath::Pi())));
               histos1[sel].insert(make_pair("met_"+colname+"_dphi2_"+chn,  new TH1D("met_"+colname+"_dphi2_"+chn+"_"+sel,  ";"+colname+" |#Delta#phi(#it{E}_{T}^{miss},l_{2})|;Events", 64,0,TMath::Pi())));
               histos1[sel].insert(make_pair("met_"+colname+"_dphi12_"+chn, new TH1D("met_"+colname+"_dphi12_"+chn+"_"+sel, ";"+colname+" |#Delta#phi(#it{E}_{T}^{miss},ll)|;Events",    64,0,TMath::Pi())));
               if(sel==otree_sel) histos1[sel].insert(make_pair("met_"+colname+"_et_"+chn, new TH1D("met_"+colname+"_et_"+chn, ";"+colname+" #it{E}_{T}^{miss} ("+clabel+") [GeV];Events", 600,0,6000)));
            }

            //// TH2 histos
            histos2[sel].insert(make_pair("ptJet_vs_dRminMuJet_"+chn, new TH2D("ptJet_vs_dRminMuJet_"+chn+"_"+sel, ";#DeltaR(Jet,#mu);Jet p_{T} [GeV];Events",250,0,6, 250,0,500) ));
            histos2[sel].insert(make_pair("ptJet_vs_dRminElJet_"+chn, new TH2D("ptJet_vs_dRminElJet_"+chn+"_"+sel, ";#DeltaR(Jet,#it{e});Jet p_{T} [GeV];Events",250,0,6, 250,0,500) ));
            histos2[sel].insert(make_pair("ptJet_vs_MuEloss_"+chn, new TH2D("ptJet_vs_MuEloss_"+chn+"_"+sel, ";Muon E_{loss} [GeV];Jet p_{T} [GeV];Events",250,0,500, 250,0,500) ));
            histos2[sel].insert(make_pair("ptJet_vs_MuMeasEloss_"+chn, new TH2D("ptJet_vs_MuMeasEloss_"+chn+"_"+sel, ";Muon measured E_{loss} [GeV];Jet p_{T} [GeV];Events",250,0,500, 250,0,500) ));
            histos2[sel].insert(make_pair("ptJet_vs_MuParamEloss_"+chn, new TH2D("ptJet_vs_MuParamEloss_"+chn+"_"+sel, ";Muon parametrized E_{loss} [GeV];Jet p_{T} [GeV];Events",250,0,500, 250,0,500) ));
         }

         //// important!!!
         for(TMapTSP2TH1::iterator it=histos1[sel].begin() ; it!=histos1[sel].end() ; ++it) wk()->addOutput( it->second );
         for(TMapTSP2TH2::iterator it=histos2[sel].begin() ; it!=histos2[sel].end() ; ++it) wk()->addOutput( it->second );
      }

      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::GridCutFlow(TString sel, TString chn)
   {
      for(unsigned int i=0 ; i<cuts[chn].size() ; ++i)
      {
         Int_t bin = i+1;
         TString cut = cuts[chn][i];
         TString precut = cut;
         precut.ReplaceAll("evt:","");
         if(!cut.Contains("evt:")) break;
         if(cut=="evt:DxAOD") precut = "processed";
         histos1[sel]["cutflow_raw_"+chn]->SetBinContent(bin,gridCFraw[precut]);
         histos1[sel]["cutflow_wgt_"+chn]->SetBinContent(bin,gridCFwgt[precut]);
         if(cut=="evt:xAOD")
         {
            histos1[sel]["cutflow_raw_"+chn]->SetBinContent(bin,gridEC["initial_raw"]);
            histos1[sel]["cutflow_wgt_"+chn]->SetBinContent(bin,gridEC["initial_weighted"]);
         }
      }
      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::FillCutFlow(TString sel, TString chn, float weight)
   {
      for(TMapTSi::iterator it=cutflow[sel][chn].begin() ; it!=cutflow[sel][chn].end() ; ++it)
      {
         TString cut   = it->first;
         TString npass = it->second;
         // [sel][eeX]["e:XXX"]:  require >1
         // [sel][eeX]["ee:XXX"]: require==1
         // [sel][uuX]["u:XXX"]:  require >1
         // [sel][uuX]["uu:XXX"]: require==1
         // [sel][euX]["l:XXX"]:  require >1
         // [sel][euX]["eu:XXX"]: require==1
         TString lchn = "";
         if(chn.Contains("ee")) lchn = "ee";
         if(chn.Contains("uu")) lchn = "uu";
         if(chn.Contains("eu")) lchn = "eu";
         bool is1l  = (!cut.Contains(lchn));
         bool isevt = (cut.Contains("evt:"));
		 if(isevt) continue; // this is done with GridCutFlow()
         bool passthiscut = ((is1l && npass>=2) || (!is1l && npass==1));
         if(passthiscut)
         {
            Int_t bin = histos1[sel]["cutflow_raw_"+chn]->GetXaxis()->FindBin(cut);
            histos1[sel]["cutflow_raw_"+chn]->AddBinContent(bin);
            histos1[sel]["cutflow_wgt_"+chn]->AddBinContent(bin,weight);
         }
      }
      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::FillHistograms(TString sel, float weight)
   {
      TString x = (sel=="preOR") ? "_preor_" : "_";

      for(unsigned int i=0 ; i<channels.size() ; ++i)
      {
         TString chn = channels[i];

         FillCutFlow(sel,chn,weight);

         // trivial for inclusive channels but necessary for exclusive ones...
         bool pass = false;
         if(chn.BeginsWith("ee")) pass = (flags[sel]["pass_only_ee"] && flags[sel]["pass_"+chn]);
         if(chn.BeginsWith("uu")) pass = (flags[sel]["pass_only_uu"] && flags[sel]["pass_"+chn]);
         if(chn.BeginsWith("eu")) pass = (flags[sel]["pass_only_eu"] && flags[sel]["pass_"+chn]);


         //// efficiency for single leptons 
         for(unsigned int j=0 ; j<in_vf["mu_truthPt"]->size() ; ++j)
         {
            if(in_vf["mu_truthPt"]->at(j)*MeV2GeV<30.)      continue;
            if(TMath::Abs(in_vf["mu_truthEta"]->at(j))>2.5) continue;
            histos1[sel]["effd_pt_mu_inacc_"+chn]->Fill(in_vf["mu_truthPt"]->at(j)*MeV2GeV,weight);
         }
         for(unsigned int j=0 ; j<good_mu[sel].size() ; ++j)
         {
            if(!good_mu[sel][j]) continue;
            histos1[sel]["effn_pt_mu_selec_"+chn]->Fill(good_mup[sel][j].Pt()*MeV2GeV,weight);
            /// can also ask for reco+matching
            bool ismatched = false;
            for(unsigned int k=0 ; k<in_vf["mu_truthPt"]->size() ; ++k)
            {
               if(in_vf["mu_truthPt"]->at(k)*MeV2GeV<30.)      continue;
               if(TMath::Abs(in_vf["mu_truthEta"]->at(k))>2.5) continue;
               TLorentzVector p;
               p.SetPtEtaPhiM(in_vf["mu_truthPt"]->at(k),in_vf["mu_truthEta"]->at(k),in_vf["mu_truthPhi"]->at(k),105.6583745);
               if(p.DeltaR(good_mup[sel][j])<0.01) { ismatched = true; break; }
            }
			if(ismatched) histos1[sel]["effn_pt_mu_selmat_"+chn]->Fill(good_mup[sel][j].Pt()*MeV2GeV,weight);
         }

 
         if(pass)
         {
            /// pt, m, p/m, met/m etc
            histos1[sel]["pt_l1_"+chn]->Fill(fvars[sel]["pt1_"+chn],weight);
            histos1[sel]["pt_l2_"+chn]->Fill(fvars[sel]["pt2_"+chn],weight);
            histos1[sel]["m_"+chn]->Fill(fvars[sel]["m_"+chn],weight);
            histos1[sel]["metOmll_"+chn]->Fill(fvars[sel]["metOmll_"+chn],weight);
            histos1[sel]["pllOmll_"+chn]->Fill(fvars[sel]["pllOmll_"+chn],weight);
            histos1[sel]["ptllOmll_"+chn]->Fill(fvars[sel]["ptllOmll_"+chn],weight);
      
            /// complex fvars
            histos1[sel]["mt2_"+chn]->Fill(fvars[sel]["mt2_"+chn],weight);
            histos1[sel]["lt_"+chn]->Fill(fvars[sel]["lt_"+chn],weight);
            histos1[sel]["ht_"+chn]->Fill(fvars[sel]["ht_"+chn],weight);
            histos1[sel]["st_"+chn]->Fill(fvars[sel]["st_"+chn],weight);
            histos1[sel]["htmlt_"+chn]->Fill(fvars[sel]["htmlt_"+chn],weight);
            histos1[sel]["meff_"+chn]->Fill(fvars[sel]["meff_"+chn],weight);
      
            /// multiplicities
            histos1[sel]["n_el_"+chn]->Fill(ngoodel[sel],weight);
            histos1[sel]["n_mu_"+chn]->Fill(ngoodmu[sel],weight);
            histos1[sel]["n_ph_"+chn]->Fill(ngoodph[sel],weight);
            histos1[sel]["n_jet_"+chn]->Fill(ngoodjet[sel],weight);
            histos1[sel]["n_bjet_"+chn]->Fill(ngoodbjet[sel],weight);
      
            /// METs dphi
            for(unsigned int i=0 ; i<metcollections.size() ; ++i)
            {
               TString colname = metcollections[i];
               histos1[sel]["met_"+colname+"_dphi1_"+chn]->Fill(fvars[sel]["met_"+colname+"_dphi1_"+chn],weight);
               histos1[sel]["met_"+colname+"_dphi2_"+chn]->Fill(fvars[sel]["met_"+colname+"_dphi2_"+chn],weight);
               histos1[sel]["met_"+colname+"_dphi12_"+chn]->Fill(fvars[sel]["met_"+colname+"_dphi12_"+chn],weight);
               if(sel==otree_sel) histos1[sel]["met_"+colname+"_et_"+chn]->Fill(mets.et[colname]*MeV2GeV,weight); // depends on the channel slection!
            }

            //// TH2 histos
            for(unsigned int l=0 ; l<good_mu[sel].size() ; ++l)
            {
               if(!good_mu[sel][l]) continue;
               float drmin = 999.;
               int ijet = -1;
               for(unsigned int j=0 ; j<good_jet[sel].size() ; ++j)
               {
                  if(!good_jet[sel][j]) continue;
                  float dr = good_jetp[sel][j].DeltaR(good_mup[sel][l]);
                  ijet  = (dr<drmin) ? j : ijet;
                  drmin = (dr<drmin) ? dr : drmin;
               }
               // std::cout << "Muon-Jet: ijet=" << ijet << ", drmin=" << drmin << ", njets=" << good_jet[sel].size() << std::endl;
               if(ijet>=0)
               {
                  histos2[sel]["ptJet_vs_dRminMuJet_"+chn]->Fill(drmin,good_jetp[sel][ijet].Pt()*MeV2GeV,weight);
                  if(drmin<0.4)
                  {
                     histos2[sel]["ptJet_vs_MuEloss_"+chn]->Fill(in_vf["mu"+x+"EnergyLoss"]->at(l)*MeV2GeV,good_jetp[sel][ijet].Pt()*MeV2GeV,weight);
                     histos2[sel]["ptJet_vs_MuMeasEloss_"+chn]->Fill(in_vf["mu"+x+"MeasEnergyLoss"]->at(l)*MeV2GeV,good_jetp[sel][ijet].Pt()*MeV2GeV,weight);
                     histos2[sel]["ptJet_vs_MuParamEloss_"+chn]->Fill(in_vf["mu"+x+"ParamEnergyLoss"]->at(l)*MeV2GeV,good_jetp[sel][ijet].Pt()*MeV2GeV,weight);
                  }
               }
            }

            for(unsigned int l=0 ; l<good_el[sel].size() ; ++l)
            {
               if(!good_el[sel][l]) continue;
               float drmin = 999.;
               int ijet = -1;
               for(unsigned int j=0 ; j<good_jet[sel].size() ; ++j)
               {
                  if(!good_jet[sel][j]) continue;
                  float dr = good_jetp[sel][j].DeltaR(good_elp[sel][l]);
                  ijet  = (dr<drmin) ? j : ijet;
                  drmin = (dr<drmin) ? dr : drmin;
               }
               // std::cout << "Electron-Jet: ijet=" << ijet << ", drmin=" << drmin << ", njets=" << good_jet[sel].size() << std::endl;
               if(ijet>=0) histos2[sel]["ptJet_vs_dRminElJet_"+chn]->Fill(drmin,good_jetp[sel][ijet].Pt()*MeV2GeV,weight);
            }

         }
      }

      return EL::StatusCode::SUCCESS;
   }
}
