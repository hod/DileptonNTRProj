// event loop
#include <EventLoop/Worker.h>

#include "DileptonNTReader/DileptonNTReader.h"
#include "check.h"


namespace DNR
{
   EL::StatusCode DileptonNTReader::DumpInfo(TString sel)
   {
      // if(sel=="preOR") return EL::StatusCode::SUCCESS;
      float lep_pt_threshold = 3000.;

      // bool dumpevent = (in_ul["event"]==10127);
      bool dumpevent = false; //(m_eventCounter==55504 || m_eventCounter==17172 || m_eventCounter==70557);
	
      bool dump_uu = false;
      if(flags[sel]["pass_uu"])
      {
         for(int i=0 ; i<(int)good_mu[sel].size() ; ++i)
         {
            if(!good_mu[sel][i]) continue;
            if(good_mup[sel][i].Pt()*MeV2GeV>lep_pt_threshold)
            {
               dump_uu = true;
               break;
            }
         }
         if(dump_uu) DumpMuons(sel,"uu");
      }


      if(flags[sel]["pass_ee"])
      {
         bool dump_ee = false;
         for(int i=0 ; i<(int)good_el[sel].size() ; ++i)
         {
            if(!good_el[sel][i]) continue;
            if(good_elp[sel][i].Pt()*MeV2GeV>lep_pt_threshold)
            {
               dump_ee = true;
               break;
            }
         }
         if(dump_ee) DumpElectrons(sel,"ee");
      }


      if(flags[sel]["pass_ee"])
      {
         bool dump_eu = false;
         for(int i=0 ; i<(int)good_mu[sel].size() ; ++i)
         {
            if(!good_mu[sel][i]) continue;
            if(good_mup[sel][i].Pt()*MeV2GeV>lep_pt_threshold)
            {
               dump_eu = true;
               break;
            }
         }
         for(int i=0 ; i<(int)good_el[sel].size() && !dump_eu ; ++i)
         {
            if(!good_el[sel][i]) continue;
            if(good_elp[sel][i].Pt()*MeV2GeV>2500.)
            {
               dump_eu = true;
               break;
            }
         }
         if(dump_eu) { DumpMuons(sel,"eu"); DumpElectrons(sel,"eu"); }
      }


      if(dumpevent)
      {
         DumpMuons(sel,"uu");
         DumpMuons(sel,"eu");
         DumpElectrons(sel,"ee");
         DumpElectrons(sel,"eu");
      }

 
      return EL::StatusCode::SUCCESS;
   }
 




   EL::StatusCode DileptonNTReader::DumpMuons(TString sel, TString chn, bool sigonly)
   {
      TString x = (sel=="preOR") ? "_preor_" : "_";
	
      std::cout << "<<<<<<<<<< Event << " << in_ul["event"] << " (counter="<< m_eventCounter << ") " << sel << " >>>>>>>>>>" << std::endl;
      std::cout << "Channel: " << chn << std::endl;
      std::cout << "Mass(" << chn << ")=" << fvars[sel]["m_"+chn] << std::endl;
      std::cout << "MET(truth)=" << mets.et["truth"]*MeV2GeV << std::endl;
      std::cout << "MET("<<nominalmet<<")=" << mets.et[nominalmet]*MeV2GeV << std::endl;
      std::cout << "MET(track)=" << mets.et["track"]*MeV2GeV << std::endl;
      for(int i=0 ; i<(int)good_mu[sel].size() ; ++i)
      {
         // if(!good_mu[sel][i]) continue;
         if(sigonly && chn.Contains("uu") && (i!=mu1[sel] && i!=mu2[sel])) continue;
         if(sigonly && chn.Contains("eu") && i!=mu1[sel])                  continue;
         std::cout << "Muon index: " << i << std::endl;
         std::cout << "   isHighPt    = " << in_vi["mu"+x+"isHighPt"]->at(i) << std::endl;
         std::cout << "   isBadHighPt = " << in_vi["mu"+x+"isBadHighPt"]->at(i) << std::endl;
         std::cout << "   isBad       = " << in_vi["mu"+x+"isBad"]->at(i) << std::endl;
         std::cout << "   -----------------------" << std::endl;
         std::cout << "   pt = " << in_vf["mu"+x+"pt"]->at(i)*MeV2GeV << " +- " << in_vf["mu"+x+"ptErr"]->at(i)*MeV2GeV << " : " << in_vf["mu"+x+"ptErr"]->at(i)/in_vf["mu"+x+"pt"]->at(i)*100 << "\%" << std::endl;
         std::cout << "   eta = " << in_vf["mu"+x+"eta"]->at(i) << std::endl;
         std::cout << "   phi = " << in_vf["mu"+x+"phi"]->at(i) << std::endl;
         std::cout << "   -----------------------" << std::endl;
         bool matched = false;
         for(unsigned int j=0 ; j<in_vf["mu"+x+"truthPt"]->size() ; ++j)
         {
			TLorentzVector p;
            p.SetPtEtaPhiM(in_vf["mu"+x+"truthPt"]->at(j),in_vf["mu"+x+"truthEta"]->at(j),in_vf["mu"+x+"truthPhi"]->at(j),105.6583745);
            if(good_mup[sel][i].DeltaR(p)>0.01) continue;
            std::cout << "   truth_pt = " <<     in_vf["mu"+x+"truthPt"]->at(j)*MeV2GeV << std::endl;
            std::cout << "   truth_eta = " <<    in_vf["mu"+x+"truthEta"]->at(j) << std::endl;
            std::cout << "   truth_phi = " <<    in_vf["mu"+x+"truthPhi"]->at(j) << std::endl;
            std::cout << "   truth_qop = " <<    in_vf["mu"+x+"truthQOverP"]->at(j)/MeV2GeV << std::endl;
            matched = true;
			break;
         }
         if(!matched) std::cout << "   cannot find truth match" << std::endl;
         std::cout << "   -----------------------" << std::endl;
         std::cout << "   finalFitPt = " <<        in_vf["mu"+x+"finalFitPt"]->at(i)*MeV2GeV << " +- " << in_vf["mu"+x+"finalFitPtErr"]->at(i)*MeV2GeV << std::endl;
         std::cout << "   finalFitEta = " <<       in_vf["mu"+x+"finalFitEta"]->at(i) << std::endl;
         std::cout << "   finalFitPhi = " <<       in_vf["mu"+x+"finalFitPhi"]->at(i) << std::endl;
         std::cout << "   finalFitQOverP = " <<    in_vf["mu"+x+"finalFitQOverP"]->at(i)/MeV2GeV << " +- " << in_vf["mu"+x+"finalFitQOverPErr"]->at(i)/MeV2GeV << std::endl;
         std::cout << "   -----------------------" << std::endl;
         std::cout << "   id_pt = " <<     in_vf["mu"+x+"id_pt"]->at(i)*MeV2GeV << " +- " << in_vf["mu"+x+"id_ptErr"]->at(i)*MeV2GeV << std::endl;
         std::cout << "   id_eta = " <<    in_vf["mu"+x+"id_eta"]->at(i) << std::endl;
         std::cout << "   id_phi = " <<    in_vf["mu"+x+"id_phi"]->at(i) << std::endl;
         std::cout << "   id_qop = " <<    in_vf["mu"+x+"id_qop"]->at(i)/MeV2GeV << " +- " << in_vf["mu"+x+"id_qoperr"]->at(i)/MeV2GeV << std::endl;
         std::cout << "   -----------------------" << std::endl;
         std::cout << "   ms_pt = " <<     in_vf["mu"+x+"ms_pt"]->at(i)*MeV2GeV << " +- " << in_vf["mu"+x+"ms_ptErr"]->at(i)*MeV2GeV << std::endl;
         std::cout << "   ms_eta = " <<    in_vf["mu"+x+"ms_eta"]->at(i) << std::endl;
         std::cout << "   ms_phi = " <<    in_vf["mu"+x+"ms_phi"]->at(i) << std::endl;
         std::cout << "   ms_qop = " <<    in_vf["mu"+x+"ms_qop"]->at(i)/MeV2GeV << " +- " << in_vf["mu"+x+"me_qoperr"]->at(i)/MeV2GeV << std::endl;
         std::cout << "   -----------------------" << std::endl;
         std::cout << "   me_pt = " <<     in_vf["mu"+x+"me_pt"]->at(i)*MeV2GeV << " +- " << in_vf["mu"+x+"me_ptErr"]->at(i)*MeV2GeV << std::endl;
         std::cout << "   me_eta = " <<    in_vf["mu"+x+"me_eta"]->at(i) << std::endl;
         std::cout << "   me_phi = " <<    in_vf["mu"+x+"me_phi"]->at(i) << std::endl;
         std::cout << "   me_qop = " <<    in_vf["mu"+x+"me_qop"]->at(i)/MeV2GeV << " +- " << in_vf["mu"+x+"me_qoperr"]->at(i)/MeV2GeV << std::endl;
         std::cout << "   -----------------------" << std::endl;
         std::cout << "   charge = " << in_vf["mu"+x+"charge"]->at(i) << std::endl;
         std::cout << "   d0sig = " << in_vf["mu"+x+"d0sig"]->at(i) << std::endl;
         std::cout << "   z0sig = " << in_vf["mu"+x+"z0sig"]->at(i) << std::endl;
         std::cout << "   author = " << in_vi["mu"+x+"author"]->at(i) << std::endl;
         // std::cout << "   nUnspoiledCscHits = " << in_vi["mu"+x+"nUnspoiledCscHits"]->at(i) << std::endl;
         std::cout << "   pbalsig = " << in_vf["mu"+x+"momentumBalanceSignificance"]->at(i) << std::endl;
         std::cout << "   curvsig = " << in_vf["mu"+x+"scatteringCurvatureSignificance"]->at(i) << std::endl;
         std::cout << "   neigsig = " << in_vf["mu"+x+"scatteringNeighbourSignificance"]->at(i) << std::endl;
         std::cout << "   segmentDeltaEta = " << in_vf["mu"+x+"segmentDeltaEta"]->at(i) << std::endl;
         std::cout << "   segmentDeltaPhi = " << in_vf["mu"+x+"segmentDeltaPhi"]->at(i) << std::endl;
         std::cout << "   segmentChi2OverDoF = " << in_vf["mu"+x+"segmentChi2OverDoF"]->at(i) << std::endl;
         std::cout << "   FSR_CandidateEnergy = " << in_vf["mu"+x+"FSR_CandidateEnergy"]->at(i)*MeV2GeV << std::endl;
         std::cout << "   EnergyLoss = " << in_vf["mu"+x+"EnergyLoss"]->at(i)*MeV2GeV << "+- " << in_vf["mu"+x+"EnergyLossSigma"]->at(i)*MeV2GeV<< std::endl;
         std::cout << "   ParamEnergyLoss = " << in_vf["mu"+x+"ParamEnergyLoss"]->at(i)*MeV2GeV << " +" << in_vf["mu"+x+"ParamEnergyLossSigmaPlus"]->at(i)*MeV2GeV << " -" << in_vf["mu"+x+"ParamEnergyLossSigmaMinus"]->at(i)*MeV2GeV << std::endl;
         std::cout << "   MeasEnergyLoss = " << in_vf["mu"+x+"MeasEnergyLoss"]->at(i)*MeV2GeV << " +- " << in_vf["mu"+x+"MeasEnergyLossSigma"]->at(i)*MeV2GeV << std::endl;
         std::cout << "   msInnerMatchChi2 = " << in_vf["mu"+x+"msInnerMatchChi2"]->at(i) << std::endl;
         std::cout << "   msInnerMatchDOF = " << in_vf["mu"+x+"msInnerMatchDOF"]->at(i) << std::endl;
         std::cout << "   energyLossType = " << in_vi["mu"+x+"energyLossType"]->at(i) << std::endl;
      }
      std::cout << "<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>\n" << std::endl;
      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::DumpElectrons(TString sel, TString chn, bool sigonly)
   {
      TString x = (sel=="preOR") ? "_preor_" : "_";
	
      std::cout << "<<<<<<<<<< Event << " << in_ul["event"] << " (counter="<< m_eventCounter << ") " << sel << " >>>>>>>>>>" << std::endl;
      std::cout << "Channel: " << chn << std::endl;
      std::cout << "Mass(" << chn << ")=" << fvars[sel]["m_"+chn] << std::endl;
      std::cout << "MET(truth)=" << mets.et["truth"]*MeV2GeV << std::endl;
      std::cout << "MET("<<nominalmet<<")=" << mets.et[nominalmet]*MeV2GeV << std::endl;
      std::cout << "MET(track)=" << mets.et["track"]*MeV2GeV << std::endl;
      for(int i=0 ; i<(int)good_el[sel].size() ; ++i)
      {
         // if(!good_el[sel][i]) continue;
         // if(sigonly && chn.Contains("ee") && (i!=el1[sel] && i!=el2[sel])) continue;
         // if(sigonly && chn.Contains("eu") && i!=el1[sel])                  continue;
         std::cout << "Electron index: " << i << " --> pass? " << good_el[sel][i] << std::endl;
         std::cout << "   pt        = " << in_vf["el"+x+"pt"]->at(i) << std::endl;
         std::cout << "   cl_etaBE2 = " << in_vf["el"+x+"cl_etaBE2"]->at(i) << std::endl;
         std::cout << "   eta       = " << in_vf["el"+x+"eta"]->at(i) << std::endl;
         std::cout << "   phi       = " << in_vf["el"+x+"phi"]->at(i) << std::endl;
         std::cout << "   OQ        = " << in_vi["el"+x+"isGoodOQ"]->at(i) << std::endl;
         std::cout << "   LHID      = " << in_vi["el"+x+"passLHID"]->at(i) << std::endl;
      }
      return EL::StatusCode::SUCCESS;
   }

}