// event loop
#include <EventLoop/Worker.h>

#include "DileptonNTReader/DileptonNTReader.h"
#include "check.h"

namespace DNR
{
   EL::StatusCode DileptonNTReader::SetOutBranch(TString name, int enumtype)
   {
      if(enumtype==UL64t){ out_uls.insert(make_pair(name,0));                 otree->Branch(name, &out_uls[name]);   }
      if(enumtype==INT)  { out_ints.insert(make_pair(name,-999));             otree->Branch(name, &out_ints[name]);   }
      if(enumtype==FLT)  { out_floats.insert(make_pair(name,-999.));          otree->Branch(name, &out_floats[name]); }
      if(enumtype==TLV)  { out_tlvs.insert(make_pair(name,TLorentzVector())); otree->Branch(name, &out_tlvs[name]);   }
      if(enumtype==VINT) { vector<int>            vitmp;   out_vints.insert(make_pair(name,vitmp));     otree->Branch(name, &out_vints[name]);   }
      if(enumtype==VFLT) { vector<float>          vftmp;   out_vfloats.insert(make_pair(name,vftmp));   otree->Branch(name, &out_vfloats[name]); }
      if(enumtype==VTLV) { vector<TLorentzVector> vtlvtmp; out_vtlvs.insert(make_pair(name,vtlvtmp));   otree->Branch(name, &out_vtlvs[name]);   }
      return EL::StatusCode::SUCCESS;
   }
   EL::StatusCode DileptonNTReader::ClearOutBranches()
   {
      TLorentzVector ptmp;
      for(TMapTSUL64::iterator it=out_uls.begin()      ; it!=out_uls.end()     ; ++it) { it->second = 0;     }
      for(TMapTSi::iterator    it=out_ints.begin()     ; it!=out_ints.end()    ; ++it) { it->second = -999;  }
      for(TMapTSf::iterator    it=out_floats.begin()   ; it!=out_floats.end()  ; ++it) { it->second = -999;  }
      for(TMapTStlv::iterator  it=out_tlvs.begin()     ; it!=out_tlvs.end()    ; ++it) { it->second = ptmp;  }
      for(TMapTSvi::iterator   it=out_vints.begin()    ; it!=out_vints.end()   ; ++it) { it->second.clear(); }
      for(TMapTSvf::iterator   it=out_vfloats.begin()  ; it!=out_vfloats.end() ; ++it) { it->second.clear(); }
      for(TMapTSvtlv::iterator it=out_vtlvs.begin()    ; it!=out_vtlvs.end()   ; ++it) { it->second.clear(); }
      return EL::StatusCode::SUCCESS;
   }
   EL::StatusCode DileptonNTReader::SetOutBranches()
   {
      SetOutBranch("event",UL64t);
      SetOutBranch("run",INT);
      SetOutBranch("lbn",INT);
      SetOutBranch("bcid",INT);
      SetOutBranch("year",INT);
      
      SetOutBranch("n_vx",INT);
      SetOutBranch("averageIntPerXing",FLT);
      SetOutBranch("corAverageIntPerXing",FLT);
      
      SetOutBranch("pdf_x1",FLT);
      SetOutBranch("pdf_x2",FLT);
      SetOutBranch("pdf_pdf",FLT);
      SetOutBranch("pdf_pdf2",FLT);
      SetOutBranch("pdf_scale",FLT);
      
      SetOutBranch("pu_weight",FLT);
      SetOutBranch("btag_weight",FLT);
      SetOutBranch("jvt_weight",FLT);
      SetOutBranch("jvt_all_weight",FLT);
      SetOutBranch("mconly_weight",FLT);
      SetOutBranch("sh22_weight",FLT);
      SetOutBranch("mu_SF_tot",FLT);
      SetOutBranch("kF_weight",FLT);
      SetOutBranch("xsec",FLT);
      SetOutBranch("geneff",FLT);
      SetOutBranch("Nevt",FLT);
      
      SetOutBranch("mconly_weights",VFLT);

      for(unsigned int i=0 ; i<leptonchannels.size() ; ++i)
      {
         TString lchn = leptonchannels[i];

         SetOutBranch("pass_"+lchn,INT);
         SetOutBranch("pass_only_"+lchn,INT);

         SetOutBranch("pass_"+lchn+"0b",INT);
         SetOutBranch("pass_"+lchn+"1b",INT);
         SetOutBranch("pass_"+lchn+"2b",INT);
         SetOutBranch("pass_"+lchn+"met",INT);

         SetOutBranch("m_"+lchn,FLT);
         SetOutBranch("mt2_"+lchn,FLT);
         SetOutBranch("metOmll_"+lchn,FLT);
         SetOutBranch("lt_"+lchn,FLT);
         SetOutBranch("ht_"+lchn,FLT);
         SetOutBranch("st_"+lchn,FLT);
         SetOutBranch("htmlt_"+lchn,FLT);
         SetOutBranch("meff_"+lchn,FLT);
         SetOutBranch("pllOmll_"+lchn,FLT);
         SetOutBranch("ptllOmll_"+lchn,FLT);
         SetOutBranch("pllmetdphi_"+lchn,FLT);
      }
      
      SetOutBranch("el_SF",VFLT);
      SetOutBranch("el_SFiso",VFLT);
      SetOutBranch("el_pt",VFLT);
      SetOutBranch("el_eta",VFLT);
      SetOutBranch("el_phi",VFLT);
      SetOutBranch("el_e",VFLT);
      SetOutBranch("mu_SF",VFLT);
      SetOutBranch("mu_SFiso",VFLT);
      SetOutBranch("mu_pt",VFLT);
      SetOutBranch("mu_ptErr",VFLT);
      SetOutBranch("mu_eta",VFLT);
      SetOutBranch("mu_phi",VFLT);
      SetOutBranch("mu_e",VFLT);
      SetOutBranch("ph_pt",VFLT);
      SetOutBranch("ph_eta",VFLT);
      SetOutBranch("ph_phi",VFLT);
      SetOutBranch("ph_e",VFLT);
      SetOutBranch("jet_pt",VFLT);
      SetOutBranch("jet_eta",VFLT);
      SetOutBranch("jet_phi",VFLT);
      SetOutBranch("jet_e",VFLT);
      SetOutBranch("bjet_pt",VFLT);
      SetOutBranch("bjet_eta",VFLT);
      SetOutBranch("bjet_phi",VFLT);
      SetOutBranch("bjet_e",VFLT);

      SetOutBranch("met_wmuon_cst_et",FLT);
      SetOutBranch("met_wmuon_cst_phi",FLT);
      SetOutBranch("met_track_et",FLT);
      SetOutBranch("met_track_phi",FLT);
      SetOutBranch("met_truth_et",FLT);
      SetOutBranch("met_truth_phi",FLT);

      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::FillOutTree()
   {
      TString sel = otree_sel; //"pstOR";
      if(!flags[sel]["pass_ee"] && !flags[sel]["pass_uu"] && !flags[sel]["pass_eu"]) return EL::StatusCode::SUCCESS;

      //// general
      out_uls["event"]  = in_ul["event"];
      out_ints["run"]   = in_i["run"];
      out_ints["lbn"]   = in_i["lbn"];
      out_ints["bcid"]  = in_i["bcid"];
      out_ints["year"]  = in_i["year"];
      
      out_ints["n_vx"]                   = in_i["n_vx"];
      out_floats["averageIntPerXing"]    = in_f["averageIntPerXing"];
      out_floats["corAverageIntPerXing"] = in_f["corAverageIntPerXing"];
      
      out_floats["pdf_x1"] = in_f["pdf_x1"];
      out_floats["pdf_x2"] = in_f["pdf_x2"];
      out_floats["pdf_pdf"] = in_f["pdf_pdf1"];
      out_floats["pdf_pdf2"] = in_f["pdf_pdf2"];
      out_floats["pdf_scale"] = in_f["pdf_scale"];
      
      out_floats["pu_weight"]      = in_f["pu_weight"];
      out_floats["btag_weight"]    = in_f["btag_weight"];
      out_floats["jvt_weight"]     = in_f["jvt_weight"];
      out_floats["jvt_all_weight"] = in_f["jvt_all_weight"];
      out_floats["mconly_weight"]  = in_f["mconly_weight"];
      out_floats["sh22_weight"]    = in_f["sh22_weight"]; // SherpaVjetsNjetsWeight
      out_floats["mu_SF_tot"]      = in_f["mu_SF_tot"];
      out_floats["kF_weight"]      = in_f["kF_weight"];
      out_floats["xsec"]           = in_f["xsec"];
      out_floats["geneff"]         = in_f["geneff"];
      out_floats["Nevt"]           = gridEC["initial_raw"];
	  for(unsigned int i=0 ; i<in_vf["mconly_weights"]->size() ; ++i) out_vfloats["mconly_weights"].push_back( in_vf["mconly_weights"]->at(i) );


      for(unsigned int i=0 ; i<leptonchannels.size() ; ++i)
      {
         TString lchn = leptonchannels[i];

         /// channel-specific flags         
         out_ints["pass_"+lchn] = flags[sel]["pass_"+lchn];
         out_ints["pass_only_"+lchn] = flags[sel]["pass_only_"+lchn];

         out_ints["pass_"+lchn+"0b"]  = flags[sel]["pass_"+lchn+"0b"];
         out_ints["pass_"+lchn+"1b"]  = flags[sel]["pass_"+lchn+"1b"];
         out_ints["pass_"+lchn+"2b"]  = flags[sel]["pass_"+lchn+"2b"];
         out_ints["pass_"+lchn+"met"] = flags[sel]["pass_"+lchn+"met"];
      
         /// channel-specific vars
         out_floats["m_"+lchn]        = fvars[sel]["m_"+lchn];
         out_floats["mt2_"+lchn]      = fvars[sel]["mt2_"+lchn];
         out_floats["metOmll_"+lchn]  = fvars[sel]["metOmll_"+lchn];
         out_floats["lt_"+lchn]       = fvars[sel]["lt_"+lchn];
         out_floats["ht_"+lchn]       = fvars[sel]["ht_"+lchn];
         out_floats["st_"+lchn]       = fvars[sel]["st_"+lchn];
         out_floats["htmlt_"+lchn]    = fvars[sel]["htmlt_"+lchn];
         out_floats["meff_"+lchn]     = fvars[sel]["meff_"+lchn];
         out_floats["pllOmll_"+lchn]  = fvars[sel]["pllOmll_"+lchn];
         out_floats["ptllOmll_"+lchn] = fvars[sel]["ptllOmll_"+lchn];
         out_floats["pllmetdphi_"+lchn] = fvars[sel]["met_"+nominalmet+"_dphi12_"+lchn];
      }


	  /// only ee
      if(flags[sel]["pass_only_ee"])
      {
         /// fill the first 2 electrons if it is an ee-only event
         /// then fill the rest of the good electrons, skipping the ones forming the ee candidate
         /// then fill all good muons
         out_vfloats["el_SF"].push_back( els[sel].SF->at(el1[sel]) );
         out_vfloats["el_SF"].push_back( els[sel].SF->at(el2[sel]) );
         out_vfloats["el_SFiso"].push_back( els[sel].SFiso->at(el1[sel]) );
         out_vfloats["el_SFiso"].push_back( els[sel].SFiso->at(el2[sel]) );
         out_vfloats["el_pt"].push_back( good_elp[sel][el1[sel]].Pt()*MeV2GeV );
         out_vfloats["el_pt"].push_back( good_elp[sel][el2[sel]].Pt()*MeV2GeV );
         out_vfloats["el_eta"].push_back( good_elp[sel][el1[sel]].Eta() );
         out_vfloats["el_eta"].push_back( good_elp[sel][el2[sel]].Eta() );
         out_vfloats["el_phi"].push_back( good_elp[sel][el1[sel]].Phi() );
         out_vfloats["el_phi"].push_back( good_elp[sel][el2[sel]].Phi() );
         out_vfloats["el_e"].push_back( good_elp[sel][el1[sel]].E()*MeV2GeV );
         out_vfloats["el_e"].push_back( good_elp[sel][el2[sel]].E()*MeV2GeV );
         for(int i=0 ; i<(int)good_elp[sel].size() ; ++i)
         {
            if(!good_el[sel][i])           continue;
			if(i==el1[sel] || i==el2[sel]) continue;
			out_vfloats["el_SF"].push_back(     els[sel].SF->at(i) );
			out_vfloats["el_SFiso"].push_back(  els[sel].SFiso->at(i) );
            out_vfloats["el_pt"].push_back(  good_elp[sel][i].Pt()*MeV2GeV );
            out_vfloats["el_eta"].push_back( good_elp[sel][i].Eta() );
            out_vfloats["el_phi"].push_back( good_elp[sel][i].Phi() );
            out_vfloats["el_e"].push_back(   good_elp[sel][i].E()*MeV2GeV );
         }
         for(int i=0 ; i<(int)good_mup[sel].size() ; ++i)
         {
            if(!good_mu[sel][i]) continue;
            out_vfloats["mu_SF"].push_back(     mus[sel].SF->at(i) );
			out_vfloats["mu_SFiso"].push_back(  mus[sel].SFiso->at(i) );
            out_vfloats["mu_pt"].push_back(  good_mup[sel][i].Pt()*MeV2GeV );
            out_vfloats["mu_ptErr"].push_back(  mus[sel].ptErr->at(i)*MeV2GeV );
            out_vfloats["mu_eta"].push_back( good_mup[sel][i].Eta() );
            out_vfloats["mu_phi"].push_back( good_mup[sel][i].Phi() );
            out_vfloats["mu_e"].push_back(   good_mup[sel][i].E()*MeV2GeV );
         }
		
      }


      /// only uu
      if(flags[sel]["pass_only_uu"])
      {
         /// fill the first 2 muons if it is an uu-only event
         /// then fill the rest of the good muons, skipping the ones forming the uu candidate
         /// then fill all good electrons
         out_vfloats["mu_SF"].push_back( mus[sel].SF->at(mu1[sel]) );
         out_vfloats["mu_SF"].push_back( mus[sel].SF->at(mu2[sel]) );
         out_vfloats["mu_SFiso"].push_back( mus[sel].SFiso->at(mu1[sel]) );
         out_vfloats["mu_SFiso"].push_back( mus[sel].SFiso->at(mu2[sel]) );
         out_vfloats["mu_pt"].push_back( good_mup[sel][mu1[sel]].Pt()*MeV2GeV );
         out_vfloats["mu_pt"].push_back( good_mup[sel][mu2[sel]].Pt()*MeV2GeV );
         out_vfloats["mu_ptErr"].push_back( mus[sel].ptErr->at(mu1[sel])*MeV2GeV );
         out_vfloats["mu_ptErr"].push_back( mus[sel].ptErr->at(mu2[sel])*MeV2GeV );
         out_vfloats["mu_eta"].push_back( good_mup[sel][mu1[sel]].Eta() );
         out_vfloats["mu_eta"].push_back( good_mup[sel][mu2[sel]].Eta() );
         out_vfloats["mu_phi"].push_back( good_mup[sel][mu1[sel]].Phi() );
         out_vfloats["mu_phi"].push_back( good_mup[sel][mu2[sel]].Phi() );
         out_vfloats["mu_e"].push_back( good_mup[sel][mu1[sel]].E()*MeV2GeV );
         out_vfloats["mu_e"].push_back( good_mup[sel][mu2[sel]].E()*MeV2GeV );
         for(int i=0 ; i<(int)good_mup[sel].size() ; ++i)
         {
            if(!good_mu[sel][i])           continue;
			if(i==mu1[sel] || i==mu2[sel]) continue;
			out_vfloats["mu_SF"].push_back(     mus[sel].SF->at(i) );
			out_vfloats["mu_SFiso"].push_back(  mus[sel].SFiso->at(i) );
            out_vfloats["mu_pt"].push_back(  good_mup[sel][i].Pt()*MeV2GeV );
            out_vfloats["mu_ptErr"].push_back(  mus[sel].ptErr->at(i)*MeV2GeV );
            out_vfloats["mu_eta"].push_back( good_mup[sel][i].Eta() );
            out_vfloats["mu_phi"].push_back( good_mup[sel][i].Phi() );
            out_vfloats["mu_e"].push_back(   good_mup[sel][i].E()*MeV2GeV );
         }
         for(int i=0 ; i<(int)good_elp[sel].size() ; ++i)
         {
            if(!good_el[sel][i]) continue;
            out_vfloats["el_SF"].push_back(     els[sel].SF->at(i) );
			out_vfloats["el_SFiso"].push_back(  els[sel].SFiso->at(i) );
            out_vfloats["el_pt"].push_back(  good_elp[sel][i].Pt()*MeV2GeV );
            out_vfloats["el_eta"].push_back( good_elp[sel][i].Eta() );
            out_vfloats["el_phi"].push_back( good_elp[sel][i].Phi() );
            out_vfloats["el_e"].push_back(   good_elp[sel][i].E()*MeV2GeV );
         }
      }


      /// only eu
      if(flags[sel]["pass_only_eu"])
      {
         /// fill the first electron and muon if it is an eu-only event
         /// then fill the rest of the good electrons, skipping the one forming the eu candidate
         /// then fill the rest of the good muons, skipping the one forming the eu candidate
         out_vfloats["el_SF"].push_back( els[sel].SF->at(el1[sel]) );
         out_vfloats["mu_SF"].push_back( mus[sel].SF->at(mu1[sel]) );
         out_vfloats["el_SFiso"].push_back( els[sel].SFiso->at(el1[sel]) );
         out_vfloats["mu_SFiso"].push_back( mus[sel].SFiso->at(mu1[sel]) );
         out_vfloats["el_pt"].push_back( good_elp[sel][el1[sel]].Pt()*MeV2GeV );
         out_vfloats["mu_pt"].push_back( good_mup[sel][mu1[sel]].Pt()*MeV2GeV );
         out_vfloats["mu_ptErr"].push_back( mus[sel].ptErr->at(mu1[sel])*MeV2GeV );
         out_vfloats["el_eta"].push_back( good_elp[sel][el1[sel]].Eta() );
         out_vfloats["mu_eta"].push_back( good_mup[sel][mu1[sel]].Eta() );
         out_vfloats["el_phi"].push_back( good_elp[sel][el1[sel]].Phi() );
         out_vfloats["mu_phi"].push_back( good_mup[sel][mu1[sel]].Phi() );
         out_vfloats["el_e"].push_back( good_elp[sel][el1[sel]].E()*MeV2GeV );
         out_vfloats["mu_e"].push_back( good_mup[sel][mu1[sel]].E()*MeV2GeV );
         for(int i=0 ; i<(int)good_elp[sel].size() ; ++i)
         {
            if(!good_el[sel][i]) continue;
			if(i==el1[sel])      continue;
            out_vfloats["el_SF"].push_back(     els[sel].SF->at(i) );
			out_vfloats["el_SFiso"].push_back(  els[sel].SFiso->at(i) );
            out_vfloats["el_pt"].push_back(  good_elp[sel][i].Pt()*MeV2GeV );
            out_vfloats["el_eta"].push_back( good_elp[sel][i].Eta() );
            out_vfloats["el_phi"].push_back( good_elp[sel][i].Phi() );
            out_vfloats["el_e"].push_back(   good_elp[sel][i].E()*MeV2GeV );
         }
         for(int i=0 ; i<(int)good_mup[sel].size() ; ++i)
         {
            if(!good_mu[sel][i]) continue;
			if(i==mu1[sel])      continue;
			out_vfloats["mu_SF"].push_back(     mus[sel].SF->at(i) );
			out_vfloats["mu_SFiso"].push_back(  mus[sel].SFiso->at(i) );
            out_vfloats["mu_pt"].push_back(  good_mup[sel][i].Pt()*MeV2GeV );
            out_vfloats["mu_ptErr"].push_back(  mus[sel].ptErr->at(i)*MeV2GeV );
            out_vfloats["mu_eta"].push_back( good_mup[sel][i].Eta() );
            out_vfloats["mu_phi"].push_back( good_mup[sel][i].Phi() );
            out_vfloats["mu_e"].push_back(   good_mup[sel][i].E()*MeV2GeV );
         }
      }
      
      /// photons, jets and b-jets
      for(int i=0 ; i<(int)good_ph[sel].size() ; ++i)
      {
         if(!good_ph[sel][i]) continue;
         out_vfloats["ph_pt"].push_back( good_php[sel][i].Pt()*MeV2GeV );
         out_vfloats["ph_eta"].push_back( good_php[sel][i].Eta() );
         out_vfloats["ph_phi"].push_back( good_php[sel][i].Phi() );
         out_vfloats["ph_e"].push_back( good_php[sel][i].E()*MeV2GeV );
      }
      for(int i=0 ; i<(int)good_jet[sel].size() ; ++i)
      {
         if(!good_jet[sel][i]) continue;
         out_vfloats["jet_pt"].push_back( good_jetp[sel][i].Pt()*MeV2GeV );
         out_vfloats["jet_eta"].push_back( good_jetp[sel][i].Eta() );
         out_vfloats["jet_phi"].push_back( good_jetp[sel][i].Phi() );
         out_vfloats["jet_e"].push_back( good_jetp[sel][i].E()*MeV2GeV );
      }
      for(int i=0 ; i<(int)good_bjet[sel].size() ; ++i)
      {
         if(!good_bjet[sel][i]) continue;
         out_vfloats["bjet_pt"].push_back( good_bjetp[sel][i].Pt()*MeV2GeV );
         out_vfloats["bjet_eta"].push_back( good_bjetp[sel][i].Eta() );
         out_vfloats["bjet_phi"].push_back( good_bjetp[sel][i].Phi() );
         out_vfloats["bjet_e"].push_back( good_bjetp[sel][i].E()*MeV2GeV );
      }

      /// fill the mets
      out_floats["met_wmuon_cst_et"]  = mets.et[nominalmet]*MeV2GeV;
      out_floats["met_wmuon_cst_phi"] = mets.phi[nominalmet];
      out_floats["met_track_et"]      = mets.et["track"]*MeV2GeV;
      out_floats["met_track_phi"]     = mets.phi["track"];
      out_floats["met_truth_et"]      = mets.et["truth"]*MeV2GeV;  
      out_floats["met_truth_phi"]     = mets.phi["truth"]; 

      //// fill the tree
      otree->Fill();
      if(m_eventCounter%10000==0 && m_eventCounter!=0)
      {
         otree->FlushBaskets();
         // otree->Write("", TObject::kOverwrite); // not sure if that'd help
      }

      return EL::StatusCode::SUCCESS;
   }

}