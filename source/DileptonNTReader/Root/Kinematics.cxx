// event loop
#include <EventLoop/Worker.h>

#include "DileptonNTReader/DileptonNTReader.h"
#include "check.h"
#include "lester_mt2_bisect.h"


namespace DNR
{
   EL::StatusCode DileptonNTReader::CalculateVars(TString sel)
   {
      for(unsigned int i=0 ; i<channels.size() ; ++i)
      {
         TString chn = channels[i];

         // trivial for inclusive channels but necessary for exclusive ones...
         bool pass = false;
         if(chn.BeginsWith("ee")) pass = (flags[sel]["pass_only_ee"] && flags[sel]["pass_"+chn]);
         if(chn.BeginsWith("uu")) pass = (flags[sel]["pass_only_uu"] && flags[sel]["pass_"+chn]);
         if(chn.BeginsWith("eu")) pass = (flags[sel]["pass_only_eu"] && flags[sel]["pass_"+chn]);
         
         if(pass)
         {
            /// calculate some vars
            PoM(sel,chn);
            LTHTST(sel,chn);
            MT2(sel,chn);
            MEFF(sel,chn);
         }
      }

      return EL::StatusCode::SUCCESS;
   }

	
   EL::StatusCode DileptonNTReader::PoM(TString sel, TString channel)
   {
      TLorentzVector p1;
      TLorentzVector p2;
      if(channel=="ee") { p1 = good_elp[sel][el1[sel]]; p2 = good_elp[sel][el2[sel]]; }
      if(channel=="uu") { p1 = good_mup[sel][mu1[sel]]; p2 = good_mup[sel][mu2[sel]]; }
      if(channel=="eu") { p1 = good_elp[sel][el1[sel]]; p2 = good_mup[sel][mu1[sel]]; }
      fvars[sel]["pt1_"+channel] = p1.Pt()*MeV2GeV;
      fvars[sel]["pt2_"+channel] = p2.Pt()*MeV2GeV;

      TLorentzVector p;
      if(channel=="ee") p = good_elp[sel][el1[sel]]+good_elp[sel][el2[sel]];
      if(channel=="uu") p = good_mup[sel][mu1[sel]]+good_mup[sel][mu2[sel]];
      if(channel=="eu") p = good_elp[sel][el1[sel]]+good_mup[sel][mu1[sel]];
      fvars[sel]["m_"+channel]        = p.M()*MeV2GeV;
      fvars[sel]["pllOmll_"+channel]  = p.P()/p.M();
      fvars[sel]["ptllOmll_"+channel] = p.Pt()/p.M();

      float metOm = mets.et[nominalmet]/fvars[sel]["m_"+channel];
      fvars[sel]["metOmll_"+channel] = metOm;

      for(unsigned int i=0 ; i<metcollections.size() ; ++i)
      {
         TString colname = metcollections[i];
         fvars[sel]["met_"+colname+"_dphi1_"+channel]  = TMath::Abs(TVector2::Phi_mpi_pi(mets.phi[colname]-p1.Phi()));
         fvars[sel]["met_"+colname+"_dphi2_"+channel]  = TMath::Abs(TVector2::Phi_mpi_pi(mets.phi[colname]-p2.Phi()));
         fvars[sel]["met_"+colname+"_dphi12_"+channel] = TMath::Abs(TVector2::Phi_mpi_pi(mets.phi[colname]-p.Phi()));
      }

      return EL::StatusCode::SUCCESS;
   }
	
	
   EL::StatusCode DileptonNTReader::MT2(TString sel, TString channel)
   {
      double mVisA = -1;
      double pxA   = -1;
      double pyA   = -1;

      double mVisB = -1;
      double pxB   = -1;
      double pyB   = -1;
      
      fvars[sel]["mt2_"+channel] = -999;

      if(channel=="ee")
      {
         mVisA = good_elp[sel][el1[sel]].M()*MeV2GeV;   // mass of visible object on side A.  Must be >=0.
         pxA   = good_elp[sel][el1[sel]].Px()*MeV2GeV;  // x momentum of visible object on side A.
         pyA   = good_elp[sel][el1[sel]].Py()*MeV2GeV;  // y momentum of visible object on side A.
       
         mVisB = good_elp[sel][el2[sel]].M()*MeV2GeV;  // mass of visible object on side B.  Must be >=0.
         pxB   = good_elp[sel][el2[sel]].Px()*MeV2GeV; // x momentum of visible object on side B.
         pyB   = good_elp[sel][el2[sel]].Py()*MeV2GeV; // y momentum of visible object on side B.
      }
      if(channel=="uu")
      {
         mVisA = good_mup[sel][mu1[sel]].M()*MeV2GeV;   // mass of visible object on side A.  Must be >=0.
         pxA   = good_mup[sel][mu1[sel]].Px()*MeV2GeV;  // x momentum of visible object on side A.
         pyA   = good_mup[sel][mu1[sel]].Py()*MeV2GeV;  // y momentum of visible object on side A.

         mVisB = good_mup[sel][mu2[sel]].M()*MeV2GeV;  // mass of visible object on side B.  Must be >=0.
         pxB   = good_mup[sel][mu2[sel]].Px()*MeV2GeV; // x momentum of visible object on side B.
         pyB   = good_mup[sel][mu2[sel]].Py()*MeV2GeV; // y momentum of visible object on side B.
      }
      if(channel=="eu")
      {
         mVisA = good_elp[sel][el1[sel]].M()*MeV2GeV;   // mass of visible object on side A.  Must be >=0.
         pxA   = good_elp[sel][el1[sel]].Px()*MeV2GeV;  // x momentum of visible object on side A.
         pyA   = good_elp[sel][el1[sel]].Py()*MeV2GeV;  // y momentum of visible object on side A.

         mVisB = good_mup[sel][mu1[sel]].M()*MeV2GeV;  // mass of visible object on side B.  Must be >=0.
         pxB   = good_mup[sel][mu1[sel]].Px()*MeV2GeV; // x momentum of visible object on side B.
         pyB   = good_mup[sel][mu1[sel]].Py()*MeV2GeV; // y momentum of visible object on side B.
      }

      double pxMiss = (mets.etx[nominalmet])*MeV2GeV; // x component of missing transverse momentum.
	  double pyMiss = (mets.ety[nominalmet])*MeV2GeV; // y component of missing transverse momentum.
	  
	  double chiA = 80; // hypothesised mass of invisible on side A.  Must be >=0.
	  double chiB = 80; // hypothesised mass of invisible on side B.  Must be >=0.
      
	  double desiredPrecisionOnMt2 = 0; // Must be >=0.  If 0 alg aims for machine precision.  if >0, MT2 computed to supplied absolute precision.
      
	  asymm_mt2_lester_bisect::disableCopyrightMessage();
      
	  double mt2 = asymm_mt2_lester_bisect::get_mT2(mVisA,pxA,pyA, mVisB,pxB,pyB, pxMiss,pyMiss, chiA,chiB,desiredPrecisionOnMt2);
      fvars[sel]["mt2_"+channel] = mt2;

      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::LTHTST(TString sel, TString channel)
   {
      fvars[sel]["ht_"+channel] = 0;
	  for(int i=0 ; i<(int)good_jet[sel].size() ; ++i)
      {
         if(!good_jet[sel][i]) continue;
         fvars[sel]["ht_"+channel] += good_jetp[sel][i].Pt()*MeV2GeV;
      }

      if(channel=="ee") fvars[sel]["lt_ee"] = (good_elp[sel][el1[sel]].Pt() + good_elp[sel][el2[sel]].Pt())*MeV2GeV;
      if(channel=="uu") fvars[sel]["lt_uu"] = (good_mup[sel][mu1[sel]].Pt() + good_mup[sel][mu2[sel]].Pt())*MeV2GeV;
      if(channel=="eu") fvars[sel]["lt_eu"] = (good_elp[sel][el1[sel]].Pt() + good_mup[sel][mu1[sel]].Pt())*MeV2GeV;

      fvars[sel]["st_"+channel]    = fvars[sel]["lt_"+channel] + fvars[sel]["ht_"+channel];
      fvars[sel]["htmlt_"+channel] = fvars[sel]["ht_"+channel] - fvars[sel]["lt_"+channel];

      return EL::StatusCode::SUCCESS;
   }


   EL::StatusCode DileptonNTReader::MEFF(TString sel, TString channel)
   {
      fvars[sel]["meff_"+channel] = -999;
      TLorentzVector pjet;
      bool atleastonejet = false;
	  for(int i=0 ; i<(int)good_jet[sel].size() ; ++i)
      {
         if(!good_jet[sel][i]) continue;
         pjet += good_jetp[sel][i];
         if(!atleastonejet) atleastonejet = true;
      }
      
      TLorentzVector pmet;
      pmet.SetPxPyPzE(mets.etx[nominalmet], mets.ety[nominalmet], 0., mets.et[nominalmet]);

      TLorentzVector plep;
      if(channel=="ee") plep = (good_elp[sel][el1[sel]] + good_elp[sel][el2[sel]]);
      if(channel=="uu") plep = (good_mup[sel][mu1[sel]] + good_mup[sel][mu2[sel]]);
      if(channel=="eu") plep = (good_elp[sel][el1[sel]] + good_mup[sel][mu1[sel]]);

   	  float meff = (atleastonejet) ? (plep+pmet+pjet).M()*MeV2GeV : (plep+pmet).M()*MeV2GeV;

      fvars[sel]["meff_"+channel] = meff;

      return EL::StatusCode::SUCCESS;
   }
}