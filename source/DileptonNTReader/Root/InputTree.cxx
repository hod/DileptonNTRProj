// event loop
#include <EventLoop/Worker.h>

#include "DileptonNTReader/DileptonNTReader.h"
#include "check.h"

namespace DNR
{
   EL::StatusCode DileptonNTReader::AddBranch(TString name, int type)
   {
      if(type==UL64t)       in_ul.insert(make_pair(name,0));
      else if(type==INTt)   in_i.insert(make_pair(name,-999));
      else if(type==FLTt)   in_f.insert(make_pair(name,-999.));
      else if(type==VINTp)  in_vi.insert(make_pair(name,  (vector<int>*)NULL ));
      else if(type==VUINTp) in_vui.insert(make_pair(name, (vector<unsigned int>*)NULL ));
      else if(type==VFLTp)  in_vf.insert(make_pair(name,  (vector<float>*)NULL ));
      else return EL::StatusCode::FAILURE;

      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode DileptonNTReader::SetInputBranches()
   {
      for(TMapTSUL64::iterator  it=in_ul.begin()  ; it!=in_ul.end()  ; ++it) wk()->tree()->SetBranchAddress(it->first, &in_ul[it->first]);
      for(TMapTSIt::iterator    it=in_i.begin()   ; it!=in_i.end()   ; ++it) wk()->tree()->SetBranchAddress(it->first, &in_i[it->first]);
      for(TMapTSFt::iterator    it=in_f.begin()   ; it!=in_f.end()   ; ++it) wk()->tree()->SetBranchAddress(it->first, &in_f[it->first]);
      for(TMapTSP2vi::iterator  it=in_vi.begin()  ; it!=in_vi.end()  ; ++it) wk()->tree()->SetBranchAddress(it->first, &in_vi[it->first]);
      for(TMapTSP2vf::iterator  it=in_vf.begin()  ; it!=in_vf.end()  ; ++it) wk()->tree()->SetBranchAddress(it->first, &in_vf[it->first]);
      for(TMapTSP2vui::iterator it=in_vui.begin() ; it!=in_vui.end() ; ++it) wk()->tree()->SetBranchAddress(it->first, &in_vui[it->first]);


      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode DileptonNTReader::DecalreInputBranches()
   {
      std::cout << "setting input tree" << std::endl;

      //// then enable one by one
      AddBranch("pdf_x1",    FLTt);
      AddBranch("pdf_x2",    FLTt);
      AddBranch("pdf_pdf1",  FLTt);
      AddBranch("pdf_pdf2",  FLTt);
      AddBranch("pdf_scale", FLTt);
      
      AddBranch("event",UL64t);
      AddBranch("run",  INTt);
      AddBranch("lbn",  INTt);
      AddBranch("bcid", INTt);
      AddBranch("year", INTt);

      AddBranch("n_vx", INTt);
      AddBranch("averageIntPerXing",    FLTt);
      AddBranch("corAverageIntPerXing", FLTt);
      
      AddBranch("pu_weight",      FLTt);
      AddBranch("btag_weight",    FLTt);
      AddBranch("jvt_weight",     FLTt);
      AddBranch("jvt_all_weight", FLTt);
      AddBranch("mconly_weight",  FLTt);
      AddBranch("sh22_weight",    FLTt); // SherpaVjetsNjetsWeight
      AddBranch("mu_SF_tot",      FLTt);
      AddBranch("kF_weight",      FLTt);
      AddBranch("xsec",           FLTt);
      AddBranch("geneff",         FLTt);
      AddBranch("mconly_weights", VFLTp);
      
      for(unsigned int i=0 ; i<selections.size() ; ++i)
      {
         TString x = (selections[i]=="preOR") ? "_preor_" : "_";

         AddBranch("el"+x+"SF",  VFLTp);
         AddBranch("el"+x+"SF_iso",VFLTp);
         AddBranch("el"+x+"pt",  VFLTp);
         AddBranch("el"+x+"eta", VFLTp);
         AddBranch("el"+x+"phi", VFLTp);
         AddBranch("el"+x+"m",   VFLTp);
         AddBranch("el"+x+"e",   VFLTp);
         AddBranch("el"+x+"charge", VFLTp);
         AddBranch("el"+x+"d0sig",  VFLTp);
         AddBranch("el"+x+"z0sig",  VFLTp);
         AddBranch("el"+x+"isotool_pass_loose", VINTp);
         AddBranch("el"+x+"cl_etaBE2", VFLTp);
         AddBranch("el"+x+"isGoodOQ",  VINTp);
         AddBranch("el"+x+"passLHID",  VINTp);
         AddBranch("el"+x+"passChID",  VINTp);
      
         AddBranch("mu"+x+"SF",  VFLTp);
         AddBranch("mu"+x+"SF_iso", VFLTp);
         AddBranch("mu"+x+"pt",  VFLTp);
         AddBranch("mu"+x+"ptErr",VFLTp);
         AddBranch("mu"+x+"eta", VFLTp);
         AddBranch("mu"+x+"phi", VFLTp);
         AddBranch("mu"+x+"m",   VFLTp);
         AddBranch("mu"+x+"e",   VFLTp);
         AddBranch("mu"+x+"charge",VFLTp);
         AddBranch("mu"+x+"d0sig",VFLTp);
         AddBranch("mu"+x+"z0sig",VFLTp);
         AddBranch("mu"+x+"isotool_pass_loosetrackonly", VINTp);
         AddBranch("mu"+x+"isCB", VINTp);
         AddBranch("mu"+x+"isBad", VINTp);
         AddBranch("mu"+x+"isBadHighPt", VINTp);
         AddBranch("mu"+x+"isHighPt", VINTp);
         AddBranch("mu"+x+"quality", VINTp);

         AddBranch("mu"+x+"author", VINTp);
         // AddBranch("mu"+x+"nUnspoiledCscHits", VINTp);
         AddBranch("mu"+x+"momentumBalanceSignificance", VFLTp);
         AddBranch("mu"+x+"scatteringCurvatureSignificance", VFLTp);
         AddBranch("mu"+x+"scatteringNeighbourSignificance", VFLTp);
         AddBranch("mu"+x+"segmentDeltaEta", VFLTp);
         AddBranch("mu"+x+"segmentDeltaPhi", VFLTp);
         AddBranch("mu"+x+"segmentChi2OverDoF", VFLTp);
         AddBranch("mu"+x+"FSR_CandidateEnergy", VFLTp);
         AddBranch("mu"+x+"EnergyLoss", VFLTp);
         AddBranch("mu"+x+"EnergyLossSigma", VFLTp);
         AddBranch("mu"+x+"ParamEnergyLoss", VFLTp);
         AddBranch("mu"+x+"ParamEnergyLossSigmaPlus", VFLTp);
         AddBranch("mu"+x+"ParamEnergyLossSigmaMinus", VFLTp);
         AddBranch("mu"+x+"MeasEnergyLoss", VFLTp);
         AddBranch("mu"+x+"MeasEnergyLossSigma", VFLTp);
         AddBranch("mu"+x+"msInnerMatchChi2", VFLTp);
         AddBranch("mu"+x+"msInnerMatchDOF", VFLTp);
         AddBranch("mu"+x+"energyLossType", VINTp);
         AddBranch("mu"+x+"finalFitPt", VFLTp);
         AddBranch("mu"+x+"finalFitPtErr", VFLTp);
         AddBranch("mu"+x+"finalFitEta", VFLTp);
         AddBranch("mu"+x+"finalFitPhi", VFLTp);
         AddBranch("mu"+x+"finalFitQOverP", VFLTp);
         AddBranch("mu"+x+"finalFitQOverPErr", VFLTp);
         AddBranch("mu"+x+"id_pt", VFLTp);
         AddBranch("mu"+x+"id_ptErr", VFLTp);
         AddBranch("mu"+x+"id_eta", VFLTp);
         AddBranch("mu"+x+"id_phi", VFLTp);
         AddBranch("mu"+x+"id_qop", VFLTp);
         AddBranch("mu"+x+"id_qoperr", VFLTp);
         AddBranch("mu"+x+"ms_pt", VFLTp);
         AddBranch("mu"+x+"ms_ptErr", VFLTp);
         AddBranch("mu"+x+"ms_eta", VFLTp);
         AddBranch("mu"+x+"ms_phi", VFLTp);
         AddBranch("mu"+x+"ms_qop", VFLTp);
         AddBranch("mu"+x+"ms_qoperr", VFLTp);
         AddBranch("mu"+x+"me_pt", VFLTp);
         AddBranch("mu"+x+"me_ptErr", VFLTp);
         AddBranch("mu"+x+"me_eta", VFLTp);
         AddBranch("mu"+x+"me_phi", VFLTp);
         AddBranch("mu"+x+"me_qop", VFLTp);
         AddBranch("mu"+x+"me_qoperr", VFLTp);

         AddBranch("mu"+x+"truthEta",VFLTp);
         AddBranch("mu"+x+"truthPhi",VFLTp);
         AddBranch("mu"+x+"truthPt",VFLTp);
         AddBranch("mu"+x+"truthTheta",VFLTp);
         AddBranch("mu"+x+"truthQOverP",VFLTp);
      
         AddBranch("jet"+x+"pt", VFLTp);
         // AddBranch("jet"+x+"timing", VFLTp);
         AddBranch("jet"+x+"eta", VFLTp);
         AddBranch("jet"+x+"phi", VFLTp);
         AddBranch("jet"+x+"m", VFLTp);
         AddBranch("jet"+x+"fch", VFLTp);
         AddBranch("jet"+x+"fmax", VFLTp);
         // AddBranch("jet"+x+"emfrac", VFLTp);
         // AddBranch("jet"+x+"jvt", VFLTp);
         AddBranch("jet"+x+"cleaning", VINTp);
         AddBranch("jet"+x+"isbjet", VINTp);
         AddBranch("jet"+x+"isbjet_loose", VINTp);
         // AddBranch("jet"+x+"MV2c20_discriminant", VFLTp);
         // AddBranch("jet"+x+"MV2c10_discriminant", VFLTp);
      
         AddBranch("ph"+x+"pt",  VFLTp);
         AddBranch("ph"+x+"eta", VFLTp);
         AddBranch("ph"+x+"phi", VFLTp);
         AddBranch("ph"+x+"m",   VFLTp);
      }

      for(unsigned int i=0 ; i<metcollections.size() ; ++i)
      {
         TString colname = metcollections[i];
         AddBranch("met_"+colname+"_et",   FLTt);
         AddBranch("met_"+colname+"_etx",  FLTt);
         AddBranch("met_"+colname+"_ety",  FLTt);
         AddBranch("met_"+colname+"_sumet",FLTt);
         AddBranch("met_"+colname+"_phi",  FLTt);
      }

      for(unsigned int i=0 ; i<hlttriggers.size() ; ++i)
	  {
         TString trig = hlttriggers[i];
         AddBranch("trigger_HLT_"+trig, INTt);
      }

      return EL::StatusCode::SUCCESS;
   }
}