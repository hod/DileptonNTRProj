// Dear emacs, this is -*- c++ -*-
#ifndef DILEPTONNTREADER_CHECK_H
#define DILEPTONNTREADER_CHECK_H

// Silence warnings about unused functions:
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"

// Status code include(s):
#include "EventLoop/StatusCode.h"
#include "AsgTools/StatusCode.h"
#include "PATInterfaces/CorrectionCode.h"

/// Helper macro checking return values in EL::Algorithm-s
///
/// Usage: CHECK( "initialize", somePossiblyFailingFunction() );
///
#define CHECK( LOC, EXP )                                      \
   do {                                                         \
      const auto sc = EXP;                                      \
      if( isFailure( sc ) ) {                                   \
         Error( #LOC, "Failed to execute\"%s\"", #EXP );        \
         return EL::StatusCode::FAILURE;                        \
      }                                                         \
   } while( 0 )

namespace {

   /// Function checking if an EL::StatusCode is in failure mode
   bool isFailure( const EL::StatusCode& sc ) {
      return ( sc == EL::StatusCode::FAILURE );
   }

   /// Function checking if a StatusCode is in failure mode
   bool isFailure( const StatusCode& sc ) {
      return sc.isFailure();
   }

} // private namespace

// Undo the GCC modifications:
#pragma GCC diagnostic pop

#endif // DILEPTONNTREADER_CHECK_H
