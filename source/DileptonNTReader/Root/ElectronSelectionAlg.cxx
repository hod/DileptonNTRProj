// System include(s):
#include <memory>
#include <cmath>

// Tool include(s):
#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"

// Local include(s):
#include "DileptonNTReader/ElectronSelectionAlg.h"
#include "check.h"

// Declare the class to ROOT:
ClassImp( DNR::ElectronSelectionAlg )

namespace DNR {

   ElectronSelectionAlg::ElectronSelectionAlg() : EL::Algorithm() {}

   EL::StatusCode ElectronSelectionAlg::setupJob( EL::Job& job )
   {
      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode ElectronSelectionAlg::initialize()
   {
      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode ElectronSelectionAlg::execute()
   {
      return EL::StatusCode::SUCCESS;
   }

} // namespace DNR
