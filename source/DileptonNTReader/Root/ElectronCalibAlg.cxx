// System include(s):
#include <memory>

// Tool include(s):
#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"

// Local include(s):
#include "DileptonNTReader/ElectronCalibAlg.h"
#include "check.h"

// Declare the class to ROOT:
ClassImp( DNR::ElectronCalibAlg )

namespace DNR {

   ElectronCalibAlg::ElectronCalibAlg() : EL::Algorithm() {}

   EL::StatusCode ElectronCalibAlg::setupJob( EL::Job& job )
   {
      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode ElectronCalibAlg::initialize()
   {
      return EL::StatusCode::SUCCESS;
   }

   EL::StatusCode ElectronCalibAlg::execute()
   {
      return EL::StatusCode::SUCCESS;
   }

} // namespace DNR
