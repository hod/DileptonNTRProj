// Dear emacs, this is -*- c++ -*-
#ifndef DILEPTONNTREADER_DILEPTONNTREADER_H
#define DILEPTONNTREADER_DILEPTONNTREADER_H

// Tool include(s):
#include "EventLoop/Algorithm.h"
#include "AsgTools/AnaToolHandle.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"

#include <TLorentzVector.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TH2.h>

using namespace std;

// Forward declaration(s):
class TH1;

/// Namespace holding all of the DileptonNTReader code
namespace DNR {
	
   typedef map<TString,TString >                 TMapTSTS;
   typedef map<TString,ULong64_t >               TMapTSUL64;
   typedef map<TString,Int_t >                   TMapTSIt;
   typedef map<TString,Float_t >                 TMapTSFt;
   typedef map<TString,int >                     TMapTSi;
   typedef map<TString,bool >                    TMapTSb;
   typedef map<TString,float >                   TMapTSf;
   typedef map<TString,TLorentzVector >          TMapTStlv;
   typedef map<TString,vector<TString> >         TMapTSvTS;
   typedef map<TString,vector<int> >             TMapTSvi;
   typedef map<TString,vector<int>* >            TMapTSP2vi;
   typedef map<TString,vector<unsigned int> >    TMapTSvui;
   typedef map<TString,vector<unsigned int>* >   TMapTSP2vui;
   typedef map<TString,vector<float> >           TMapTSvf;
   typedef map<TString,vector<float>* >          TMapTSP2vf;
   typedef map<TString,vector<bool> >            TMapTSvb;
   typedef map<TString,vector<bool>* >           TMapTSP2vb;
   typedef map<TString,vector<TLorentzVector> >  TMapTSvtlv;
   typedef map<TString,vector<TLorentzVector>* > TMapTSP2vtlv;
   typedef map<TString,TH1* >                    TMapTSP2TH1;
   typedef map<TString,TH2* >                    TMapTSP2TH2;
   typedef map<TString,TMapTSi >                 T2MapTSi;
   typedef map<TString,TMapTSf >                 T2MapTSf;
   typedef map<TString,TMapTSb >                 T2MapTSb;
   typedef map<TString,TMapTSP2TH1 >             T2MapTSP2TH1;
   typedef map<TString,TMapTSP2TH2 >             T2MapTSP2TH2;
   typedef map<TString,T2MapTSi >                T3MapTSi;

   struct Electrons
   {
         vector<float>* SF;
         vector<float>* SFiso;
         vector<float>* cl_etaBE2;
         vector<float>* pt;
         vector<float>* eta;
         vector<float>* phi;
         vector<float>* m;
         vector<float>* e;
         vector<float>* charge;
         vector<int>*   isGoodOQ;
         vector<float>* d0sig;
         vector<float>* z0sig;
         vector<int>*   passLHID;
         vector<int>*   passChID;
         vector<int>*   isotool_pass_loose;
   };
   struct Muons
   {	
         vector<float>* SF;
         vector<float>* SFiso;
         vector<float>* pt;
         vector<float>* ptErr;
         vector<float>* eta;
         vector<float>* phi;
         vector<float>* m;
         vector<float>* e;
         vector<float>* charge;
         vector<int>*   isCB;
         vector<int>*   isBad;
         vector<int>*   isBadHighPt;
         vector<float>* d0sig;
         vector<float>* z0sig;
         vector<int>*   isHighPt;
         vector<int>*   isotool_pass_loosetrackonly;
   };
   struct Photons
   {	
         vector<float>* pt;
         vector<float>* eta;
         vector<float>* phi;
         vector<float>* m;
   };
   struct Jets
   {	
         vector<float>* pt;
         vector<float>* eta;
         vector<float>* phi;
         vector<float>* m;
         vector<float>* fch;
         vector<float>* fmax;
         vector<int>*   cleaning;
   };
   struct Bjets
   {	
         vector<float>* pt;
         vector<float>* eta;
         vector<float>* phi;
         vector<float>* m;
         vector<int>*   cleaning;
         vector<int>*   isbjet;
         vector<int>*   isbjet_loose;
   };
   struct Mets
   {	
      TMapTSf et;
      TMapTSf etx;
      TMapTSf ety;
      TMapTSf sumet;
      TMapTSf phi;
   };
   struct Triggers
   {	
      TMapTSi pass;
   };

   typedef map<TString,Electrons > TMapTSElectrons;
   typedef map<TString,Muons >     TMapTSMuons;
   typedef map<TString,Photons >   TMapTSPhotons;
   typedef map<TString,Jets >      TMapTSJets;
   typedef map<TString,Bjets >     TMapTSBjets;
 
   enum Types
   {	
      INT, FLT, TLV,
      VINT, VUINT, VFLT, VTLV,
      VINTp, VUINTp, VFLTp, VTLVp, // pointers
      UL64t, INTt, FLTt,           // ROOT builtin's
   };


   class DileptonNTReader : public EL::Algorithm {

   public:
      /// Default algorithm constructor
      DileptonNTReader();

      /// @name Functions inherited from EL::Algorithm
      /// @{

      /// Function helping to set up the job
      EL::StatusCode setupJob( EL::Job& job );
    
      EL::StatusCode changeInput( bool firstFile );

      /// Function run at the start of the job, to initialise the histograms
      EL::StatusCode histInitialize();

      /// Function initialising the algorithm
      EL::StatusCode initialize();

      /// Function executing the algorithm
      EL::StatusCode execute();

      /// more virtual methods
      //EL::StatusCode fileExecute();
      //EL::StatusCode postExecute();
      EL::StatusCode finalize();
      // EL::StatusCode histFinalize();


      //// in here:
      EL::StatusCode SetMets();
      EL::StatusCode SetTriggers();
      EL::StatusCode ResetVars(TString sel);
      EL::StatusCode ApplySelection(TString sel);
      EL::StatusCode InitSelection(TString sel);
      //// in InputTree.cxx:
      EL::StatusCode AddBranch(TString name, int type);
      EL::StatusCode SetInputBranches();
      EL::StatusCode DecalreInputBranches();
      //// in Histograms.cxx:
      EL::StatusCode SetLogBins(Int_t nbins, Double_t min, Double_t max, Double_t* xpoints);
      EL::StatusCode SetSqrtBins(Int_t nbins, Double_t min, Double_t max, Double_t* xpoints);
      EL::StatusCode GetInfoFromGridHist();
      EL::StatusCode BookHistograms();
      EL::StatusCode FillCutFlow(TString sel, TString chn, float weight=1.);
      EL::StatusCode GridCutFlow(TString sel, TString chn);
      EL::StatusCode FillHistograms(TString sel, float weight=1.);
      //// in Kinematics.cxx:
      EL::StatusCode PoM(TString sel, TString channel);
      EL::StatusCode MT2(TString sel, TString channel);
      EL::StatusCode LTHTST(TString sel, TString channel);
      EL::StatusCode MEFF(TString sel, TString channel);
      EL::StatusCode CalculateVars(TString sel);
      //// in OutputTree.cxx:
      EL::StatusCode SetOutBranch(TString name, int enumtype);
      EL::StatusCode ClearOutBranches();
      EL::StatusCode SetOutBranches();
      EL::StatusCode FillOutTree();
      //// in InfoDumper.cxx
      EL::StatusCode DumpMuons(TString sel, TString chn, bool sigonly=true);
      EL::StatusCode DumpElectrons(TString sel, TString chn, bool sigonly=true);
      EL::StatusCode DumpInfo(TString sel);
      

      /// @}

      /// initialised in the steering 
      /// Have to be public!!!
      std::string outputName; // output tree file name
      TString nominalmet;     // ...
      TString gridhistpath;   // ...
      TString otree_sel;   // ...

      /// for the output tree
      TTree*     otree; //!
      TMapTSUL64 out_uls; //!
      TMapTSi    out_ints; //!
      TMapTSf    out_floats; //!
      TMapTStlv  out_tlvs; //!
      TMapTSvi   out_vints; //!
      TMapTSvf   out_vfloats; //!
      TMapTSvtlv out_vtlvs; //!

   private:
      /// configurations
      vector<TString> selections; //!
      vector<TString> leptonchannels; //!
      vector<TString> channels; //!
      TMapTSvTS       cuts;    //!
      TString last_e_cut; //!
      TString last_u_cut; //!
      vector<TString> metcollections; //!
      vector<TString> hlttriggers; //!

      /// for the selection
      TMapTSvb good_el; //!
      TMapTSvb good_mu; //!
      TMapTSvb good_ph; //!
      TMapTSvb good_jet; //!
      TMapTSvb good_bjet; //!
      TMapTSvtlv good_elp; //!
      TMapTSvtlv good_mup; //!
      TMapTSvtlv good_php; //!
      TMapTSvtlv good_jetp; //!
      TMapTSvtlv good_bjetp; //!
      TMapTSi ngoodel; //!
      TMapTSi ngoodmu; //!
      TMapTSi ngoodph; //!
      TMapTSi ngoodjet; //!
      TMapTSi ngoodbjet; //!
      TMapTSi el1; //!
      TMapTSi el2; //!
      TMapTSi mu1; //!
      TMapTSi mu2; //!

      // informaton from grid processing
      vector<TString> gridCF; //!
      TMapTSf gridCFraw; //!
	  TMapTSf gridCFwgt; //!
      TMapTSf gridEC; //!

      // the variables
      T2MapTSf fvars; //!
      T2MapTSb flags; //!
      T2MapTSi counters; //!
      T3MapTSi cutflow; //!

      /// the "objects"
      TMapTSElectrons els; //! // structures to hold the electrons
      TMapTSMuons     mus; //! // structures to hold the muons
      TMapTSPhotons   phs; //! // structures to hold the photons
      TMapTSJets      jets; //! // structures to hold the jets
      TMapTSBjets     bjets; //! // structures to hold the bjets
      Mets            mets; //! // structures to hold the bjets
      Triggers        trigs; //! // structures to hold the triggers

      /// histograms
      T2MapTSP2TH1 histos1;
      T2MapTSP2TH2 histos2;

      /// Branches
      TMapTSUL64  in_ul;
      TMapTSIt    in_i;
      TMapTSFt    in_f;
      TMapTSP2vi  in_vi;
      TMapTSP2vf  in_vf;
      TMapTSP2vui in_vui;
      
      // other variables
      int m_eventCounter; //!
      float MeV2GeV;
      float GeV2MeV;

      // Declare the class to ROOT:
      ClassDef( DNR::DileptonNTReader, 1 )

   }; // class DileptonNTReader

} // namespace DNR

#endif // DILEPTONNTREADER_DILEPTONNTREADER_H
