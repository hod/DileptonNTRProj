// Dear emacs, this is -*- c++ -*-
#ifndef DILEPTONNTREADER_ELECTRONCALIBALG_H
#define DILEPTONNTREADER_ELECTRONCALIBALG_H

// Tool include(s):
#include "EventLoop/Algorithm.h"
#include "AsgTools/AnaToolHandle.h"

namespace DNR {

   /// Algorithm calibrating electrons for the analysis
   ///
   /// This is a simple algorithm creating a shallow copy of the electrons
   /// coming from the input file, and calibrating them, and putting them
   /// into the transient store.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class ElectronCalibAlg : public EL::Algorithm {

   public:
      /// Default algorithm constructor
      ElectronCalibAlg();

      /// @name Functions inherited from EL::Algorithm
      /// @{

      /// Function helping to set up the job
      EL::StatusCode setupJob( EL::Job& job );

      /// Function initialising the algorithm
      EL::StatusCode initialize();

      /// Function executing the algorithm
      EL::StatusCode execute();

      /// @}

   private:
      // Declare the class to ROOT:
      ClassDef( DNR::ElectronCalibAlg, 1 )

   }; // class ElectronCalibAlg

} // namespace DNR

#endif // DILEPTONNTREADER_ELECTRONCALIBALG_H
