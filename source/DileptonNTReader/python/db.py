#!/usr/bin/env python
import ROOT
from ROOT import *
import bins

bkgtag  = "BKG08092017v1"
sigtag  = "SIG08092017v1"
tsttag  = "TST04102017v1"
suffix  = "_minitrees.root"
hsuffix = "_hist"
basedir = "/eos/user/h/hod/NTUP/mc16_13TeV/test/"
rundir  = "/eos/user/h/hod/TREE/mc16_13TeV/test/"
plotdir = "/eos/user/h/hod/TREE/mc16_13TeV/plots/"

###############

subdirs = [ "PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee",
            "PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu",
            
            "Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO",

            "Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi",
            "Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi",

            "PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee",
            "PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu",
]


sliced = { "301000-301018":"PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee",
           "301021-301038":"PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu",
}


###############
bkgs = [ "301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180",
         "301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250",
         "301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400",
         "301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600",
         "301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800",
         "301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000",
         "301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250",
         "301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500",
         "301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750",
         "301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000",
         "301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250",
         "301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500",
         "301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750",
         "301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000",
         "301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500",
         "301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000",
         "301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500",
         "301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000",
         "301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M",
         
         "301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250",
         "301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400",
         "301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800",
         "301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000",
         "301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250",
         "301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500",
         "301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750",
         "301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000",
         "301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250",
         "301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500",
         "301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750",
         "301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000",
         "301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500",
         "301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000",
         "301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500",
         "301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000",
         "301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M",
         
         "410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO",
         
         #"361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee",
         #"361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu",
]

###############

sigs = [ "301215.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi2000",
         "301216.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000",
         "301217.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi4000",
         "301218.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi5000",

         "301220.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi2000", 
         "301221.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi3000", 
         "301222.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi4000", 
         "301223.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi5000", 
]

tsts = [ 
         "301220.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi2000",
         "301215.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi2000",
]

###############

mets     = [ #"eleterm", "jetterm", "muonterm", "phterm", "muonterm_cst", "muonterm_tst", "softerm_cst", "softerm_tst",
            "noelectron_cst", "noelectron_tst", "nomuon_cst", "nomuon_tst", "nophoton_cst", "nophoton_tst", "wmuon_cst", "wmuon_tst",
            "nophcalib_nomuon_cst", "nophcalib_nomuon_tst", "nophcalib_wmuon_cst", "nophcalib_wmuon_tst",
            "track", "truth",
]

###############

channels = ["ee","uu","eu",
            "ee0b","uu0b","eu0b",
            "ee1b","uu1b","eu1b",
            "ee2b","uu2b","eu2b",
            "eemet","uumet","eumet",
]

variables = [
   "cutflow_raw",
   "cutflow_wgt",
   "m",
   "mt2",
   "meff",
   "metOmll",
   "pllOmll",
   "ptllOmll",
   "lt",
   "ht",
   "st",
   "htmlt",
   "pt_l1",
   "n_el",
   "n_mu",
   "n_ph",
   "n_jet",
   "n_bjet",
]
for met in mets:
   variables.append("met_"+met+"_et")

##############

nbins_mee, bins_mee = bins.GetSearchBinning("ee",6000.,1.)
nbins_muu, bins_muu = bins.GetSearchBinning("uu",6000.,1.)
nbins_mee_6TeV, bins_mee_6TeV = bins.GetSearchBinning("ee",6000.,3.)
nbins_muu_6TeV, bins_muu_6TeV = bins.GetSearchBinning("uu",6000.,3.)
nbins_mee_10TeV, bins_mee_10TeV = bins.GetSearchBinning("ee",10000.,3.)
nbins_muu_10TeV, bins_muu_10TeV = bins.GetSearchBinning("uu",10000.,5.)
histograms = {}
histograms.update({"m_eebins_6TeV"  : {"dim":1, "titles":";m_{clabel} [GeV] (in ee binnig);Events",        "nbinsx":nbins_mee_6TeV,  "xbins":bins_mee_6TeV,  "logx":1, "logy":1} })
histograms.update({"m_uubins_6TeV"  : {"dim":1, "titles":";m_{clabel} [GeV] (in #mu#mu binning);Events",   "nbinsx":nbins_muu_6TeV,  "xbins":bins_muu_6TeV,  "logx":1, "logy":1} })
histograms.update({"m_eebins_10TeV" : {"dim":1, "titles":";m_{clabel} [GeV] (in ee binnig);Events",        "nbinsx":nbins_mee_10TeV, "xbins":bins_mee_10TeV, "logx":1, "logy":1} })
histograms.update({"m_uubins_10TeV" : {"dim":1, "titles":";m_{clabel} [GeV] (in #mu#mu binning);Events",   "nbinsx":nbins_muu_10TeV, "xbins":bins_muu_10TeV, "logx":1, "logy":1} })
histograms.update({"m_eebins"       : {"dim":1, "titles":";m_{clabel} [GeV] (in ee binnig);Events",        "nbinsx":nbins_mee,       "xbins":bins_mee,       "logx":1, "logy":1} })
histograms.update({"m_uubins"       : {"dim":1, "titles":";m_{clabel} [GeV] (in #mu#mu binning);Events",   "nbinsx":nbins_muu,       "xbins":bins_muu,       "logx":1, "logy":1} })
histograms.update({"m"              : {"dim":1, "titles":";m_{clabel} [GeV] (in arbitrary binning);Events","nbinsx":350, "xmin":80, "xmax":7080, "logx":0, "logy":1} })
histograms.update({"pt1"            : {"dim":1, "titles":";p_{T} leading [GeV];Events",                    "nbinsx":300, "xmin":0,  "xmax":3000, "logx":0, "logy":1} })
histograms.update({"pt2"            : {"dim":1, "titles":";p_{T} subleading [GeV];Events",                 "nbinsx":300, "xmin":0,  "xmax":3000, "logx":0, "logy":1} })
histograms.update({"mt2"            : {"dim":1, "titles":";m_{T2} [GeV];Events",                           "nbinsx":250, "xmin":0,  "xmax":500,  "logx":0, "logy":1} })
histograms.update({"met"            : {"dim":1, "titles":";E_{T}^{miss} [GeV];Events",                     "nbinsx":150, "xmin":0,  "xmax":3000, "logx":0, "logy":1} })
histograms.update({"dRmin1Jet"      : {"dim":1, "titles":";#DeltaR_{min}(jet,l_{1});Events",               "nbinsx":120, "xmin":0,  "xmax":6,    "logx":0, "logy":0} })
histograms.update({"dRmin2Jet"      : {"dim":1, "titles":";#DeltaR_{min}(jet,l_{2});Events",               "nbinsx":120, "xmin":0,  "xmax":6,    "logx":0, "logy":0} })
histograms.update({"dRmin1Bjet"     : {"dim":1, "titles":";#DeltaR_{min}(bjet,l_{1});Events",              "nbinsx":120, "xmin":0,  "xmax":6,    "logx":0, "logy":0} })
histograms.update({"dRmin2Bjet"     : {"dim":1, "titles":";#DeltaR_{min}(bjet,l_{2});Events",              "nbinsx":120, "xmin":0,  "xmax":6,    "logx":0, "logy":0} })

histograms.update({"dRmin2Jet_vs_dRmin1Jet" : {"dim":2, "titles":";#DeltaR_{min}(jet,l2);#DeltaR_{min}(jet,l1);Events",             "nbinsx":120,"xmin":0,"xmax":6, "nbinsy":120,"ymin":0,"ymax":6,   "logx":0,"logy":0,"logz":0} })
histograms.update({"dPtrel_vs_dRmin1Jet"    : {"dim":2, "titles":";#DeltaR_{min}(jet,l1);#Deltap_{T}(jet,l1)/p_{T}(l1) [%];Events", "nbinsx":100,"xmin":0,"xmax":6, "nbinsy":100,"ymin":0,"ymax":500, "logx":0,"logy":0,"logz":0} })
histograms.update({"dPtrel_vs_dRmin2Jet"    : {"dim":2, "titles":";#DeltaR_{min}(jet,l2);#Deltap_{T}(jet,l2)/p_{T}(l2) [%];Events", "nbinsx":100,"xmin":0,"xmax":6, "nbinsy":100,"ymin":0,"ymax":500, "logx":0,"logy":0,"logz":0} })
histograms.update({"JetPt_vs_dRmin1Jet"     : {"dim":2, "titles":";#DeltaR_{min}(jet,l1);p_{T}(jet) [GeV];Events",                  "nbinsx":100,"xmin":0,"xmax":6, "nbinsy":100,"ymin":0,"ymax":200, "logx":0,"logy":0,"logz":0} })
histograms.update({"JetPt_vs_dRmin2Jet"     : {"dim":2, "titles":";#DeltaR_{min}(jet,l2);p_{T}(jet) [GeV];Events",                  "nbinsx":100,"xmin":0,"xmax":6, "nbinsy":100,"ymin":0,"ymax":200, "logx":0,"logy":0,"logz":0} })

############

stackorder_DYee = {"ee":4, "uu":4, "eu":4,   "ee0b":4, "uu0b":4, "eu0b":4,   "ee1b":4, "uu1b":4, "eu1b":4,  "ee2b":4, "uu2b":4, "eu2b":4,  "eemet":4, "uumet":4, "eumet":4,}
stackorder_DYuu = {"ee":5, "uu":5, "eu":3,   "ee0b":5, "uu0b":5, "eu0b":3,   "ee1b":3, "uu1b":3, "eu1b":3,  "ee2b":3, "uu2b":3, "eu2b":3,  "eemet":3, "uumet":3, "eumet":3,}
stackorder_TT   = {"ee":3, "uu":3, "eu":5,   "ee0b":3, "uu0b":3, "eu0b":5,   "ee1b":5, "uu1b":5, "eu1b":5,  "ee2b":5, "uu2b":5, "eu2b":5,  "eemet":5, "uumet":5, "eumet":5,}
stackorder_DB   = {"ee":2, "uu":2, "eu":2,   "ee0b":2, "uu0b":2, "eu0b":2,   "ee1b":2, "uu1b":2, "eu1b":2,  "ee2b":2, "uu2b":2, "eu2b":2,  "eemet":2, "uumet":2, "eumet":2,}
stackorder_MJ   = {"ee":1, "uu":1, "eu":1,   "ee0b":1, "uu0b":1, "eu0b":1,   "ee1b":1, "uu1b":1, "eu1b":1,  "ee2b":1, "uu2b":1, "eu2b":1,  "eemet":1, "uumet":1, "eumet":1,}

hattr = {}

hattr.update({"Data":                                                    {"col":ROOT.kBlack,    "label":"Data",               "drawopt":"", "marker":20, "msize":0.8}})

hattr.update({"PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee":                   {"stackorder":stackorder_DYee, "skipstack":0, "col":ROOT.kAzure-9,  "label":"Drell-Yan (ee)",     "drawopt":"Fhist",}})
hattr.update({"PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu":                 {"stackorder":stackorder_DYuu, "skipstack":0, "col":ROOT.kAzure-9,  "label":"Drell-Yan (#mu#mu)", "drawopt":"Fhist",}})
hattr.update({"410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO":   {"stackorder":stackorder_TT  , "skipstack":0, "col":ROOT.kRed+1,    "label":"Top quark",          "drawopt":"Fhist",}})
hattr.update({"Diboson_sample_name":                                     {"stackorder":stackorder_DB  , "skipstack":1, "col":ROOT.kOrange,   "label":"Diboson",            "drawopt":"Fhist",}})
hattr.update({"Multijet_sample_name":                                    {"stackorder":stackorder_MJ  , "skipstack":1, "col":ROOT.kYellow-9, "label":"Multijet",           "drawopt":"Fhist",}})

hattr.update({"301215.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi2000"  : {"order":1, "skipsig":0, "col":2,             "linestyle":1, "label":"2 TeV Z'#rightarrow ee",            "drawopt":"hist",}})
hattr.update({"301216.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000"  : {"order":3, "skipsig":0, "col":ROOT.kGreen+2, "linestyle":1, "label":"3 TeV Z'#rightarrow ee",            "drawopt":"hist",}})
hattr.update({"301217.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi4000"  : {"order":5, "skipsig":0, "col":4,             "linestyle":1, "label":"4 TeV Z'#rightarrow ee",            "drawopt":"hist",}})
hattr.update({"301218.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi5000"  : {"order":7, "skipsig":0, "col":6,             "linestyle":1, "label":"5 TeV Z'#rightarrow ee",            "drawopt":"hist",}})
hattr.update({"301220.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi2000": {"order":2, "skipsig":0, "col":2,             "linestyle":2, "label":"2 TeV Z'#rightarrow #mu#mu",            "drawopt":"hist",}})
hattr.update({"301221.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi3000": {"order":4, "skipsig":0, "col":ROOT.kGreen+2, "linestyle":2, "label":"3 TeV Z'#rightarrow #mu#mu",            "drawopt":"hist",}})
hattr.update({"301222.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi4000": {"order":6, "skipsig":0, "col":4,             "linestyle":2, "label":"4 TeV Z'#rightarrow #mu#mu",            "drawopt":"hist",}})
hattr.update({"301223.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi5000": {"order":8, "skipsig":0, "col":6,             "linestyle":2, "label":"5 TeV Z'#rightarrow #mu#mu",            "drawopt":"hist",}})


#############