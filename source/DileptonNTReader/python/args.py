#!/usr/bin/env python
import os, sys
import argparse
parser = argparse.ArgumentParser(description='run NTR...')
parser.add_argument('-isdry', metavar='dry run',  required=True,  help='is dry run?: [-isdry 0/1]')
parser.add_argument('-ds',    metavar='datasets', required=True,  help='datasets: [-ds s/b/sb/t]')
parser.add_argument('-i',     metavar='DSID',     required=False, help='the DSID: [-i 301000]')
argus  = parser.parse_args()
isdry  = False if(argus.isdry=="0") else True
ds     = argus.ds
ID     = ""
if(argus.isdry!="0" and argus.isdry!="1"):
  print "isdry option is invalid: "+isdry
  print "quitting"
  quit()
if(argus.ds!="s" and argus.ds!="b" and argus.ds!="sb" and argus.ds!="t"):
  print "ds option is invalid: "+ds
  print "quitting"
  quit()
else:
   if(argus.ds=="t"): print "running tests only!"
   if(argus.ds=="s"): print "running singnals only!"
   if(argus.ds=="b"): print "running bakcgrouns only!"
   if(argus.ds=="sb"): print "running singnals and bakcgrouns!"
if(argus.i is None):
   print "running without specific DSID"
elif(len(argus.i)!=6):
   print "DSID has wrong length="+len(argus.i)+": DSID="+argus.i
else:
   ID = argus.i
   print "running with specific DSID="+ID
