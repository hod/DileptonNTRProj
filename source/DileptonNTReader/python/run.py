#!/usr/bin/env python
import db
import os, sys
import subprocess
from subprocess import call
import args

samples = []
if(args.ds=="b"):  samples = db.bkgs
if(args.ds=="s"):  samples = db.sigs
if(args.ds=="sb"): samples = db.bkgs+db.sigs
if(args.ds=="t"):  samples = db.tsts

def testoutput(err,out,pattern="Number of processed events ="):
   emptyerrfile = (os.stat(err).st_size==0)
   goodoutfile  = (pattern in open(out).read())
   if( not emptyerrfile and not goodoutfile ):
      print "******************************"
      print "*********   ERROR!   *********"
      print "********* check logs *********"
      print "******************************"
   else: print "Success :)"


def counthistfiles(directory):
   path, dirs, files = os.walk(directory).next()
   file_count = len(files)
   return file_count


for sample in samples:
   if(args.ID!="" and args.ID not in sample): continue
   targetdir = db.rundir+sample if(args.ds!="t") else (db.rundir+sample).replace("TREE","TSTS")
   if(not args.isdry):
      p = subprocess.Popen("rm -rf "+targetdir, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
   for subdir in db.subdirs:
      if(subdir not in sample): continue
      tag = ""
      if(sample in db.bkgs): tag = db.bkgtag
      if(sample in db.sigs): tag = db.sigtag
      samplepath = db.basedir+tag+"/"+subdir+"/user.hod."+tag+".mc16_13TeV."+sample
      treespath  = samplepath+db.suffix
      histspath  = samplepath+db.hsuffix
      dataset    = samplepath+db.suffix
      histset    = samplepath+db.hsuffix
      executable = "build/"+os.environ.get('CMTCONFIG')+"/bin/ATestRun"
      hadd       = "hadd -f  "+targetdir+"/hist-grid.root  "+histset+"/*.root"
      copy       = "/bin/cp -f  "+histset+"/*.root  "+targetdir+"/hist-grid.root"
      nhists     = counthistfiles(histset)
      submit     = executable+"  "+dataset+"  "+targetdir
      if(nhists>1):  submit = hadd+"  ;  "+submit
      if(nhists==1): submit = copy+"  ;  "+submit
      if(nhists==0): submit = "!!!ERROR!!!"
      if(args.isdry): submit = "echo "+submit
      print "Submitting: "+submit
      p = subprocess.Popen(submit, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      f = open(targetdir+"/log.out", 'w')
      f.write(out)
      f.close()
      f = open(targetdir+"/log.err", 'w')
      f.write(err)
      f.close()
      testoutput(targetdir+"/log.err", targetdir+"/log.out")
