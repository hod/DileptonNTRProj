#!/usr/bin/python
import ROOT
from ROOT import *
import os
import db
import args

samples = []
if(args.ds=="b"):  samples = db.bkgs
if(args.ds=="s"):  samples = db.sigs
if(args.ds=="sb"): samples = db.bkgs+db.sigs

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0);

smooth = True
ymin   = 1.0e-3
ymax   = 2.5e+4
lumiifb = 36.

rootname = db.plotdir+"histos.all."+args.ds+".root"
froot = TFile(rootname,"READ")
print "Reading: ",froot.GetName()

def GetStackOrder(channel):
   stackorder = {}
   for sample,attr in db.hattr.iteritems():
      if(sample in db.sigs): continue
      if(sample=="Data"):    continue
      if("stackorder" not in attr.keys()): continue
      if("skipstack"  not in attr.keys()): continue
      order = int(attr["stackorder"][channel])
      if(attr["skipstack"]): continue ## can ignore some samples
      if(order in stackorder): continue
      stackorder.update({order:sample})
   return stackorder

def GetSigOrder():
   sigorder = {}
   for sample,attr in db.hattr.iteritems():
      if(sample not in db.sigs): continue
      if("order"    not in attr.keys()): continue
      if("skipsig"  not in attr.keys()): continue
      order = int(attr["order"])
      if(attr["skipsig"]): continue ## can ignore some samples
      if(order in sigorder): continue
      sigorder.update({order:sample})
   return sigorder

# stackvars  = [ "m", "m_eebins", "m_eebins_6TeV", "m_eebins_10TeV", "m_uubins", "m_uubins_6TeV", "m_uubins_10TeV", "pt1", "pt2", "mt2", "met" ]
stackvars  = [ "m_eebins_6TeV", "m_uubins_6TeV", "pt1", "pt2", "mt2", "met" ]

pdfname = db.plotdir+"stacks.all."+args.ds+".pdf"

cnv = TCanvas("cnv","",500,500)
cnv.SaveAs(pdfname+"(")

for channel in db.channels:
   clabel = channel.replace("u","#mu")
   for var in stackvars:
      titles=""

      stack = {}
      for sample,attr in db.hattr.iteritems():
         if(sample in db.sigs): continue
         if(sample=="Data"):    continue
         if(attr["skipstack"]): continue ## can ignore some samples
         if("ee" in channel and "DYmumu" in sample): continue
         if("uu" in channel and "DYee"   in sample): continue
         hname = sample+"_"+channel+"-"+var
         stack.update( {sample:froot.Get(hname)} )
         selectionlabel = var+" in "+channel.replace("u","#mu")+" selection"
         titles = selectionlabel+";"+stack[sample].GetXaxis().GetTitle()+";"+stack[sample].GetYaxis().GetTitle() ## all the same...
     
      signals = {}
      for sample,attr in db.hattr.iteritems():
         if(sample not in db.sigs): continue
         if("ee" in channel and "Zprime_NoInt_mumu" in sample): continue
         if("uu" in channel and "Zprime_NoInt_ee"   in sample): continue
         hname = sample+"_"+channel+"-"+var
         signals.update( {sample:froot.Get(hname)} )
	
      ## e+u DY is a sum of both ee and uu DY histogram
      if(channel=="eu"):
         stack["PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee"].Add(stack["PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu"])

      for name, hist in stack.iteritems():
         if(smooth): hist.Smooth()
         hist.Scale(lumiifb)
         hist.SetMinimum(ymin)
         hist.SetMaximum(ymax)
      for sample, hist in signals.iteritems():
         if(smooth): hist.Smooth()
         hist.Scale(lumiifb)
         hist.SetMinimum(ymin)
         hist.SetMaximum(ymax)

      hs = THStack("hs_"+channel+"_"+var,"")
      hs.SetTitle(titles)
      stackorder = GetStackOrder(channel)
      for order,sample in stackorder.iteritems():
         if("ee" in channel and "DYmumu" in sample): continue
         if("uu" in channel and "DYee"   in sample): continue
         if("eu" in channel and "DYmumu" in sample): continue ## that is just mumu (available in ee for the eu channel)
         hs.Add(stack[sample],"hist")

      leg = TLegend(0.50,0.70,0.85,0.85)#,NULL,"brNDC")
      leg.SetFillStyle(4000) #will be transparent
      leg.SetFillColor(0)
      leg.SetTextFont(42)
      leg.SetBorderSize(0)
      # for order,sample in stackorder.iteritems():
      for order in reversed(sorted(stackorder.keys())):
         sample = stackorder[order]
         label = db.hattr[sample]["label"]
         if("ee" in channel and "DYmumu" in sample): continue
         if("uu" in channel and "DYee"   in sample): continue
         if("eu" in channel and "DYmumu" in sample): continue ## that is just mumu
         if("eu" in channel and "DYee"   in sample): label = "Drell-Yan (ee+#mu#mu)" ## that is the sum of ee and mumu
         leg.AddEntry(stack[sample],label,"f")

      sigorder = GetSigOrder()
      for order,sample in sigorder.iteritems():
         if(sample not in db.sigs): continue
         if("ee" in channel and "Zprime_NoInt_mumu" in sample): continue
         if("uu" in channel and "Zprime_NoInt_ee"   in sample): continue
         label = db.hattr[sample]["label"]
         leg.AddEntry(signals[sample],label,"l")

      hs.SetMinimum(ymin)
      hs.SetMaximum(ymax)
      cnv = TCanvas("cnv_"+channel+"_"+var,"",500,500)
      if(db.histograms[var]["logx"]==1): cnv.SetLogx()
      if(db.histograms[var]["logy"]==1): cnv.SetLogy()
      cnv.SetTicks(1,1)
      hs.Draw("hsit")
      for sample, hist in signals.iteritems():
         drawopt = db.hattr[sample]["drawopt"]+" same"
         hist.Draw(drawopt)
      leg.Draw("same")
      cnv.Update()
      cnv.RedrawAxis()
      # cnv.SaveAs(db.plotdir+"stack_"+channel+"_"+var+".pdf")
      cnv.SaveAs(pdfname)

cnv = TCanvas("cnv","",500,500)
cnv.SaveAs(pdfname+")")