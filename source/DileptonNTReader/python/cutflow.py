#!/usr/bin/env python
import os, sys
import math
import ROOT
from ROOT import *
import rootstyle
from array import *
import numpy as np
import db
import args

## all the samples you need
samples = []
if(args.ds=="b"):  samples = db.bkgs
if(args.ds=="s"):  samples = db.sigs
if(args.ds=="sb"): samples = db.bkgs+db.sigs
if(args.ds=="t"):  samples = db.tsts


## styles
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0);
ROOT.gStyle.SetOptFit(0);


## all iterations
versions = ["_preOR","_pstOR"]

## output file
#txtname = db.plotdir+"cutflow.all."+args.ds+".txt"
txtname = db.plotdir+"cutflow.all."+args.ds+".txt" if(args.ds!="t") else (db.plotdir+"cutflow.all."+args.ds+".txt").replace("TREE","TSTS")
f = open(txtname, 'w')


def getmaxlength(h,sORi):
   maxlength = 0
   for b in range(1,h.GetNbinsX()):
      cut = h.GetXaxis().GetBinLabel(b)
      Npass = h.GetBinContent(b)
      if(cut==""): continue
      length = len(cut) if(sORi=="s") else len(str(Npass))
      maxlength = length if(length>maxlength) else maxlength
   return maxlength


def printcf(h):
   name = h.GetName()
   maxlength_cut  = getmaxlength(h,"s")
   maxlength_Npass = getmaxlength(h,"i")
   line = "-------- %s ---------" % name
   f.write(line+"\n")
   for b in range(1,h.GetNbinsX()+1):
      cut = h.GetXaxis().GetBinLabel(b)
      Npass = h.GetBinContent(b)
      NpassPrev = h.GetBinContent(b-1) if(b>1) else Npass
      #if(cut==""): continue
      eff = (Npass/NpassPrev)*100 if(NpassPrev>0) else 0 
      eff = float("{0:.2f}".format( eff ))
      length = len(cut)
      delta = maxlength_cut-length
      for i in range(1,delta+1): cut += " "
      sNpass = str(Npass)
      length = len(sNpass)
      delta = maxlength_Npass-length
      for i in range(1,delta+1): sNpass = " "+sNpass
      sNpass = sNpass.replace(".0","")
      line = cut+"   "+sNpass+"   "+str(eff)+"[%]"
      f.write(line+"\n")
   f.write("\n")


## all the files you have
files = {}
for sample in samples:
   #tfile = db.rundir+sample+"/hist-sample.root"
   tfile = db.rundir+sample+"/hist-sample.root" if(args.ds!="t") else (db.rundir+sample+"/hist-sample.root").replace("TREE","TSTS")
   files.update({sample : TFile(tfile,"READ")})

## get all the histos...
histos = {}
for sample in samples:
   if(args.ID!="" and args.ID not in sample): continue
   for version in versions:
      for channel in db.channels:
         fullhistname = sample+"_cutflow_raw_"+channel+version
         hist = files[sample].Get("cutflow_raw_"+channel+version).Clone(fullhistname)
         printcf(hist)
         histos.update({fullhistname : hist})
f.close() 
print "cutflows written to: ",txtname



if(args.isdry):
   print "All histos:"
   print histos
   print "Dry run - quitting"
   quit()



## plots
#pdfname = db.plotdir+"cutflow.all."+args.ds+".pdf"
pdfname = db.plotdir+"cutflow.all."+args.ds+".pdf" if(args.ds!="t") else (db.plotdir+"cutflow.all."+args.ds+".pdf").replace("TREE","TSTS")

cnv = TCanvas("c","",500,500)
cnv.SaveAs(pdfname+"(")

for sample in samples:
   if(args.ID!="" and args.ID not in sample): continue
   cnv = TCanvas(sample+"_cutflow_raw","",1000,1000)
   cnv.Divide(1,3)
   pads = [ cnv.cd(1), cnv.cd(2), cnv.cd(3) ]
   for pad in pads:
      pad.SetTicks(1,1)
      #pad.SetLogy()
   i=0
   for channel in db.channels:
      if(i>2): continue ### skip the exclusive channels
      pads[i].cd()
      for version in versions:
         color  = ROOT.kBlack if("pre" in version) else ROOT.kRed
         #hmin = histos[sample+"_cutflow_raw_"+channel+version].GetMinimum() 
         #histos[sample+"_cutflow_raw_"+channel+version].SetMinimum(hmin*0.1)
         histos[sample+"_cutflow_raw_"+channel+version].SetMinimum(0)
         histos[sample+"_cutflow_raw_"+channel+version].SetLineColor(color)
         histos[sample+"_cutflow_raw_"+channel+version].SetMarkerColor(color)
         histos[sample+"_cutflow_raw_"+channel+version].SetTitle(sample)
         if("pre" in version): histos[sample+"_cutflow_raw_"+channel+version].Draw("text0")
         else:                 histos[sample+"_cutflow_raw_"+channel+version].Draw("same text0")
      pads[i].Update()
      pads[i].RedrawAxis()
      i+=1
   cnv.SaveAs(pdfname)

cnv = TCanvas("c","",500,500)
cnv.SaveAs(pdfname+")")
