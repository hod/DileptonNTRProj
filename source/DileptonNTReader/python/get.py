#!/usr/bin/env python
import os, sys
import subprocess
from subprocess import call

bkg_pattern = "user.hod.BKG22082017v1.mc16_13TeV"
sig_pattern = "user.hod.SIG22082017v1.mc16_13TeV"

formats = ["minitrees.root/", "_hist/"]

basedir = "/eos/user/h/hod/NTUP/mc16_13TeV/test/"

bkg_samples = ["Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO",
               "PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu",
               "PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee",
]

sig_samples = ["Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi",
               "Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi",
]

for sample in bkg_samples:
   version = bkg_pattern.replace("user.hod.","").replace(".mc16_13TeV","")
   p = subprocess.Popen("rm -rf "+basedir+version+"/"+sample, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()
   print err
   p = subprocess.Popen("mkdir -p "+basedir+version+"/"+sample, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()
   print err
   for form in formats:
      p = subprocess.Popen("rucio get --dir "+basedir+version+"/"+sample+"  "+bkg_pattern+"*"+sample+"*"+form, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      print err

'''
for sample in sig_samples:
   version = sig_pattern.replace("user.hod.","").replace(".mc16_13TeV","")
   p = subprocess.Popen("rm -rf "+basedir+version+"/"+sample, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()
   print err
   p = subprocess.Popen("mkdir -p "+basedir+version+"/"+sample, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()
   print err
   for form in formats:
      p = subprocess.Popen("rucio get --dir "+basedir+version+"/"+sample+"  "+sig_pattern+"*"+sample+"*"+form, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      print err
'''
