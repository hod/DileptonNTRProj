#!/usr/bin/python
import ROOT
from ROOT import *
import os
import math
import subprocess
import rootstyle
from array import *
import numpy as np
import db
import progressbar as pb


gROOT.LoadMacro( "source/DileptonNTReader/macros/Loader.C+" )


def CheckDSID(sample):
   samplex = sample
   thisdsid = int(sample.split(".")[0])
   for s in db.sliced.keys():
      dsid0 = int(s.split("-")[0])
      dsid1 = int(s.split("-")[1])
      if(thisdsid>=dsid0 and thisdsid<=dsid1):
         samplex = db.sliced[s]
         break
   if(samplex!=sample): print "sample: %s, is sliced!" % sample
   else:                print "sample: %s, not sliced" % sample
   return samplex

## get all the histos...
histos = {}
def BookAll(samples,DSID):
   for sample in samples:
      if(DSID!="" and DSID not in sample): continue
      sample = CheckDSID(sample) ### change the name if it is sliced
      for channel in db.channels:
         clabel = channel.replace("u","#mu") # this is dangerouse
         leptonclable = channel[:2] # jsut the lepton channel name (inclusive ones: ee,uu,eu)
         for var in db.histograms.keys():
            hname   = sample+"_"+channel+"-"+var
            if(hname in histos): continue ### skip the rest of the slices
            htitles = db.histograms[var]["titles"].replace("clabel",leptonclable)
            dim     = db.histograms[var]["dim"]
            if(dim==1):
               if("xmin" in db.histograms[var]):
                  nbinsx  = db.histograms[var]["nbinsx"]
                  xmin    = db.histograms[var]["xmin"]
                  xmax    = db.histograms[var]["xmax"]
                  histos.update({ hname : TH1D(hname,htitles,nbinsx,xmin,xmax) })
               else:
                  nbinsx  = db.histograms[var]["nbinsx"]
                  xbins   = db.histograms[var]["xbins"]
                  histos.update({ hname : TH1D(hname,htitles,nbinsx,xbins) })
            if(dim==2):
               nbinsx  = db.histograms[var]["nbinsx"]
               xmin    = db.histograms[var]["xmin"]
               xmax    = db.histograms[var]["xmax"]
               nbinsy  = db.histograms[var]["nbinsy"]
               ymin    = db.histograms[var]["ymin"]
               ymax    = db.histograms[var]["ymax"]
               histos.update({ hname : TH2D(hname,htitles,nbinsx,xmin,xmax,nbinsy,ymin,ymax) })
            histos[hname].Sumw2()
            histos[hname].SetTitle(sample+", "+clabel+" selection")
   

def GetSignalLeptons(event,channel):
   l1 = TLorentzVector()
   l2 = TLorentzVector()
   if("ee" in channel):
      l1.SetPtEtaPhiE(event.el_pt[0], event.el_eta[0], event.el_phi[0], event.el_e[0])
      l2.SetPtEtaPhiE(event.el_pt[1], event.el_eta[1], event.el_phi[1], event.el_e[1])
   if("uu" in channel):
      l1.SetPtEtaPhiE(event.mu_pt[0], event.mu_eta[0], event.mu_phi[0], event.mu_e[0])
      l2.SetPtEtaPhiE(event.mu_pt[1], event.mu_eta[1], event.mu_phi[1], event.mu_e[1])
   if("eu" in channel):
      l1.SetPtEtaPhiE(event.el_pt[0], event.el_eta[0], event.el_phi[0], event.el_e[0])
      l2.SetPtEtaPhiE(event.mu_pt[0], event.mu_eta[0], event.mu_phi[0], event.mu_e[0])
   return l1, l2


def GetOtherLeptons(event,channel):
   el = []
   mu = []
   ltmp = TLorentzVector()
   if("ee" in channel):
      for i in xrange(2,event.el_pt.size()):
         ltmp.SetPtEtaPhiE(event.el_pt[i], event.el_eta[i], event.el_phi[i], event.el_e[i])
         el.append(ltmp)
      for i in xrange(event.mu_pt.size()):
         ltmp.SetPtEtaPhiE(event.mu_pt[i], event.mu_eta[i], event.mu_phi[i], event.mu_e[i])
         mu.append(ltmp)
   if("uu" in channel):
      for i in xrange(2,event.el_pt.size()):
         ltmp.SetPtEtaPhiE(event.mu_pt[i], event.mu_eta[i], event.mu_phi[i], event.mu_e[i])
         mu.append(ltmp)
      for i in xrange(event.el_pt.size()):
         ltmp.SetPtEtaPhiE(event.el_pt[i], event.el_eta[i], event.el_phi[i], event.el_e[i])
         el.append(ltmp)
   if("eu" in channel):
      for i in xrange(1,event.el_pt.size()):
         ltmp.SetPtEtaPhiE(event.el_pt[i], event.el_eta[i], event.el_phi[i], event.el_e[i])
         el.append(ltmp)
      for i in xrange(1,event.mu_pt.size()):
         ltmp.SetPtEtaPhiE(event.mu_pt[i], event.mu_eta[i], event.mu_phi[i], event.mu_e[i])
         mu.append(ltmp)
   return el,mu


def GetJets(event,channel):
   jet = []
   bjet = []
   jtmp = TLorentzVector()
   for i in xrange(event.jet_pt.size()):
      jtmp.SetPtEtaPhiE(event.jet_pt[i], event.jet_eta[i], event.jet_phi[i], event.jet_e[i])
      jet.append(jtmp)
   for i in xrange(event.bjet_pt.size()):
      jtmp.SetPtEtaPhiE(event.bjet_pt[i], event.bjet_eta[i], event.bjet_phi[i], event.bjet_e[i])
      bjet.append(jtmp)
   return jet, bjet


def GetMinDRlj(lep,jets):
   mindr = 999
   index = -1
   for j in xrange(len(jets)):
      dr = lep.DeltaR(jets[j])
      index = j  if(dr<mindr) else index
      mindr = dr if(dr<mindr) else mindr
   return mindr, index


def AcceptEvent(event,channel):
   ### first check if the inclusive channel only passed
   if("ee" in channel and not event.pass_only_ee==1): return False
   if("uu" in channel and not event.pass_only_uu==1): return False
   if("eu" in channel and not event.pass_only_eu==1): return False
   ### then check the channel itself (trivial for the inclusive channels)
   if(channel=="ee0b"  and not event.pass_ee0b):  return False
   if(channel=="ee1b"  and not event.pass_ee1b):  return False
   if(channel=="ee2b"  and not event.pass_ee2b):  return False
   if(channel=="eemet" and not event.pass_eemet): return False
   ###
   if(channel=="uu0b"  and not event.pass_uu0b):  return False
   if(channel=="uu1b"  and not event.pass_uu1b):  return False
   if(channel=="uu2b"  and not event.pass_uu2b):  return False
   if(channel=="uumet" and not event.pass_uumet): return False
   ###
   if(channel=="eu0b"  and not event.pass_eu0b):  return False
   if(channel=="eu1b"  and not event.pass_eu1b):  return False
   if(channel=="eu2b"  and not event.pass_eu2b):  return False
   if(channel=="eumet" and not event.pass_eumet): return False
   ###
   return True ### accept


def Analyze(event,sample,channel):
   if(not AcceptEvent(event,channel)): return 0
   l1, l2 = GetSignalLeptons(event,channel)
   el, mu = GetOtherLeptons(event,channel)
   jet, bjet = GetJets(event,channel)

   hname = sample+"_"+channel+"-"

   nb2fb = 1.e6
   wgt = event.kF_weight
   #TODO!!! temporary workaround since 410252 is not available in LPX tool yet :(
   if(event.xsec>0 and sample!="410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO"): wgt *= 1./(event.Nevt/(event.xsec*nb2fb)/event.geneff)
   else:                                                                                 wgt *= 1./(event.Nevt/(0.07873*nb2fb))

   if("ee" in channel):
      histos[hname+"m"].Fill(event.m_ee,wgt)
      histos[hname+"m_eebins_10TeV"].Fill(event.m_ee,wgt)
      histos[hname+"m_uubins_10TeV"].Fill(event.m_ee,wgt)
      histos[hname+"m_eebins_6TeV"].Fill(event.m_ee,wgt)
      histos[hname+"m_uubins_6TeV"].Fill(event.m_ee,wgt)
      histos[hname+"m_eebins"].Fill(event.m_ee,wgt)
      histos[hname+"m_uubins"].Fill(event.m_ee,wgt)
      histos[hname+"pt1"].Fill(event.el_pt[0],wgt)
      histos[hname+"pt2"].Fill(event.el_pt[1],wgt)
      histos[hname+"mt2"].Fill(event.mt2_ee,wgt)
   if("uu" in channel):
      histos[hname+"m"].Fill(event.m_uu,wgt)
      histos[hname+"m_eebins_10TeV"].Fill(event.m_uu,wgt)
      histos[hname+"m_uubins_10TeV"].Fill(event.m_uu,wgt)
      histos[hname+"m_eebins_6TeV"].Fill(event.m_uu,wgt)
      histos[hname+"m_uubins_6TeV"].Fill(event.m_uu,wgt)
      histos[hname+"m_eebins"].Fill(event.m_uu,wgt)
      histos[hname+"m_uubins"].Fill(event.m_uu,wgt)
      histos[hname+"pt1"].Fill(event.mu_pt[0],wgt)
      histos[hname+"pt2"].Fill(event.mu_pt[1],wgt)
      histos[hname+"mt2"].Fill(event.mt2_uu,wgt)
   if("eu" in channel):
      histos[hname+"m"].Fill(event.m_eu,wgt)
      histos[hname+"m_eebins_10TeV"].Fill(event.m_eu,wgt)
      histos[hname+"m_uubins_10TeV"].Fill(event.m_eu,wgt)
      histos[hname+"m_eebins_6TeV"].Fill(event.m_eu,wgt)
      histos[hname+"m_uubins_6TeV"].Fill(event.m_eu,wgt)
      histos[hname+"m_eebins"].Fill(event.m_eu,wgt)
      histos[hname+"m_uubins"].Fill(event.m_eu,wgt)
      histos[hname+"pt1"].Fill(event.el_pt[0],wgt)
      histos[hname+"pt2"].Fill(event.mu_pt[0],wgt)
      histos[hname+"mt2"].Fill(event.mt2_eu,wgt)

   drminl1jet ,jl1  = GetMinDRlj(l1,jet)
   drminl2jet ,jl2  = GetMinDRlj(l2,jet)
   drminl1bjet,bjl1 = GetMinDRlj(l1,bjet)
   drminl2bjet,bjl2 = GetMinDRlj(l2,bjet)
   if(jl1>-1):  histos[hname+"dRmin1Jet"].Fill( drminl1jet ,wgt)
   if(jl2>-1):  histos[hname+"dRmin2Jet"].Fill( drminl2jet ,wgt)
   if(bjl1>-1): histos[hname+"dRmin1Bjet"].Fill( drminl1bjet ,wgt)
   if(bjl2>-1): histos[hname+"dRmin2Bjet"].Fill( drminl2bjet ,wgt)

   if(jl1>-1 and jl2>-1): histos[hname+"dRmin2Jet_vs_dRmin1Jet"].Fill( drminl1jet, drminl2jet ,wgt)
   if(jl1>-1):
      histos[hname+"dPtrel_vs_dRmin1Jet"].Fill( drminl1jet, ROOT.TMath.Abs(l1.Pt()-jet[jl1].Pt())/l1.Pt()*100 ,wgt)
      histos[hname+"JetPt_vs_dRmin1Jet"].Fill( drminl1jet, jet[jl1].Pt() ,wgt)
   if(jl2>-1):
      histos[hname+"dPtrel_vs_dRmin1Jet"].Fill( drminl2jet, ROOT.TMath.Abs(l2.Pt()-jet[jl2].Pt())/l2.Pt()*100 ,wgt)
      histos[hname+"JetPt_vs_dRmin2Jet"].Fill( drminl2jet, jet[jl2].Pt() ,wgt)

   histos[hname+"met"].Fill(event.met_wmuon_cst_et,wgt)

   return 1


def RunAll(samples,DSID):
   for sample in samples:
      if(DSID!="" and DSID not in sample): continue
      targetdir  = db.rundir+sample
      filename = db.rundir+sample+"/data-tinytrees/sample.root"
      print "getting tree from ",filename
      tfile = TFile(filename,"READ")
      tree = tfile.Get("nominal")
      nevents = tree.GetEntries()
      print "with %d events" % nevents
      sample = CheckDSID(sample) ### change the name if it is sliced
      bar = pb.ProgressBar(nevents, fmt=pb.ProgressBar.DEFAULT)
      n=1 ### init n
      for event in tree:
         bar.current += 1
         bar()
         for channel in db.channels:
            status = Analyze(event,sample,channel)
         n+=1 ### propagate n
      bar.done()