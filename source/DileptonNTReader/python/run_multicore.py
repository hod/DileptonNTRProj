#!/usr/bin/env python
import db
import os, sys
import subprocess
from subprocess import call
import Queue
import threading
import multiprocessing
import args

samples = []
if(args.ds=="b"):  samples = db.bkgs
if(args.ds=="s"):  samples = db.sigs
if(args.ds=="sb"): samples = db.bkgs+db.sigs
if(args.ds=="t"):  samples = db.tsts


def testoutput(err,out,pattern="Number of processed events ="):
   emptyerrfile = (os.stat(err).st_size==0)
   goodoutfile  = (pattern in open(out).read())
   if( not emptyerrfile and not goodoutfile ):
      print "******************************"
      print "*********   ERROR!   *********"
      print "********* check logs *********"
      print "******************************"
   else: print "Success :)"

def counthistfiles(directory):
   # print "entering dir: ",directory
   path, dirs, files = os.walk(directory).next()
   file_count = len(files)
   return file_count

q = Queue.Queue()
for sample in samples:
   if(args.ID!="" and args.ID not in sample): continue
   targetdir = db.rundir+sample if(args.ds!="t") else (db.rundir+sample).replace("TREE","TSTS")
   if(not args.isdry):
      p = subprocess.Popen("rm -rf "+targetdir, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
   for subdir in db.subdirs:
      if(subdir not in sample): continue
      tag = ""
      if(sample in db.bkgs): tag = db.bkgtag
      if(sample in db.sigs): tag = db.sigtag
      if(sample in db.tsts): tag = db.tsttag
      samplepath = db.basedir+tag+"/"+subdir+"/user.hod."+tag+".mc16_13TeV."+sample
      treespath  = samplepath+db.suffix
      histspath  = samplepath+db.hsuffix
      dataset    = samplepath+db.suffix
      histset    = samplepath+db.hsuffix
      executable = "build/"+os.environ.get('CMTCONFIG')+"/bin/ATestRun"
      hadd       = "hadd -f  "+histset+"/hist-grid.root  "+histset+"/user.hod.*.hist-output.root"
      copy       = "/bin/cp -f  "+histset+"/user.hod.*.hist-output.root  "+histset+"/hist-grid.root"
      print "histset=",histset
      nhists     = counthistfiles(histset)
      submit     = executable+"  "+dataset+"  "+targetdir
      if(nhists>1):  cmd = hadd+"  ;  "+submit
      if(nhists==1): cmd = copy+"  ;  "+submit
      if(nhists==0): cmd = "!!!ERROR!!!"
      if(args.isdry): cmd = "echo "+cmd
      #print "Submitting: "+cmd
      q.put(cmd)


def worker():
   while True:
      item = q.get()
      # execute a task: call a shell program and wait until it completes
      command = str(item)
      words = command.split()
      lastword = len(words)-1
      thistargetdir = words[lastword]
      print "\nSubmitting: "+command+"\nWith logs in: "+thistargetdir
      p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      f = open(thistargetdir+"/log.out", 'w')
      f.write(out)
      f.close()
      f = open(thistargetdir+"/log.err", 'w')
      f.write(err)
      f.close()
      testoutput(thistargetdir+"/log.err", thistargetdir+"/log.out")
      q.task_done()


cpus=multiprocessing.cpu_count() #detect number of cores
print("Creating %d threads" % cpus)
for i in range(cpus):
   t = threading.Thread(target=worker)
   t.daemon = True
   t.start()

q.join() # block until all tasks are done


