#!/usr/bin/env python
import os, sys
import math
import ROOT
from ROOT import *
import rootstyle
from array import *
import numpy as np
import db
import args

## all the samples you need
samples = []
if(args.ds=="b"):  samples = db.bkgs
if(args.ds=="s"):  samples = db.sigs
if(args.ds=="sb"): samples = db.bkgs+db.sigs


## styles
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0);
ROOT.gStyle.SetOptFit(0);


## all iterations
versions = ["_preOR","_pstOR"]

## all the files you have
files = {}
for sample in samples:
   files.update({sample : TFile(db.rundir+sample+"/hist-sample.root","READ")})

## get all the histos...
histos = {}
for sample in samples:
   if(args.ID!="" and args.ID not in sample): continue
   for variable in db.variables:
      for version in versions:
         for channel in db.channels:
            fullhistname = sample+"_"+variable+"_"+channel+version
            histos.update({fullhistname : files[sample].Get(variable+"_"+channel+version).Clone(fullhistname)})

if(args.isdry):
   print "All histos:"
   print histos
   print "Dry run - quitting"
   quit()
   


## plots
pdfname = db.plotdir+"OR.all."+args.ds+".pdf"

cnv = TCanvas("c","",500,500)
cnv.SaveAs(pdfname+"(")

for sample in samples:
   if(args.ID!="" and args.ID not in sample): continue
   for variable in db.variables:
      cnv = TCanvas(sample+"_"+variable,"",500,500)
      cnv.cd()
      cnv.SetTicks(1,1)
      cnv.SetLogy()
      for version in versions:
         for channel in db.channels:
            color  = ROOT.kBlack if("pre" in version) else ROOT.kRed
            marker = 20 if("pre" in version) else 24
            histos[sample+"_"+variable+"_"+channel+version].SetLineColor(color)
            histos[sample+"_"+variable+"_"+channel+version].SetMarkerColor(color)
            histos[sample+"_"+variable+"_"+channel+version].SetMarkerStyle(marker)
            histos[sample+"_"+variable+"_"+channel+version].SetTitle(sample)
            if("pre" in version): histos[sample+"_"+variable+"_"+channel+version].Draw()
            else:                 histos[sample+"_"+variable+"_"+channel+version].Draw("same")
         cnv.Update()
         cnv.SaveAs(pdfname)

cnv = TCanvas("c","",500,500)
cnv.SaveAs(pdfname+")")
