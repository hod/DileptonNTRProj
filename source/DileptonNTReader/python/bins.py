#!/usr/bin/env python
import math
import array
from array import *

def GetSearchBinning(channel,xend,divideBy):
   # Bin edges in GeV
   xbins = [80,] #the lowest edge first
   # Electron resolution parameters
   constA = 0.1637
   constB = 0.006490
   constC = 0.726
   # Muon resolution parameters
   par0_SearchMuonCh = 1.49850e+00
   par1_SearchMuonCh = 4.75540e-03
   par2_SearchMuonCh = -6.13551e-07
   par3_SearchMuonCh = 6.21498e-11
   i = 1
   while xbins[i-1]<xend:
      relResol = 1.
      if(channel=="ee"):
         termA = constA/math.sqrt(xbins[i-1])
         termB = constB
         termC = constC/xbins[i-1]
         relResol = math.sqrt(termA*termA + termB*termB + termC*termC)
      elif(channel=="uu"):
         relResol = (par0_SearchMuonCh + par1_SearchMuonCh*xbins[i-1] + par2_SearchMuonCh*xbins[i-1]*xbins[i-1] + par3_SearchMuonCh*xbins[i-1]*xbins[i-1]*xbins[i-1])/100.
      else: print "ERROR: channel "+channel+" not recognized; returning flat binning"
      xbins.append( xbins[i-1]*(1.+(relResol/divideBy)) )
      #print xbins[i]
      i += 1
   arrxbins = array("d", xbins)
   nbins = len(xbins)-1
   return nbins, arrxbins
