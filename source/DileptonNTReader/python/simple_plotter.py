#!/usr/bin/python
import ROOT
from ROOT import *
import os
import math
import subprocess
import rootstyle
from array import *
import numpy as np
import db
import simple_reader as reader
import args

samples = []
if(args.ds=="b"):  samples = db.bkgs
if(args.ds=="s"):  samples = db.sigs
if(args.ds=="sb"): samples = db.bkgs+db.sigs

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)

### book and fill all histograms
reader.BookAll(samples,args.ID)
reader.RunAll(samples,args.ID)

def ParseHistName(hname):
   ### hname can be DSID.some_long_sample_name_xx-var for non-sliced samples, with xx being the channel
   ### or some_long_sample_name_xx-var for sliced samples, with xx being the channel
   words  = hname.split("-")
   nwords = len(words)
   var = words[nwords-1]
   # sample = words[0][:-3]
   subwords = words[0].split("_")
   nsubwords = len(subwords)
   lastword = subwords[nsubwords-1]
   nlastword = len(lastword)
   sample = words[0][:-(nlastword+1)]
   return var, sample

## simple plots
def SimplePlot():
   pdfname = db.plotdir+"histos.all."+args.ds+".pdf"
   rootname = db.plotdir+"histos.all."+args.ds+".root"
   froot = TFile(rootname,"RECREATE")
   cnv = TCanvas("c","",500,500)
   cnv.SaveAs(pdfname+"(")
   for hname,hist in reader.histos.iteritems():
      var, sample = ParseHistName(hname)
      cnv = TCanvas(hname,"",500,500)
      cnv.cd()
      cnv.SetTicks(1,1)
      if(db.histograms[var]["logx"]==1): cnv.SetLogx()
      if(db.histograms[var]["logy"]==1): cnv.SetLogy()
      if(db.histograms[var]["dim"]==1):
         if(db.hattr[sample]["drawopt"]==""):
            hist.SetLineColor(db.hattr[sample]["col"])
            hist.SetMarkerColor(db.hattr[sample]["col"])
            hist.SetMarkerStyle(db.hattr[sample]["marker"])
            hist.SetMarkerSize(db.hattr[sample]["msize"])
            hist.Draw(db.hattr[sample]["drawopt"])
         elif(db.hattr[sample]["drawopt"]=="hist"):
            hist.SetLineColor(db.hattr[sample]["col"])
            hist.SetLineStyle(db.hattr[sample]["linestyle"])
            hist.SetLineWidth(2)
            hist.Draw(db.hattr[sample]["drawopt"])
         else:
            hist.SetLineColor(db.hattr[sample]["col"])
            hist.SetFillColor(db.hattr[sample]["col"])
            hist.Draw(db.hattr[sample]["drawopt"])
      if(db.histograms[var]["dim"]==2):
         if(db.histograms[var]["logz"]): cnv.SetLogz()
         hist.Draw("colz")      
      cnv.Update()
      cnv.SaveAs(pdfname)
      hist.Write()
      
   cnv = TCanvas("c","",500,500)
   cnv.SaveAs(pdfname+")")
   froot.Write()
   froot.Close()

SimplePlot()