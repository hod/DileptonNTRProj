#!/usr/bin/env python
import db
import os, sys
import subprocess
from subprocess import call
import Queue
import threading
import multiprocessing
import args

samples = []
if(args.ds=="b"):  samples = db.bkgs
if(args.ds=="s"):  samples = db.sigs
if(args.ds=="sb"): samples = db.bkgs+db.sigs
if(args.ds=="t"):
   print "Cannot rucio get a test sample..."
   quit()

q = Queue.Queue()
for sample in samples:
   if(args.ID!="" and args.ID not in sample): continue
   targetdir  = db.rundir+sample
   for subdir in db.subdirs:
      if(subdir not in sample): continue
      tag = ""
      if(sample in db.bkgs): tag = db.bkgtag
      if(sample in db.sigs): tag = db.sigtag
      mkdir = "mkdir -p "+db.basedir+tag+"/"+subdir
      rucio1 = "rucio get --dir "+db.basedir+tag+"/"+subdir+"/  user.hod."+tag+".mc16_13TeV."+sample+"_minitrees.root/"
      rucio2 = "rucio get --dir "+db.basedir+tag+"/"+subdir+"/  user.hod."+tag+".mc16_13TeV."+sample+"_hist/"
      rucio3 = "rucio get --dir "+db.basedir+tag+"/"+subdir+"/  user.hod."+tag+".mc16_13TeV."+sample+".log/"
      dsid  = sample.split(".")[0]
      get = mkdir+" ; "+rucio1+" ; "+rucio2+" ; "+rucio3+" ; echo DSID: "+dsid
      if(args.isdry): get = "echo "+mkdir+" ; echo "+rucio1+" ; "+rucio2+" ; "+rucio3+" ; echo DSID: "+dsid
      q.put(get)


def worker():
   while True:
      item = q.get()
      # execute a task: call a shell program and wait until it completes
      command = str(item)
      thistargetdir = command.split()[3] if(args.isdry) else command.split()[2]
      words = command.split()
      thisdsid = words[len(words)-1]
      print "Getting: "+command+"\nWith logs in: "+thistargetdir+" --> DSID: "+thisdsid
      p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      if(not args.isdry):
         f = open(thistargetdir+"/log."+thisdsid+".out", 'w')
         f.write(out)
         f.close()
         f = open(thistargetdir+"/log."+thisdsid+".err", 'w')
         f.write(err)
         f.close()
         q.task_done()


cpus=multiprocessing.cpu_count() #detect number of cores
print("Creating %d threads" % cpus)
for i in range(cpus):
   t = threading.Thread(target=worker)
   t.daemon = True
   t.start()

q.join() # block until all tasks are done
