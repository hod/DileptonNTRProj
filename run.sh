#!/bin/sh
source build/*/setup.sh

# 1. uncomment *one* line at a time to run
# 2. run with "-isdry 1" to see that the commands look OK
# 3. if so, change "-isdry 1" to "-isdry 0" and "-ds" can be "s/b/sb"

# multicore on a single machine:
# python source/DileptonNTReader/python/run_multicore.py -isdry 1 -ds s

# single core:
# python source/DileptonNTReader/python/run_multicore.py -isdry 1 -ds s
