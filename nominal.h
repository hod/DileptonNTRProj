//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Sep  8 16:01:20 2017 by ROOT version 6.08/06
// from TTree nominal/
// found on file: /eos/user/h/hod/NTUP/mc16_13TeV/test/SIG08092017v0/Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi/user.hod.SIG08092017v0.mc16_13TeV.301216.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000_minitrees.root/user.hod.12112467._000001.minitrees.root
//////////////////////////////////////////////////////////

#ifndef nominal_h
#define nominal_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class nominal {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           run;
   ULong64_t       event;
   Int_t           last;
   Int_t           year;
   Int_t           n_jet;
   Int_t           n_jet_preor;
   Int_t           n_mu_preor;
   Int_t           n_el_preor;
   Int_t           n_ph_preor;
   Int_t           n_bjet;
   Int_t           n_el;
   Int_t           n_el_baseline;
   Int_t           n_mu_baseline;
   Int_t           n_mu_baseline_bad;
   Int_t           n_allmu_bad;
   Int_t           n_mu;
   Float_t         munu_mT;
   Float_t         enu_mT;
   Float_t         mumu_m;
   Float_t         ee_m;
   Float_t         sh22_weight;
   Float_t         mconly_weight;
   vector<float>   *mconly_weights;
   Float_t         kF_weight;
   Float_t         xsec;
   Float_t         geneff;
   Float_t         pu_weight;
   Float_t         btag_weight;
   Float_t         jvt_weight;
   Float_t         jvt_all_weight;
   vector<int>     *truth_W_decay;
   Float_t         GenFiltMet;
   vector<float>   *newSH_weight;
   Int_t           n_truthTop;
   Int_t           passTSTCleaning;
   Float_t         averageIntPerXing;
   Float_t         corAverageIntPerXing;
   Int_t           n_vx;
   Float_t         pu_hash;
   Float_t         mu_SF_tot;
   Int_t           trigger_matched_electron;
   Int_t           trigger_matched_muon;
   Int_t           trigger_HLT_2e17_lhloose;
   Int_t           trigger_HLT_2e17_lhvloose;
   Int_t           trigger_HLT_2e17_lhvloose_nod0;
   Int_t           trigger_HLT_2mu14;
   Int_t           trigger_HLT_e120_lhloose;
   Int_t           trigger_HLT_e140_lhloose_nod0;
   Int_t           trigger_HLT_e24_lhmedium_L1EM20VH;
   Int_t           trigger_HLT_e24_lhmedium_L1EM20VHI;
   Int_t           trigger_HLT_e24_lhmedium_nod0_L1EM20VH;
   Int_t           trigger_HLT_e24_lhtight_nod0_ivarloose;
   Int_t           trigger_HLT_e26_lhtight_ivarloose;
   Int_t           trigger_HLT_e26_lhtight_nod0_ivarloose;
   Int_t           trigger_HLT_e60_lhmedium;
   Int_t           trigger_HLT_e60_lhmedium_nod0;
   Int_t           trigger_HLT_e60_medium;
   Int_t           trigger_HLT_mu20_2mu4noL1;
   Int_t           trigger_HLT_mu20_ivarloose_L1MU15;
   Int_t           trigger_HLT_mu22_mu8noL1;
   Int_t           trigger_HLT_mu24_ivarmedium;
   Int_t           trigger_HLT_mu26_imedium;
   Int_t           trigger_HLT_mu26_ivarmedium;
   Int_t           trigger_HLT_mu40;
   Int_t           trigger_HLT_mu50;
   Float_t         weight_VJetsEW_Nominal;
   Float_t         weight_VJetsEW_vjets_d1K_NLO_High;
   Float_t         weight_VJetsEW_vjets_d1K_NLO_Low;
   Float_t         weight_VJetsEW_vjets_d1kappa_EW_High;
   Float_t         weight_VJetsEW_vjets_d1kappa_EW_Low;
   Float_t         weight_VJetsEW_vjets_d2K_NLO_High;
   Float_t         weight_VJetsEW_vjets_d2K_NLO_Low;
   Float_t         weight_VJetsEW_vjets_d2kappa_EW_High;
   Float_t         weight_VJetsEW_vjets_d2kappa_EW_Low;
   Float_t         weight_VJetsEW_vjets_d3K_NLO_High;
   Float_t         weight_VJetsEW_vjets_d3K_NLO_Low;
   Float_t         weight_VJetsEW_vjets_d3kappa_EW_High;
   Float_t         weight_VJetsEW_vjets_d3kappa_EW_Low;
   Float_t         weight_VJetsEW_vjets_dK_NLO_fix_High;
   Float_t         weight_VJetsEW_vjets_dK_NLO_fix_Low;
   Float_t         weight_VJetsEW_vjets_dK_NLO_mix_High;
   Float_t         weight_VJetsEW_vjets_dK_NLO_mix_Low;
   Int_t           trigger_pass;
   Int_t           trigger_matched_HLT_e60_lhmedium;
   Int_t           trigger_matched_HLT_e120_lhloose;
   Int_t           trigger_matched_HLT_e24_lhmedium_L1EM18VH;
   Int_t           trigger_matched_HLT_e24_lhmedium_L1EM20VH;
   Int_t           lbn;
   Int_t           bcid;
   Float_t         pdf_x1;
   Float_t         pdf_x2;
   Float_t         pdf_pdf1;
   Float_t         pdf_pdf2;
   Float_t         pdf_scale;
   Int_t           flag_bib;
   Int_t           flag_bib_raw;
   Int_t           flag_sct;
   Int_t           flag_core;
   Int_t           trigger_HLT_2e12_lhloose_L12EM10VH;
   Int_t           trigger_HLT_2e12_lhvloose_nod0_L12EM10VH;
   Int_t           trigger_HLT_2e17_lhloose;
   Int_t           trigger_HLT_2e17_lhvloose;
   Int_t           trigger_HLT_2e17_lhvloose_nod0;
   Int_t           trigger_HLT_2e17_loose;
   Int_t           trigger_HLT_2mu10;
   Int_t           trigger_HLT_2mu14;
   Int_t           trigger_HLT_e120_lhloose_nod0;
   Int_t           trigger_HLT_e17_lhloose_2e9_lhloose;
   Int_t           trigger_HLT_e17_lhloose_nod0_2e9_lhloose_nod0;
   Int_t           trigger_HLT_e24_lhmedium_L1EM18VH;
   Int_t           trigger_HLT_e24_lhmedium_L1EM20VHI;
   Int_t           trigger_HLT_e24_lhmedium_iloose_L1EM20VH;
   Int_t           trigger_HLT_e24_lhmedium_nod0_L1EM20VH;
   Int_t           trigger_HLT_e24_lhmedium_nod0_ivarloose;
   Int_t           trigger_HLT_e24_lhtight_iloose;
   Int_t           trigger_HLT_e26_lhtight_ivarloose;
   Int_t           trigger_HLT_e28_tight_iloose;
   Int_t           trigger_HLT_e60_medium;
   Int_t           trigger_HLT_mu20_2mu4noL1;
   Int_t           trigger_HLT_mu20_iloose_L1MU15;
   Int_t           trigger_HLT_mu20_ivarloose_L1MU15;
   Int_t           trigger_HLT_mu20_mu8noL1;
   Int_t           trigger_HLT_mu22_mu8noL1;
   Int_t           trigger_HLT_mu24_iloose;
   Int_t           trigger_HLT_mu24_iloose_L1MU15;
   Int_t           trigger_HLT_mu24_imedium;
   Int_t           trigger_HLT_mu24_ivarloose;
   Int_t           trigger_HLT_mu24_ivarloose_L1MU15;
   Int_t           trigger_HLT_mu24_ivarmedium;
   Int_t           trigger_HLT_mu26_imedium;
   Int_t           trigger_HLT_mu26_ivarmedium;
   Int_t           trigger_HLT_mu40;
   Int_t           trigger_HLT_mu50;
   Int_t           hfor;
   Int_t           n_ph;
   Int_t           n_ph_tight;
   Int_t           n_ph_baseline;
   Int_t           n_ph_baseline_tight;
   Int_t           pdf_id1;
   Int_t           pdf_id2;
   Int_t           bb_decision;
   Float_t         shatR;
   Float_t         gaminvR;
   Float_t         gaminvRp1;
   Float_t         dphi_BETA_R;
   Float_t         dphi_J1_J2_R;
   Float_t         gamma_Rp1;
   Float_t         costhetaR;
   Float_t         dphi_R_Rp1;
   Float_t         mdeltaR;
   Float_t         cosptR;
   Float_t         costhetaRp1;
   Float_t         jj_m;
   Float_t         mumu_pt;
   Float_t         mumu_eta;
   Float_t         mumu_phi;
   Float_t         ee_pt;
   Float_t         ee_eta;
   Float_t         ee_phi;
   Int_t           n_jet_truth;
   Float_t         truth_jet1_pt;
   Float_t         truth_jet1_eta;
   Float_t         truth_jet1_phi;
   Float_t         truth_jet1_m;
   Float_t         truth_jet2_pt;
   Float_t         truth_jet2_eta;
   Float_t         truth_jet2_phi;
   Float_t         truth_jet2_m;
   Float_t         truth_ph1_pt;
   Float_t         truth_ph1_eta;
   Float_t         truth_ph1_phi;
   Int_t           truth_ph1_type;
   Float_t         truth_V_pt;
   Float_t         truth_V_eta;
   Float_t         truth_V_phi;
   Float_t         truth_V_m;
   vector<float>   *truth_mu_pt;
   vector<float>   *truth_mu_eta;
   vector<float>   *truth_mu_phi;
   vector<float>   *truth_mu_m;
   vector<int>     *truth_mu_status;
   Float_t         evsf_baseline_nominal_EL;
   Float_t         evsf_baseline_nominal_MU;
   Float_t         evsf_baseline_nominal_PH;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1up;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1down;
   Float_t         evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_ISO_STAT__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_ISO_STAT__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_ISO_SYS__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_ISO_SYS__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_STAT__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_STAT__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_SYS__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_RECO_SYS__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t         evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t         evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1up;
   Float_t         evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1down;
   Float_t         evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1up;
   Float_t         evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1down;
   Float_t         evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1up;
   Float_t         evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1down;
   Float_t         evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1up;
   Float_t         met_eleterm_et;
   Float_t         met_eleterm_etx;
   Float_t         met_eleterm_ety;
   Float_t         met_eleterm_sumet;
   Float_t         met_eleterm_phi;
   Float_t         met_jetterm_et;
   Float_t         met_jetterm_etx;
   Float_t         met_jetterm_ety;
   Float_t         met_jetterm_sumet;
   Float_t         met_jetterm_phi;
   Float_t         met_muonterm_et;
   Float_t         met_muonterm_etx;
   Float_t         met_muonterm_ety;
   Float_t         met_muonterm_sumet;
   Float_t         met_muonterm_phi;
   Float_t         met_muonterm_cst_et;
   Float_t         met_muonterm_cst_etx;
   Float_t         met_muonterm_cst_ety;
   Float_t         met_muonterm_cst_sumet;
   Float_t         met_muonterm_cst_phi;
   Float_t         met_muonterm_tst_et;
   Float_t         met_muonterm_tst_etx;
   Float_t         met_muonterm_tst_ety;
   Float_t         met_muonterm_tst_sumet;
   Float_t         met_muonterm_tst_phi;
   Float_t         met_noelectron_cst_et;
   Float_t         met_noelectron_cst_etx;
   Float_t         met_noelectron_cst_ety;
   Float_t         met_noelectron_cst_sumet;
   Float_t         met_noelectron_cst_phi;
   Float_t         met_noelectron_tst_et;
   Float_t         met_noelectron_tst_etx;
   Float_t         met_noelectron_tst_ety;
   Float_t         met_noelectron_tst_sumet;
   Float_t         met_noelectron_tst_phi;
   Float_t         met_nomuon_cst_et;
   Float_t         met_nomuon_cst_etx;
   Float_t         met_nomuon_cst_ety;
   Float_t         met_nomuon_cst_sumet;
   Float_t         met_nomuon_cst_phi;
   Float_t         met_nomuon_tst_et;
   Float_t         met_nomuon_tst_etx;
   Float_t         met_nomuon_tst_ety;
   Float_t         met_nomuon_tst_sumet;
   Float_t         met_nomuon_tst_phi;
   Float_t         met_nophcalib_nomuon_cst_et;
   Float_t         met_nophcalib_nomuon_cst_etx;
   Float_t         met_nophcalib_nomuon_cst_ety;
   Float_t         met_nophcalib_nomuon_cst_sumet;
   Float_t         met_nophcalib_nomuon_cst_phi;
   Float_t         met_nophcalib_nomuon_tst_et;
   Float_t         met_nophcalib_nomuon_tst_etx;
   Float_t         met_nophcalib_nomuon_tst_ety;
   Float_t         met_nophcalib_nomuon_tst_sumet;
   Float_t         met_nophcalib_nomuon_tst_phi;
   Float_t         met_nophcalib_wmuon_cst_et;
   Float_t         met_nophcalib_wmuon_cst_etx;
   Float_t         met_nophcalib_wmuon_cst_ety;
   Float_t         met_nophcalib_wmuon_cst_sumet;
   Float_t         met_nophcalib_wmuon_cst_phi;
   Float_t         met_nophcalib_wmuon_tst_et;
   Float_t         met_nophcalib_wmuon_tst_etx;
   Float_t         met_nophcalib_wmuon_tst_ety;
   Float_t         met_nophcalib_wmuon_tst_sumet;
   Float_t         met_nophcalib_wmuon_tst_phi;
   Float_t         met_nophoton_cst_et;
   Float_t         met_nophoton_cst_etx;
   Float_t         met_nophoton_cst_ety;
   Float_t         met_nophoton_cst_sumet;
   Float_t         met_nophoton_cst_phi;
   Float_t         met_nophoton_tst_et;
   Float_t         met_nophoton_tst_etx;
   Float_t         met_nophoton_tst_ety;
   Float_t         met_nophoton_tst_sumet;
   Float_t         met_nophoton_tst_phi;
   Float_t         met_phterm_et;
   Float_t         met_phterm_etx;
   Float_t         met_phterm_ety;
   Float_t         met_phterm_sumet;
   Float_t         met_phterm_phi;
   Float_t         met_softerm_cst_et;
   Float_t         met_softerm_cst_etx;
   Float_t         met_softerm_cst_ety;
   Float_t         met_softerm_cst_sumet;
   Float_t         met_softerm_cst_phi;
   Float_t         met_softerm_tst_et;
   Float_t         met_softerm_tst_etx;
   Float_t         met_softerm_tst_ety;
   Float_t         met_softerm_tst_sumet;
   Float_t         met_softerm_tst_phi;
   Float_t         met_track_et;
   Float_t         met_track_etx;
   Float_t         met_track_ety;
   Float_t         met_track_sumet;
   Float_t         met_track_phi;
   Float_t         met_truth_et;
   Float_t         met_truth_etx;
   Float_t         met_truth_ety;
   Float_t         met_truth_sumet;
   Float_t         met_truth_phi;
   Float_t         met_wmuon_cst_et;
   Float_t         met_wmuon_cst_etx;
   Float_t         met_wmuon_cst_ety;
   Float_t         met_wmuon_cst_sumet;
   Float_t         met_wmuon_cst_phi;
   Float_t         met_wmuon_tst_et;
   Float_t         met_wmuon_tst_etx;
   Float_t         met_wmuon_tst_ety;
   Float_t         met_wmuon_tst_sumet;
   Float_t         met_wmuon_tst_phi;
   vector<float>   *mu_pt;
   vector<float>   *mu_ptErr;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_SF;
   vector<float>   *mu_SF_iso;
   vector<int>     *mu_isotool_pass_loosetrackonly;
   vector<int>     *mu_isotool_pass_fixedcuttighttrackonly;
   vector<float>   *mu_m;
   vector<float>   *mu_e;
   vector<float>   *mu_d0;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_z0;
   vector<float>   *mu_z0sig;
   vector<float>   *mu_charge;
   vector<float>   *mu_id_pt;
   vector<float>   *mu_id_ptErr;
   vector<float>   *mu_id_eta;
   vector<float>   *mu_id_phi;
   vector<float>   *mu_id_m;
   vector<float>   *mu_id_qop;
   vector<float>   *mu_id_qoperr;
   vector<float>   *mu_ms_pt;
   vector<float>   *mu_ms_ptErr;
   vector<float>   *mu_ms_eta;
   vector<float>   *mu_ms_phi;
   vector<float>   *mu_ms_m;
   vector<float>   *mu_ms_qop;
   vector<float>   *mu_ms_qoperr;
   vector<float>   *mu_me_pt;
   vector<float>   *mu_me_ptErr;
   vector<float>   *mu_me_eta;
   vector<float>   *mu_me_phi;
   vector<float>   *mu_me_m;
   vector<float>   *mu_me_qop;
   vector<float>   *mu_me_qoperr;
   vector<float>   *mu_mse_pt;
   vector<float>   *mu_mse_ptErr;
   vector<float>   *mu_mse_eta;
   vector<float>   *mu_mse_phi;
   vector<float>   *mu_mse_m;
   vector<float>   *mu_mse_qop;
   vector<float>   *mu_mse_qoperr;
   vector<float>   *mu_ptcone20;
   vector<float>   *mu_ptvarcone20;
   vector<float>   *mu_etcone20;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptcone30;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_etcone30;
   vector<float>   *mu_topoetcone30;
   vector<float>   *mu_ptcone40;
   vector<float>   *mu_ptvarcone40;
   vector<float>   *mu_etcone40;
   vector<float>   *mu_topoetcone40;
   vector<int>     *mu_author;
   vector<int>     *mu_quality;
   vector<int>     *mu_isSA;
   vector<int>     *mu_isST;
   vector<int>     *mu_isCB;
   vector<int>     *mu_isTight;
   vector<int>     *mu_isMedium;
   vector<int>     *mu_isLoose;
   vector<int>     *mu_isVeryLoose;
   vector<int>     *mu_isHighPt;
   vector<int>     *mu_isBad;
   vector<int>     *mu_isBadHighPt;
   vector<float>   *mu_finalFitPt;
   vector<float>   *mu_finalFitPtErr;
   vector<float>   *mu_finalFitEta;
   vector<float>   *mu_finalFitPhi;
   vector<float>   *mu_finalFitQOverP;
   vector<float>   *mu_finalFitQOverPErr;
   vector<float>   *mu_truthEta;
   vector<float>   *mu_truthPhi;
   vector<float>   *mu_truthPt;
   vector<float>   *mu_truthTheta;
   vector<float>   *mu_truthQOverP;
   vector<int>     *mu_isotool_pass_loose;
   vector<int>     *mu_isotool_pass_gradient;
   vector<int>     *mu_isotool_pass_gradientloose;
   vector<float>   *mu_met_nomuon_dphi;
   vector<float>   *mu_met_wmuon_dphi;
   vector<int>     *mu_truth_origin;
   vector<int>     *mu_truth_type;
   vector<int>     *mu_muonType;
   vector<float>   *mu_momentumBalanceSignificance;
   vector<float>   *mu_CaloLRLikelihood;
   vector<int>     *mu_CaloMuonIDTag;
   vector<int>     *mu_numberOfPrecisionLayers;
   vector<int>     *mu_numberOfPrecisionHoleLayers;
   vector<int>     *mu_numberOfGoodPrecisionLayers;
   vector<int>     *mu_innerSmallHits;
   vector<int>     *mu_innerLargeHits;
   vector<int>     *mu_innerSmallHoles;
   vector<int>     *mu_innerLargeHoles;
   vector<int>     *mu_middleSmallHoles;
   vector<int>     *mu_middleLargeHoles;
   vector<int>     *mu_outerSmallHoles;
   vector<int>     *mu_outerLargeHoles;
   vector<int>     *mu_extendedSmallHoles;
   vector<int>     *mu_extendedLargeHoles;
   vector<int>     *mu_innerClosePrecisionHits;
   vector<int>     *mu_middleClosePrecisionHits;
   vector<int>     *mu_outerClosePrecisionHits;
   vector<int>     *mu_extendedClosePrecisionHits;
   vector<int>     *mu_innerOutBoundsPrecisionHits;
   vector<int>     *mu_middleOutBoundsPrecisionHits;
   vector<int>     *mu_outerOutBoundsPrecisionHits;
   vector<int>     *mu_extendedOutBoundsPrecisionHits;
   vector<int>     *mu_combinedTrackOutBoundsPrecisionHits;
   vector<int>     *mu_middleLargeHits;
   vector<int>     *mu_middleSmallHits;
   vector<int>     *mu_outerLargeHits;
   vector<int>     *mu_outerSmallHits;
   vector<int>     *mu_extendedSmallHits;
   vector<int>     *mu_extendedLargeHits;
   vector<int>     *mu_numberOfPixelHits;
   vector<int>     *mu_numberOfPixelDeadSensors;
   vector<int>     *mu_numberOfSCTHits;
   vector<int>     *mu_numberOfSCTDeadSensors;
   vector<int>     *mu_numberOfPixelHoles;
   vector<int>     *mu_numberOfSCTHoles;
   vector<int>     *mu_numberOfTRTHits;
   vector<int>     *mu_numberOfTRTOutliers;
   vector<float>   *mu_primarytrketa;
   vector<int>     *mu_numberOfInnermostPixelLayerHits;
   vector<float>   *mu_cbtrketa;
   vector<float>   *mu_spectrometerFieldIntegral;
   vector<float>   *mu_scatteringCurvatureSignificance;
   vector<float>   *mu_scatteringNeighbourSignificance;
   vector<int>     *mu_allAuthors;
   vector<int>     *mu_numberOfPhiLayers;
   vector<int>     *mu_numberOfPhiHoleLayers;
   vector<int>     *mu_numberOfTriggerEtaLayers;
   vector<int>     *mu_numberOfTriggerEtaHoleLayers;
   vector<int>     *mu_nUnspoiledCscHits;
   vector<float>   *mu_FSR_CandidateEnergy;
   vector<float>   *mu_EnergyLoss;
   vector<float>   *mu_EnergyLossSigma;
   vector<float>   *mu_ParamEnergyLoss;
   vector<float>   *mu_ParamEnergyLossSigmaPlus;
   vector<float>   *mu_ParamEnergyLossSigmaMinus;
   vector<float>   *mu_MeasEnergyLoss;
   vector<float>   *mu_MeasEnergyLossSigma;
   vector<float>   *mu_msInnerMatchChi2;
   vector<int>     *mu_msInnerMatchDOF;
   vector<int>     *mu_energyLossType;
   vector<int>     *mu_phiLayer1Hits;
   vector<int>     *mu_phiLayer2Hits;
   vector<int>     *mu_phiLayer3Hits;
   vector<int>     *mu_phiLayer4Hits;
   vector<int>     *mu_etaLayer1Hits;
   vector<int>     *mu_etaLayer2Hits;
   vector<int>     *mu_etaLayer3Hits;
   vector<int>     *mu_etaLayer4Hits;
   vector<int>     *mu_phiLayer1Holes;
   vector<int>     *mu_phiLayer2Holes;
   vector<int>     *mu_phiLayer3Holes;
   vector<int>     *mu_phiLayer4Holes;
   vector<int>     *mu_etaLayer1Holes;
   vector<int>     *mu_etaLayer2Holes;
   vector<int>     *mu_etaLayer3Holes;
   vector<int>     *mu_etaLayer4Holes;
   vector<int>     *mu_phiLayer1RPCHits;
   vector<int>     *mu_phiLayer2RPCHits;
   vector<int>     *mu_phiLayer3RPCHits;
   vector<int>     *mu_etaLayer1RPCHits;
   vector<int>     *mu_etaLayer2RPCHits;
   vector<int>     *mu_etaLayer3RPCHits;
   vector<int>     *mu_phiLayer1RPCHoles;
   vector<int>     *mu_phiLayer2RPCHoles;
   vector<int>     *mu_phiLayer3RPCHoles;
   vector<int>     *mu_etaLayer1RPCHoles;
   vector<int>     *mu_etaLayer2RPCHoles;
   vector<int>     *mu_etaLayer3RPCHoles;
   vector<int>     *mu_phiLayer1TGCHits;
   vector<int>     *mu_phiLayer2TGCHits;
   vector<int>     *mu_phiLayer3TGCHits;
   vector<int>     *mu_phiLayer4TGCHits;
   vector<int>     *mu_etaLayer1TGCHits;
   vector<int>     *mu_etaLayer2TGCHits;
   vector<int>     *mu_etaLayer3TGCHits;
   vector<int>     *mu_etaLayer4TGCHits;
   vector<int>     *mu_phiLayer1TGCHoles;
   vector<int>     *mu_phiLayer2TGCHoles;
   vector<int>     *mu_phiLayer3TGCHoles;
   vector<int>     *mu_phiLayer4TGCHoles;
   vector<int>     *mu_etaLayer1TGCHoles;
   vector<int>     *mu_etaLayer2TGCHoles;
   vector<int>     *mu_etaLayer3TGCHoles;
   vector<int>     *mu_etaLayer4TGCHoles;
   vector<float>   *mu_segmentDeltaEta;
   vector<float>   *mu_segmentDeltaPhi;
   vector<float>   *mu_segmentChi2OverDoF;
   vector<float>   *mu_t0;
   vector<float>   *mu_beta;
   vector<float>   *mu_meanDeltaADCCountsMDT;
   vector<int>     *mu_msOuterMatchDOF;
   vector<float>   *mu_preor_pt;
   vector<float>   *mu_preor_ptErr;
   vector<float>   *mu_preor_eta;
   vector<float>   *mu_preor_phi;
   vector<float>   *mu_preor_SF;
   vector<float>   *mu_preor_SF_iso;
   vector<int>     *mu_preor_isotool_pass_loosetrackonly;
   vector<int>     *mu_preor_isotool_pass_fixedcuttighttrackonly;
   vector<float>   *mu_preor_m;
   vector<float>   *mu_preor_e;
   vector<float>   *mu_preor_d0;
   vector<float>   *mu_preor_d0sig;
   vector<float>   *mu_preor_z0;
   vector<float>   *mu_preor_z0sig;
   vector<float>   *mu_preor_charge;
   vector<float>   *mu_preor_id_pt;
   vector<float>   *mu_preor_id_ptErr;
   vector<float>   *mu_preor_id_eta;
   vector<float>   *mu_preor_id_phi;
   vector<float>   *mu_preor_id_m;
   vector<float>   *mu_preor_id_qop;
   vector<float>   *mu_preor_id_qoperr;
   vector<float>   *mu_preor_ms_pt;
   vector<float>   *mu_preor_ms_ptErr;
   vector<float>   *mu_preor_ms_eta;
   vector<float>   *mu_preor_ms_phi;
   vector<float>   *mu_preor_ms_m;
   vector<float>   *mu_preor_ms_qop;
   vector<float>   *mu_preor_ms_qoperr;
   vector<float>   *mu_preor_me_pt;
   vector<float>   *mu_preor_me_ptErr;
   vector<float>   *mu_preor_me_eta;
   vector<float>   *mu_preor_me_phi;
   vector<float>   *mu_preor_me_m;
   vector<float>   *mu_preor_me_qop;
   vector<float>   *mu_preor_me_qoperr;
   vector<float>   *mu_preor_mse_pt;
   vector<float>   *mu_preor_mse_ptErr;
   vector<float>   *mu_preor_mse_eta;
   vector<float>   *mu_preor_mse_phi;
   vector<float>   *mu_preor_mse_m;
   vector<float>   *mu_preor_mse_qop;
   vector<float>   *mu_preor_mse_qoperr;
   vector<float>   *mu_preor_ptcone20;
   vector<float>   *mu_preor_ptvarcone20;
   vector<float>   *mu_preor_etcone20;
   vector<float>   *mu_preor_topoetcone20;
   vector<float>   *mu_preor_ptcone30;
   vector<float>   *mu_preor_ptvarcone30;
   vector<float>   *mu_preor_etcone30;
   vector<float>   *mu_preor_topoetcone30;
   vector<float>   *mu_preor_ptcone40;
   vector<float>   *mu_preor_ptvarcone40;
   vector<float>   *mu_preor_etcone40;
   vector<float>   *mu_preor_topoetcone40;
   vector<int>     *mu_preor_author;
   vector<int>     *mu_preor_quality;
   vector<int>     *mu_preor_isSA;
   vector<int>     *mu_preor_isST;
   vector<int>     *mu_preor_isCB;
   vector<int>     *mu_preor_isTight;
   vector<int>     *mu_preor_isMedium;
   vector<int>     *mu_preor_isLoose;
   vector<int>     *mu_preor_isVeryLoose;
   vector<int>     *mu_preor_isHighPt;
   vector<int>     *mu_preor_isBad;
   vector<int>     *mu_preor_isBadHighPt;
   vector<float>   *mu_preor_finalFitPt;
   vector<float>   *mu_preor_finalFitPtErr;
   vector<float>   *mu_preor_finalFitEta;
   vector<float>   *mu_preor_finalFitPhi;
   vector<float>   *mu_preor_finalFitQOverP;
   vector<float>   *mu_preor_finalFitQOverPErr;
   vector<float>   *mu_preor_truthEta;
   vector<float>   *mu_preor_truthPhi;
   vector<float>   *mu_preor_truthPt;
   vector<float>   *mu_preor_truthTheta;
   vector<float>   *mu_preor_truthQOverP;
   vector<int>     *mu_preor_isotool_pass_loose;
   vector<int>     *mu_preor_isotool_pass_gradient;
   vector<int>     *mu_preor_isotool_pass_gradientloose;
   vector<float>   *mu_preor_met_nomuon_dphi;
   vector<float>   *mu_preor_met_wmuon_dphi;
   vector<int>     *mu_preor_truth_origin;
   vector<int>     *mu_preor_truth_type;
   vector<int>     *mu_preor_muonType;
   vector<float>   *mu_preor_momentumBalanceSignificance;
   vector<float>   *mu_preor_CaloLRLikelihood;
   vector<int>     *mu_preor_CaloMuonIDTag;
   vector<int>     *mu_preor_numberOfPrecisionLayers;
   vector<int>     *mu_preor_numberOfPrecisionHoleLayers;
   vector<int>     *mu_preor_numberOfGoodPrecisionLayers;
   vector<int>     *mu_preor_innerSmallHits;
   vector<int>     *mu_preor_innerLargeHits;
   vector<int>     *mu_preor_innerSmallHoles;
   vector<int>     *mu_preor_innerLargeHoles;
   vector<int>     *mu_preor_middleSmallHoles;
   vector<int>     *mu_preor_middleLargeHoles;
   vector<int>     *mu_preor_outerSmallHoles;
   vector<int>     *mu_preor_outerLargeHoles;
   vector<int>     *mu_preor_extendedSmallHoles;
   vector<int>     *mu_preor_extendedLargeHoles;
   vector<int>     *mu_preor_innerClosePrecisionHits;
   vector<int>     *mu_preor_middleClosePrecisionHits;
   vector<int>     *mu_preor_outerClosePrecisionHits;
   vector<int>     *mu_preor_extendedClosePrecisionHits;
   vector<int>     *mu_preor_innerOutBoundsPrecisionHits;
   vector<int>     *mu_preor_middleOutBoundsPrecisionHits;
   vector<int>     *mu_preor_outerOutBoundsPrecisionHits;
   vector<int>     *mu_preor_extendedOutBoundsPrecisionHits;
   vector<int>     *mu_preor_combinedTrackOutBoundsPrecisionHits;
   vector<int>     *mu_preor_middleLargeHits;
   vector<int>     *mu_preor_middleSmallHits;
   vector<int>     *mu_preor_outerLargeHits;
   vector<int>     *mu_preor_outerSmallHits;
   vector<int>     *mu_preor_extendedSmallHits;
   vector<int>     *mu_preor_extendedLargeHits;
   vector<int>     *mu_preor_numberOfPixelHits;
   vector<int>     *mu_preor_numberOfPixelDeadSensors;
   vector<int>     *mu_preor_numberOfSCTHits;
   vector<int>     *mu_preor_numberOfSCTDeadSensors;
   vector<int>     *mu_preor_numberOfPixelHoles;
   vector<int>     *mu_preor_numberOfSCTHoles;
   vector<int>     *mu_preor_numberOfTRTHits;
   vector<int>     *mu_preor_numberOfTRTOutliers;
   vector<float>   *mu_preor_primarytrketa;
   vector<int>     *mu_preor_numberOfInnermostPixelLayerHits;
   vector<float>   *mu_preor_cbtrketa;
   vector<float>   *mu_preor_spectrometerFieldIntegral;
   vector<float>   *mu_preor_scatteringCurvatureSignificance;
   vector<float>   *mu_preor_scatteringNeighbourSignificance;
   vector<int>     *mu_preor_allAuthors;
   vector<int>     *mu_preor_numberOfPhiLayers;
   vector<int>     *mu_preor_numberOfPhiHoleLayers;
   vector<int>     *mu_preor_numberOfTriggerEtaLayers;
   vector<int>     *mu_preor_numberOfTriggerEtaHoleLayers;
   vector<int>     *mu_preor_nUnspoiledCscHits;
   vector<float>   *mu_preor_FSR_CandidateEnergy;
   vector<float>   *mu_preor_EnergyLoss;
   vector<float>   *mu_preor_EnergyLossSigma;
   vector<float>   *mu_preor_ParamEnergyLoss;
   vector<float>   *mu_preor_ParamEnergyLossSigmaPlus;
   vector<float>   *mu_preor_ParamEnergyLossSigmaMinus;
   vector<float>   *mu_preor_MeasEnergyLoss;
   vector<float>   *mu_preor_MeasEnergyLossSigma;
   vector<float>   *mu_preor_msInnerMatchChi2;
   vector<int>     *mu_preor_msInnerMatchDOF;
   vector<int>     *mu_preor_energyLossType;
   vector<int>     *mu_preor_phiLayer1Hits;
   vector<int>     *mu_preor_phiLayer2Hits;
   vector<int>     *mu_preor_phiLayer3Hits;
   vector<int>     *mu_preor_phiLayer4Hits;
   vector<int>     *mu_preor_etaLayer1Hits;
   vector<int>     *mu_preor_etaLayer2Hits;
   vector<int>     *mu_preor_etaLayer3Hits;
   vector<int>     *mu_preor_etaLayer4Hits;
   vector<int>     *mu_preor_phiLayer1Holes;
   vector<int>     *mu_preor_phiLayer2Holes;
   vector<int>     *mu_preor_phiLayer3Holes;
   vector<int>     *mu_preor_phiLayer4Holes;
   vector<int>     *mu_preor_etaLayer1Holes;
   vector<int>     *mu_preor_etaLayer2Holes;
   vector<int>     *mu_preor_etaLayer3Holes;
   vector<int>     *mu_preor_etaLayer4Holes;
   vector<int>     *mu_preor_phiLayer1RPCHits;
   vector<int>     *mu_preor_phiLayer2RPCHits;
   vector<int>     *mu_preor_phiLayer3RPCHits;
   vector<int>     *mu_preor_etaLayer1RPCHits;
   vector<int>     *mu_preor_etaLayer2RPCHits;
   vector<int>     *mu_preor_etaLayer3RPCHits;
   vector<int>     *mu_preor_phiLayer1RPCHoles;
   vector<int>     *mu_preor_phiLayer2RPCHoles;
   vector<int>     *mu_preor_phiLayer3RPCHoles;
   vector<int>     *mu_preor_etaLayer1RPCHoles;
   vector<int>     *mu_preor_etaLayer2RPCHoles;
   vector<int>     *mu_preor_etaLayer3RPCHoles;
   vector<int>     *mu_preor_phiLayer1TGCHits;
   vector<int>     *mu_preor_phiLayer2TGCHits;
   vector<int>     *mu_preor_phiLayer3TGCHits;
   vector<int>     *mu_preor_phiLayer4TGCHits;
   vector<int>     *mu_preor_etaLayer1TGCHits;
   vector<int>     *mu_preor_etaLayer2TGCHits;
   vector<int>     *mu_preor_etaLayer3TGCHits;
   vector<int>     *mu_preor_etaLayer4TGCHits;
   vector<int>     *mu_preor_phiLayer1TGCHoles;
   vector<int>     *mu_preor_phiLayer2TGCHoles;
   vector<int>     *mu_preor_phiLayer3TGCHoles;
   vector<int>     *mu_preor_phiLayer4TGCHoles;
   vector<int>     *mu_preor_etaLayer1TGCHoles;
   vector<int>     *mu_preor_etaLayer2TGCHoles;
   vector<int>     *mu_preor_etaLayer3TGCHoles;
   vector<int>     *mu_preor_etaLayer4TGCHoles;
   vector<float>   *mu_preor_segmentDeltaEta;
   vector<float>   *mu_preor_segmentDeltaPhi;
   vector<float>   *mu_preor_segmentChi2OverDoF;
   vector<float>   *mu_preor_t0;
   vector<float>   *mu_preor_beta;
   vector<float>   *mu_preor_meanDeltaADCCountsMDT;
   vector<int>     *mu_preor_msOuterMatchDOF;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<float>   *el_SF;
   vector<float>   *el_SF_iso;
   vector<float>   *el_SF_trigger;
   vector<float>   *el_SF_tot;
   vector<float>   *el_SF_tot_isotight;
   vector<int>     *el_isotool_pass_loosetrackonly;
   vector<int>     *el_isotool_pass_fixedcuttighttrackonly;
   vector<int>     *el_isotool_pass_fixedcuttight;
   vector<float>   *el_m;
   vector<float>   *el_e;
   vector<int>     *el_isGoodOQ;
   vector<int>     *el_passLHID;
   vector<float>   *el_charge;
   vector<float>   *el_id_pt;
   vector<float>   *el_id_eta;
   vector<float>   *el_id_phi;
   vector<float>   *el_id_m;
   vector<float>   *el_cl_pt;
   vector<float>   *el_cl_eta;
   vector<float>   *el_cl_etaBE2;
   vector<float>   *el_cl_phi;
   vector<float>   *el_cl_m;
   vector<float>   *el_ptcone20;
   vector<float>   *el_ptvarcone20;
   vector<float>   *el_etcone20;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptcone30;
   vector<float>   *el_ptvarcone30;
   vector<float>   *el_etcone30;
   vector<float>   *el_topoetcone30;
   vector<float>   *el_ptcone40;
   vector<float>   *el_ptvarcone40;
   vector<float>   *el_etcone40;
   vector<float>   *el_topoetcone40;
   vector<float>   *el_d0;
   vector<float>   *el_d0sig;
   vector<float>   *el_z0;
   vector<float>   *el_z0sig;
   vector<float>   *el_demaxs1;
   vector<float>   *el_fside;
   vector<float>   *el_weta2;
   vector<float>   *el_ws3;
   vector<float>   *el_eratio;
   vector<float>   *el_reta;
   vector<float>   *el_rphi;
   vector<float>   *el_time_cl;
   vector<float>   *el_time_maxEcell;
   vector<float>   *el_truth_pt;
   vector<float>   *el_truth_eta;
   vector<float>   *el_truth_phi;
   vector<float>   *el_truth_E;
   vector<int>     *el_author;
   vector<int>     *el_isConv;
   vector<int>     *el_passChID;
   vector<float>   *el_ecisBDT;
   vector<int>     *el_truth_matched;
   vector<int>     *el_truth_mothertype;
   vector<int>     *el_truth_status;
   vector<int>     *el_truth_type;
   vector<int>     *el_truth_typebkg;
   vector<int>     *el_truth_origin;
   vector<int>     *el_truth_originbkg;
   vector<int>     *el_isotool_pass_loose;
   vector<int>     *el_isotool_pass_gradient;
   vector<int>     *el_isotool_pass_gradientloose;
   vector<float>   *el_met_nomuon_dphi;
   vector<float>   *el_met_wmuon_dphi;
   vector<float>   *el_met_noelectron_dphi;
   vector<float>   *el_preor_pt;
   vector<float>   *el_preor_eta;
   vector<float>   *el_preor_phi;
   vector<float>   *el_preor_SF;
   vector<float>   *el_preor_SF_iso;
   vector<float>   *el_preor_SF_trigger;
   vector<float>   *el_preor_SF_tot;
   vector<float>   *el_preor_SF_tot_isotight;
   vector<int>     *el_preor_isotool_pass_loosetrackonly;
   vector<int>     *el_preor_isotool_pass_fixedcuttighttrackonly;
   vector<int>     *el_preor_isotool_pass_fixedcuttight;
   vector<float>   *el_preor_m;
   vector<float>   *el_preor_e;
   vector<int>     *el_preor_isGoodOQ;
   vector<int>     *el_preor_passLHID;
   vector<float>   *el_preor_charge;
   vector<float>   *el_preor_id_pt;
   vector<float>   *el_preor_id_eta;
   vector<float>   *el_preor_id_phi;
   vector<float>   *el_preor_id_m;
   vector<float>   *el_preor_cl_pt;
   vector<float>   *el_preor_cl_eta;
   vector<float>   *el_preor_cl_etaBE2;
   vector<float>   *el_preor_cl_phi;
   vector<float>   *el_preor_cl_m;
   vector<float>   *el_preor_ptcone20;
   vector<float>   *el_preor_ptvarcone20;
   vector<float>   *el_preor_etcone20;
   vector<float>   *el_preor_topoetcone20;
   vector<float>   *el_preor_ptcone30;
   vector<float>   *el_preor_ptvarcone30;
   vector<float>   *el_preor_etcone30;
   vector<float>   *el_preor_topoetcone30;
   vector<float>   *el_preor_ptcone40;
   vector<float>   *el_preor_ptvarcone40;
   vector<float>   *el_preor_etcone40;
   vector<float>   *el_preor_topoetcone40;
   vector<float>   *el_preor_d0;
   vector<float>   *el_preor_d0sig;
   vector<float>   *el_preor_z0;
   vector<float>   *el_preor_z0sig;
   vector<float>   *el_preor_demaxs1;
   vector<float>   *el_preor_fside;
   vector<float>   *el_preor_weta2;
   vector<float>   *el_preor_ws3;
   vector<float>   *el_preor_eratio;
   vector<float>   *el_preor_reta;
   vector<float>   *el_preor_rphi;
   vector<float>   *el_preor_time_cl;
   vector<float>   *el_preor_time_maxEcell;
   vector<float>   *el_preor_truth_pt;
   vector<float>   *el_preor_truth_eta;
   vector<float>   *el_preor_truth_phi;
   vector<float>   *el_preor_truth_E;
   vector<int>     *el_preor_author;
   vector<int>     *el_preor_isConv;
   vector<int>     *el_preor_passChID;
   vector<float>   *el_preor_ecisBDT;
   vector<int>     *el_preor_truth_matched;
   vector<int>     *el_preor_truth_mothertype;
   vector<int>     *el_preor_truth_status;
   vector<int>     *el_preor_truth_type;
   vector<int>     *el_preor_truth_typebkg;
   vector<int>     *el_preor_truth_origin;
   vector<int>     *el_preor_truth_originbkg;
   vector<int>     *el_preor_isotool_pass_loose;
   vector<int>     *el_preor_isotool_pass_gradient;
   vector<int>     *el_preor_isotool_pass_gradientloose;
   vector<float>   *el_preor_met_nomuon_dphi;
   vector<float>   *el_preor_met_wmuon_dphi;
   vector<float>   *el_preor_met_noelectron_dphi;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_m;
   vector<float>   *jet_fmax;
   vector<float>   *jet_fch;
   vector<float>   *jet_MV2c00_discriminant;
   vector<float>   *jet_MV2c10_discriminant;
   vector<float>   *jet_MV2c20_discriminant;
   vector<int>     *jet_isbjet;
   vector<int>     *jet_PartonTruthLabelID;
   vector<int>     *jet_ConeTruthLabelID;
   vector<float>   *jet_met_nomuon_dphi;
   vector<float>   *jet_met_wmuon_dphi;
   vector<float>   *jet_met_noelectron_dphi;
   vector<float>   *jet_met_nophoton_dphi;
   vector<float>   *jet_weight;
   vector<float>   *jet_raw_pt;
   vector<float>   *jet_raw_eta;
   vector<float>   *jet_raw_phi;
   vector<float>   *jet_raw_m;
   vector<float>   *jet_timing;
   vector<float>   *jet_emfrac;
   vector<float>   *jet_hecf;
   vector<float>   *jet_hecq;
   vector<float>   *jet_larq;
   vector<float>   *jet_avglarq;
   vector<float>   *jet_negE;
   vector<float>   *jet_lambda;
   vector<float>   *jet_lambda2;
   vector<float>   *jet_jvtxf;
   vector<int>     *jet_fmaxi;
   vector<int>     *jet_isbjet_loose;
   vector<float>   *jet_jvt;
   vector<int>     *jet_cleaning;
   vector<float>   *jet_TruthLabelDeltaR_B;
   vector<float>   *jet_TruthLabelDeltaR_C;
   vector<float>   *jet_TruthLabelDeltaR_T;
   vector<float>   *jet_preor_pt;
   vector<float>   *jet_preor_eta;
   vector<float>   *jet_preor_phi;
   vector<float>   *jet_preor_m;
   vector<float>   *jet_preor_fmax;
   vector<float>   *jet_preor_fch;
   vector<float>   *jet_preor_MV2c00_discriminant;
   vector<float>   *jet_preor_MV2c10_discriminant;
   vector<float>   *jet_preor_MV2c20_discriminant;
   vector<int>     *jet_preor_isbjet;
   vector<int>     *jet_preor_PartonTruthLabelID;
   vector<int>     *jet_preor_ConeTruthLabelID;
   vector<float>   *jet_preor_met_nomuon_dphi;
   vector<float>   *jet_preor_met_wmuon_dphi;
   vector<float>   *jet_preor_met_noelectron_dphi;
   vector<float>   *jet_preor_met_nophoton_dphi;
   vector<float>   *jet_preor_weight;
   vector<float>   *jet_preor_raw_pt;
   vector<float>   *jet_preor_raw_eta;
   vector<float>   *jet_preor_raw_phi;
   vector<float>   *jet_preor_raw_m;
   vector<float>   *jet_preor_timing;
   vector<float>   *jet_preor_emfrac;
   vector<float>   *jet_preor_hecf;
   vector<float>   *jet_preor_hecq;
   vector<float>   *jet_preor_larq;
   vector<float>   *jet_preor_avglarq;
   vector<float>   *jet_preor_negE;
   vector<float>   *jet_preor_lambda;
   vector<float>   *jet_preor_lambda2;
   vector<float>   *jet_preor_jvtxf;
   vector<int>     *jet_preor_fmaxi;
   vector<int>     *jet_preor_isbjet_loose;
   vector<float>   *jet_preor_jvt;
   vector<int>     *jet_preor_cleaning;
   vector<float>   *jet_preor_TruthLabelDeltaR_B;
   vector<float>   *jet_preor_TruthLabelDeltaR_C;
   vector<float>   *jet_preor_TruthLabelDeltaR_T;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_SF;
   vector<float>   *ph_SF_iso;
   vector<int>     *ph_isotool_pass_fixedcuttightcaloonly;
   vector<int>     *ph_isotool_pass_fixedcuttight;
   vector<int>     *ph_isotool_pass_fixedcutloose;
   vector<float>   *ph_m;
   vector<float>   *ph_ptcone20;
   vector<float>   *ph_ptvarcone20;
   vector<float>   *ph_etcone20;
   vector<float>   *ph_topoetcone20;
   vector<float>   *ph_ptcone30;
   vector<float>   *ph_ptvarcone30;
   vector<float>   *ph_etcone30;
   vector<float>   *ph_topoetcone30;
   vector<float>   *ph_ptcone40;
   vector<float>   *ph_ptvarcone40;
   vector<float>   *ph_etcone40;
   vector<float>   *ph_topoetcone40;
   vector<int>     *ph_isTight;
   vector<int>     *ph_isEM;
   vector<unsigned int> *ph_OQ;
   vector<float>   *ph_Rphi;
   vector<float>   *ph_weta2;
   vector<float>   *ph_fracs1;
   vector<float>   *ph_weta1;
   vector<float>   *ph_emaxs1;
   vector<float>   *ph_f1;
   vector<float>   *ph_Reta;
   vector<float>   *ph_wtots1;
   vector<float>   *ph_Eratio;
   vector<float>   *ph_Rhad;
   vector<float>   *ph_Rhad1;
   vector<float>   *ph_e277;
   vector<float>   *ph_deltae;
   vector<int>     *ph_author;
   vector<int>     *ph_isConv;
   vector<float>   *ph_met_nomuon_dphi;
   vector<float>   *ph_met_wmuon_dphi;
   vector<float>   *ph_met_nophoton_dphi;
   vector<float>   *ph_preor_pt;
   vector<float>   *ph_preor_eta;
   vector<float>   *ph_preor_phi;
   vector<float>   *ph_preor_SF;
   vector<float>   *ph_preor_SF_iso;
   vector<int>     *ph_preor_isotool_pass_fixedcuttightcaloonly;
   vector<int>     *ph_preor_isotool_pass_fixedcuttight;
   vector<int>     *ph_preor_isotool_pass_fixedcutloose;
   vector<float>   *ph_preor_m;
   vector<float>   *ph_preor_ptcone20;
   vector<float>   *ph_preor_ptvarcone20;
   vector<float>   *ph_preor_etcone20;
   vector<float>   *ph_preor_topoetcone20;
   vector<float>   *ph_preor_ptcone30;
   vector<float>   *ph_preor_ptvarcone30;
   vector<float>   *ph_preor_etcone30;
   vector<float>   *ph_preor_topoetcone30;
   vector<float>   *ph_preor_ptcone40;
   vector<float>   *ph_preor_ptvarcone40;
   vector<float>   *ph_preor_etcone40;
   vector<float>   *ph_preor_topoetcone40;
   vector<int>     *ph_preor_isTight;
   vector<int>     *ph_preor_isEM;
   vector<unsigned int> *ph_preor_OQ;
   vector<float>   *ph_preor_Rphi;
   vector<float>   *ph_preor_weta2;
   vector<float>   *ph_preor_fracs1;
   vector<float>   *ph_preor_weta1;
   vector<float>   *ph_preor_emaxs1;
   vector<float>   *ph_preor_f1;
   vector<float>   *ph_preor_Reta;
   vector<float>   *ph_preor_wtots1;
   vector<float>   *ph_preor_Eratio;
   vector<float>   *ph_preor_Rhad;
   vector<float>   *ph_preor_Rhad1;
   vector<float>   *ph_preor_e277;
   vector<float>   *ph_preor_deltae;
   vector<int>     *ph_preor_author;
   vector<int>     *ph_preor_isConv;
   vector<float>   *ph_preor_met_nomuon_dphi;
   vector<float>   *ph_preor_met_wmuon_dphi;
   vector<float>   *ph_preor_met_nophoton_dphi;
   Int_t           hem_n;
   vector<float>   *hem_pt;
   vector<float>   *hem_eta;
   vector<float>   *hem_phi;
   vector<float>   *hem_m;
   vector<float>   *tau_pt;
   vector<float>   *tau_eta;
   vector<float>   *tau_phi;
   vector<int>     *tau_idtool_pass_loose;
   vector<int>     *tau_idtool_pass_medium;
   vector<int>     *tau_idtool_pass_tight;
   Int_t           tau_multiplicity;
   Int_t           tau_medium_multiplicity;
   Int_t           tau_tight_multiplicity;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_event;   //!
   TBranch        *b_last;   //!
   TBranch        *b_year;   //!
   TBranch        *b_n_jet;   //!
   TBranch        *b_n_jet_preor;   //!
   TBranch        *b_n_mu_preor;   //!
   TBranch        *b_n_el_preor;   //!
   TBranch        *b_n_ph_preor;   //!
   TBranch        *b_n_bjet;   //!
   TBranch        *b_n_el;   //!
   TBranch        *b_n_el_baseline;   //!
   TBranch        *b_n_mu_baseline;   //!
   TBranch        *b_n_mu_baseline_bad;   //!
   TBranch        *b_n_allmu_bad;   //!
   TBranch        *b_n_mu;   //!
   TBranch        *b_munu_mT;   //!
   TBranch        *b_enu_mT;   //!
   TBranch        *b_mumu_m;   //!
   TBranch        *b_ee_m;   //!
   TBranch        *b_sh22_weight;   //!
   TBranch        *b_mconly_weight;   //!
   TBranch        *b_mconly_weights;   //!
   TBranch        *b_kF_weight;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_geneff;   //!
   TBranch        *b_pu_weight;   //!
   TBranch        *b_btag_weight;   //!
   TBranch        *b_jvt_weight;   //!
   TBranch        *b_jvt_all_weight;   //!
   TBranch        *b_truth_W_decay;   //!
   TBranch        *b_GenFiltMet;   //!
   TBranch        *b_newSH_weight;   //!
   TBranch        *b_n_truthTop;   //!
   TBranch        *b_passTSTCleaning;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_corAverageIntPerXing;   //!
   TBranch        *b_n_vx;   //!
   TBranch        *b_pu_hash;   //!
   TBranch        *b_mu_SF_tot;   //!
   TBranch        *b_trigger_matched_electron;   //!
   TBranch        *b_trigger_matched_muon;   //!
   TBranch        *b_trigger_HLT_2e17_lhloose;   //!
   TBranch        *b_trigger_HLT_2e17_lhvloose;   //!
   TBranch        *b_trigger_HLT_2e17_lhvloose_nod0;   //!
   TBranch        *b_trigger_HLT_2mu14;   //!
   TBranch        *b_trigger_HLT_e120_lhloose;   //!
   TBranch        *b_trigger_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_L1EM20VHI;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_nod0_L1EM20VH;   //!
   TBranch        *b_trigger_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_trigger_HLT_e26_lhtight_ivarloose;   //!
   TBranch        *b_trigger_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_trigger_HLT_e60_lhmedium;   //!
   TBranch        *b_trigger_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_trigger_HLT_e60_medium;   //!
   TBranch        *b_trigger_HLT_mu20_2mu4noL1;   //!
   TBranch        *b_trigger_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu22_mu8noL1;   //!
   TBranch        *b_trigger_HLT_mu24_ivarmedium;   //!
   TBranch        *b_trigger_HLT_mu26_imedium;   //!
   TBranch        *b_trigger_HLT_mu26_ivarmedium;   //!
   TBranch        *b_trigger_HLT_mu40;   //!
   TBranch        *b_trigger_HLT_mu50;   //!
   TBranch        *b_weight_VJetsEW_Nominal;   //!
   TBranch        *b_weight_VJetsEW_vjets_d1K_NLO_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_d1K_NLO_Low;   //!
   TBranch        *b_weight_VJetsEW_vjets_d1kappa_EW_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_d1kappa_EW_Low;   //!
   TBranch        *b_weight_VJetsEW_vjets_d2K_NLO_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_d2K_NLO_Low;   //!
   TBranch        *b_weight_VJetsEW_vjets_d2kappa_EW_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_d2kappa_EW_Low;   //!
   TBranch        *b_weight_VJetsEW_vjets_d3K_NLO_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_d3K_NLO_Low;   //!
   TBranch        *b_weight_VJetsEW_vjets_d3kappa_EW_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_d3kappa_EW_Low;   //!
   TBranch        *b_weight_VJetsEW_vjets_dK_NLO_fix_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_dK_NLO_fix_Low;   //!
   TBranch        *b_weight_VJetsEW_vjets_dK_NLO_mix_High;   //!
   TBranch        *b_weight_VJetsEW_vjets_dK_NLO_mix_Low;   //!
   TBranch        *b_trigger_pass;   //!
   TBranch        *b_trigger_matched_HLT_e60_lhmedium;   //!
   TBranch        *b_trigger_matched_HLT_e120_lhloose;   //!
   TBranch        *b_trigger_matched_HLT_e24_lhmedium_L1EM18VH;   //!
   TBranch        *b_trigger_matched_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_pdf_x1;   //!
   TBranch        *b_pdf_x2;   //!
   TBranch        *b_pdf_pdf1;   //!
   TBranch        *b_pdf_pdf2;   //!
   TBranch        *b_pdf_scale;   //!
   TBranch        *b_flag_bib;   //!
   TBranch        *b_flag_bib_raw;   //!
   TBranch        *b_flag_sct;   //!
   TBranch        *b_flag_core;   //!
   TBranch        *b_trigger_HLT_2e12_lhloose_L12EM10VH;   //!
   TBranch        *b_trigger_HLT_2e12_lhvloose_nod0_L12EM10VH;   //!
   TBranch        *b_trigger_HLT_2e17_lhloose;   //!
   TBranch        *b_trigger_HLT_2e17_lhvloose;   //!
   TBranch        *b_trigger_HLT_2e17_lhvloose_nod0;   //!
   TBranch        *b_trigger_HLT_2e17_loose;   //!
   TBranch        *b_trigger_HLT_2mu10;   //!
   TBranch        *b_trigger_HLT_2mu14;   //!
   TBranch        *b_trigger_HLT_e120_lhloose_nod0;   //!
   TBranch        *b_trigger_HLT_e17_lhloose_2e9_lhloose;   //!
   TBranch        *b_trigger_HLT_e17_lhloose_nod0_2e9_lhloose_nod0;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_L1EM18VH;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_L1EM20VHI;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_iloose_L1EM20VH;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_nod0_L1EM20VH;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_nod0_ivarloose;   //!
   TBranch        *b_trigger_HLT_e24_lhtight_iloose;   //!
   TBranch        *b_trigger_HLT_e26_lhtight_ivarloose;   //!
   TBranch        *b_trigger_HLT_e28_tight_iloose;   //!
   TBranch        *b_trigger_HLT_e60_medium;   //!
   TBranch        *b_trigger_HLT_mu20_2mu4noL1;   //!
   TBranch        *b_trigger_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu20_ivarloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu20_mu8noL1;   //!
   TBranch        *b_trigger_HLT_mu22_mu8noL1;   //!
   TBranch        *b_trigger_HLT_mu24_iloose;   //!
   TBranch        *b_trigger_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu24_imedium;   //!
   TBranch        *b_trigger_HLT_mu24_ivarloose;   //!
   TBranch        *b_trigger_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu24_ivarmedium;   //!
   TBranch        *b_trigger_HLT_mu26_imedium;   //!
   TBranch        *b_trigger_HLT_mu26_ivarmedium;   //!
   TBranch        *b_trigger_HLT_mu40;   //!
   TBranch        *b_trigger_HLT_mu50;   //!
   TBranch        *b_hfor;   //!
   TBranch        *b_n_ph;   //!
   TBranch        *b_n_ph_tight;   //!
   TBranch        *b_n_ph_baseline;   //!
   TBranch        *b_n_ph_baseline_tight;   //!
   TBranch        *b_pdf_id1;   //!
   TBranch        *b_pdf_id2;   //!
   TBranch        *b_bb_decision;   //!
   TBranch        *b_shatR;   //!
   TBranch        *b_gaminvR;   //!
   TBranch        *b_gaminvRp1;   //!
   TBranch        *b_dphi_BETA_R;   //!
   TBranch        *b_dphi_J1_J2_R;   //!
   TBranch        *b_gamma_Rp1;   //!
   TBranch        *b_costhetaR;   //!
   TBranch        *b_dphi_R_Rp1;   //!
   TBranch        *b_mdeltaR;   //!
   TBranch        *b_cosptR;   //!
   TBranch        *b_costhetaRp1;   //!
   TBranch        *b_jj_m;   //!
   TBranch        *b_mumu_pt;   //!
   TBranch        *b_mumu_eta;   //!
   TBranch        *b_mumu_phi;   //!
   TBranch        *b_ee_pt;   //!
   TBranch        *b_ee_eta;   //!
   TBranch        *b_ee_phi;   //!
   TBranch        *b_n_jet_truth;   //!
   TBranch        *b_truth_jet1_pt;   //!
   TBranch        *b_truth_jet1_eta;   //!
   TBranch        *b_truth_jet1_phi;   //!
   TBranch        *b_truth_jet1_m;   //!
   TBranch        *b_truth_jet2_pt;   //!
   TBranch        *b_truth_jet2_eta;   //!
   TBranch        *b_truth_jet2_phi;   //!
   TBranch        *b_truth_jet2_m;   //!
   TBranch        *b_truth_ph1_pt;   //!
   TBranch        *b_truth_ph1_eta;   //!
   TBranch        *b_truth_ph1_phi;   //!
   TBranch        *b_truth_ph1_type;   //!
   TBranch        *b_truth_V_pt;   //!
   TBranch        *b_truth_V_eta;   //!
   TBranch        *b_truth_V_phi;   //!
   TBranch        *b_truth_V_m;   //!
   TBranch        *b_truth_mu_pt;   //!
   TBranch        *b_truth_mu_eta;   //!
   TBranch        *b_truth_mu_phi;   //!
   TBranch        *b_truth_mu_m;   //!
   TBranch        *b_truth_mu_status;   //!
   TBranch        *b_evsf_baseline_nominal_EL;   //!
   TBranch        *b_evsf_baseline_nominal_MU;   //!
   TBranch        *b_evsf_baseline_nominal_PH;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1up;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1down;   //!
   TBranch        *b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1down;   //!
   TBranch        *b_evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1up;   //!
   TBranch        *b_evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1down;   //!
   TBranch        *b_evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1up;   //!
   TBranch        *b_met_eleterm_et;   //!
   TBranch        *b_met_eleterm_etx;   //!
   TBranch        *b_met_eleterm_ety;   //!
   TBranch        *b_met_eleterm_sumet;   //!
   TBranch        *b_met_eleterm_phi;   //!
   TBranch        *b_met_jetterm_et;   //!
   TBranch        *b_met_jetterm_etx;   //!
   TBranch        *b_met_jetterm_ety;   //!
   TBranch        *b_met_jetterm_sumet;   //!
   TBranch        *b_met_jetterm_phi;   //!
   TBranch        *b_met_muonterm_et;   //!
   TBranch        *b_met_muonterm_etx;   //!
   TBranch        *b_met_muonterm_ety;   //!
   TBranch        *b_met_muonterm_sumet;   //!
   TBranch        *b_met_muonterm_phi;   //!
   TBranch        *b_met_muonterm_cst_et;   //!
   TBranch        *b_met_muonterm_cst_etx;   //!
   TBranch        *b_met_muonterm_cst_ety;   //!
   TBranch        *b_met_muonterm_cst_sumet;   //!
   TBranch        *b_met_muonterm_cst_phi;   //!
   TBranch        *b_met_muonterm_tst_et;   //!
   TBranch        *b_met_muonterm_tst_etx;   //!
   TBranch        *b_met_muonterm_tst_ety;   //!
   TBranch        *b_met_muonterm_tst_sumet;   //!
   TBranch        *b_met_muonterm_tst_phi;   //!
   TBranch        *b_met_noelectron_cst_et;   //!
   TBranch        *b_met_noelectron_cst_etx;   //!
   TBranch        *b_met_noelectron_cst_ety;   //!
   TBranch        *b_met_noelectron_cst_sumet;   //!
   TBranch        *b_met_noelectron_cst_phi;   //!
   TBranch        *b_met_noelectron_tst_et;   //!
   TBranch        *b_met_noelectron_tst_etx;   //!
   TBranch        *b_met_noelectron_tst_ety;   //!
   TBranch        *b_met_noelectron_tst_sumet;   //!
   TBranch        *b_met_noelectron_tst_phi;   //!
   TBranch        *b_met_nomuon_cst_et;   //!
   TBranch        *b_met_nomuon_cst_etx;   //!
   TBranch        *b_met_nomuon_cst_ety;   //!
   TBranch        *b_met_nomuon_cst_sumet;   //!
   TBranch        *b_met_nomuon_cst_phi;   //!
   TBranch        *b_met_nomuon_tst_et;   //!
   TBranch        *b_met_nomuon_tst_etx;   //!
   TBranch        *b_met_nomuon_tst_ety;   //!
   TBranch        *b_met_nomuon_tst_sumet;   //!
   TBranch        *b_met_nomuon_tst_phi;   //!
   TBranch        *b_met_nophcalib_nomuon_cst_et;   //!
   TBranch        *b_met_nophcalib_nomuon_cst_etx;   //!
   TBranch        *b_met_nophcalib_nomuon_cst_ety;   //!
   TBranch        *b_met_nophcalib_nomuon_cst_sumet;   //!
   TBranch        *b_met_nophcalib_nomuon_cst_phi;   //!
   TBranch        *b_met_nophcalib_nomuon_tst_et;   //!
   TBranch        *b_met_nophcalib_nomuon_tst_etx;   //!
   TBranch        *b_met_nophcalib_nomuon_tst_ety;   //!
   TBranch        *b_met_nophcalib_nomuon_tst_sumet;   //!
   TBranch        *b_met_nophcalib_nomuon_tst_phi;   //!
   TBranch        *b_met_nophcalib_wmuon_cst_et;   //!
   TBranch        *b_met_nophcalib_wmuon_cst_etx;   //!
   TBranch        *b_met_nophcalib_wmuon_cst_ety;   //!
   TBranch        *b_met_nophcalib_wmuon_cst_sumet;   //!
   TBranch        *b_met_nophcalib_wmuon_cst_phi;   //!
   TBranch        *b_met_nophcalib_wmuon_tst_et;   //!
   TBranch        *b_met_nophcalib_wmuon_tst_etx;   //!
   TBranch        *b_met_nophcalib_wmuon_tst_ety;   //!
   TBranch        *b_met_nophcalib_wmuon_tst_sumet;   //!
   TBranch        *b_met_nophcalib_wmuon_tst_phi;   //!
   TBranch        *b_met_nophoton_cst_et;   //!
   TBranch        *b_met_nophoton_cst_etx;   //!
   TBranch        *b_met_nophoton_cst_ety;   //!
   TBranch        *b_met_nophoton_cst_sumet;   //!
   TBranch        *b_met_nophoton_cst_phi;   //!
   TBranch        *b_met_nophoton_tst_et;   //!
   TBranch        *b_met_nophoton_tst_etx;   //!
   TBranch        *b_met_nophoton_tst_ety;   //!
   TBranch        *b_met_nophoton_tst_sumet;   //!
   TBranch        *b_met_nophoton_tst_phi;   //!
   TBranch        *b_met_phterm_et;   //!
   TBranch        *b_met_phterm_etx;   //!
   TBranch        *b_met_phterm_ety;   //!
   TBranch        *b_met_phterm_sumet;   //!
   TBranch        *b_met_phterm_phi;   //!
   TBranch        *b_met_softerm_cst_et;   //!
   TBranch        *b_met_softerm_cst_etx;   //!
   TBranch        *b_met_softerm_cst_ety;   //!
   TBranch        *b_met_softerm_cst_sumet;   //!
   TBranch        *b_met_softerm_cst_phi;   //!
   TBranch        *b_met_softerm_tst_et;   //!
   TBranch        *b_met_softerm_tst_etx;   //!
   TBranch        *b_met_softerm_tst_ety;   //!
   TBranch        *b_met_softerm_tst_sumet;   //!
   TBranch        *b_met_softerm_tst_phi;   //!
   TBranch        *b_met_track_et;   //!
   TBranch        *b_met_track_etx;   //!
   TBranch        *b_met_track_ety;   //!
   TBranch        *b_met_track_sumet;   //!
   TBranch        *b_met_track_phi;   //!
   TBranch        *b_met_truth_et;   //!
   TBranch        *b_met_truth_etx;   //!
   TBranch        *b_met_truth_ety;   //!
   TBranch        *b_met_truth_sumet;   //!
   TBranch        *b_met_truth_phi;   //!
   TBranch        *b_met_wmuon_cst_et;   //!
   TBranch        *b_met_wmuon_cst_etx;   //!
   TBranch        *b_met_wmuon_cst_ety;   //!
   TBranch        *b_met_wmuon_cst_sumet;   //!
   TBranch        *b_met_wmuon_cst_phi;   //!
   TBranch        *b_met_wmuon_tst_et;   //!
   TBranch        *b_met_wmuon_tst_etx;   //!
   TBranch        *b_met_wmuon_tst_ety;   //!
   TBranch        *b_met_wmuon_tst_sumet;   //!
   TBranch        *b_met_wmuon_tst_phi;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_ptErr;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_SF;   //!
   TBranch        *b_mu_SF_iso;   //!
   TBranch        *b_mu_isotool_pass_loosetrackonly;   //!
   TBranch        *b_mu_isotool_pass_fixedcuttighttrackonly;   //!
   TBranch        *b_mu_m;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_d0;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_z0;   //!
   TBranch        *b_mu_z0sig;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_id_pt;   //!
   TBranch        *b_mu_id_ptErr;   //!
   TBranch        *b_mu_id_eta;   //!
   TBranch        *b_mu_id_phi;   //!
   TBranch        *b_mu_id_m;   //!
   TBranch        *b_mu_id_qop;   //!
   TBranch        *b_mu_id_qoperr;   //!
   TBranch        *b_mu_ms_pt;   //!
   TBranch        *b_mu_ms_ptErr;   //!
   TBranch        *b_mu_ms_eta;   //!
   TBranch        *b_mu_ms_phi;   //!
   TBranch        *b_mu_ms_m;   //!
   TBranch        *b_mu_ms_qop;   //!
   TBranch        *b_mu_ms_qoperr;   //!
   TBranch        *b_mu_me_pt;   //!
   TBranch        *b_mu_me_ptErr;   //!
   TBranch        *b_mu_me_eta;   //!
   TBranch        *b_mu_me_phi;   //!
   TBranch        *b_mu_me_m;   //!
   TBranch        *b_mu_me_qop;   //!
   TBranch        *b_mu_me_qoperr;   //!
   TBranch        *b_mu_mse_pt;   //!
   TBranch        *b_mu_mse_ptErr;   //!
   TBranch        *b_mu_mse_eta;   //!
   TBranch        *b_mu_mse_phi;   //!
   TBranch        *b_mu_mse_m;   //!
   TBranch        *b_mu_mse_qop;   //!
   TBranch        *b_mu_mse_qoperr;   //!
   TBranch        *b_mu_ptcone20;   //!
   TBranch        *b_mu_ptvarcone20;   //!
   TBranch        *b_mu_etcone20;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptcone30;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_etcone30;   //!
   TBranch        *b_mu_topoetcone30;   //!
   TBranch        *b_mu_ptcone40;   //!
   TBranch        *b_mu_ptvarcone40;   //!
   TBranch        *b_mu_etcone40;   //!
   TBranch        *b_mu_topoetcone40;   //!
   TBranch        *b_mu_author;   //!
   TBranch        *b_mu_quality;   //!
   TBranch        *b_mu_isSA;   //!
   TBranch        *b_mu_isST;   //!
   TBranch        *b_mu_isCB;   //!
   TBranch        *b_mu_isTight;   //!
   TBranch        *b_mu_isMedium;   //!
   TBranch        *b_mu_isLoose;   //!
   TBranch        *b_mu_isVeryLoose;   //!
   TBranch        *b_mu_isHighPt;   //!
   TBranch        *b_mu_isBad;   //!
   TBranch        *b_mu_isBadHighPt;   //!
   TBranch        *b_mu_finalFitPt;   //!
   TBranch        *b_mu_finalFitPtErr;   //!
   TBranch        *b_mu_finalFitEta;   //!
   TBranch        *b_mu_finalFitPhi;   //!
   TBranch        *b_mu_finalFitQOverP;   //!
   TBranch        *b_mu_finalFitQOverPErr;   //!
   TBranch        *b_mu_truthEta;   //!
   TBranch        *b_mu_truthPhi;   //!
   TBranch        *b_mu_truthPt;   //!
   TBranch        *b_mu_truthTheta;   //!
   TBranch        *b_mu_truthQOverP;   //!
   TBranch        *b_mu_isotool_pass_loose;   //!
   TBranch        *b_mu_isotool_pass_gradient;   //!
   TBranch        *b_mu_isotool_pass_gradientloose;   //!
   TBranch        *b_mu_met_nomuon_dphi;   //!
   TBranch        *b_mu_met_wmuon_dphi;   //!
   TBranch        *b_mu_truth_origin;   //!
   TBranch        *b_mu_truth_type;   //!
   TBranch        *b_mu_muonType;   //!
   TBranch        *b_mu_momentumBalanceSignificance;   //!
   TBranch        *b_mu_CaloLRLikelihood;   //!
   TBranch        *b_mu_CaloMuonIDTag;   //!
   TBranch        *b_mu_numberOfPrecisionLayers;   //!
   TBranch        *b_mu_numberOfPrecisionHoleLayers;   //!
   TBranch        *b_mu_numberOfGoodPrecisionLayers;   //!
   TBranch        *b_mu_innerSmallHits;   //!
   TBranch        *b_mu_innerLargeHits;   //!
   TBranch        *b_mu_innerSmallHoles;   //!
   TBranch        *b_mu_innerLargeHoles;   //!
   TBranch        *b_mu_middleSmallHoles;   //!
   TBranch        *b_mu_middleLargeHoles;   //!
   TBranch        *b_mu_outerSmallHoles;   //!
   TBranch        *b_mu_outerLargeHoles;   //!
   TBranch        *b_mu_extendedSmallHoles;   //!
   TBranch        *b_mu_extendedLargeHoles;   //!
   TBranch        *b_mu_innerClosePrecisionHits;   //!
   TBranch        *b_mu_middleClosePrecisionHits;   //!
   TBranch        *b_mu_outerClosePrecisionHits;   //!
   TBranch        *b_mu_extendedClosePrecisionHits;   //!
   TBranch        *b_mu_innerOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_middleOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_outerOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_extendedOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_combinedTrackOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_middleLargeHits;   //!
   TBranch        *b_mu_middleSmallHits;   //!
   TBranch        *b_mu_outerLargeHits;   //!
   TBranch        *b_mu_outerSmallHits;   //!
   TBranch        *b_mu_extendedSmallHits;   //!
   TBranch        *b_mu_extendedLargeHits;   //!
   TBranch        *b_mu_numberOfPixelHits;   //!
   TBranch        *b_mu_numberOfPixelDeadSensors;   //!
   TBranch        *b_mu_numberOfSCTHits;   //!
   TBranch        *b_mu_numberOfSCTDeadSensors;   //!
   TBranch        *b_mu_numberOfPixelHoles;   //!
   TBranch        *b_mu_numberOfSCTHoles;   //!
   TBranch        *b_mu_numberOfTRTHits;   //!
   TBranch        *b_mu_numberOfTRTOutliers;   //!
   TBranch        *b_mu_primarytrketa;   //!
   TBranch        *b_mu_numberOfInnermostPixelLayerHits;   //!
   TBranch        *b_mu_cbtrketa;   //!
   TBranch        *b_mu_spectrometerFieldIntegral;   //!
   TBranch        *b_mu_scatteringCurvatureSignificance;   //!
   TBranch        *b_mu_scatteringNeighbourSignificance;   //!
   TBranch        *b_mu_allAuthors;   //!
   TBranch        *b_mu_numberOfPhiLayers;   //!
   TBranch        *b_mu_numberOfPhiHoleLayers;   //!
   TBranch        *b_mu_numberOfTriggerEtaLayers;   //!
   TBranch        *b_mu_numberOfTriggerEtaHoleLayers;   //!
   TBranch        *b_mu_nUnspoiledCscHits;   //!
   TBranch        *b_mu_FSR_CandidateEnergy;   //!
   TBranch        *b_mu_EnergyLoss;   //!
   TBranch        *b_mu_EnergyLossSigma;   //!
   TBranch        *b_mu_ParamEnergyLoss;   //!
   TBranch        *b_mu_ParamEnergyLossSigmaPlus;   //!
   TBranch        *b_mu_ParamEnergyLossSigmaMinus;   //!
   TBranch        *b_mu_MeasEnergyLoss;   //!
   TBranch        *b_mu_MeasEnergyLossSigma;   //!
   TBranch        *b_mu_msInnerMatchChi2;   //!
   TBranch        *b_mu_msInnerMatchDOF;   //!
   TBranch        *b_mu_energyLossType;   //!
   TBranch        *b_mu_phiLayer1Hits;   //!
   TBranch        *b_mu_phiLayer2Hits;   //!
   TBranch        *b_mu_phiLayer3Hits;   //!
   TBranch        *b_mu_phiLayer4Hits;   //!
   TBranch        *b_mu_etaLayer1Hits;   //!
   TBranch        *b_mu_etaLayer2Hits;   //!
   TBranch        *b_mu_etaLayer3Hits;   //!
   TBranch        *b_mu_etaLayer4Hits;   //!
   TBranch        *b_mu_phiLayer1Holes;   //!
   TBranch        *b_mu_phiLayer2Holes;   //!
   TBranch        *b_mu_phiLayer3Holes;   //!
   TBranch        *b_mu_phiLayer4Holes;   //!
   TBranch        *b_mu_etaLayer1Holes;   //!
   TBranch        *b_mu_etaLayer2Holes;   //!
   TBranch        *b_mu_etaLayer3Holes;   //!
   TBranch        *b_mu_etaLayer4Holes;   //!
   TBranch        *b_mu_phiLayer1RPCHits;   //!
   TBranch        *b_mu_phiLayer2RPCHits;   //!
   TBranch        *b_mu_phiLayer3RPCHits;   //!
   TBranch        *b_mu_etaLayer1RPCHits;   //!
   TBranch        *b_mu_etaLayer2RPCHits;   //!
   TBranch        *b_mu_etaLayer3RPCHits;   //!
   TBranch        *b_mu_phiLayer1RPCHoles;   //!
   TBranch        *b_mu_phiLayer2RPCHoles;   //!
   TBranch        *b_mu_phiLayer3RPCHoles;   //!
   TBranch        *b_mu_etaLayer1RPCHoles;   //!
   TBranch        *b_mu_etaLayer2RPCHoles;   //!
   TBranch        *b_mu_etaLayer3RPCHoles;   //!
   TBranch        *b_mu_phiLayer1TGCHits;   //!
   TBranch        *b_mu_phiLayer2TGCHits;   //!
   TBranch        *b_mu_phiLayer3TGCHits;   //!
   TBranch        *b_mu_phiLayer4TGCHits;   //!
   TBranch        *b_mu_etaLayer1TGCHits;   //!
   TBranch        *b_mu_etaLayer2TGCHits;   //!
   TBranch        *b_mu_etaLayer3TGCHits;   //!
   TBranch        *b_mu_etaLayer4TGCHits;   //!
   TBranch        *b_mu_phiLayer1TGCHoles;   //!
   TBranch        *b_mu_phiLayer2TGCHoles;   //!
   TBranch        *b_mu_phiLayer3TGCHoles;   //!
   TBranch        *b_mu_phiLayer4TGCHoles;   //!
   TBranch        *b_mu_etaLayer1TGCHoles;   //!
   TBranch        *b_mu_etaLayer2TGCHoles;   //!
   TBranch        *b_mu_etaLayer3TGCHoles;   //!
   TBranch        *b_mu_etaLayer4TGCHoles;   //!
   TBranch        *b_mu_segmentDeltaEta;   //!
   TBranch        *b_mu_segmentDeltaPhi;   //!
   TBranch        *b_mu_segmentChi2OverDoF;   //!
   TBranch        *b_mu_t0;   //!
   TBranch        *b_mu_beta;   //!
   TBranch        *b_mu_meanDeltaADCCountsMDT;   //!
   TBranch        *b_mu_msOuterMatchDOF;   //!
   TBranch        *b_mu_preor_pt;   //!
   TBranch        *b_mu_preor_ptErr;   //!
   TBranch        *b_mu_preor_eta;   //!
   TBranch        *b_mu_preor_phi;   //!
   TBranch        *b_mu_preor_SF;   //!
   TBranch        *b_mu_preor_SF_iso;   //!
   TBranch        *b_mu_preor_isotool_pass_loosetrackonly;   //!
   TBranch        *b_mu_preor_isotool_pass_fixedcuttighttrackonly;   //!
   TBranch        *b_mu_preor_m;   //!
   TBranch        *b_mu_preor_e;   //!
   TBranch        *b_mu_preor_d0;   //!
   TBranch        *b_mu_preor_d0sig;   //!
   TBranch        *b_mu_preor_z0;   //!
   TBranch        *b_mu_preor_z0sig;   //!
   TBranch        *b_mu_preor_charge;   //!
   TBranch        *b_mu_preor_id_pt;   //!
   TBranch        *b_mu_preor_id_ptErr;   //!
   TBranch        *b_mu_preor_id_eta;   //!
   TBranch        *b_mu_preor_id_phi;   //!
   TBranch        *b_mu_preor_id_m;   //!
   TBranch        *b_mu_preor_id_qop;   //!
   TBranch        *b_mu_preor_id_qoperr;   //!
   TBranch        *b_mu_preor_ms_pt;   //!
   TBranch        *b_mu_preor_ms_ptErr;   //!
   TBranch        *b_mu_preor_ms_eta;   //!
   TBranch        *b_mu_preor_ms_phi;   //!
   TBranch        *b_mu_preor_ms_m;   //!
   TBranch        *b_mu_preor_ms_qop;   //!
   TBranch        *b_mu_preor_ms_qoperr;   //!
   TBranch        *b_mu_preor_me_pt;   //!
   TBranch        *b_mu_preor_me_ptErr;   //!
   TBranch        *b_mu_preor_me_eta;   //!
   TBranch        *b_mu_preor_me_phi;   //!
   TBranch        *b_mu_preor_me_m;   //!
   TBranch        *b_mu_preor_me_qop;   //!
   TBranch        *b_mu_preor_me_qoperr;   //!
   TBranch        *b_mu_preor_mse_pt;   //!
   TBranch        *b_mu_preor_mse_ptErr;   //!
   TBranch        *b_mu_preor_mse_eta;   //!
   TBranch        *b_mu_preor_mse_phi;   //!
   TBranch        *b_mu_preor_mse_m;   //!
   TBranch        *b_mu_preor_mse_qop;   //!
   TBranch        *b_mu_preor_mse_qoperr;   //!
   TBranch        *b_mu_preor_ptcone20;   //!
   TBranch        *b_mu_preor_ptvarcone20;   //!
   TBranch        *b_mu_preor_etcone20;   //!
   TBranch        *b_mu_preor_topoetcone20;   //!
   TBranch        *b_mu_preor_ptcone30;   //!
   TBranch        *b_mu_preor_ptvarcone30;   //!
   TBranch        *b_mu_preor_etcone30;   //!
   TBranch        *b_mu_preor_topoetcone30;   //!
   TBranch        *b_mu_preor_ptcone40;   //!
   TBranch        *b_mu_preor_ptvarcone40;   //!
   TBranch        *b_mu_preor_etcone40;   //!
   TBranch        *b_mu_preor_topoetcone40;   //!
   TBranch        *b_mu_preor_author;   //!
   TBranch        *b_mu_preor_quality;   //!
   TBranch        *b_mu_preor_isSA;   //!
   TBranch        *b_mu_preor_isST;   //!
   TBranch        *b_mu_preor_isCB;   //!
   TBranch        *b_mu_preor_isTight;   //!
   TBranch        *b_mu_preor_isMedium;   //!
   TBranch        *b_mu_preor_isLoose;   //!
   TBranch        *b_mu_preor_isVeryLoose;   //!
   TBranch        *b_mu_preor_isHighPt;   //!
   TBranch        *b_mu_preor_isBad;   //!
   TBranch        *b_mu_preor_isBadHighPt;   //!
   TBranch        *b_mu_preor_finalFitPt;   //!
   TBranch        *b_mu_preor_finalFitPtErr;   //!
   TBranch        *b_mu_preor_finalFitEta;   //!
   TBranch        *b_mu_preor_finalFitPhi;   //!
   TBranch        *b_mu_preor_finalFitQOverP;   //!
   TBranch        *b_mu_preor_finalFitQOverPErr;   //!
   TBranch        *b_mu_preor_truthEta;   //!
   TBranch        *b_mu_preor_truthPhi;   //!
   TBranch        *b_mu_preor_truthPt;   //!
   TBranch        *b_mu_preor_truthTheta;   //!
   TBranch        *b_mu_preor_truthQOverP;   //!
   TBranch        *b_mu_preor_isotool_pass_loose;   //!
   TBranch        *b_mu_preor_isotool_pass_gradient;   //!
   TBranch        *b_mu_preor_isotool_pass_gradientloose;   //!
   TBranch        *b_mu_preor_met_nomuon_dphi;   //!
   TBranch        *b_mu_preor_met_wmuon_dphi;   //!
   TBranch        *b_mu_preor_truth_origin;   //!
   TBranch        *b_mu_preor_truth_type;   //!
   TBranch        *b_mu_preor_muonType;   //!
   TBranch        *b_mu_preor_momentumBalanceSignificance;   //!
   TBranch        *b_mu_preor_CaloLRLikelihood;   //!
   TBranch        *b_mu_preor_CaloMuonIDTag;   //!
   TBranch        *b_mu_preor_numberOfPrecisionLayers;   //!
   TBranch        *b_mu_preor_numberOfPrecisionHoleLayers;   //!
   TBranch        *b_mu_preor_numberOfGoodPrecisionLayers;   //!
   TBranch        *b_mu_preor_innerSmallHits;   //!
   TBranch        *b_mu_preor_innerLargeHits;   //!
   TBranch        *b_mu_preor_innerSmallHoles;   //!
   TBranch        *b_mu_preor_innerLargeHoles;   //!
   TBranch        *b_mu_preor_middleSmallHoles;   //!
   TBranch        *b_mu_preor_middleLargeHoles;   //!
   TBranch        *b_mu_preor_outerSmallHoles;   //!
   TBranch        *b_mu_preor_outerLargeHoles;   //!
   TBranch        *b_mu_preor_extendedSmallHoles;   //!
   TBranch        *b_mu_preor_extendedLargeHoles;   //!
   TBranch        *b_mu_preor_innerClosePrecisionHits;   //!
   TBranch        *b_mu_preor_middleClosePrecisionHits;   //!
   TBranch        *b_mu_preor_outerClosePrecisionHits;   //!
   TBranch        *b_mu_preor_extendedClosePrecisionHits;   //!
   TBranch        *b_mu_preor_innerOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_preor_middleOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_preor_outerOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_preor_extendedOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_preor_combinedTrackOutBoundsPrecisionHits;   //!
   TBranch        *b_mu_preor_middleLargeHits;   //!
   TBranch        *b_mu_preor_middleSmallHits;   //!
   TBranch        *b_mu_preor_outerLargeHits;   //!
   TBranch        *b_mu_preor_outerSmallHits;   //!
   TBranch        *b_mu_preor_extendedSmallHits;   //!
   TBranch        *b_mu_preor_extendedLargeHits;   //!
   TBranch        *b_mu_preor_numberOfPixelHits;   //!
   TBranch        *b_mu_preor_numberOfPixelDeadSensors;   //!
   TBranch        *b_mu_preor_numberOfSCTHits;   //!
   TBranch        *b_mu_preor_numberOfSCTDeadSensors;   //!
   TBranch        *b_mu_preor_numberOfPixelHoles;   //!
   TBranch        *b_mu_preor_numberOfSCTHoles;   //!
   TBranch        *b_mu_preor_numberOfTRTHits;   //!
   TBranch        *b_mu_preor_numberOfTRTOutliers;   //!
   TBranch        *b_mu_preor_primarytrketa;   //!
   TBranch        *b_mu_preor_numberOfInnermostPixelLayerHits;   //!
   TBranch        *b_mu_preor_cbtrketa;   //!
   TBranch        *b_mu_preor_spectrometerFieldIntegral;   //!
   TBranch        *b_mu_preor_scatteringCurvatureSignificance;   //!
   TBranch        *b_mu_preor_scatteringNeighbourSignificance;   //!
   TBranch        *b_mu_preor_allAuthors;   //!
   TBranch        *b_mu_preor_numberOfPhiLayers;   //!
   TBranch        *b_mu_preor_numberOfPhiHoleLayers;   //!
   TBranch        *b_mu_preor_numberOfTriggerEtaLayers;   //!
   TBranch        *b_mu_preor_numberOfTriggerEtaHoleLayers;   //!
   TBranch        *b_mu_preor_nUnspoiledCscHits;   //!
   TBranch        *b_mu_preor_FSR_CandidateEnergy;   //!
   TBranch        *b_mu_preor_EnergyLoss;   //!
   TBranch        *b_mu_preor_EnergyLossSigma;   //!
   TBranch        *b_mu_preor_ParamEnergyLoss;   //!
   TBranch        *b_mu_preor_ParamEnergyLossSigmaPlus;   //!
   TBranch        *b_mu_preor_ParamEnergyLossSigmaMinus;   //!
   TBranch        *b_mu_preor_MeasEnergyLoss;   //!
   TBranch        *b_mu_preor_MeasEnergyLossSigma;   //!
   TBranch        *b_mu_preor_msInnerMatchChi2;   //!
   TBranch        *b_mu_preor_msInnerMatchDOF;   //!
   TBranch        *b_mu_preor_energyLossType;   //!
   TBranch        *b_mu_preor_phiLayer1Hits;   //!
   TBranch        *b_mu_preor_phiLayer2Hits;   //!
   TBranch        *b_mu_preor_phiLayer3Hits;   //!
   TBranch        *b_mu_preor_phiLayer4Hits;   //!
   TBranch        *b_mu_preor_etaLayer1Hits;   //!
   TBranch        *b_mu_preor_etaLayer2Hits;   //!
   TBranch        *b_mu_preor_etaLayer3Hits;   //!
   TBranch        *b_mu_preor_etaLayer4Hits;   //!
   TBranch        *b_mu_preor_phiLayer1Holes;   //!
   TBranch        *b_mu_preor_phiLayer2Holes;   //!
   TBranch        *b_mu_preor_phiLayer3Holes;   //!
   TBranch        *b_mu_preor_phiLayer4Holes;   //!
   TBranch        *b_mu_preor_etaLayer1Holes;   //!
   TBranch        *b_mu_preor_etaLayer2Holes;   //!
   TBranch        *b_mu_preor_etaLayer3Holes;   //!
   TBranch        *b_mu_preor_etaLayer4Holes;   //!
   TBranch        *b_mu_preor_phiLayer1RPCHits;   //!
   TBranch        *b_mu_preor_phiLayer2RPCHits;   //!
   TBranch        *b_mu_preor_phiLayer3RPCHits;   //!
   TBranch        *b_mu_preor_etaLayer1RPCHits;   //!
   TBranch        *b_mu_preor_etaLayer2RPCHits;   //!
   TBranch        *b_mu_preor_etaLayer3RPCHits;   //!
   TBranch        *b_mu_preor_phiLayer1RPCHoles;   //!
   TBranch        *b_mu_preor_phiLayer2RPCHoles;   //!
   TBranch        *b_mu_preor_phiLayer3RPCHoles;   //!
   TBranch        *b_mu_preor_etaLayer1RPCHoles;   //!
   TBranch        *b_mu_preor_etaLayer2RPCHoles;   //!
   TBranch        *b_mu_preor_etaLayer3RPCHoles;   //!
   TBranch        *b_mu_preor_phiLayer1TGCHits;   //!
   TBranch        *b_mu_preor_phiLayer2TGCHits;   //!
   TBranch        *b_mu_preor_phiLayer3TGCHits;   //!
   TBranch        *b_mu_preor_phiLayer4TGCHits;   //!
   TBranch        *b_mu_preor_etaLayer1TGCHits;   //!
   TBranch        *b_mu_preor_etaLayer2TGCHits;   //!
   TBranch        *b_mu_preor_etaLayer3TGCHits;   //!
   TBranch        *b_mu_preor_etaLayer4TGCHits;   //!
   TBranch        *b_mu_preor_phiLayer1TGCHoles;   //!
   TBranch        *b_mu_preor_phiLayer2TGCHoles;   //!
   TBranch        *b_mu_preor_phiLayer3TGCHoles;   //!
   TBranch        *b_mu_preor_phiLayer4TGCHoles;   //!
   TBranch        *b_mu_preor_etaLayer1TGCHoles;   //!
   TBranch        *b_mu_preor_etaLayer2TGCHoles;   //!
   TBranch        *b_mu_preor_etaLayer3TGCHoles;   //!
   TBranch        *b_mu_preor_etaLayer4TGCHoles;   //!
   TBranch        *b_mu_preor_segmentDeltaEta;   //!
   TBranch        *b_mu_preor_segmentDeltaPhi;   //!
   TBranch        *b_mu_preor_segmentChi2OverDoF;   //!
   TBranch        *b_mu_preor_t0;   //!
   TBranch        *b_mu_preor_beta;   //!
   TBranch        *b_mu_preor_meanDeltaADCCountsMDT;   //!
   TBranch        *b_mu_preor_msOuterMatchDOF;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_SF;   //!
   TBranch        *b_el_SF_iso;   //!
   TBranch        *b_el_SF_trigger;   //!
   TBranch        *b_el_SF_tot;   //!
   TBranch        *b_el_SF_tot_isotight;   //!
   TBranch        *b_el_isotool_pass_loosetrackonly;   //!
   TBranch        *b_el_isotool_pass_fixedcuttighttrackonly;   //!
   TBranch        *b_el_isotool_pass_fixedcuttight;   //!
   TBranch        *b_el_m;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_isGoodOQ;   //!
   TBranch        *b_el_passLHID;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_id_pt;   //!
   TBranch        *b_el_id_eta;   //!
   TBranch        *b_el_id_phi;   //!
   TBranch        *b_el_id_m;   //!
   TBranch        *b_el_cl_pt;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_cl_etaBE2;   //!
   TBranch        *b_el_cl_phi;   //!
   TBranch        *b_el_cl_m;   //!
   TBranch        *b_el_ptcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_etcone20;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptcone30;   //!
   TBranch        *b_el_ptvarcone30;   //!
   TBranch        *b_el_etcone30;   //!
   TBranch        *b_el_topoetcone30;   //!
   TBranch        *b_el_ptcone40;   //!
   TBranch        *b_el_ptvarcone40;   //!
   TBranch        *b_el_etcone40;   //!
   TBranch        *b_el_topoetcone40;   //!
   TBranch        *b_el_d0;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_z0;   //!
   TBranch        *b_el_z0sig;   //!
   TBranch        *b_el_demaxs1;   //!
   TBranch        *b_el_fside;   //!
   TBranch        *b_el_weta2;   //!
   TBranch        *b_el_ws3;   //!
   TBranch        *b_el_eratio;   //!
   TBranch        *b_el_reta;   //!
   TBranch        *b_el_rphi;   //!
   TBranch        *b_el_time_cl;   //!
   TBranch        *b_el_time_maxEcell;   //!
   TBranch        *b_el_truth_pt;   //!
   TBranch        *b_el_truth_eta;   //!
   TBranch        *b_el_truth_phi;   //!
   TBranch        *b_el_truth_E;   //!
   TBranch        *b_el_author;   //!
   TBranch        *b_el_isConv;   //!
   TBranch        *b_el_passChID;   //!
   TBranch        *b_el_ecisBDT;   //!
   TBranch        *b_el_truth_matched;   //!
   TBranch        *b_el_truth_mothertype;   //!
   TBranch        *b_el_truth_status;   //!
   TBranch        *b_el_truth_type;   //!
   TBranch        *b_el_truth_typebkg;   //!
   TBranch        *b_el_truth_origin;   //!
   TBranch        *b_el_truth_originbkg;   //!
   TBranch        *b_el_isotool_pass_loose;   //!
   TBranch        *b_el_isotool_pass_gradient;   //!
   TBranch        *b_el_isotool_pass_gradientloose;   //!
   TBranch        *b_el_met_nomuon_dphi;   //!
   TBranch        *b_el_met_wmuon_dphi;   //!
   TBranch        *b_el_met_noelectron_dphi;   //!
   TBranch        *b_el_preor_pt;   //!
   TBranch        *b_el_preor_eta;   //!
   TBranch        *b_el_preor_phi;   //!
   TBranch        *b_el_preor_SF;   //!
   TBranch        *b_el_preor_SF_iso;   //!
   TBranch        *b_el_preor_SF_trigger;   //!
   TBranch        *b_el_preor_SF_tot;   //!
   TBranch        *b_el_preor_SF_tot_isotight;   //!
   TBranch        *b_el_preor_isotool_pass_loosetrackonly;   //!
   TBranch        *b_el_preor_isotool_pass_fixedcuttighttrackonly;   //!
   TBranch        *b_el_preor_isotool_pass_fixedcuttight;   //!
   TBranch        *b_el_preor_m;   //!
   TBranch        *b_el_preor_e;   //!
   TBranch        *b_el_preor_isGoodOQ;   //!
   TBranch        *b_el_preor_passLHID;   //!
   TBranch        *b_el_preor_charge;   //!
   TBranch        *b_el_preor_id_pt;   //!
   TBranch        *b_el_preor_id_eta;   //!
   TBranch        *b_el_preor_id_phi;   //!
   TBranch        *b_el_preor_id_m;   //!
   TBranch        *b_el_preor_cl_pt;   //!
   TBranch        *b_el_preor_cl_eta;   //!
   TBranch        *b_el_preor_cl_etaBE2;   //!
   TBranch        *b_el_preor_cl_phi;   //!
   TBranch        *b_el_preor_cl_m;   //!
   TBranch        *b_el_preor_ptcone20;   //!
   TBranch        *b_el_preor_ptvarcone20;   //!
   TBranch        *b_el_preor_etcone20;   //!
   TBranch        *b_el_preor_topoetcone20;   //!
   TBranch        *b_el_preor_ptcone30;   //!
   TBranch        *b_el_preor_ptvarcone30;   //!
   TBranch        *b_el_preor_etcone30;   //!
   TBranch        *b_el_preor_topoetcone30;   //!
   TBranch        *b_el_preor_ptcone40;   //!
   TBranch        *b_el_preor_ptvarcone40;   //!
   TBranch        *b_el_preor_etcone40;   //!
   TBranch        *b_el_preor_topoetcone40;   //!
   TBranch        *b_el_preor_d0;   //!
   TBranch        *b_el_preor_d0sig;   //!
   TBranch        *b_el_preor_z0;   //!
   TBranch        *b_el_preor_z0sig;   //!
   TBranch        *b_el_preor_demaxs1;   //!
   TBranch        *b_el_preor_fside;   //!
   TBranch        *b_el_preor_weta2;   //!
   TBranch        *b_el_preor_ws3;   //!
   TBranch        *b_el_preor_eratio;   //!
   TBranch        *b_el_preor_reta;   //!
   TBranch        *b_el_preor_rphi;   //!
   TBranch        *b_el_preor_time_cl;   //!
   TBranch        *b_el_preor_time_maxEcell;   //!
   TBranch        *b_el_preor_truth_pt;   //!
   TBranch        *b_el_preor_truth_eta;   //!
   TBranch        *b_el_preor_truth_phi;   //!
   TBranch        *b_el_preor_truth_E;   //!
   TBranch        *b_el_preor_author;   //!
   TBranch        *b_el_preor_isConv;   //!
   TBranch        *b_el_preor_passChID;   //!
   TBranch        *b_el_preor_ecisBDT;   //!
   TBranch        *b_el_preor_truth_matched;   //!
   TBranch        *b_el_preor_truth_mothertype;   //!
   TBranch        *b_el_preor_truth_status;   //!
   TBranch        *b_el_preor_truth_type;   //!
   TBranch        *b_el_preor_truth_typebkg;   //!
   TBranch        *b_el_preor_truth_origin;   //!
   TBranch        *b_el_preor_truth_originbkg;   //!
   TBranch        *b_el_preor_isotool_pass_loose;   //!
   TBranch        *b_el_preor_isotool_pass_gradient;   //!
   TBranch        *b_el_preor_isotool_pass_gradientloose;   //!
   TBranch        *b_el_preor_met_nomuon_dphi;   //!
   TBranch        *b_el_preor_met_wmuon_dphi;   //!
   TBranch        *b_el_preor_met_noelectron_dphi;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_jet_fmax;   //!
   TBranch        *b_jet_fch;   //!
   TBranch        *b_jet_MV2c00_discriminant;   //!
   TBranch        *b_jet_MV2c10_discriminant;   //!
   TBranch        *b_jet_MV2c20_discriminant;   //!
   TBranch        *b_jet_isbjet;   //!
   TBranch        *b_jet_PartonTruthLabelID;   //!
   TBranch        *b_jet_ConeTruthLabelID;   //!
   TBranch        *b_jet_met_nomuon_dphi;   //!
   TBranch        *b_jet_met_wmuon_dphi;   //!
   TBranch        *b_jet_met_noelectron_dphi;   //!
   TBranch        *b_jet_met_nophoton_dphi;   //!
   TBranch        *b_jet_weight;   //!
   TBranch        *b_jet_raw_pt;   //!
   TBranch        *b_jet_raw_eta;   //!
   TBranch        *b_jet_raw_phi;   //!
   TBranch        *b_jet_raw_m;   //!
   TBranch        *b_jet_timing;   //!
   TBranch        *b_jet_emfrac;   //!
   TBranch        *b_jet_hecf;   //!
   TBranch        *b_jet_hecq;   //!
   TBranch        *b_jet_larq;   //!
   TBranch        *b_jet_avglarq;   //!
   TBranch        *b_jet_negE;   //!
   TBranch        *b_jet_lambda;   //!
   TBranch        *b_jet_lambda2;   //!
   TBranch        *b_jet_jvtxf;   //!
   TBranch        *b_jet_fmaxi;   //!
   TBranch        *b_jet_isbjet_loose;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_cleaning;   //!
   TBranch        *b_jet_TruthLabelDeltaR_B;   //!
   TBranch        *b_jet_TruthLabelDeltaR_C;   //!
   TBranch        *b_jet_TruthLabelDeltaR_T;   //!
   TBranch        *b_jet_preor_pt;   //!
   TBranch        *b_jet_preor_eta;   //!
   TBranch        *b_jet_preor_phi;   //!
   TBranch        *b_jet_preor_m;   //!
   TBranch        *b_jet_preor_fmax;   //!
   TBranch        *b_jet_preor_fch;   //!
   TBranch        *b_jet_preor_MV2c00_discriminant;   //!
   TBranch        *b_jet_preor_MV2c10_discriminant;   //!
   TBranch        *b_jet_preor_MV2c20_discriminant;   //!
   TBranch        *b_jet_preor_isbjet;   //!
   TBranch        *b_jet_preor_PartonTruthLabelID;   //!
   TBranch        *b_jet_preor_ConeTruthLabelID;   //!
   TBranch        *b_jet_preor_met_nomuon_dphi;   //!
   TBranch        *b_jet_preor_met_wmuon_dphi;   //!
   TBranch        *b_jet_preor_met_noelectron_dphi;   //!
   TBranch        *b_jet_preor_met_nophoton_dphi;   //!
   TBranch        *b_jet_preor_weight;   //!
   TBranch        *b_jet_preor_raw_pt;   //!
   TBranch        *b_jet_preor_raw_eta;   //!
   TBranch        *b_jet_preor_raw_phi;   //!
   TBranch        *b_jet_preor_raw_m;   //!
   TBranch        *b_jet_preor_timing;   //!
   TBranch        *b_jet_preor_emfrac;   //!
   TBranch        *b_jet_preor_hecf;   //!
   TBranch        *b_jet_preor_hecq;   //!
   TBranch        *b_jet_preor_larq;   //!
   TBranch        *b_jet_preor_avglarq;   //!
   TBranch        *b_jet_preor_negE;   //!
   TBranch        *b_jet_preor_lambda;   //!
   TBranch        *b_jet_preor_lambda2;   //!
   TBranch        *b_jet_preor_jvtxf;   //!
   TBranch        *b_jet_preor_fmaxi;   //!
   TBranch        *b_jet_preor_isbjet_loose;   //!
   TBranch        *b_jet_preor_jvt;   //!
   TBranch        *b_jet_preor_cleaning;   //!
   TBranch        *b_jet_preor_TruthLabelDeltaR_B;   //!
   TBranch        *b_jet_preor_TruthLabelDeltaR_C;   //!
   TBranch        *b_jet_preor_TruthLabelDeltaR_T;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_SF;   //!
   TBranch        *b_ph_SF_iso;   //!
   TBranch        *b_ph_isotool_pass_fixedcuttightcaloonly;   //!
   TBranch        *b_ph_isotool_pass_fixedcuttight;   //!
   TBranch        *b_ph_isotool_pass_fixedcutloose;   //!
   TBranch        *b_ph_m;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptvarcone20;   //!
   TBranch        *b_ph_etcone20;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_ptvarcone30;   //!
   TBranch        *b_ph_etcone30;   //!
   TBranch        *b_ph_topoetcone30;   //!
   TBranch        *b_ph_ptcone40;   //!
   TBranch        *b_ph_ptvarcone40;   //!
   TBranch        *b_ph_etcone40;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_isTight;   //!
   TBranch        *b_ph_isEM;   //!
   TBranch        *b_ph_OQ;   //!
   TBranch        *b_ph_Rphi;   //!
   TBranch        *b_ph_weta2;   //!
   TBranch        *b_ph_fracs1;   //!
   TBranch        *b_ph_weta1;   //!
   TBranch        *b_ph_emaxs1;   //!
   TBranch        *b_ph_f1;   //!
   TBranch        *b_ph_Reta;   //!
   TBranch        *b_ph_wtots1;   //!
   TBranch        *b_ph_Eratio;   //!
   TBranch        *b_ph_Rhad;   //!
   TBranch        *b_ph_Rhad1;   //!
   TBranch        *b_ph_e277;   //!
   TBranch        *b_ph_deltae;   //!
   TBranch        *b_ph_author;   //!
   TBranch        *b_ph_isConv;   //!
   TBranch        *b_ph_met_nomuon_dphi;   //!
   TBranch        *b_ph_met_wmuon_dphi;   //!
   TBranch        *b_ph_met_nophoton_dphi;   //!
   TBranch        *b_ph_preor_pt;   //!
   TBranch        *b_ph_preor_eta;   //!
   TBranch        *b_ph_preor_phi;   //!
   TBranch        *b_ph_preor_SF;   //!
   TBranch        *b_ph_preor_SF_iso;   //!
   TBranch        *b_ph_preor_isotool_pass_fixedcuttightcaloonly;   //!
   TBranch        *b_ph_preor_isotool_pass_fixedcuttight;   //!
   TBranch        *b_ph_preor_isotool_pass_fixedcutloose;   //!
   TBranch        *b_ph_preor_m;   //!
   TBranch        *b_ph_preor_ptcone20;   //!
   TBranch        *b_ph_preor_ptvarcone20;   //!
   TBranch        *b_ph_preor_etcone20;   //!
   TBranch        *b_ph_preor_topoetcone20;   //!
   TBranch        *b_ph_preor_ptcone30;   //!
   TBranch        *b_ph_preor_ptvarcone30;   //!
   TBranch        *b_ph_preor_etcone30;   //!
   TBranch        *b_ph_preor_topoetcone30;   //!
   TBranch        *b_ph_preor_ptcone40;   //!
   TBranch        *b_ph_preor_ptvarcone40;   //!
   TBranch        *b_ph_preor_etcone40;   //!
   TBranch        *b_ph_preor_topoetcone40;   //!
   TBranch        *b_ph_preor_isTight;   //!
   TBranch        *b_ph_preor_isEM;   //!
   TBranch        *b_ph_preor_OQ;   //!
   TBranch        *b_ph_preor_Rphi;   //!
   TBranch        *b_ph_preor_weta2;   //!
   TBranch        *b_ph_preor_fracs1;   //!
   TBranch        *b_ph_preor_weta1;   //!
   TBranch        *b_ph_preor_emaxs1;   //!
   TBranch        *b_ph_preor_f1;   //!
   TBranch        *b_ph_preor_Reta;   //!
   TBranch        *b_ph_preor_wtots1;   //!
   TBranch        *b_ph_preor_Eratio;   //!
   TBranch        *b_ph_preor_Rhad;   //!
   TBranch        *b_ph_preor_Rhad1;   //!
   TBranch        *b_ph_preor_e277;   //!
   TBranch        *b_ph_preor_deltae;   //!
   TBranch        *b_ph_preor_author;   //!
   TBranch        *b_ph_preor_isConv;   //!
   TBranch        *b_ph_preor_met_nomuon_dphi;   //!
   TBranch        *b_ph_preor_met_wmuon_dphi;   //!
   TBranch        *b_ph_preor_met_nophoton_dphi;   //!
   TBranch        *b_hem_n;   //!
   TBranch        *b_hem_pt;   //!
   TBranch        *b_hem_eta;   //!
   TBranch        *b_hem_phi;   //!
   TBranch        *b_hem_m;   //!
   TBranch        *b_tau_pt;   //!
   TBranch        *b_tau_eta;   //!
   TBranch        *b_tau_phi;   //!
   TBranch        *b_tau_idtool_pass_loose;   //!
   TBranch        *b_tau_idtool_pass_medium;   //!
   TBranch        *b_tau_idtool_pass_tight;   //!
   TBranch        *b_tau_multiplicity;   //!
   TBranch        *b_tau_medium_multiplicity;   //!
   TBranch        *b_tau_tight_multiplicity;   //!

   nominal(TTree *tree=0);
   virtual ~nominal();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef nominal_cxx
nominal::nominal(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/user/h/hod/NTUP/mc16_13TeV/test/SIG08092017v0/Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi/user.hod.SIG08092017v0.mc16_13TeV.301216.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000_minitrees.root/user.hod.12112467._000001.minitrees.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/user/h/hod/NTUP/mc16_13TeV/test/SIG08092017v0/Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi/user.hod.SIG08092017v0.mc16_13TeV.301216.Pythia8EvtGen_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000_minitrees.root/user.hod.12112467._000001.minitrees.root");
      }
      f->GetObject("nominal",tree);

   }
   Init(tree);
}

nominal::~nominal()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t nominal::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t nominal::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void nominal::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mconly_weights = 0;
   truth_W_decay = 0;
   newSH_weight = 0;
   truth_mu_pt = 0;
   truth_mu_eta = 0;
   truth_mu_phi = 0;
   truth_mu_m = 0;
   truth_mu_status = 0;
   mu_pt = 0;
   mu_ptErr = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_SF = 0;
   mu_SF_iso = 0;
   mu_isotool_pass_loosetrackonly = 0;
   mu_isotool_pass_fixedcuttighttrackonly = 0;
   mu_m = 0;
   mu_e = 0;
   mu_d0 = 0;
   mu_d0sig = 0;
   mu_z0 = 0;
   mu_z0sig = 0;
   mu_charge = 0;
   mu_id_pt = 0;
   mu_id_ptErr = 0;
   mu_id_eta = 0;
   mu_id_phi = 0;
   mu_id_m = 0;
   mu_id_qop = 0;
   mu_id_qoperr = 0;
   mu_ms_pt = 0;
   mu_ms_ptErr = 0;
   mu_ms_eta = 0;
   mu_ms_phi = 0;
   mu_ms_m = 0;
   mu_ms_qop = 0;
   mu_ms_qoperr = 0;
   mu_me_pt = 0;
   mu_me_ptErr = 0;
   mu_me_eta = 0;
   mu_me_phi = 0;
   mu_me_m = 0;
   mu_me_qop = 0;
   mu_me_qoperr = 0;
   mu_mse_pt = 0;
   mu_mse_ptErr = 0;
   mu_mse_eta = 0;
   mu_mse_phi = 0;
   mu_mse_m = 0;
   mu_mse_qop = 0;
   mu_mse_qoperr = 0;
   mu_ptcone20 = 0;
   mu_ptvarcone20 = 0;
   mu_etcone20 = 0;
   mu_topoetcone20 = 0;
   mu_ptcone30 = 0;
   mu_ptvarcone30 = 0;
   mu_etcone30 = 0;
   mu_topoetcone30 = 0;
   mu_ptcone40 = 0;
   mu_ptvarcone40 = 0;
   mu_etcone40 = 0;
   mu_topoetcone40 = 0;
   mu_author = 0;
   mu_quality = 0;
   mu_isSA = 0;
   mu_isST = 0;
   mu_isCB = 0;
   mu_isTight = 0;
   mu_isMedium = 0;
   mu_isLoose = 0;
   mu_isVeryLoose = 0;
   mu_isHighPt = 0;
   mu_isBad = 0;
   mu_isBadHighPt = 0;
   mu_finalFitPt = 0;
   mu_finalFitPtErr = 0;
   mu_finalFitEta = 0;
   mu_finalFitPhi = 0;
   mu_finalFitQOverP = 0;
   mu_finalFitQOverPErr = 0;
   mu_truthEta = 0;
   mu_truthPhi = 0;
   mu_truthPt = 0;
   mu_truthTheta = 0;
   mu_truthQOverP = 0;
   mu_isotool_pass_loose = 0;
   mu_isotool_pass_gradient = 0;
   mu_isotool_pass_gradientloose = 0;
   mu_met_nomuon_dphi = 0;
   mu_met_wmuon_dphi = 0;
   mu_truth_origin = 0;
   mu_truth_type = 0;
   mu_muonType = 0;
   mu_momentumBalanceSignificance = 0;
   mu_CaloLRLikelihood = 0;
   mu_CaloMuonIDTag = 0;
   mu_numberOfPrecisionLayers = 0;
   mu_numberOfPrecisionHoleLayers = 0;
   mu_numberOfGoodPrecisionLayers = 0;
   mu_innerSmallHits = 0;
   mu_innerLargeHits = 0;
   mu_innerSmallHoles = 0;
   mu_innerLargeHoles = 0;
   mu_middleSmallHoles = 0;
   mu_middleLargeHoles = 0;
   mu_outerSmallHoles = 0;
   mu_outerLargeHoles = 0;
   mu_extendedSmallHoles = 0;
   mu_extendedLargeHoles = 0;
   mu_innerClosePrecisionHits = 0;
   mu_middleClosePrecisionHits = 0;
   mu_outerClosePrecisionHits = 0;
   mu_extendedClosePrecisionHits = 0;
   mu_innerOutBoundsPrecisionHits = 0;
   mu_middleOutBoundsPrecisionHits = 0;
   mu_outerOutBoundsPrecisionHits = 0;
   mu_extendedOutBoundsPrecisionHits = 0;
   mu_combinedTrackOutBoundsPrecisionHits = 0;
   mu_middleLargeHits = 0;
   mu_middleSmallHits = 0;
   mu_outerLargeHits = 0;
   mu_outerSmallHits = 0;
   mu_extendedSmallHits = 0;
   mu_extendedLargeHits = 0;
   mu_numberOfPixelHits = 0;
   mu_numberOfPixelDeadSensors = 0;
   mu_numberOfSCTHits = 0;
   mu_numberOfSCTDeadSensors = 0;
   mu_numberOfPixelHoles = 0;
   mu_numberOfSCTHoles = 0;
   mu_numberOfTRTHits = 0;
   mu_numberOfTRTOutliers = 0;
   mu_primarytrketa = 0;
   mu_numberOfInnermostPixelLayerHits = 0;
   mu_cbtrketa = 0;
   mu_spectrometerFieldIntegral = 0;
   mu_scatteringCurvatureSignificance = 0;
   mu_scatteringNeighbourSignificance = 0;
   mu_allAuthors = 0;
   mu_numberOfPhiLayers = 0;
   mu_numberOfPhiHoleLayers = 0;
   mu_numberOfTriggerEtaLayers = 0;
   mu_numberOfTriggerEtaHoleLayers = 0;
   mu_nUnspoiledCscHits = 0;
   mu_FSR_CandidateEnergy = 0;
   mu_EnergyLoss = 0;
   mu_EnergyLossSigma = 0;
   mu_ParamEnergyLoss = 0;
   mu_ParamEnergyLossSigmaPlus = 0;
   mu_ParamEnergyLossSigmaMinus = 0;
   mu_MeasEnergyLoss = 0;
   mu_MeasEnergyLossSigma = 0;
   mu_msInnerMatchChi2 = 0;
   mu_msInnerMatchDOF = 0;
   mu_energyLossType = 0;
   mu_phiLayer1Hits = 0;
   mu_phiLayer2Hits = 0;
   mu_phiLayer3Hits = 0;
   mu_phiLayer4Hits = 0;
   mu_etaLayer1Hits = 0;
   mu_etaLayer2Hits = 0;
   mu_etaLayer3Hits = 0;
   mu_etaLayer4Hits = 0;
   mu_phiLayer1Holes = 0;
   mu_phiLayer2Holes = 0;
   mu_phiLayer3Holes = 0;
   mu_phiLayer4Holes = 0;
   mu_etaLayer1Holes = 0;
   mu_etaLayer2Holes = 0;
   mu_etaLayer3Holes = 0;
   mu_etaLayer4Holes = 0;
   mu_phiLayer1RPCHits = 0;
   mu_phiLayer2RPCHits = 0;
   mu_phiLayer3RPCHits = 0;
   mu_etaLayer1RPCHits = 0;
   mu_etaLayer2RPCHits = 0;
   mu_etaLayer3RPCHits = 0;
   mu_phiLayer1RPCHoles = 0;
   mu_phiLayer2RPCHoles = 0;
   mu_phiLayer3RPCHoles = 0;
   mu_etaLayer1RPCHoles = 0;
   mu_etaLayer2RPCHoles = 0;
   mu_etaLayer3RPCHoles = 0;
   mu_phiLayer1TGCHits = 0;
   mu_phiLayer2TGCHits = 0;
   mu_phiLayer3TGCHits = 0;
   mu_phiLayer4TGCHits = 0;
   mu_etaLayer1TGCHits = 0;
   mu_etaLayer2TGCHits = 0;
   mu_etaLayer3TGCHits = 0;
   mu_etaLayer4TGCHits = 0;
   mu_phiLayer1TGCHoles = 0;
   mu_phiLayer2TGCHoles = 0;
   mu_phiLayer3TGCHoles = 0;
   mu_phiLayer4TGCHoles = 0;
   mu_etaLayer1TGCHoles = 0;
   mu_etaLayer2TGCHoles = 0;
   mu_etaLayer3TGCHoles = 0;
   mu_etaLayer4TGCHoles = 0;
   mu_segmentDeltaEta = 0;
   mu_segmentDeltaPhi = 0;
   mu_segmentChi2OverDoF = 0;
   mu_t0 = 0;
   mu_beta = 0;
   mu_meanDeltaADCCountsMDT = 0;
   mu_msOuterMatchDOF = 0;
   mu_preor_pt = 0;
   mu_preor_ptErr = 0;
   mu_preor_eta = 0;
   mu_preor_phi = 0;
   mu_preor_SF = 0;
   mu_preor_SF_iso = 0;
   mu_preor_isotool_pass_loosetrackonly = 0;
   mu_preor_isotool_pass_fixedcuttighttrackonly = 0;
   mu_preor_m = 0;
   mu_preor_e = 0;
   mu_preor_d0 = 0;
   mu_preor_d0sig = 0;
   mu_preor_z0 = 0;
   mu_preor_z0sig = 0;
   mu_preor_charge = 0;
   mu_preor_id_pt = 0;
   mu_preor_id_ptErr = 0;
   mu_preor_id_eta = 0;
   mu_preor_id_phi = 0;
   mu_preor_id_m = 0;
   mu_preor_id_qop = 0;
   mu_preor_id_qoperr = 0;
   mu_preor_ms_pt = 0;
   mu_preor_ms_ptErr = 0;
   mu_preor_ms_eta = 0;
   mu_preor_ms_phi = 0;
   mu_preor_ms_m = 0;
   mu_preor_ms_qop = 0;
   mu_preor_ms_qoperr = 0;
   mu_preor_me_pt = 0;
   mu_preor_me_ptErr = 0;
   mu_preor_me_eta = 0;
   mu_preor_me_phi = 0;
   mu_preor_me_m = 0;
   mu_preor_me_qop = 0;
   mu_preor_me_qoperr = 0;
   mu_preor_mse_pt = 0;
   mu_preor_mse_ptErr = 0;
   mu_preor_mse_eta = 0;
   mu_preor_mse_phi = 0;
   mu_preor_mse_m = 0;
   mu_preor_mse_qop = 0;
   mu_preor_mse_qoperr = 0;
   mu_preor_ptcone20 = 0;
   mu_preor_ptvarcone20 = 0;
   mu_preor_etcone20 = 0;
   mu_preor_topoetcone20 = 0;
   mu_preor_ptcone30 = 0;
   mu_preor_ptvarcone30 = 0;
   mu_preor_etcone30 = 0;
   mu_preor_topoetcone30 = 0;
   mu_preor_ptcone40 = 0;
   mu_preor_ptvarcone40 = 0;
   mu_preor_etcone40 = 0;
   mu_preor_topoetcone40 = 0;
   mu_preor_author = 0;
   mu_preor_quality = 0;
   mu_preor_isSA = 0;
   mu_preor_isST = 0;
   mu_preor_isCB = 0;
   mu_preor_isTight = 0;
   mu_preor_isMedium = 0;
   mu_preor_isLoose = 0;
   mu_preor_isVeryLoose = 0;
   mu_preor_isHighPt = 0;
   mu_preor_isBad = 0;
   mu_preor_isBadHighPt = 0;
   mu_preor_finalFitPt = 0;
   mu_preor_finalFitPtErr = 0;
   mu_preor_finalFitEta = 0;
   mu_preor_finalFitPhi = 0;
   mu_preor_finalFitQOverP = 0;
   mu_preor_finalFitQOverPErr = 0;
   mu_preor_truthEta = 0;
   mu_preor_truthPhi = 0;
   mu_preor_truthPt = 0;
   mu_preor_truthTheta = 0;
   mu_preor_truthQOverP = 0;
   mu_preor_isotool_pass_loose = 0;
   mu_preor_isotool_pass_gradient = 0;
   mu_preor_isotool_pass_gradientloose = 0;
   mu_preor_met_nomuon_dphi = 0;
   mu_preor_met_wmuon_dphi = 0;
   mu_preor_truth_origin = 0;
   mu_preor_truth_type = 0;
   mu_preor_muonType = 0;
   mu_preor_momentumBalanceSignificance = 0;
   mu_preor_CaloLRLikelihood = 0;
   mu_preor_CaloMuonIDTag = 0;
   mu_preor_numberOfPrecisionLayers = 0;
   mu_preor_numberOfPrecisionHoleLayers = 0;
   mu_preor_numberOfGoodPrecisionLayers = 0;
   mu_preor_innerSmallHits = 0;
   mu_preor_innerLargeHits = 0;
   mu_preor_innerSmallHoles = 0;
   mu_preor_innerLargeHoles = 0;
   mu_preor_middleSmallHoles = 0;
   mu_preor_middleLargeHoles = 0;
   mu_preor_outerSmallHoles = 0;
   mu_preor_outerLargeHoles = 0;
   mu_preor_extendedSmallHoles = 0;
   mu_preor_extendedLargeHoles = 0;
   mu_preor_innerClosePrecisionHits = 0;
   mu_preor_middleClosePrecisionHits = 0;
   mu_preor_outerClosePrecisionHits = 0;
   mu_preor_extendedClosePrecisionHits = 0;
   mu_preor_innerOutBoundsPrecisionHits = 0;
   mu_preor_middleOutBoundsPrecisionHits = 0;
   mu_preor_outerOutBoundsPrecisionHits = 0;
   mu_preor_extendedOutBoundsPrecisionHits = 0;
   mu_preor_combinedTrackOutBoundsPrecisionHits = 0;
   mu_preor_middleLargeHits = 0;
   mu_preor_middleSmallHits = 0;
   mu_preor_outerLargeHits = 0;
   mu_preor_outerSmallHits = 0;
   mu_preor_extendedSmallHits = 0;
   mu_preor_extendedLargeHits = 0;
   mu_preor_numberOfPixelHits = 0;
   mu_preor_numberOfPixelDeadSensors = 0;
   mu_preor_numberOfSCTHits = 0;
   mu_preor_numberOfSCTDeadSensors = 0;
   mu_preor_numberOfPixelHoles = 0;
   mu_preor_numberOfSCTHoles = 0;
   mu_preor_numberOfTRTHits = 0;
   mu_preor_numberOfTRTOutliers = 0;
   mu_preor_primarytrketa = 0;
   mu_preor_numberOfInnermostPixelLayerHits = 0;
   mu_preor_cbtrketa = 0;
   mu_preor_spectrometerFieldIntegral = 0;
   mu_preor_scatteringCurvatureSignificance = 0;
   mu_preor_scatteringNeighbourSignificance = 0;
   mu_preor_allAuthors = 0;
   mu_preor_numberOfPhiLayers = 0;
   mu_preor_numberOfPhiHoleLayers = 0;
   mu_preor_numberOfTriggerEtaLayers = 0;
   mu_preor_numberOfTriggerEtaHoleLayers = 0;
   mu_preor_nUnspoiledCscHits = 0;
   mu_preor_FSR_CandidateEnergy = 0;
   mu_preor_EnergyLoss = 0;
   mu_preor_EnergyLossSigma = 0;
   mu_preor_ParamEnergyLoss = 0;
   mu_preor_ParamEnergyLossSigmaPlus = 0;
   mu_preor_ParamEnergyLossSigmaMinus = 0;
   mu_preor_MeasEnergyLoss = 0;
   mu_preor_MeasEnergyLossSigma = 0;
   mu_preor_msInnerMatchChi2 = 0;
   mu_preor_msInnerMatchDOF = 0;
   mu_preor_energyLossType = 0;
   mu_preor_phiLayer1Hits = 0;
   mu_preor_phiLayer2Hits = 0;
   mu_preor_phiLayer3Hits = 0;
   mu_preor_phiLayer4Hits = 0;
   mu_preor_etaLayer1Hits = 0;
   mu_preor_etaLayer2Hits = 0;
   mu_preor_etaLayer3Hits = 0;
   mu_preor_etaLayer4Hits = 0;
   mu_preor_phiLayer1Holes = 0;
   mu_preor_phiLayer2Holes = 0;
   mu_preor_phiLayer3Holes = 0;
   mu_preor_phiLayer4Holes = 0;
   mu_preor_etaLayer1Holes = 0;
   mu_preor_etaLayer2Holes = 0;
   mu_preor_etaLayer3Holes = 0;
   mu_preor_etaLayer4Holes = 0;
   mu_preor_phiLayer1RPCHits = 0;
   mu_preor_phiLayer2RPCHits = 0;
   mu_preor_phiLayer3RPCHits = 0;
   mu_preor_etaLayer1RPCHits = 0;
   mu_preor_etaLayer2RPCHits = 0;
   mu_preor_etaLayer3RPCHits = 0;
   mu_preor_phiLayer1RPCHoles = 0;
   mu_preor_phiLayer2RPCHoles = 0;
   mu_preor_phiLayer3RPCHoles = 0;
   mu_preor_etaLayer1RPCHoles = 0;
   mu_preor_etaLayer2RPCHoles = 0;
   mu_preor_etaLayer3RPCHoles = 0;
   mu_preor_phiLayer1TGCHits = 0;
   mu_preor_phiLayer2TGCHits = 0;
   mu_preor_phiLayer3TGCHits = 0;
   mu_preor_phiLayer4TGCHits = 0;
   mu_preor_etaLayer1TGCHits = 0;
   mu_preor_etaLayer2TGCHits = 0;
   mu_preor_etaLayer3TGCHits = 0;
   mu_preor_etaLayer4TGCHits = 0;
   mu_preor_phiLayer1TGCHoles = 0;
   mu_preor_phiLayer2TGCHoles = 0;
   mu_preor_phiLayer3TGCHoles = 0;
   mu_preor_phiLayer4TGCHoles = 0;
   mu_preor_etaLayer1TGCHoles = 0;
   mu_preor_etaLayer2TGCHoles = 0;
   mu_preor_etaLayer3TGCHoles = 0;
   mu_preor_etaLayer4TGCHoles = 0;
   mu_preor_segmentDeltaEta = 0;
   mu_preor_segmentDeltaPhi = 0;
   mu_preor_segmentChi2OverDoF = 0;
   mu_preor_t0 = 0;
   mu_preor_beta = 0;
   mu_preor_meanDeltaADCCountsMDT = 0;
   mu_preor_msOuterMatchDOF = 0;
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_SF = 0;
   el_SF_iso = 0;
   el_SF_trigger = 0;
   el_SF_tot = 0;
   el_SF_tot_isotight = 0;
   el_isotool_pass_loosetrackonly = 0;
   el_isotool_pass_fixedcuttighttrackonly = 0;
   el_isotool_pass_fixedcuttight = 0;
   el_m = 0;
   el_e = 0;
   el_isGoodOQ = 0;
   el_passLHID = 0;
   el_charge = 0;
   el_id_pt = 0;
   el_id_eta = 0;
   el_id_phi = 0;
   el_id_m = 0;
   el_cl_pt = 0;
   el_cl_eta = 0;
   el_cl_etaBE2 = 0;
   el_cl_phi = 0;
   el_cl_m = 0;
   el_ptcone20 = 0;
   el_ptvarcone20 = 0;
   el_etcone20 = 0;
   el_topoetcone20 = 0;
   el_ptcone30 = 0;
   el_ptvarcone30 = 0;
   el_etcone30 = 0;
   el_topoetcone30 = 0;
   el_ptcone40 = 0;
   el_ptvarcone40 = 0;
   el_etcone40 = 0;
   el_topoetcone40 = 0;
   el_d0 = 0;
   el_d0sig = 0;
   el_z0 = 0;
   el_z0sig = 0;
   el_demaxs1 = 0;
   el_fside = 0;
   el_weta2 = 0;
   el_ws3 = 0;
   el_eratio = 0;
   el_reta = 0;
   el_rphi = 0;
   el_time_cl = 0;
   el_time_maxEcell = 0;
   el_truth_pt = 0;
   el_truth_eta = 0;
   el_truth_phi = 0;
   el_truth_E = 0;
   el_author = 0;
   el_isConv = 0;
   el_passChID = 0;
   el_ecisBDT = 0;
   el_truth_matched = 0;
   el_truth_mothertype = 0;
   el_truth_status = 0;
   el_truth_type = 0;
   el_truth_typebkg = 0;
   el_truth_origin = 0;
   el_truth_originbkg = 0;
   el_isotool_pass_loose = 0;
   el_isotool_pass_gradient = 0;
   el_isotool_pass_gradientloose = 0;
   el_met_nomuon_dphi = 0;
   el_met_wmuon_dphi = 0;
   el_met_noelectron_dphi = 0;
   el_preor_pt = 0;
   el_preor_eta = 0;
   el_preor_phi = 0;
   el_preor_SF = 0;
   el_preor_SF_iso = 0;
   el_preor_SF_trigger = 0;
   el_preor_SF_tot = 0;
   el_preor_SF_tot_isotight = 0;
   el_preor_isotool_pass_loosetrackonly = 0;
   el_preor_isotool_pass_fixedcuttighttrackonly = 0;
   el_preor_isotool_pass_fixedcuttight = 0;
   el_preor_m = 0;
   el_preor_e = 0;
   el_preor_isGoodOQ = 0;
   el_preor_passLHID = 0;
   el_preor_charge = 0;
   el_preor_id_pt = 0;
   el_preor_id_eta = 0;
   el_preor_id_phi = 0;
   el_preor_id_m = 0;
   el_preor_cl_pt = 0;
   el_preor_cl_eta = 0;
   el_preor_cl_etaBE2 = 0;
   el_preor_cl_phi = 0;
   el_preor_cl_m = 0;
   el_preor_ptcone20 = 0;
   el_preor_ptvarcone20 = 0;
   el_preor_etcone20 = 0;
   el_preor_topoetcone20 = 0;
   el_preor_ptcone30 = 0;
   el_preor_ptvarcone30 = 0;
   el_preor_etcone30 = 0;
   el_preor_topoetcone30 = 0;
   el_preor_ptcone40 = 0;
   el_preor_ptvarcone40 = 0;
   el_preor_etcone40 = 0;
   el_preor_topoetcone40 = 0;
   el_preor_d0 = 0;
   el_preor_d0sig = 0;
   el_preor_z0 = 0;
   el_preor_z0sig = 0;
   el_preor_demaxs1 = 0;
   el_preor_fside = 0;
   el_preor_weta2 = 0;
   el_preor_ws3 = 0;
   el_preor_eratio = 0;
   el_preor_reta = 0;
   el_preor_rphi = 0;
   el_preor_time_cl = 0;
   el_preor_time_maxEcell = 0;
   el_preor_truth_pt = 0;
   el_preor_truth_eta = 0;
   el_preor_truth_phi = 0;
   el_preor_truth_E = 0;
   el_preor_author = 0;
   el_preor_isConv = 0;
   el_preor_passChID = 0;
   el_preor_ecisBDT = 0;
   el_preor_truth_matched = 0;
   el_preor_truth_mothertype = 0;
   el_preor_truth_status = 0;
   el_preor_truth_type = 0;
   el_preor_truth_typebkg = 0;
   el_preor_truth_origin = 0;
   el_preor_truth_originbkg = 0;
   el_preor_isotool_pass_loose = 0;
   el_preor_isotool_pass_gradient = 0;
   el_preor_isotool_pass_gradientloose = 0;
   el_preor_met_nomuon_dphi = 0;
   el_preor_met_wmuon_dphi = 0;
   el_preor_met_noelectron_dphi = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_m = 0;
   jet_fmax = 0;
   jet_fch = 0;
   jet_MV2c00_discriminant = 0;
   jet_MV2c10_discriminant = 0;
   jet_MV2c20_discriminant = 0;
   jet_isbjet = 0;
   jet_PartonTruthLabelID = 0;
   jet_ConeTruthLabelID = 0;
   jet_met_nomuon_dphi = 0;
   jet_met_wmuon_dphi = 0;
   jet_met_noelectron_dphi = 0;
   jet_met_nophoton_dphi = 0;
   jet_weight = 0;
   jet_raw_pt = 0;
   jet_raw_eta = 0;
   jet_raw_phi = 0;
   jet_raw_m = 0;
   jet_timing = 0;
   jet_emfrac = 0;
   jet_hecf = 0;
   jet_hecq = 0;
   jet_larq = 0;
   jet_avglarq = 0;
   jet_negE = 0;
   jet_lambda = 0;
   jet_lambda2 = 0;
   jet_jvtxf = 0;
   jet_fmaxi = 0;
   jet_isbjet_loose = 0;
   jet_jvt = 0;
   jet_cleaning = 0;
   jet_TruthLabelDeltaR_B = 0;
   jet_TruthLabelDeltaR_C = 0;
   jet_TruthLabelDeltaR_T = 0;
   jet_preor_pt = 0;
   jet_preor_eta = 0;
   jet_preor_phi = 0;
   jet_preor_m = 0;
   jet_preor_fmax = 0;
   jet_preor_fch = 0;
   jet_preor_MV2c00_discriminant = 0;
   jet_preor_MV2c10_discriminant = 0;
   jet_preor_MV2c20_discriminant = 0;
   jet_preor_isbjet = 0;
   jet_preor_PartonTruthLabelID = 0;
   jet_preor_ConeTruthLabelID = 0;
   jet_preor_met_nomuon_dphi = 0;
   jet_preor_met_wmuon_dphi = 0;
   jet_preor_met_noelectron_dphi = 0;
   jet_preor_met_nophoton_dphi = 0;
   jet_preor_weight = 0;
   jet_preor_raw_pt = 0;
   jet_preor_raw_eta = 0;
   jet_preor_raw_phi = 0;
   jet_preor_raw_m = 0;
   jet_preor_timing = 0;
   jet_preor_emfrac = 0;
   jet_preor_hecf = 0;
   jet_preor_hecq = 0;
   jet_preor_larq = 0;
   jet_preor_avglarq = 0;
   jet_preor_negE = 0;
   jet_preor_lambda = 0;
   jet_preor_lambda2 = 0;
   jet_preor_jvtxf = 0;
   jet_preor_fmaxi = 0;
   jet_preor_isbjet_loose = 0;
   jet_preor_jvt = 0;
   jet_preor_cleaning = 0;
   jet_preor_TruthLabelDeltaR_B = 0;
   jet_preor_TruthLabelDeltaR_C = 0;
   jet_preor_TruthLabelDeltaR_T = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_SF = 0;
   ph_SF_iso = 0;
   ph_isotool_pass_fixedcuttightcaloonly = 0;
   ph_isotool_pass_fixedcuttight = 0;
   ph_isotool_pass_fixedcutloose = 0;
   ph_m = 0;
   ph_ptcone20 = 0;
   ph_ptvarcone20 = 0;
   ph_etcone20 = 0;
   ph_topoetcone20 = 0;
   ph_ptcone30 = 0;
   ph_ptvarcone30 = 0;
   ph_etcone30 = 0;
   ph_topoetcone30 = 0;
   ph_ptcone40 = 0;
   ph_ptvarcone40 = 0;
   ph_etcone40 = 0;
   ph_topoetcone40 = 0;
   ph_isTight = 0;
   ph_isEM = 0;
   ph_OQ = 0;
   ph_Rphi = 0;
   ph_weta2 = 0;
   ph_fracs1 = 0;
   ph_weta1 = 0;
   ph_emaxs1 = 0;
   ph_f1 = 0;
   ph_Reta = 0;
   ph_wtots1 = 0;
   ph_Eratio = 0;
   ph_Rhad = 0;
   ph_Rhad1 = 0;
   ph_e277 = 0;
   ph_deltae = 0;
   ph_author = 0;
   ph_isConv = 0;
   ph_met_nomuon_dphi = 0;
   ph_met_wmuon_dphi = 0;
   ph_met_nophoton_dphi = 0;
   ph_preor_pt = 0;
   ph_preor_eta = 0;
   ph_preor_phi = 0;
   ph_preor_SF = 0;
   ph_preor_SF_iso = 0;
   ph_preor_isotool_pass_fixedcuttightcaloonly = 0;
   ph_preor_isotool_pass_fixedcuttight = 0;
   ph_preor_isotool_pass_fixedcutloose = 0;
   ph_preor_m = 0;
   ph_preor_ptcone20 = 0;
   ph_preor_ptvarcone20 = 0;
   ph_preor_etcone20 = 0;
   ph_preor_topoetcone20 = 0;
   ph_preor_ptcone30 = 0;
   ph_preor_ptvarcone30 = 0;
   ph_preor_etcone30 = 0;
   ph_preor_topoetcone30 = 0;
   ph_preor_ptcone40 = 0;
   ph_preor_ptvarcone40 = 0;
   ph_preor_etcone40 = 0;
   ph_preor_topoetcone40 = 0;
   ph_preor_isTight = 0;
   ph_preor_isEM = 0;
   ph_preor_OQ = 0;
   ph_preor_Rphi = 0;
   ph_preor_weta2 = 0;
   ph_preor_fracs1 = 0;
   ph_preor_weta1 = 0;
   ph_preor_emaxs1 = 0;
   ph_preor_f1 = 0;
   ph_preor_Reta = 0;
   ph_preor_wtots1 = 0;
   ph_preor_Eratio = 0;
   ph_preor_Rhad = 0;
   ph_preor_Rhad1 = 0;
   ph_preor_e277 = 0;
   ph_preor_deltae = 0;
   ph_preor_author = 0;
   ph_preor_isConv = 0;
   ph_preor_met_nomuon_dphi = 0;
   ph_preor_met_wmuon_dphi = 0;
   ph_preor_met_nophoton_dphi = 0;
   hem_pt = 0;
   hem_eta = 0;
   hem_phi = 0;
   hem_m = 0;
   tau_pt = 0;
   tau_eta = 0;
   tau_phi = 0;
   tau_idtool_pass_loose = 0;
   tau_idtool_pass_medium = 0;
   tau_idtool_pass_tight = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("last", &last, &b_last);
   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("n_jet", &n_jet, &b_n_jet);
   fChain->SetBranchAddress("n_jet_preor", &n_jet_preor, &b_n_jet_preor);
   fChain->SetBranchAddress("n_mu_preor", &n_mu_preor, &b_n_mu_preor);
   fChain->SetBranchAddress("n_el_preor", &n_el_preor, &b_n_el_preor);
   fChain->SetBranchAddress("n_ph_preor", &n_ph_preor, &b_n_ph_preor);
   fChain->SetBranchAddress("n_bjet", &n_bjet, &b_n_bjet);
   fChain->SetBranchAddress("n_el", &n_el, &b_n_el);
   fChain->SetBranchAddress("n_el_baseline", &n_el_baseline, &b_n_el_baseline);
   fChain->SetBranchAddress("n_mu_baseline", &n_mu_baseline, &b_n_mu_baseline);
   fChain->SetBranchAddress("n_mu_baseline_bad", &n_mu_baseline_bad, &b_n_mu_baseline_bad);
   fChain->SetBranchAddress("n_allmu_bad", &n_allmu_bad, &b_n_allmu_bad);
   fChain->SetBranchAddress("n_mu", &n_mu, &b_n_mu);
   fChain->SetBranchAddress("munu_mT", &munu_mT, &b_munu_mT);
   fChain->SetBranchAddress("enu_mT", &enu_mT, &b_enu_mT);
   fChain->SetBranchAddress("mumu_m", &mumu_m, &b_mumu_m);
   fChain->SetBranchAddress("ee_m", &ee_m, &b_ee_m);
   fChain->SetBranchAddress("sh22_weight", &sh22_weight, &b_sh22_weight);
   fChain->SetBranchAddress("mconly_weight", &mconly_weight, &b_mconly_weight);
   fChain->SetBranchAddress("mconly_weights", &mconly_weights, &b_mconly_weights);
   fChain->SetBranchAddress("kF_weight", &kF_weight, &b_kF_weight);
   fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
   fChain->SetBranchAddress("geneff", &geneff, &b_geneff);
   fChain->SetBranchAddress("pu_weight", &pu_weight, &b_pu_weight);
   fChain->SetBranchAddress("btag_weight", &btag_weight, &b_btag_weight);
   fChain->SetBranchAddress("jvt_weight", &jvt_weight, &b_jvt_weight);
   fChain->SetBranchAddress("jvt_all_weight", &jvt_all_weight, &b_jvt_all_weight);
   fChain->SetBranchAddress("truth_W_decay", &truth_W_decay, &b_truth_W_decay);
   fChain->SetBranchAddress("GenFiltMet", &GenFiltMet, &b_GenFiltMet);
   fChain->SetBranchAddress("newSH_weight", &newSH_weight, &b_newSH_weight);
   fChain->SetBranchAddress("n_truthTop", &n_truthTop, &b_n_truthTop);
   fChain->SetBranchAddress("passTSTCleaning", &passTSTCleaning, &b_passTSTCleaning);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("corAverageIntPerXing", &corAverageIntPerXing, &b_corAverageIntPerXing);
   fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
   fChain->SetBranchAddress("pu_hash", &pu_hash, &b_pu_hash);
   fChain->SetBranchAddress("mu_SF_tot", &mu_SF_tot, &b_mu_SF_tot);
   fChain->SetBranchAddress("trigger_matched_electron", &trigger_matched_electron, &b_trigger_matched_electron);
   fChain->SetBranchAddress("trigger_matched_muon", &trigger_matched_muon, &b_trigger_matched_muon);
   fChain->SetBranchAddress("trigger_HLT_2e17_lhloose", &trigger_HLT_2e17_lhloose, &b_trigger_HLT_2e17_lhloose);
   fChain->SetBranchAddress("trigger_HLT_2e17_lhvloose", &trigger_HLT_2e17_lhvloose, &b_trigger_HLT_2e17_lhvloose);
   fChain->SetBranchAddress("trigger_HLT_2e17_lhvloose_nod0", &trigger_HLT_2e17_lhvloose_nod0, &b_trigger_HLT_2e17_lhvloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_2mu14", &trigger_HLT_2mu14, &b_trigger_HLT_2mu14);
   fChain->SetBranchAddress("trigger_HLT_e120_lhloose", &trigger_HLT_e120_lhloose, &b_trigger_HLT_e120_lhloose);
   fChain->SetBranchAddress("trigger_HLT_e140_lhloose_nod0", &trigger_HLT_e140_lhloose_nod0, &b_trigger_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_L1EM20VH", &trigger_HLT_e24_lhmedium_L1EM20VH, &b_trigger_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_L1EM20VHI", &trigger_HLT_e24_lhmedium_L1EM20VHI, &b_trigger_HLT_e24_lhmedium_L1EM20VHI);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_nod0_L1EM20VH", &trigger_HLT_e24_lhmedium_nod0_L1EM20VH, &b_trigger_HLT_e24_lhmedium_nod0_L1EM20VH);
   fChain->SetBranchAddress("trigger_HLT_e24_lhtight_nod0_ivarloose", &trigger_HLT_e24_lhtight_nod0_ivarloose, &b_trigger_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e26_lhtight_ivarloose", &trigger_HLT_e26_lhtight_ivarloose, &b_trigger_HLT_e26_lhtight_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e26_lhtight_nod0_ivarloose", &trigger_HLT_e26_lhtight_nod0_ivarloose, &b_trigger_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e60_lhmedium", &trigger_HLT_e60_lhmedium, &b_trigger_HLT_e60_lhmedium);
   fChain->SetBranchAddress("trigger_HLT_e60_lhmedium_nod0", &trigger_HLT_e60_lhmedium_nod0, &b_trigger_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("trigger_HLT_e60_medium", &trigger_HLT_e60_medium, &b_trigger_HLT_e60_medium);
   fChain->SetBranchAddress("trigger_HLT_mu20_2mu4noL1", &trigger_HLT_mu20_2mu4noL1, &b_trigger_HLT_mu20_2mu4noL1);
   fChain->SetBranchAddress("trigger_HLT_mu20_ivarloose_L1MU15", &trigger_HLT_mu20_ivarloose_L1MU15, &b_trigger_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("trigger_HLT_mu22_mu8noL1", &trigger_HLT_mu22_mu8noL1, &b_trigger_HLT_mu22_mu8noL1);
   fChain->SetBranchAddress("trigger_HLT_mu24_ivarmedium", &trigger_HLT_mu24_ivarmedium, &b_trigger_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("trigger_HLT_mu26_imedium", &trigger_HLT_mu26_imedium, &b_trigger_HLT_mu26_imedium);
   fChain->SetBranchAddress("trigger_HLT_mu26_ivarmedium", &trigger_HLT_mu26_ivarmedium, &b_trigger_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("trigger_HLT_mu40", &trigger_HLT_mu40, &b_trigger_HLT_mu40);
   fChain->SetBranchAddress("trigger_HLT_mu50", &trigger_HLT_mu50, &b_trigger_HLT_mu50);
   fChain->SetBranchAddress("weight_VJetsEW_Nominal", &weight_VJetsEW_Nominal, &b_weight_VJetsEW_Nominal);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d1K_NLO_High", &weight_VJetsEW_vjets_d1K_NLO_High, &b_weight_VJetsEW_vjets_d1K_NLO_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d1K_NLO_Low", &weight_VJetsEW_vjets_d1K_NLO_Low, &b_weight_VJetsEW_vjets_d1K_NLO_Low);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d1kappa_EW_High", &weight_VJetsEW_vjets_d1kappa_EW_High, &b_weight_VJetsEW_vjets_d1kappa_EW_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d1kappa_EW_Low", &weight_VJetsEW_vjets_d1kappa_EW_Low, &b_weight_VJetsEW_vjets_d1kappa_EW_Low);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d2K_NLO_High", &weight_VJetsEW_vjets_d2K_NLO_High, &b_weight_VJetsEW_vjets_d2K_NLO_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d2K_NLO_Low", &weight_VJetsEW_vjets_d2K_NLO_Low, &b_weight_VJetsEW_vjets_d2K_NLO_Low);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d2kappa_EW_High", &weight_VJetsEW_vjets_d2kappa_EW_High, &b_weight_VJetsEW_vjets_d2kappa_EW_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d2kappa_EW_Low", &weight_VJetsEW_vjets_d2kappa_EW_Low, &b_weight_VJetsEW_vjets_d2kappa_EW_Low);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d3K_NLO_High", &weight_VJetsEW_vjets_d3K_NLO_High, &b_weight_VJetsEW_vjets_d3K_NLO_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d3K_NLO_Low", &weight_VJetsEW_vjets_d3K_NLO_Low, &b_weight_VJetsEW_vjets_d3K_NLO_Low);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d3kappa_EW_High", &weight_VJetsEW_vjets_d3kappa_EW_High, &b_weight_VJetsEW_vjets_d3kappa_EW_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_d3kappa_EW_Low", &weight_VJetsEW_vjets_d3kappa_EW_Low, &b_weight_VJetsEW_vjets_d3kappa_EW_Low);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_dK_NLO_fix_High", &weight_VJetsEW_vjets_dK_NLO_fix_High, &b_weight_VJetsEW_vjets_dK_NLO_fix_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_dK_NLO_fix_Low", &weight_VJetsEW_vjets_dK_NLO_fix_Low, &b_weight_VJetsEW_vjets_dK_NLO_fix_Low);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_dK_NLO_mix_High", &weight_VJetsEW_vjets_dK_NLO_mix_High, &b_weight_VJetsEW_vjets_dK_NLO_mix_High);
   fChain->SetBranchAddress("weight_VJetsEW_vjets_dK_NLO_mix_Low", &weight_VJetsEW_vjets_dK_NLO_mix_Low, &b_weight_VJetsEW_vjets_dK_NLO_mix_Low);
   fChain->SetBranchAddress("trigger_pass", &trigger_pass, &b_trigger_pass);
   fChain->SetBranchAddress("trigger_matched_HLT_e60_lhmedium", &trigger_matched_HLT_e60_lhmedium, &b_trigger_matched_HLT_e60_lhmedium);
   fChain->SetBranchAddress("trigger_matched_HLT_e120_lhloose", &trigger_matched_HLT_e120_lhloose, &b_trigger_matched_HLT_e120_lhloose);
   fChain->SetBranchAddress("trigger_matched_HLT_e24_lhmedium_L1EM18VH", &trigger_matched_HLT_e24_lhmedium_L1EM18VH, &b_trigger_matched_HLT_e24_lhmedium_L1EM18VH);
   fChain->SetBranchAddress("trigger_matched_HLT_e24_lhmedium_L1EM20VH", &trigger_matched_HLT_e24_lhmedium_L1EM20VH, &b_trigger_matched_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("pdf_x1", &pdf_x1, &b_pdf_x1);
   fChain->SetBranchAddress("pdf_x2", &pdf_x2, &b_pdf_x2);
   fChain->SetBranchAddress("pdf_pdf1", &pdf_pdf1, &b_pdf_pdf1);
   fChain->SetBranchAddress("pdf_pdf2", &pdf_pdf2, &b_pdf_pdf2);
   fChain->SetBranchAddress("pdf_scale", &pdf_scale, &b_pdf_scale);
   fChain->SetBranchAddress("flag_bib", &flag_bib, &b_flag_bib);
   fChain->SetBranchAddress("flag_bib_raw", &flag_bib_raw, &b_flag_bib_raw);
   fChain->SetBranchAddress("flag_sct", &flag_sct, &b_flag_sct);
   fChain->SetBranchAddress("flag_core", &flag_core, &b_flag_core);
   fChain->SetBranchAddress("trigger_HLT_2e12_lhloose_L12EM10VH", &trigger_HLT_2e12_lhloose_L12EM10VH, &b_trigger_HLT_2e12_lhloose_L12EM10VH);
   fChain->SetBranchAddress("trigger_HLT_2e12_lhvloose_nod0_L12EM10VH", &trigger_HLT_2e12_lhvloose_nod0_L12EM10VH, &b_trigger_HLT_2e12_lhvloose_nod0_L12EM10VH);
//    fChain->SetBranchAddress("trigger_HLT_2e17_lhloose", &trigger_HLT_2e17_lhloose, &b_trigger_HLT_2e17_lhloose);
//    fChain->SetBranchAddress("trigger_HLT_2e17_lhvloose", &trigger_HLT_2e17_lhvloose, &b_trigger_HLT_2e17_lhvloose);
//    fChain->SetBranchAddress("trigger_HLT_2e17_lhvloose_nod0", &trigger_HLT_2e17_lhvloose_nod0, &b_trigger_HLT_2e17_lhvloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_2e17_loose", &trigger_HLT_2e17_loose, &b_trigger_HLT_2e17_loose);
   fChain->SetBranchAddress("trigger_HLT_2mu10", &trigger_HLT_2mu10, &b_trigger_HLT_2mu10);
//    fChain->SetBranchAddress("trigger_HLT_2mu14", &trigger_HLT_2mu14, &b_trigger_HLT_2mu14);
   fChain->SetBranchAddress("trigger_HLT_e120_lhloose_nod0", &trigger_HLT_e120_lhloose_nod0, &b_trigger_HLT_e120_lhloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_e17_lhloose_2e9_lhloose", &trigger_HLT_e17_lhloose_2e9_lhloose, &b_trigger_HLT_e17_lhloose_2e9_lhloose);
   fChain->SetBranchAddress("trigger_HLT_e17_lhloose_nod0_2e9_lhloose_nod0", &trigger_HLT_e17_lhloose_nod0_2e9_lhloose_nod0, &b_trigger_HLT_e17_lhloose_nod0_2e9_lhloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_L1EM18VH", &trigger_HLT_e24_lhmedium_L1EM18VH, &b_trigger_HLT_e24_lhmedium_L1EM18VH);
//    fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_L1EM20VHI", &trigger_HLT_e24_lhmedium_L1EM20VHI, &b_trigger_HLT_e24_lhmedium_L1EM20VHI);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_iloose_L1EM20VH", &trigger_HLT_e24_lhmedium_iloose_L1EM20VH, &b_trigger_HLT_e24_lhmedium_iloose_L1EM20VH);
//    fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_nod0_L1EM20VH", &trigger_HLT_e24_lhmedium_nod0_L1EM20VH, &b_trigger_HLT_e24_lhmedium_nod0_L1EM20VH);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_nod0_ivarloose", &trigger_HLT_e24_lhmedium_nod0_ivarloose, &b_trigger_HLT_e24_lhmedium_nod0_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e24_lhtight_iloose", &trigger_HLT_e24_lhtight_iloose, &b_trigger_HLT_e24_lhtight_iloose);
//    fChain->SetBranchAddress("trigger_HLT_e26_lhtight_ivarloose", &trigger_HLT_e26_lhtight_ivarloose, &b_trigger_HLT_e26_lhtight_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e28_tight_iloose", &trigger_HLT_e28_tight_iloose, &b_trigger_HLT_e28_tight_iloose);
//    fChain->SetBranchAddress("trigger_HLT_e60_medium", &trigger_HLT_e60_medium, &b_trigger_HLT_e60_medium);
//    fChain->SetBranchAddress("trigger_HLT_mu20_2mu4noL1", &trigger_HLT_mu20_2mu4noL1, &b_trigger_HLT_mu20_2mu4noL1);
   fChain->SetBranchAddress("trigger_HLT_mu20_iloose_L1MU15", &trigger_HLT_mu20_iloose_L1MU15, &b_trigger_HLT_mu20_iloose_L1MU15);
//    fChain->SetBranchAddress("trigger_HLT_mu20_ivarloose_L1MU15", &trigger_HLT_mu20_ivarloose_L1MU15, &b_trigger_HLT_mu20_ivarloose_L1MU15);
   fChain->SetBranchAddress("trigger_HLT_mu20_mu8noL1", &trigger_HLT_mu20_mu8noL1, &b_trigger_HLT_mu20_mu8noL1);
//    fChain->SetBranchAddress("trigger_HLT_mu22_mu8noL1", &trigger_HLT_mu22_mu8noL1, &b_trigger_HLT_mu22_mu8noL1);
   fChain->SetBranchAddress("trigger_HLT_mu24_iloose", &trigger_HLT_mu24_iloose, &b_trigger_HLT_mu24_iloose);
   fChain->SetBranchAddress("trigger_HLT_mu24_iloose_L1MU15", &trigger_HLT_mu24_iloose_L1MU15, &b_trigger_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("trigger_HLT_mu24_imedium", &trigger_HLT_mu24_imedium, &b_trigger_HLT_mu24_imedium);
   fChain->SetBranchAddress("trigger_HLT_mu24_ivarloose", &trigger_HLT_mu24_ivarloose, &b_trigger_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_mu24_ivarloose_L1MU15", &trigger_HLT_mu24_ivarloose_L1MU15, &b_trigger_HLT_mu24_ivarloose_L1MU15);
//    fChain->SetBranchAddress("trigger_HLT_mu24_ivarmedium", &trigger_HLT_mu24_ivarmedium, &b_trigger_HLT_mu24_ivarmedium);
//    fChain->SetBranchAddress("trigger_HLT_mu26_imedium", &trigger_HLT_mu26_imedium, &b_trigger_HLT_mu26_imedium);
//    fChain->SetBranchAddress("trigger_HLT_mu26_ivarmedium", &trigger_HLT_mu26_ivarmedium, &b_trigger_HLT_mu26_ivarmedium);
//    fChain->SetBranchAddress("trigger_HLT_mu40", &trigger_HLT_mu40, &b_trigger_HLT_mu40);
//    fChain->SetBranchAddress("trigger_HLT_mu50", &trigger_HLT_mu50, &b_trigger_HLT_mu50);
   fChain->SetBranchAddress("hfor", &hfor, &b_hfor);
   fChain->SetBranchAddress("n_ph", &n_ph, &b_n_ph);
   fChain->SetBranchAddress("n_ph_tight", &n_ph_tight, &b_n_ph_tight);
   fChain->SetBranchAddress("n_ph_baseline", &n_ph_baseline, &b_n_ph_baseline);
   fChain->SetBranchAddress("n_ph_baseline_tight", &n_ph_baseline_tight, &b_n_ph_baseline_tight);
   fChain->SetBranchAddress("pdf_id1", &pdf_id1, &b_pdf_id1);
   fChain->SetBranchAddress("pdf_id2", &pdf_id2, &b_pdf_id2);
   fChain->SetBranchAddress("bb_decision", &bb_decision, &b_bb_decision);
   fChain->SetBranchAddress("shatR", &shatR, &b_shatR);
   fChain->SetBranchAddress("gaminvR", &gaminvR, &b_gaminvR);
   fChain->SetBranchAddress("gaminvRp1", &gaminvRp1, &b_gaminvRp1);
   fChain->SetBranchAddress("dphi_BETA_R", &dphi_BETA_R, &b_dphi_BETA_R);
   fChain->SetBranchAddress("dphi_J1_J2_R", &dphi_J1_J2_R, &b_dphi_J1_J2_R);
   fChain->SetBranchAddress("gamma_Rp1", &gamma_Rp1, &b_gamma_Rp1);
   fChain->SetBranchAddress("costhetaR", &costhetaR, &b_costhetaR);
   fChain->SetBranchAddress("dphi_R_Rp1", &dphi_R_Rp1, &b_dphi_R_Rp1);
   fChain->SetBranchAddress("mdeltaR", &mdeltaR, &b_mdeltaR);
   fChain->SetBranchAddress("cosptR", &cosptR, &b_cosptR);
   fChain->SetBranchAddress("costhetaRp1", &costhetaRp1, &b_costhetaRp1);
   fChain->SetBranchAddress("jj_m", &jj_m, &b_jj_m);
   fChain->SetBranchAddress("mumu_pt", &mumu_pt, &b_mumu_pt);
   fChain->SetBranchAddress("mumu_eta", &mumu_eta, &b_mumu_eta);
   fChain->SetBranchAddress("mumu_phi", &mumu_phi, &b_mumu_phi);
   fChain->SetBranchAddress("ee_pt", &ee_pt, &b_ee_pt);
   fChain->SetBranchAddress("ee_eta", &ee_eta, &b_ee_eta);
   fChain->SetBranchAddress("ee_phi", &ee_phi, &b_ee_phi);
   fChain->SetBranchAddress("n_jet_truth", &n_jet_truth, &b_n_jet_truth);
   fChain->SetBranchAddress("truth_jet1_pt", &truth_jet1_pt, &b_truth_jet1_pt);
   fChain->SetBranchAddress("truth_jet1_eta", &truth_jet1_eta, &b_truth_jet1_eta);
   fChain->SetBranchAddress("truth_jet1_phi", &truth_jet1_phi, &b_truth_jet1_phi);
   fChain->SetBranchAddress("truth_jet1_m", &truth_jet1_m, &b_truth_jet1_m);
   fChain->SetBranchAddress("truth_jet2_pt", &truth_jet2_pt, &b_truth_jet2_pt);
   fChain->SetBranchAddress("truth_jet2_eta", &truth_jet2_eta, &b_truth_jet2_eta);
   fChain->SetBranchAddress("truth_jet2_phi", &truth_jet2_phi, &b_truth_jet2_phi);
   fChain->SetBranchAddress("truth_jet2_m", &truth_jet2_m, &b_truth_jet2_m);
   fChain->SetBranchAddress("truth_ph1_pt", &truth_ph1_pt, &b_truth_ph1_pt);
   fChain->SetBranchAddress("truth_ph1_eta", &truth_ph1_eta, &b_truth_ph1_eta);
   fChain->SetBranchAddress("truth_ph1_phi", &truth_ph1_phi, &b_truth_ph1_phi);
   fChain->SetBranchAddress("truth_ph1_type", &truth_ph1_type, &b_truth_ph1_type);
   fChain->SetBranchAddress("truth_V_pt", &truth_V_pt, &b_truth_V_pt);
   fChain->SetBranchAddress("truth_V_eta", &truth_V_eta, &b_truth_V_eta);
   fChain->SetBranchAddress("truth_V_phi", &truth_V_phi, &b_truth_V_phi);
   fChain->SetBranchAddress("truth_V_m", &truth_V_m, &b_truth_V_m);
   fChain->SetBranchAddress("truth_mu_pt", &truth_mu_pt, &b_truth_mu_pt);
   fChain->SetBranchAddress("truth_mu_eta", &truth_mu_eta, &b_truth_mu_eta);
   fChain->SetBranchAddress("truth_mu_phi", &truth_mu_phi, &b_truth_mu_phi);
   fChain->SetBranchAddress("truth_mu_m", &truth_mu_m, &b_truth_mu_m);
   fChain->SetBranchAddress("truth_mu_status", &truth_mu_status, &b_truth_mu_status);
   fChain->SetBranchAddress("evsf_baseline_nominal_EL", &evsf_baseline_nominal_EL, &b_evsf_baseline_nominal_EL);
   fChain->SetBranchAddress("evsf_baseline_nominal_MU", &evsf_baseline_nominal_MU, &b_evsf_baseline_nominal_MU);
   fChain->SetBranchAddress("evsf_baseline_nominal_PH", &evsf_baseline_nominal_PH, &b_evsf_baseline_nominal_PH);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP11__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP12__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP13__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP14__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP15__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP16__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP17__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_ChargeIDSel_SIMPLIFIED_UncorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP11__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP12__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP13__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP14__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_ID_CorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_CorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP11__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP12__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP13__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP14__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP15__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP16__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP17__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_Iso_SIMPLIFIED_UncorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_CorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP11__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP12__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP13__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP14__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP15__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP16__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP17__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_Reco_SIMPLIFIED_UncorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP11__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP12__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP13__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP14__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP15__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP16__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP17__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_TriggerEff_SIMPLIFIED_UncorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP0__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP10__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP11__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP12__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP13__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP14__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP15__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP16__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP17__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP1__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP2__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP3__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP4__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP5__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP6__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP7__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP8__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1down", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1down, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1up", &evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1up, &b_evsf_baseline0_syst_EL_EFF_Trigger_SIMPLIFIED_UncorrUncertaintyNP9__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1down", &evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1down, &b_evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1up", &evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1up, &b_evsf_baseline0_syst_MUON_EFF_BADMUON_STAT__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1down", &evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1down, &b_evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1up", &evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1up, &b_evsf_baseline0_syst_MUON_EFF_BADMUON_SYS__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_ISO_STAT__1down", &evsf_baseline0_syst_MUON_EFF_ISO_STAT__1down, &b_evsf_baseline0_syst_MUON_EFF_ISO_STAT__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_ISO_STAT__1up", &evsf_baseline0_syst_MUON_EFF_ISO_STAT__1up, &b_evsf_baseline0_syst_MUON_EFF_ISO_STAT__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_ISO_SYS__1down", &evsf_baseline0_syst_MUON_EFF_ISO_SYS__1down, &b_evsf_baseline0_syst_MUON_EFF_ISO_SYS__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_ISO_SYS__1up", &evsf_baseline0_syst_MUON_EFF_ISO_SYS__1up, &b_evsf_baseline0_syst_MUON_EFF_ISO_SYS__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1down", &evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1down, &b_evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1up", &evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1up, &b_evsf_baseline0_syst_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_STAT__1down", &evsf_baseline0_syst_MUON_EFF_RECO_STAT__1down, &b_evsf_baseline0_syst_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_STAT__1up", &evsf_baseline0_syst_MUON_EFF_RECO_STAT__1up, &b_evsf_baseline0_syst_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1down", &evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1down, &b_evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1up", &evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1up, &b_evsf_baseline0_syst_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_SYS__1down", &evsf_baseline0_syst_MUON_EFF_RECO_SYS__1down, &b_evsf_baseline0_syst_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_RECO_SYS__1up", &evsf_baseline0_syst_MUON_EFF_RECO_SYS__1up, &b_evsf_baseline0_syst_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1down", &evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1down, &b_evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1up", &evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1up, &b_evsf_baseline0_syst_MUON_EFF_TTVA_STAT__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1down", &evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1down, &b_evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1up", &evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1up, &b_evsf_baseline0_syst_MUON_EFF_TTVA_SYS__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1down", &evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1down, &b_evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1up", &evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1up, &b_evsf_baseline0_syst_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1down", &evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1down, &b_evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1up", &evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1up, &b_evsf_baseline0_syst_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1down", &evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1down, &b_evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1up", &evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1up, &b_evsf_baseline0_syst_PH_EFF_ID_Uncertainty__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1down", &evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1down, &b_evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1up", &evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1up, &b_evsf_baseline0_syst_PH_EFF_LOWPTISO_Uncertainty__1up);
   fChain->SetBranchAddress("evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1down", &evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1down, &b_evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1down);
   fChain->SetBranchAddress("evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1up", &evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1up, &b_evsf_baseline0_syst_PH_EFF_TRKISO_Uncertainty__1up);
   fChain->SetBranchAddress("met_eleterm_et", &met_eleterm_et, &b_met_eleterm_et);
   fChain->SetBranchAddress("met_eleterm_etx", &met_eleterm_etx, &b_met_eleterm_etx);
   fChain->SetBranchAddress("met_eleterm_ety", &met_eleterm_ety, &b_met_eleterm_ety);
   fChain->SetBranchAddress("met_eleterm_sumet", &met_eleterm_sumet, &b_met_eleterm_sumet);
   fChain->SetBranchAddress("met_eleterm_phi", &met_eleterm_phi, &b_met_eleterm_phi);
   fChain->SetBranchAddress("met_jetterm_et", &met_jetterm_et, &b_met_jetterm_et);
   fChain->SetBranchAddress("met_jetterm_etx", &met_jetterm_etx, &b_met_jetterm_etx);
   fChain->SetBranchAddress("met_jetterm_ety", &met_jetterm_ety, &b_met_jetterm_ety);
   fChain->SetBranchAddress("met_jetterm_sumet", &met_jetterm_sumet, &b_met_jetterm_sumet);
   fChain->SetBranchAddress("met_jetterm_phi", &met_jetterm_phi, &b_met_jetterm_phi);
   fChain->SetBranchAddress("met_muonterm_et", &met_muonterm_et, &b_met_muonterm_et);
   fChain->SetBranchAddress("met_muonterm_etx", &met_muonterm_etx, &b_met_muonterm_etx);
   fChain->SetBranchAddress("met_muonterm_ety", &met_muonterm_ety, &b_met_muonterm_ety);
   fChain->SetBranchAddress("met_muonterm_sumet", &met_muonterm_sumet, &b_met_muonterm_sumet);
   fChain->SetBranchAddress("met_muonterm_phi", &met_muonterm_phi, &b_met_muonterm_phi);
   fChain->SetBranchAddress("met_muonterm_cst_et", &met_muonterm_cst_et, &b_met_muonterm_cst_et);
   fChain->SetBranchAddress("met_muonterm_cst_etx", &met_muonterm_cst_etx, &b_met_muonterm_cst_etx);
   fChain->SetBranchAddress("met_muonterm_cst_ety", &met_muonterm_cst_ety, &b_met_muonterm_cst_ety);
   fChain->SetBranchAddress("met_muonterm_cst_sumet", &met_muonterm_cst_sumet, &b_met_muonterm_cst_sumet);
   fChain->SetBranchAddress("met_muonterm_cst_phi", &met_muonterm_cst_phi, &b_met_muonterm_cst_phi);
   fChain->SetBranchAddress("met_muonterm_tst_et", &met_muonterm_tst_et, &b_met_muonterm_tst_et);
   fChain->SetBranchAddress("met_muonterm_tst_etx", &met_muonterm_tst_etx, &b_met_muonterm_tst_etx);
   fChain->SetBranchAddress("met_muonterm_tst_ety", &met_muonterm_tst_ety, &b_met_muonterm_tst_ety);
   fChain->SetBranchAddress("met_muonterm_tst_sumet", &met_muonterm_tst_sumet, &b_met_muonterm_tst_sumet);
   fChain->SetBranchAddress("met_muonterm_tst_phi", &met_muonterm_tst_phi, &b_met_muonterm_tst_phi);
   fChain->SetBranchAddress("met_noelectron_cst_et", &met_noelectron_cst_et, &b_met_noelectron_cst_et);
   fChain->SetBranchAddress("met_noelectron_cst_etx", &met_noelectron_cst_etx, &b_met_noelectron_cst_etx);
   fChain->SetBranchAddress("met_noelectron_cst_ety", &met_noelectron_cst_ety, &b_met_noelectron_cst_ety);
   fChain->SetBranchAddress("met_noelectron_cst_sumet", &met_noelectron_cst_sumet, &b_met_noelectron_cst_sumet);
   fChain->SetBranchAddress("met_noelectron_cst_phi", &met_noelectron_cst_phi, &b_met_noelectron_cst_phi);
   fChain->SetBranchAddress("met_noelectron_tst_et", &met_noelectron_tst_et, &b_met_noelectron_tst_et);
   fChain->SetBranchAddress("met_noelectron_tst_etx", &met_noelectron_tst_etx, &b_met_noelectron_tst_etx);
   fChain->SetBranchAddress("met_noelectron_tst_ety", &met_noelectron_tst_ety, &b_met_noelectron_tst_ety);
   fChain->SetBranchAddress("met_noelectron_tst_sumet", &met_noelectron_tst_sumet, &b_met_noelectron_tst_sumet);
   fChain->SetBranchAddress("met_noelectron_tst_phi", &met_noelectron_tst_phi, &b_met_noelectron_tst_phi);
   fChain->SetBranchAddress("met_nomuon_cst_et", &met_nomuon_cst_et, &b_met_nomuon_cst_et);
   fChain->SetBranchAddress("met_nomuon_cst_etx", &met_nomuon_cst_etx, &b_met_nomuon_cst_etx);
   fChain->SetBranchAddress("met_nomuon_cst_ety", &met_nomuon_cst_ety, &b_met_nomuon_cst_ety);
   fChain->SetBranchAddress("met_nomuon_cst_sumet", &met_nomuon_cst_sumet, &b_met_nomuon_cst_sumet);
   fChain->SetBranchAddress("met_nomuon_cst_phi", &met_nomuon_cst_phi, &b_met_nomuon_cst_phi);
   fChain->SetBranchAddress("met_nomuon_tst_et", &met_nomuon_tst_et, &b_met_nomuon_tst_et);
   fChain->SetBranchAddress("met_nomuon_tst_etx", &met_nomuon_tst_etx, &b_met_nomuon_tst_etx);
   fChain->SetBranchAddress("met_nomuon_tst_ety", &met_nomuon_tst_ety, &b_met_nomuon_tst_ety);
   fChain->SetBranchAddress("met_nomuon_tst_sumet", &met_nomuon_tst_sumet, &b_met_nomuon_tst_sumet);
   fChain->SetBranchAddress("met_nomuon_tst_phi", &met_nomuon_tst_phi, &b_met_nomuon_tst_phi);
   fChain->SetBranchAddress("met_nophcalib_nomuon_cst_et", &met_nophcalib_nomuon_cst_et, &b_met_nophcalib_nomuon_cst_et);
   fChain->SetBranchAddress("met_nophcalib_nomuon_cst_etx", &met_nophcalib_nomuon_cst_etx, &b_met_nophcalib_nomuon_cst_etx);
   fChain->SetBranchAddress("met_nophcalib_nomuon_cst_ety", &met_nophcalib_nomuon_cst_ety, &b_met_nophcalib_nomuon_cst_ety);
   fChain->SetBranchAddress("met_nophcalib_nomuon_cst_sumet", &met_nophcalib_nomuon_cst_sumet, &b_met_nophcalib_nomuon_cst_sumet);
   fChain->SetBranchAddress("met_nophcalib_nomuon_cst_phi", &met_nophcalib_nomuon_cst_phi, &b_met_nophcalib_nomuon_cst_phi);
   fChain->SetBranchAddress("met_nophcalib_nomuon_tst_et", &met_nophcalib_nomuon_tst_et, &b_met_nophcalib_nomuon_tst_et);
   fChain->SetBranchAddress("met_nophcalib_nomuon_tst_etx", &met_nophcalib_nomuon_tst_etx, &b_met_nophcalib_nomuon_tst_etx);
   fChain->SetBranchAddress("met_nophcalib_nomuon_tst_ety", &met_nophcalib_nomuon_tst_ety, &b_met_nophcalib_nomuon_tst_ety);
   fChain->SetBranchAddress("met_nophcalib_nomuon_tst_sumet", &met_nophcalib_nomuon_tst_sumet, &b_met_nophcalib_nomuon_tst_sumet);
   fChain->SetBranchAddress("met_nophcalib_nomuon_tst_phi", &met_nophcalib_nomuon_tst_phi, &b_met_nophcalib_nomuon_tst_phi);
   fChain->SetBranchAddress("met_nophcalib_wmuon_cst_et", &met_nophcalib_wmuon_cst_et, &b_met_nophcalib_wmuon_cst_et);
   fChain->SetBranchAddress("met_nophcalib_wmuon_cst_etx", &met_nophcalib_wmuon_cst_etx, &b_met_nophcalib_wmuon_cst_etx);
   fChain->SetBranchAddress("met_nophcalib_wmuon_cst_ety", &met_nophcalib_wmuon_cst_ety, &b_met_nophcalib_wmuon_cst_ety);
   fChain->SetBranchAddress("met_nophcalib_wmuon_cst_sumet", &met_nophcalib_wmuon_cst_sumet, &b_met_nophcalib_wmuon_cst_sumet);
   fChain->SetBranchAddress("met_nophcalib_wmuon_cst_phi", &met_nophcalib_wmuon_cst_phi, &b_met_nophcalib_wmuon_cst_phi);
   fChain->SetBranchAddress("met_nophcalib_wmuon_tst_et", &met_nophcalib_wmuon_tst_et, &b_met_nophcalib_wmuon_tst_et);
   fChain->SetBranchAddress("met_nophcalib_wmuon_tst_etx", &met_nophcalib_wmuon_tst_etx, &b_met_nophcalib_wmuon_tst_etx);
   fChain->SetBranchAddress("met_nophcalib_wmuon_tst_ety", &met_nophcalib_wmuon_tst_ety, &b_met_nophcalib_wmuon_tst_ety);
   fChain->SetBranchAddress("met_nophcalib_wmuon_tst_sumet", &met_nophcalib_wmuon_tst_sumet, &b_met_nophcalib_wmuon_tst_sumet);
   fChain->SetBranchAddress("met_nophcalib_wmuon_tst_phi", &met_nophcalib_wmuon_tst_phi, &b_met_nophcalib_wmuon_tst_phi);
   fChain->SetBranchAddress("met_nophoton_cst_et", &met_nophoton_cst_et, &b_met_nophoton_cst_et);
   fChain->SetBranchAddress("met_nophoton_cst_etx", &met_nophoton_cst_etx, &b_met_nophoton_cst_etx);
   fChain->SetBranchAddress("met_nophoton_cst_ety", &met_nophoton_cst_ety, &b_met_nophoton_cst_ety);
   fChain->SetBranchAddress("met_nophoton_cst_sumet", &met_nophoton_cst_sumet, &b_met_nophoton_cst_sumet);
   fChain->SetBranchAddress("met_nophoton_cst_phi", &met_nophoton_cst_phi, &b_met_nophoton_cst_phi);
   fChain->SetBranchAddress("met_nophoton_tst_et", &met_nophoton_tst_et, &b_met_nophoton_tst_et);
   fChain->SetBranchAddress("met_nophoton_tst_etx", &met_nophoton_tst_etx, &b_met_nophoton_tst_etx);
   fChain->SetBranchAddress("met_nophoton_tst_ety", &met_nophoton_tst_ety, &b_met_nophoton_tst_ety);
   fChain->SetBranchAddress("met_nophoton_tst_sumet", &met_nophoton_tst_sumet, &b_met_nophoton_tst_sumet);
   fChain->SetBranchAddress("met_nophoton_tst_phi", &met_nophoton_tst_phi, &b_met_nophoton_tst_phi);
   fChain->SetBranchAddress("met_phterm_et", &met_phterm_et, &b_met_phterm_et);
   fChain->SetBranchAddress("met_phterm_etx", &met_phterm_etx, &b_met_phterm_etx);
   fChain->SetBranchAddress("met_phterm_ety", &met_phterm_ety, &b_met_phterm_ety);
   fChain->SetBranchAddress("met_phterm_sumet", &met_phterm_sumet, &b_met_phterm_sumet);
   fChain->SetBranchAddress("met_phterm_phi", &met_phterm_phi, &b_met_phterm_phi);
   fChain->SetBranchAddress("met_softerm_cst_et", &met_softerm_cst_et, &b_met_softerm_cst_et);
   fChain->SetBranchAddress("met_softerm_cst_etx", &met_softerm_cst_etx, &b_met_softerm_cst_etx);
   fChain->SetBranchAddress("met_softerm_cst_ety", &met_softerm_cst_ety, &b_met_softerm_cst_ety);
   fChain->SetBranchAddress("met_softerm_cst_sumet", &met_softerm_cst_sumet, &b_met_softerm_cst_sumet);
   fChain->SetBranchAddress("met_softerm_cst_phi", &met_softerm_cst_phi, &b_met_softerm_cst_phi);
   fChain->SetBranchAddress("met_softerm_tst_et", &met_softerm_tst_et, &b_met_softerm_tst_et);
   fChain->SetBranchAddress("met_softerm_tst_etx", &met_softerm_tst_etx, &b_met_softerm_tst_etx);
   fChain->SetBranchAddress("met_softerm_tst_ety", &met_softerm_tst_ety, &b_met_softerm_tst_ety);
   fChain->SetBranchAddress("met_softerm_tst_sumet", &met_softerm_tst_sumet, &b_met_softerm_tst_sumet);
   fChain->SetBranchAddress("met_softerm_tst_phi", &met_softerm_tst_phi, &b_met_softerm_tst_phi);
   fChain->SetBranchAddress("met_track_et", &met_track_et, &b_met_track_et);
   fChain->SetBranchAddress("met_track_etx", &met_track_etx, &b_met_track_etx);
   fChain->SetBranchAddress("met_track_ety", &met_track_ety, &b_met_track_ety);
   fChain->SetBranchAddress("met_track_sumet", &met_track_sumet, &b_met_track_sumet);
   fChain->SetBranchAddress("met_track_phi", &met_track_phi, &b_met_track_phi);
   fChain->SetBranchAddress("met_truth_et", &met_truth_et, &b_met_truth_et);
   fChain->SetBranchAddress("met_truth_etx", &met_truth_etx, &b_met_truth_etx);
   fChain->SetBranchAddress("met_truth_ety", &met_truth_ety, &b_met_truth_ety);
   fChain->SetBranchAddress("met_truth_sumet", &met_truth_sumet, &b_met_truth_sumet);
   fChain->SetBranchAddress("met_truth_phi", &met_truth_phi, &b_met_truth_phi);
   fChain->SetBranchAddress("met_wmuon_cst_et", &met_wmuon_cst_et, &b_met_wmuon_cst_et);
   fChain->SetBranchAddress("met_wmuon_cst_etx", &met_wmuon_cst_etx, &b_met_wmuon_cst_etx);
   fChain->SetBranchAddress("met_wmuon_cst_ety", &met_wmuon_cst_ety, &b_met_wmuon_cst_ety);
   fChain->SetBranchAddress("met_wmuon_cst_sumet", &met_wmuon_cst_sumet, &b_met_wmuon_cst_sumet);
   fChain->SetBranchAddress("met_wmuon_cst_phi", &met_wmuon_cst_phi, &b_met_wmuon_cst_phi);
   fChain->SetBranchAddress("met_wmuon_tst_et", &met_wmuon_tst_et, &b_met_wmuon_tst_et);
   fChain->SetBranchAddress("met_wmuon_tst_etx", &met_wmuon_tst_etx, &b_met_wmuon_tst_etx);
   fChain->SetBranchAddress("met_wmuon_tst_ety", &met_wmuon_tst_ety, &b_met_wmuon_tst_ety);
   fChain->SetBranchAddress("met_wmuon_tst_sumet", &met_wmuon_tst_sumet, &b_met_wmuon_tst_sumet);
   fChain->SetBranchAddress("met_wmuon_tst_phi", &met_wmuon_tst_phi, &b_met_wmuon_tst_phi);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_ptErr", &mu_ptErr, &b_mu_ptErr);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_SF", &mu_SF, &b_mu_SF);
   fChain->SetBranchAddress("mu_SF_iso", &mu_SF_iso, &b_mu_SF_iso);
   fChain->SetBranchAddress("mu_isotool_pass_loosetrackonly", &mu_isotool_pass_loosetrackonly, &b_mu_isotool_pass_loosetrackonly);
   fChain->SetBranchAddress("mu_isotool_pass_fixedcuttighttrackonly", &mu_isotool_pass_fixedcuttighttrackonly, &b_mu_isotool_pass_fixedcuttighttrackonly);
   fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_d0", &mu_d0, &b_mu_d0);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_z0", &mu_z0, &b_mu_z0);
   fChain->SetBranchAddress("mu_z0sig", &mu_z0sig, &b_mu_z0sig);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_id_pt", &mu_id_pt, &b_mu_id_pt);
   fChain->SetBranchAddress("mu_id_ptErr", &mu_id_ptErr, &b_mu_id_ptErr);
   fChain->SetBranchAddress("mu_id_eta", &mu_id_eta, &b_mu_id_eta);
   fChain->SetBranchAddress("mu_id_phi", &mu_id_phi, &b_mu_id_phi);
   fChain->SetBranchAddress("mu_id_m", &mu_id_m, &b_mu_id_m);
   fChain->SetBranchAddress("mu_id_qop", &mu_id_qop, &b_mu_id_qop);
   fChain->SetBranchAddress("mu_id_qoperr", &mu_id_qoperr, &b_mu_id_qoperr);
   fChain->SetBranchAddress("mu_ms_pt", &mu_ms_pt, &b_mu_ms_pt);
   fChain->SetBranchAddress("mu_ms_ptErr", &mu_ms_ptErr, &b_mu_ms_ptErr);
   fChain->SetBranchAddress("mu_ms_eta", &mu_ms_eta, &b_mu_ms_eta);
   fChain->SetBranchAddress("mu_ms_phi", &mu_ms_phi, &b_mu_ms_phi);
   fChain->SetBranchAddress("mu_ms_m", &mu_ms_m, &b_mu_ms_m);
   fChain->SetBranchAddress("mu_ms_qop", &mu_ms_qop, &b_mu_ms_qop);
   fChain->SetBranchAddress("mu_ms_qoperr", &mu_ms_qoperr, &b_mu_ms_qoperr);
   fChain->SetBranchAddress("mu_me_pt", &mu_me_pt, &b_mu_me_pt);
   fChain->SetBranchAddress("mu_me_ptErr", &mu_me_ptErr, &b_mu_me_ptErr);
   fChain->SetBranchAddress("mu_me_eta", &mu_me_eta, &b_mu_me_eta);
   fChain->SetBranchAddress("mu_me_phi", &mu_me_phi, &b_mu_me_phi);
   fChain->SetBranchAddress("mu_me_m", &mu_me_m, &b_mu_me_m);
   fChain->SetBranchAddress("mu_me_qop", &mu_me_qop, &b_mu_me_qop);
   fChain->SetBranchAddress("mu_me_qoperr", &mu_me_qoperr, &b_mu_me_qoperr);
   fChain->SetBranchAddress("mu_mse_pt", &mu_mse_pt, &b_mu_mse_pt);
   fChain->SetBranchAddress("mu_mse_ptErr", &mu_mse_ptErr, &b_mu_mse_ptErr);
   fChain->SetBranchAddress("mu_mse_eta", &mu_mse_eta, &b_mu_mse_eta);
   fChain->SetBranchAddress("mu_mse_phi", &mu_mse_phi, &b_mu_mse_phi);
   fChain->SetBranchAddress("mu_mse_m", &mu_mse_m, &b_mu_mse_m);
   fChain->SetBranchAddress("mu_mse_qop", &mu_mse_qop, &b_mu_mse_qop);
   fChain->SetBranchAddress("mu_mse_qoperr", &mu_mse_qoperr, &b_mu_mse_qoperr);
   fChain->SetBranchAddress("mu_ptcone20", &mu_ptcone20, &b_mu_ptcone20);
   fChain->SetBranchAddress("mu_ptvarcone20", &mu_ptvarcone20, &b_mu_ptvarcone20);
   fChain->SetBranchAddress("mu_etcone20", &mu_etcone20, &b_mu_etcone20);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptcone30", &mu_ptcone30, &b_mu_ptcone30);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_etcone30", &mu_etcone30, &b_mu_etcone30);
   fChain->SetBranchAddress("mu_topoetcone30", &mu_topoetcone30, &b_mu_topoetcone30);
   fChain->SetBranchAddress("mu_ptcone40", &mu_ptcone40, &b_mu_ptcone40);
   fChain->SetBranchAddress("mu_ptvarcone40", &mu_ptvarcone40, &b_mu_ptvarcone40);
   fChain->SetBranchAddress("mu_etcone40", &mu_etcone40, &b_mu_etcone40);
   fChain->SetBranchAddress("mu_topoetcone40", &mu_topoetcone40, &b_mu_topoetcone40);
   fChain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
   fChain->SetBranchAddress("mu_quality", &mu_quality, &b_mu_quality);
   fChain->SetBranchAddress("mu_isSA", &mu_isSA, &b_mu_isSA);
   fChain->SetBranchAddress("mu_isST", &mu_isST, &b_mu_isST);
   fChain->SetBranchAddress("mu_isCB", &mu_isCB, &b_mu_isCB);
   fChain->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
   fChain->SetBranchAddress("mu_isMedium", &mu_isMedium, &b_mu_isMedium);
   fChain->SetBranchAddress("mu_isLoose", &mu_isLoose, &b_mu_isLoose);
   fChain->SetBranchAddress("mu_isVeryLoose", &mu_isVeryLoose, &b_mu_isVeryLoose);
   fChain->SetBranchAddress("mu_isHighPt", &mu_isHighPt, &b_mu_isHighPt);
   fChain->SetBranchAddress("mu_isBad", &mu_isBad, &b_mu_isBad);
   fChain->SetBranchAddress("mu_isBadHighPt", &mu_isBadHighPt, &b_mu_isBadHighPt);
   fChain->SetBranchAddress("mu_finalFitPt", &mu_finalFitPt, &b_mu_finalFitPt);
   fChain->SetBranchAddress("mu_finalFitPtErr", &mu_finalFitPtErr, &b_mu_finalFitPtErr);
   fChain->SetBranchAddress("mu_finalFitEta", &mu_finalFitEta, &b_mu_finalFitEta);
   fChain->SetBranchAddress("mu_finalFitPhi", &mu_finalFitPhi, &b_mu_finalFitPhi);
   fChain->SetBranchAddress("mu_finalFitQOverP", &mu_finalFitQOverP, &b_mu_finalFitQOverP);
   fChain->SetBranchAddress("mu_finalFitQOverPErr", &mu_finalFitQOverPErr, &b_mu_finalFitQOverPErr);
   fChain->SetBranchAddress("mu_truthEta", &mu_truthEta, &b_mu_truthEta);
   fChain->SetBranchAddress("mu_truthPhi", &mu_truthPhi, &b_mu_truthPhi);
   fChain->SetBranchAddress("mu_truthPt", &mu_truthPt, &b_mu_truthPt);
   fChain->SetBranchAddress("mu_truthTheta", &mu_truthTheta, &b_mu_truthTheta);
   fChain->SetBranchAddress("mu_truthQOverP", &mu_truthQOverP, &b_mu_truthQOverP);
   fChain->SetBranchAddress("mu_isotool_pass_loose", &mu_isotool_pass_loose, &b_mu_isotool_pass_loose);
   fChain->SetBranchAddress("mu_isotool_pass_gradient", &mu_isotool_pass_gradient, &b_mu_isotool_pass_gradient);
   fChain->SetBranchAddress("mu_isotool_pass_gradientloose", &mu_isotool_pass_gradientloose, &b_mu_isotool_pass_gradientloose);
   fChain->SetBranchAddress("mu_met_nomuon_dphi", &mu_met_nomuon_dphi, &b_mu_met_nomuon_dphi);
   fChain->SetBranchAddress("mu_met_wmuon_dphi", &mu_met_wmuon_dphi, &b_mu_met_wmuon_dphi);
   fChain->SetBranchAddress("mu_truth_origin", &mu_truth_origin, &b_mu_truth_origin);
   fChain->SetBranchAddress("mu_truth_type", &mu_truth_type, &b_mu_truth_type);
   fChain->SetBranchAddress("mu_muonType", &mu_muonType, &b_mu_muonType);
   fChain->SetBranchAddress("mu_momentumBalanceSignificance", &mu_momentumBalanceSignificance, &b_mu_momentumBalanceSignificance);
   fChain->SetBranchAddress("mu_CaloLRLikelihood", &mu_CaloLRLikelihood, &b_mu_CaloLRLikelihood);
   fChain->SetBranchAddress("mu_CaloMuonIDTag", &mu_CaloMuonIDTag, &b_mu_CaloMuonIDTag);
   fChain->SetBranchAddress("mu_numberOfPrecisionLayers", &mu_numberOfPrecisionLayers, &b_mu_numberOfPrecisionLayers);
   fChain->SetBranchAddress("mu_numberOfPrecisionHoleLayers", &mu_numberOfPrecisionHoleLayers, &b_mu_numberOfPrecisionHoleLayers);
   fChain->SetBranchAddress("mu_numberOfGoodPrecisionLayers", &mu_numberOfGoodPrecisionLayers, &b_mu_numberOfGoodPrecisionLayers);
   fChain->SetBranchAddress("mu_innerSmallHits", &mu_innerSmallHits, &b_mu_innerSmallHits);
   fChain->SetBranchAddress("mu_innerLargeHits", &mu_innerLargeHits, &b_mu_innerLargeHits);
   fChain->SetBranchAddress("mu_innerSmallHoles", &mu_innerSmallHoles, &b_mu_innerSmallHoles);
   fChain->SetBranchAddress("mu_innerLargeHoles", &mu_innerLargeHoles, &b_mu_innerLargeHoles);
   fChain->SetBranchAddress("mu_middleSmallHoles", &mu_middleSmallHoles, &b_mu_middleSmallHoles);
   fChain->SetBranchAddress("mu_middleLargeHoles", &mu_middleLargeHoles, &b_mu_middleLargeHoles);
   fChain->SetBranchAddress("mu_outerSmallHoles", &mu_outerSmallHoles, &b_mu_outerSmallHoles);
   fChain->SetBranchAddress("mu_outerLargeHoles", &mu_outerLargeHoles, &b_mu_outerLargeHoles);
   fChain->SetBranchAddress("mu_extendedSmallHoles", &mu_extendedSmallHoles, &b_mu_extendedSmallHoles);
   fChain->SetBranchAddress("mu_extendedLargeHoles", &mu_extendedLargeHoles, &b_mu_extendedLargeHoles);
   fChain->SetBranchAddress("mu_innerClosePrecisionHits", &mu_innerClosePrecisionHits, &b_mu_innerClosePrecisionHits);
   fChain->SetBranchAddress("mu_middleClosePrecisionHits", &mu_middleClosePrecisionHits, &b_mu_middleClosePrecisionHits);
   fChain->SetBranchAddress("mu_outerClosePrecisionHits", &mu_outerClosePrecisionHits, &b_mu_outerClosePrecisionHits);
   fChain->SetBranchAddress("mu_extendedClosePrecisionHits", &mu_extendedClosePrecisionHits, &b_mu_extendedClosePrecisionHits);
   fChain->SetBranchAddress("mu_innerOutBoundsPrecisionHits", &mu_innerOutBoundsPrecisionHits, &b_mu_innerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_middleOutBoundsPrecisionHits", &mu_middleOutBoundsPrecisionHits, &b_mu_middleOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_outerOutBoundsPrecisionHits", &mu_outerOutBoundsPrecisionHits, &b_mu_outerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_extendedOutBoundsPrecisionHits", &mu_extendedOutBoundsPrecisionHits, &b_mu_extendedOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_combinedTrackOutBoundsPrecisionHits", &mu_combinedTrackOutBoundsPrecisionHits, &b_mu_combinedTrackOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_middleLargeHits", &mu_middleLargeHits, &b_mu_middleLargeHits);
   fChain->SetBranchAddress("mu_middleSmallHits", &mu_middleSmallHits, &b_mu_middleSmallHits);
   fChain->SetBranchAddress("mu_outerLargeHits", &mu_outerLargeHits, &b_mu_outerLargeHits);
   fChain->SetBranchAddress("mu_outerSmallHits", &mu_outerSmallHits, &b_mu_outerSmallHits);
   fChain->SetBranchAddress("mu_extendedSmallHits", &mu_extendedSmallHits, &b_mu_extendedSmallHits);
   fChain->SetBranchAddress("mu_extendedLargeHits", &mu_extendedLargeHits, &b_mu_extendedLargeHits);
   fChain->SetBranchAddress("mu_numberOfPixelHits", &mu_numberOfPixelHits, &b_mu_numberOfPixelHits);
   fChain->SetBranchAddress("mu_numberOfPixelDeadSensors", &mu_numberOfPixelDeadSensors, &b_mu_numberOfPixelDeadSensors);
   fChain->SetBranchAddress("mu_numberOfSCTHits", &mu_numberOfSCTHits, &b_mu_numberOfSCTHits);
   fChain->SetBranchAddress("mu_numberOfSCTDeadSensors", &mu_numberOfSCTDeadSensors, &b_mu_numberOfSCTDeadSensors);
   fChain->SetBranchAddress("mu_numberOfPixelHoles", &mu_numberOfPixelHoles, &b_mu_numberOfPixelHoles);
   fChain->SetBranchAddress("mu_numberOfSCTHoles", &mu_numberOfSCTHoles, &b_mu_numberOfSCTHoles);
   fChain->SetBranchAddress("mu_numberOfTRTHits", &mu_numberOfTRTHits, &b_mu_numberOfTRTHits);
   fChain->SetBranchAddress("mu_numberOfTRTOutliers", &mu_numberOfTRTOutliers, &b_mu_numberOfTRTOutliers);
   fChain->SetBranchAddress("mu_primarytrketa", &mu_primarytrketa, &b_mu_primarytrketa);
   fChain->SetBranchAddress("mu_numberOfInnermostPixelLayerHits", &mu_numberOfInnermostPixelLayerHits, &b_mu_numberOfInnermostPixelLayerHits);
   fChain->SetBranchAddress("mu_cbtrketa", &mu_cbtrketa, &b_mu_cbtrketa);
   fChain->SetBranchAddress("mu_spectrometerFieldIntegral", &mu_spectrometerFieldIntegral, &b_mu_spectrometerFieldIntegral);
   fChain->SetBranchAddress("mu_scatteringCurvatureSignificance", &mu_scatteringCurvatureSignificance, &b_mu_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("mu_scatteringNeighbourSignificance", &mu_scatteringNeighbourSignificance, &b_mu_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("mu_allAuthors", &mu_allAuthors, &b_mu_allAuthors);
   fChain->SetBranchAddress("mu_numberOfPhiLayers", &mu_numberOfPhiLayers, &b_mu_numberOfPhiLayers);
   fChain->SetBranchAddress("mu_numberOfPhiHoleLayers", &mu_numberOfPhiHoleLayers, &b_mu_numberOfPhiHoleLayers);
   fChain->SetBranchAddress("mu_numberOfTriggerEtaLayers", &mu_numberOfTriggerEtaLayers, &b_mu_numberOfTriggerEtaLayers);
   fChain->SetBranchAddress("mu_numberOfTriggerEtaHoleLayers", &mu_numberOfTriggerEtaHoleLayers, &b_mu_numberOfTriggerEtaHoleLayers);
   fChain->SetBranchAddress("mu_nUnspoiledCscHits", &mu_nUnspoiledCscHits, &b_mu_nUnspoiledCscHits);
   fChain->SetBranchAddress("mu_FSR_CandidateEnergy", &mu_FSR_CandidateEnergy, &b_mu_FSR_CandidateEnergy);
   fChain->SetBranchAddress("mu_EnergyLoss", &mu_EnergyLoss, &b_mu_EnergyLoss);
   fChain->SetBranchAddress("mu_EnergyLossSigma", &mu_EnergyLossSigma, &b_mu_EnergyLossSigma);
   fChain->SetBranchAddress("mu_ParamEnergyLoss", &mu_ParamEnergyLoss, &b_mu_ParamEnergyLoss);
   fChain->SetBranchAddress("mu_ParamEnergyLossSigmaPlus", &mu_ParamEnergyLossSigmaPlus, &b_mu_ParamEnergyLossSigmaPlus);
   fChain->SetBranchAddress("mu_ParamEnergyLossSigmaMinus", &mu_ParamEnergyLossSigmaMinus, &b_mu_ParamEnergyLossSigmaMinus);
   fChain->SetBranchAddress("mu_MeasEnergyLoss", &mu_MeasEnergyLoss, &b_mu_MeasEnergyLoss);
   fChain->SetBranchAddress("mu_MeasEnergyLossSigma", &mu_MeasEnergyLossSigma, &b_mu_MeasEnergyLossSigma);
   fChain->SetBranchAddress("mu_msInnerMatchChi2", &mu_msInnerMatchChi2, &b_mu_msInnerMatchChi2);
   fChain->SetBranchAddress("mu_msInnerMatchDOF", &mu_msInnerMatchDOF, &b_mu_msInnerMatchDOF);
   fChain->SetBranchAddress("mu_energyLossType", &mu_energyLossType, &b_mu_energyLossType);
   fChain->SetBranchAddress("mu_phiLayer1Hits", &mu_phiLayer1Hits, &b_mu_phiLayer1Hits);
   fChain->SetBranchAddress("mu_phiLayer2Hits", &mu_phiLayer2Hits, &b_mu_phiLayer2Hits);
   fChain->SetBranchAddress("mu_phiLayer3Hits", &mu_phiLayer3Hits, &b_mu_phiLayer3Hits);
   fChain->SetBranchAddress("mu_phiLayer4Hits", &mu_phiLayer4Hits, &b_mu_phiLayer4Hits);
   fChain->SetBranchAddress("mu_etaLayer1Hits", &mu_etaLayer1Hits, &b_mu_etaLayer1Hits);
   fChain->SetBranchAddress("mu_etaLayer2Hits", &mu_etaLayer2Hits, &b_mu_etaLayer2Hits);
   fChain->SetBranchAddress("mu_etaLayer3Hits", &mu_etaLayer3Hits, &b_mu_etaLayer3Hits);
   fChain->SetBranchAddress("mu_etaLayer4Hits", &mu_etaLayer4Hits, &b_mu_etaLayer4Hits);
   fChain->SetBranchAddress("mu_phiLayer1Holes", &mu_phiLayer1Holes, &b_mu_phiLayer1Holes);
   fChain->SetBranchAddress("mu_phiLayer2Holes", &mu_phiLayer2Holes, &b_mu_phiLayer2Holes);
   fChain->SetBranchAddress("mu_phiLayer3Holes", &mu_phiLayer3Holes, &b_mu_phiLayer3Holes);
   fChain->SetBranchAddress("mu_phiLayer4Holes", &mu_phiLayer4Holes, &b_mu_phiLayer4Holes);
   fChain->SetBranchAddress("mu_etaLayer1Holes", &mu_etaLayer1Holes, &b_mu_etaLayer1Holes);
   fChain->SetBranchAddress("mu_etaLayer2Holes", &mu_etaLayer2Holes, &b_mu_etaLayer2Holes);
   fChain->SetBranchAddress("mu_etaLayer3Holes", &mu_etaLayer3Holes, &b_mu_etaLayer3Holes);
   fChain->SetBranchAddress("mu_etaLayer4Holes", &mu_etaLayer4Holes, &b_mu_etaLayer4Holes);
   fChain->SetBranchAddress("mu_phiLayer1RPCHits", &mu_phiLayer1RPCHits, &b_mu_phiLayer1RPCHits);
   fChain->SetBranchAddress("mu_phiLayer2RPCHits", &mu_phiLayer2RPCHits, &b_mu_phiLayer2RPCHits);
   fChain->SetBranchAddress("mu_phiLayer3RPCHits", &mu_phiLayer3RPCHits, &b_mu_phiLayer3RPCHits);
   fChain->SetBranchAddress("mu_etaLayer1RPCHits", &mu_etaLayer1RPCHits, &b_mu_etaLayer1RPCHits);
   fChain->SetBranchAddress("mu_etaLayer2RPCHits", &mu_etaLayer2RPCHits, &b_mu_etaLayer2RPCHits);
   fChain->SetBranchAddress("mu_etaLayer3RPCHits", &mu_etaLayer3RPCHits, &b_mu_etaLayer3RPCHits);
   fChain->SetBranchAddress("mu_phiLayer1RPCHoles", &mu_phiLayer1RPCHoles, &b_mu_phiLayer1RPCHoles);
   fChain->SetBranchAddress("mu_phiLayer2RPCHoles", &mu_phiLayer2RPCHoles, &b_mu_phiLayer2RPCHoles);
   fChain->SetBranchAddress("mu_phiLayer3RPCHoles", &mu_phiLayer3RPCHoles, &b_mu_phiLayer3RPCHoles);
   fChain->SetBranchAddress("mu_etaLayer1RPCHoles", &mu_etaLayer1RPCHoles, &b_mu_etaLayer1RPCHoles);
   fChain->SetBranchAddress("mu_etaLayer2RPCHoles", &mu_etaLayer2RPCHoles, &b_mu_etaLayer2RPCHoles);
   fChain->SetBranchAddress("mu_etaLayer3RPCHoles", &mu_etaLayer3RPCHoles, &b_mu_etaLayer3RPCHoles);
   fChain->SetBranchAddress("mu_phiLayer1TGCHits", &mu_phiLayer1TGCHits, &b_mu_phiLayer1TGCHits);
   fChain->SetBranchAddress("mu_phiLayer2TGCHits", &mu_phiLayer2TGCHits, &b_mu_phiLayer2TGCHits);
   fChain->SetBranchAddress("mu_phiLayer3TGCHits", &mu_phiLayer3TGCHits, &b_mu_phiLayer3TGCHits);
   fChain->SetBranchAddress("mu_phiLayer4TGCHits", &mu_phiLayer4TGCHits, &b_mu_phiLayer4TGCHits);
   fChain->SetBranchAddress("mu_etaLayer1TGCHits", &mu_etaLayer1TGCHits, &b_mu_etaLayer1TGCHits);
   fChain->SetBranchAddress("mu_etaLayer2TGCHits", &mu_etaLayer2TGCHits, &b_mu_etaLayer2TGCHits);
   fChain->SetBranchAddress("mu_etaLayer3TGCHits", &mu_etaLayer3TGCHits, &b_mu_etaLayer3TGCHits);
   fChain->SetBranchAddress("mu_etaLayer4TGCHits", &mu_etaLayer4TGCHits, &b_mu_etaLayer4TGCHits);
   fChain->SetBranchAddress("mu_phiLayer1TGCHoles", &mu_phiLayer1TGCHoles, &b_mu_phiLayer1TGCHoles);
   fChain->SetBranchAddress("mu_phiLayer2TGCHoles", &mu_phiLayer2TGCHoles, &b_mu_phiLayer2TGCHoles);
   fChain->SetBranchAddress("mu_phiLayer3TGCHoles", &mu_phiLayer3TGCHoles, &b_mu_phiLayer3TGCHoles);
   fChain->SetBranchAddress("mu_phiLayer4TGCHoles", &mu_phiLayer4TGCHoles, &b_mu_phiLayer4TGCHoles);
   fChain->SetBranchAddress("mu_etaLayer1TGCHoles", &mu_etaLayer1TGCHoles, &b_mu_etaLayer1TGCHoles);
   fChain->SetBranchAddress("mu_etaLayer2TGCHoles", &mu_etaLayer2TGCHoles, &b_mu_etaLayer2TGCHoles);
   fChain->SetBranchAddress("mu_etaLayer3TGCHoles", &mu_etaLayer3TGCHoles, &b_mu_etaLayer3TGCHoles);
   fChain->SetBranchAddress("mu_etaLayer4TGCHoles", &mu_etaLayer4TGCHoles, &b_mu_etaLayer4TGCHoles);
   fChain->SetBranchAddress("mu_segmentDeltaEta", &mu_segmentDeltaEta, &b_mu_segmentDeltaEta);
   fChain->SetBranchAddress("mu_segmentDeltaPhi", &mu_segmentDeltaPhi, &b_mu_segmentDeltaPhi);
   fChain->SetBranchAddress("mu_segmentChi2OverDoF", &mu_segmentChi2OverDoF, &b_mu_segmentChi2OverDoF);
   fChain->SetBranchAddress("mu_t0", &mu_t0, &b_mu_t0);
   fChain->SetBranchAddress("mu_beta", &mu_beta, &b_mu_beta);
   fChain->SetBranchAddress("mu_meanDeltaADCCountsMDT", &mu_meanDeltaADCCountsMDT, &b_mu_meanDeltaADCCountsMDT);
   fChain->SetBranchAddress("mu_msOuterMatchDOF", &mu_msOuterMatchDOF, &b_mu_msOuterMatchDOF);
   fChain->SetBranchAddress("mu_preor_pt", &mu_preor_pt, &b_mu_preor_pt);
   fChain->SetBranchAddress("mu_preor_ptErr", &mu_preor_ptErr, &b_mu_preor_ptErr);
   fChain->SetBranchAddress("mu_preor_eta", &mu_preor_eta, &b_mu_preor_eta);
   fChain->SetBranchAddress("mu_preor_phi", &mu_preor_phi, &b_mu_preor_phi);
   fChain->SetBranchAddress("mu_preor_SF", &mu_preor_SF, &b_mu_preor_SF);
   fChain->SetBranchAddress("mu_preor_SF_iso", &mu_preor_SF_iso, &b_mu_preor_SF_iso);
   fChain->SetBranchAddress("mu_preor_isotool_pass_loosetrackonly", &mu_preor_isotool_pass_loosetrackonly, &b_mu_preor_isotool_pass_loosetrackonly);
   fChain->SetBranchAddress("mu_preor_isotool_pass_fixedcuttighttrackonly", &mu_preor_isotool_pass_fixedcuttighttrackonly, &b_mu_preor_isotool_pass_fixedcuttighttrackonly);
   fChain->SetBranchAddress("mu_preor_m", &mu_preor_m, &b_mu_preor_m);
   fChain->SetBranchAddress("mu_preor_e", &mu_preor_e, &b_mu_preor_e);
   fChain->SetBranchAddress("mu_preor_d0", &mu_preor_d0, &b_mu_preor_d0);
   fChain->SetBranchAddress("mu_preor_d0sig", &mu_preor_d0sig, &b_mu_preor_d0sig);
   fChain->SetBranchAddress("mu_preor_z0", &mu_preor_z0, &b_mu_preor_z0);
   fChain->SetBranchAddress("mu_preor_z0sig", &mu_preor_z0sig, &b_mu_preor_z0sig);
   fChain->SetBranchAddress("mu_preor_charge", &mu_preor_charge, &b_mu_preor_charge);
   fChain->SetBranchAddress("mu_preor_id_pt", &mu_preor_id_pt, &b_mu_preor_id_pt);
   fChain->SetBranchAddress("mu_preor_id_ptErr", &mu_preor_id_ptErr, &b_mu_preor_id_ptErr);
   fChain->SetBranchAddress("mu_preor_id_eta", &mu_preor_id_eta, &b_mu_preor_id_eta);
   fChain->SetBranchAddress("mu_preor_id_phi", &mu_preor_id_phi, &b_mu_preor_id_phi);
   fChain->SetBranchAddress("mu_preor_id_m", &mu_preor_id_m, &b_mu_preor_id_m);
   fChain->SetBranchAddress("mu_preor_id_qop", &mu_preor_id_qop, &b_mu_preor_id_qop);
   fChain->SetBranchAddress("mu_preor_id_qoperr", &mu_preor_id_qoperr, &b_mu_preor_id_qoperr);
   fChain->SetBranchAddress("mu_preor_ms_pt", &mu_preor_ms_pt, &b_mu_preor_ms_pt);
   fChain->SetBranchAddress("mu_preor_ms_ptErr", &mu_preor_ms_ptErr, &b_mu_preor_ms_ptErr);
   fChain->SetBranchAddress("mu_preor_ms_eta", &mu_preor_ms_eta, &b_mu_preor_ms_eta);
   fChain->SetBranchAddress("mu_preor_ms_phi", &mu_preor_ms_phi, &b_mu_preor_ms_phi);
   fChain->SetBranchAddress("mu_preor_ms_m", &mu_preor_ms_m, &b_mu_preor_ms_m);
   fChain->SetBranchAddress("mu_preor_ms_qop", &mu_preor_ms_qop, &b_mu_preor_ms_qop);
   fChain->SetBranchAddress("mu_preor_ms_qoperr", &mu_preor_ms_qoperr, &b_mu_preor_ms_qoperr);
   fChain->SetBranchAddress("mu_preor_me_pt", &mu_preor_me_pt, &b_mu_preor_me_pt);
   fChain->SetBranchAddress("mu_preor_me_ptErr", &mu_preor_me_ptErr, &b_mu_preor_me_ptErr);
   fChain->SetBranchAddress("mu_preor_me_eta", &mu_preor_me_eta, &b_mu_preor_me_eta);
   fChain->SetBranchAddress("mu_preor_me_phi", &mu_preor_me_phi, &b_mu_preor_me_phi);
   fChain->SetBranchAddress("mu_preor_me_m", &mu_preor_me_m, &b_mu_preor_me_m);
   fChain->SetBranchAddress("mu_preor_me_qop", &mu_preor_me_qop, &b_mu_preor_me_qop);
   fChain->SetBranchAddress("mu_preor_me_qoperr", &mu_preor_me_qoperr, &b_mu_preor_me_qoperr);
   fChain->SetBranchAddress("mu_preor_mse_pt", &mu_preor_mse_pt, &b_mu_preor_mse_pt);
   fChain->SetBranchAddress("mu_preor_mse_ptErr", &mu_preor_mse_ptErr, &b_mu_preor_mse_ptErr);
   fChain->SetBranchAddress("mu_preor_mse_eta", &mu_preor_mse_eta, &b_mu_preor_mse_eta);
   fChain->SetBranchAddress("mu_preor_mse_phi", &mu_preor_mse_phi, &b_mu_preor_mse_phi);
   fChain->SetBranchAddress("mu_preor_mse_m", &mu_preor_mse_m, &b_mu_preor_mse_m);
   fChain->SetBranchAddress("mu_preor_mse_qop", &mu_preor_mse_qop, &b_mu_preor_mse_qop);
   fChain->SetBranchAddress("mu_preor_mse_qoperr", &mu_preor_mse_qoperr, &b_mu_preor_mse_qoperr);
   fChain->SetBranchAddress("mu_preor_ptcone20", &mu_preor_ptcone20, &b_mu_preor_ptcone20);
   fChain->SetBranchAddress("mu_preor_ptvarcone20", &mu_preor_ptvarcone20, &b_mu_preor_ptvarcone20);
   fChain->SetBranchAddress("mu_preor_etcone20", &mu_preor_etcone20, &b_mu_preor_etcone20);
   fChain->SetBranchAddress("mu_preor_topoetcone20", &mu_preor_topoetcone20, &b_mu_preor_topoetcone20);
   fChain->SetBranchAddress("mu_preor_ptcone30", &mu_preor_ptcone30, &b_mu_preor_ptcone30);
   fChain->SetBranchAddress("mu_preor_ptvarcone30", &mu_preor_ptvarcone30, &b_mu_preor_ptvarcone30);
   fChain->SetBranchAddress("mu_preor_etcone30", &mu_preor_etcone30, &b_mu_preor_etcone30);
   fChain->SetBranchAddress("mu_preor_topoetcone30", &mu_preor_topoetcone30, &b_mu_preor_topoetcone30);
   fChain->SetBranchAddress("mu_preor_ptcone40", &mu_preor_ptcone40, &b_mu_preor_ptcone40);
   fChain->SetBranchAddress("mu_preor_ptvarcone40", &mu_preor_ptvarcone40, &b_mu_preor_ptvarcone40);
   fChain->SetBranchAddress("mu_preor_etcone40", &mu_preor_etcone40, &b_mu_preor_etcone40);
   fChain->SetBranchAddress("mu_preor_topoetcone40", &mu_preor_topoetcone40, &b_mu_preor_topoetcone40);
   fChain->SetBranchAddress("mu_preor_author", &mu_preor_author, &b_mu_preor_author);
   fChain->SetBranchAddress("mu_preor_quality", &mu_preor_quality, &b_mu_preor_quality);
   fChain->SetBranchAddress("mu_preor_isSA", &mu_preor_isSA, &b_mu_preor_isSA);
   fChain->SetBranchAddress("mu_preor_isST", &mu_preor_isST, &b_mu_preor_isST);
   fChain->SetBranchAddress("mu_preor_isCB", &mu_preor_isCB, &b_mu_preor_isCB);
   fChain->SetBranchAddress("mu_preor_isTight", &mu_preor_isTight, &b_mu_preor_isTight);
   fChain->SetBranchAddress("mu_preor_isMedium", &mu_preor_isMedium, &b_mu_preor_isMedium);
   fChain->SetBranchAddress("mu_preor_isLoose", &mu_preor_isLoose, &b_mu_preor_isLoose);
   fChain->SetBranchAddress("mu_preor_isVeryLoose", &mu_preor_isVeryLoose, &b_mu_preor_isVeryLoose);
   fChain->SetBranchAddress("mu_preor_isHighPt", &mu_preor_isHighPt, &b_mu_preor_isHighPt);
   fChain->SetBranchAddress("mu_preor_isBad", &mu_preor_isBad, &b_mu_preor_isBad);
   fChain->SetBranchAddress("mu_preor_isBadHighPt", &mu_preor_isBadHighPt, &b_mu_preor_isBadHighPt);
   fChain->SetBranchAddress("mu_preor_finalFitPt", &mu_preor_finalFitPt, &b_mu_preor_finalFitPt);
   fChain->SetBranchAddress("mu_preor_finalFitPtErr", &mu_preor_finalFitPtErr, &b_mu_preor_finalFitPtErr);
   fChain->SetBranchAddress("mu_preor_finalFitEta", &mu_preor_finalFitEta, &b_mu_preor_finalFitEta);
   fChain->SetBranchAddress("mu_preor_finalFitPhi", &mu_preor_finalFitPhi, &b_mu_preor_finalFitPhi);
   fChain->SetBranchAddress("mu_preor_finalFitQOverP", &mu_preor_finalFitQOverP, &b_mu_preor_finalFitQOverP);
   fChain->SetBranchAddress("mu_preor_finalFitQOverPErr", &mu_preor_finalFitQOverPErr, &b_mu_preor_finalFitQOverPErr);
   fChain->SetBranchAddress("mu_preor_truthEta", &mu_preor_truthEta, &b_mu_preor_truthEta);
   fChain->SetBranchAddress("mu_preor_truthPhi", &mu_preor_truthPhi, &b_mu_preor_truthPhi);
   fChain->SetBranchAddress("mu_preor_truthPt", &mu_preor_truthPt, &b_mu_preor_truthPt);
   fChain->SetBranchAddress("mu_preor_truthTheta", &mu_preor_truthTheta, &b_mu_preor_truthTheta);
   fChain->SetBranchAddress("mu_preor_truthQOverP", &mu_preor_truthQOverP, &b_mu_preor_truthQOverP);
   fChain->SetBranchAddress("mu_preor_isotool_pass_loose", &mu_preor_isotool_pass_loose, &b_mu_preor_isotool_pass_loose);
   fChain->SetBranchAddress("mu_preor_isotool_pass_gradient", &mu_preor_isotool_pass_gradient, &b_mu_preor_isotool_pass_gradient);
   fChain->SetBranchAddress("mu_preor_isotool_pass_gradientloose", &mu_preor_isotool_pass_gradientloose, &b_mu_preor_isotool_pass_gradientloose);
   fChain->SetBranchAddress("mu_preor_met_nomuon_dphi", &mu_preor_met_nomuon_dphi, &b_mu_preor_met_nomuon_dphi);
   fChain->SetBranchAddress("mu_preor_met_wmuon_dphi", &mu_preor_met_wmuon_dphi, &b_mu_preor_met_wmuon_dphi);
   fChain->SetBranchAddress("mu_preor_truth_origin", &mu_preor_truth_origin, &b_mu_preor_truth_origin);
   fChain->SetBranchAddress("mu_preor_truth_type", &mu_preor_truth_type, &b_mu_preor_truth_type);
   fChain->SetBranchAddress("mu_preor_muonType", &mu_preor_muonType, &b_mu_preor_muonType);
   fChain->SetBranchAddress("mu_preor_momentumBalanceSignificance", &mu_preor_momentumBalanceSignificance, &b_mu_preor_momentumBalanceSignificance);
   fChain->SetBranchAddress("mu_preor_CaloLRLikelihood", &mu_preor_CaloLRLikelihood, &b_mu_preor_CaloLRLikelihood);
   fChain->SetBranchAddress("mu_preor_CaloMuonIDTag", &mu_preor_CaloMuonIDTag, &b_mu_preor_CaloMuonIDTag);
   fChain->SetBranchAddress("mu_preor_numberOfPrecisionLayers", &mu_preor_numberOfPrecisionLayers, &b_mu_preor_numberOfPrecisionLayers);
   fChain->SetBranchAddress("mu_preor_numberOfPrecisionHoleLayers", &mu_preor_numberOfPrecisionHoleLayers, &b_mu_preor_numberOfPrecisionHoleLayers);
   fChain->SetBranchAddress("mu_preor_numberOfGoodPrecisionLayers", &mu_preor_numberOfGoodPrecisionLayers, &b_mu_preor_numberOfGoodPrecisionLayers);
   fChain->SetBranchAddress("mu_preor_innerSmallHits", &mu_preor_innerSmallHits, &b_mu_preor_innerSmallHits);
   fChain->SetBranchAddress("mu_preor_innerLargeHits", &mu_preor_innerLargeHits, &b_mu_preor_innerLargeHits);
   fChain->SetBranchAddress("mu_preor_innerSmallHoles", &mu_preor_innerSmallHoles, &b_mu_preor_innerSmallHoles);
   fChain->SetBranchAddress("mu_preor_innerLargeHoles", &mu_preor_innerLargeHoles, &b_mu_preor_innerLargeHoles);
   fChain->SetBranchAddress("mu_preor_middleSmallHoles", &mu_preor_middleSmallHoles, &b_mu_preor_middleSmallHoles);
   fChain->SetBranchAddress("mu_preor_middleLargeHoles", &mu_preor_middleLargeHoles, &b_mu_preor_middleLargeHoles);
   fChain->SetBranchAddress("mu_preor_outerSmallHoles", &mu_preor_outerSmallHoles, &b_mu_preor_outerSmallHoles);
   fChain->SetBranchAddress("mu_preor_outerLargeHoles", &mu_preor_outerLargeHoles, &b_mu_preor_outerLargeHoles);
   fChain->SetBranchAddress("mu_preor_extendedSmallHoles", &mu_preor_extendedSmallHoles, &b_mu_preor_extendedSmallHoles);
   fChain->SetBranchAddress("mu_preor_extendedLargeHoles", &mu_preor_extendedLargeHoles, &b_mu_preor_extendedLargeHoles);
   fChain->SetBranchAddress("mu_preor_innerClosePrecisionHits", &mu_preor_innerClosePrecisionHits, &b_mu_preor_innerClosePrecisionHits);
   fChain->SetBranchAddress("mu_preor_middleClosePrecisionHits", &mu_preor_middleClosePrecisionHits, &b_mu_preor_middleClosePrecisionHits);
   fChain->SetBranchAddress("mu_preor_outerClosePrecisionHits", &mu_preor_outerClosePrecisionHits, &b_mu_preor_outerClosePrecisionHits);
   fChain->SetBranchAddress("mu_preor_extendedClosePrecisionHits", &mu_preor_extendedClosePrecisionHits, &b_mu_preor_extendedClosePrecisionHits);
   fChain->SetBranchAddress("mu_preor_innerOutBoundsPrecisionHits", &mu_preor_innerOutBoundsPrecisionHits, &b_mu_preor_innerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_preor_middleOutBoundsPrecisionHits", &mu_preor_middleOutBoundsPrecisionHits, &b_mu_preor_middleOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_preor_outerOutBoundsPrecisionHits", &mu_preor_outerOutBoundsPrecisionHits, &b_mu_preor_outerOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_preor_extendedOutBoundsPrecisionHits", &mu_preor_extendedOutBoundsPrecisionHits, &b_mu_preor_extendedOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_preor_combinedTrackOutBoundsPrecisionHits", &mu_preor_combinedTrackOutBoundsPrecisionHits, &b_mu_preor_combinedTrackOutBoundsPrecisionHits);
   fChain->SetBranchAddress("mu_preor_middleLargeHits", &mu_preor_middleLargeHits, &b_mu_preor_middleLargeHits);
   fChain->SetBranchAddress("mu_preor_middleSmallHits", &mu_preor_middleSmallHits, &b_mu_preor_middleSmallHits);
   fChain->SetBranchAddress("mu_preor_outerLargeHits", &mu_preor_outerLargeHits, &b_mu_preor_outerLargeHits);
   fChain->SetBranchAddress("mu_preor_outerSmallHits", &mu_preor_outerSmallHits, &b_mu_preor_outerSmallHits);
   fChain->SetBranchAddress("mu_preor_extendedSmallHits", &mu_preor_extendedSmallHits, &b_mu_preor_extendedSmallHits);
   fChain->SetBranchAddress("mu_preor_extendedLargeHits", &mu_preor_extendedLargeHits, &b_mu_preor_extendedLargeHits);
   fChain->SetBranchAddress("mu_preor_numberOfPixelHits", &mu_preor_numberOfPixelHits, &b_mu_preor_numberOfPixelHits);
   fChain->SetBranchAddress("mu_preor_numberOfPixelDeadSensors", &mu_preor_numberOfPixelDeadSensors, &b_mu_preor_numberOfPixelDeadSensors);
   fChain->SetBranchAddress("mu_preor_numberOfSCTHits", &mu_preor_numberOfSCTHits, &b_mu_preor_numberOfSCTHits);
   fChain->SetBranchAddress("mu_preor_numberOfSCTDeadSensors", &mu_preor_numberOfSCTDeadSensors, &b_mu_preor_numberOfSCTDeadSensors);
   fChain->SetBranchAddress("mu_preor_numberOfPixelHoles", &mu_preor_numberOfPixelHoles, &b_mu_preor_numberOfPixelHoles);
   fChain->SetBranchAddress("mu_preor_numberOfSCTHoles", &mu_preor_numberOfSCTHoles, &b_mu_preor_numberOfSCTHoles);
   fChain->SetBranchAddress("mu_preor_numberOfTRTHits", &mu_preor_numberOfTRTHits, &b_mu_preor_numberOfTRTHits);
   fChain->SetBranchAddress("mu_preor_numberOfTRTOutliers", &mu_preor_numberOfTRTOutliers, &b_mu_preor_numberOfTRTOutliers);
   fChain->SetBranchAddress("mu_preor_primarytrketa", &mu_preor_primarytrketa, &b_mu_preor_primarytrketa);
   fChain->SetBranchAddress("mu_preor_numberOfInnermostPixelLayerHits", &mu_preor_numberOfInnermostPixelLayerHits, &b_mu_preor_numberOfInnermostPixelLayerHits);
   fChain->SetBranchAddress("mu_preor_cbtrketa", &mu_preor_cbtrketa, &b_mu_preor_cbtrketa);
   fChain->SetBranchAddress("mu_preor_spectrometerFieldIntegral", &mu_preor_spectrometerFieldIntegral, &b_mu_preor_spectrometerFieldIntegral);
   fChain->SetBranchAddress("mu_preor_scatteringCurvatureSignificance", &mu_preor_scatteringCurvatureSignificance, &b_mu_preor_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("mu_preor_scatteringNeighbourSignificance", &mu_preor_scatteringNeighbourSignificance, &b_mu_preor_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("mu_preor_allAuthors", &mu_preor_allAuthors, &b_mu_preor_allAuthors);
   fChain->SetBranchAddress("mu_preor_numberOfPhiLayers", &mu_preor_numberOfPhiLayers, &b_mu_preor_numberOfPhiLayers);
   fChain->SetBranchAddress("mu_preor_numberOfPhiHoleLayers", &mu_preor_numberOfPhiHoleLayers, &b_mu_preor_numberOfPhiHoleLayers);
   fChain->SetBranchAddress("mu_preor_numberOfTriggerEtaLayers", &mu_preor_numberOfTriggerEtaLayers, &b_mu_preor_numberOfTriggerEtaLayers);
   fChain->SetBranchAddress("mu_preor_numberOfTriggerEtaHoleLayers", &mu_preor_numberOfTriggerEtaHoleLayers, &b_mu_preor_numberOfTriggerEtaHoleLayers);
   fChain->SetBranchAddress("mu_preor_nUnspoiledCscHits", &mu_preor_nUnspoiledCscHits, &b_mu_preor_nUnspoiledCscHits);
   fChain->SetBranchAddress("mu_preor_FSR_CandidateEnergy", &mu_preor_FSR_CandidateEnergy, &b_mu_preor_FSR_CandidateEnergy);
   fChain->SetBranchAddress("mu_preor_EnergyLoss", &mu_preor_EnergyLoss, &b_mu_preor_EnergyLoss);
   fChain->SetBranchAddress("mu_preor_EnergyLossSigma", &mu_preor_EnergyLossSigma, &b_mu_preor_EnergyLossSigma);
   fChain->SetBranchAddress("mu_preor_ParamEnergyLoss", &mu_preor_ParamEnergyLoss, &b_mu_preor_ParamEnergyLoss);
   fChain->SetBranchAddress("mu_preor_ParamEnergyLossSigmaPlus", &mu_preor_ParamEnergyLossSigmaPlus, &b_mu_preor_ParamEnergyLossSigmaPlus);
   fChain->SetBranchAddress("mu_preor_ParamEnergyLossSigmaMinus", &mu_preor_ParamEnergyLossSigmaMinus, &b_mu_preor_ParamEnergyLossSigmaMinus);
   fChain->SetBranchAddress("mu_preor_MeasEnergyLoss", &mu_preor_MeasEnergyLoss, &b_mu_preor_MeasEnergyLoss);
   fChain->SetBranchAddress("mu_preor_MeasEnergyLossSigma", &mu_preor_MeasEnergyLossSigma, &b_mu_preor_MeasEnergyLossSigma);
   fChain->SetBranchAddress("mu_preor_msInnerMatchChi2", &mu_preor_msInnerMatchChi2, &b_mu_preor_msInnerMatchChi2);
   fChain->SetBranchAddress("mu_preor_msInnerMatchDOF", &mu_preor_msInnerMatchDOF, &b_mu_preor_msInnerMatchDOF);
   fChain->SetBranchAddress("mu_preor_energyLossType", &mu_preor_energyLossType, &b_mu_preor_energyLossType);
   fChain->SetBranchAddress("mu_preor_phiLayer1Hits", &mu_preor_phiLayer1Hits, &b_mu_preor_phiLayer1Hits);
   fChain->SetBranchAddress("mu_preor_phiLayer2Hits", &mu_preor_phiLayer2Hits, &b_mu_preor_phiLayer2Hits);
   fChain->SetBranchAddress("mu_preor_phiLayer3Hits", &mu_preor_phiLayer3Hits, &b_mu_preor_phiLayer3Hits);
   fChain->SetBranchAddress("mu_preor_phiLayer4Hits", &mu_preor_phiLayer4Hits, &b_mu_preor_phiLayer4Hits);
   fChain->SetBranchAddress("mu_preor_etaLayer1Hits", &mu_preor_etaLayer1Hits, &b_mu_preor_etaLayer1Hits);
   fChain->SetBranchAddress("mu_preor_etaLayer2Hits", &mu_preor_etaLayer2Hits, &b_mu_preor_etaLayer2Hits);
   fChain->SetBranchAddress("mu_preor_etaLayer3Hits", &mu_preor_etaLayer3Hits, &b_mu_preor_etaLayer3Hits);
   fChain->SetBranchAddress("mu_preor_etaLayer4Hits", &mu_preor_etaLayer4Hits, &b_mu_preor_etaLayer4Hits);
   fChain->SetBranchAddress("mu_preor_phiLayer1Holes", &mu_preor_phiLayer1Holes, &b_mu_preor_phiLayer1Holes);
   fChain->SetBranchAddress("mu_preor_phiLayer2Holes", &mu_preor_phiLayer2Holes, &b_mu_preor_phiLayer2Holes);
   fChain->SetBranchAddress("mu_preor_phiLayer3Holes", &mu_preor_phiLayer3Holes, &b_mu_preor_phiLayer3Holes);
   fChain->SetBranchAddress("mu_preor_phiLayer4Holes", &mu_preor_phiLayer4Holes, &b_mu_preor_phiLayer4Holes);
   fChain->SetBranchAddress("mu_preor_etaLayer1Holes", &mu_preor_etaLayer1Holes, &b_mu_preor_etaLayer1Holes);
   fChain->SetBranchAddress("mu_preor_etaLayer2Holes", &mu_preor_etaLayer2Holes, &b_mu_preor_etaLayer2Holes);
   fChain->SetBranchAddress("mu_preor_etaLayer3Holes", &mu_preor_etaLayer3Holes, &b_mu_preor_etaLayer3Holes);
   fChain->SetBranchAddress("mu_preor_etaLayer4Holes", &mu_preor_etaLayer4Holes, &b_mu_preor_etaLayer4Holes);
   fChain->SetBranchAddress("mu_preor_phiLayer1RPCHits", &mu_preor_phiLayer1RPCHits, &b_mu_preor_phiLayer1RPCHits);
   fChain->SetBranchAddress("mu_preor_phiLayer2RPCHits", &mu_preor_phiLayer2RPCHits, &b_mu_preor_phiLayer2RPCHits);
   fChain->SetBranchAddress("mu_preor_phiLayer3RPCHits", &mu_preor_phiLayer3RPCHits, &b_mu_preor_phiLayer3RPCHits);
   fChain->SetBranchAddress("mu_preor_etaLayer1RPCHits", &mu_preor_etaLayer1RPCHits, &b_mu_preor_etaLayer1RPCHits);
   fChain->SetBranchAddress("mu_preor_etaLayer2RPCHits", &mu_preor_etaLayer2RPCHits, &b_mu_preor_etaLayer2RPCHits);
   fChain->SetBranchAddress("mu_preor_etaLayer3RPCHits", &mu_preor_etaLayer3RPCHits, &b_mu_preor_etaLayer3RPCHits);
   fChain->SetBranchAddress("mu_preor_phiLayer1RPCHoles", &mu_preor_phiLayer1RPCHoles, &b_mu_preor_phiLayer1RPCHoles);
   fChain->SetBranchAddress("mu_preor_phiLayer2RPCHoles", &mu_preor_phiLayer2RPCHoles, &b_mu_preor_phiLayer2RPCHoles);
   fChain->SetBranchAddress("mu_preor_phiLayer3RPCHoles", &mu_preor_phiLayer3RPCHoles, &b_mu_preor_phiLayer3RPCHoles);
   fChain->SetBranchAddress("mu_preor_etaLayer1RPCHoles", &mu_preor_etaLayer1RPCHoles, &b_mu_preor_etaLayer1RPCHoles);
   fChain->SetBranchAddress("mu_preor_etaLayer2RPCHoles", &mu_preor_etaLayer2RPCHoles, &b_mu_preor_etaLayer2RPCHoles);
   fChain->SetBranchAddress("mu_preor_etaLayer3RPCHoles", &mu_preor_etaLayer3RPCHoles, &b_mu_preor_etaLayer3RPCHoles);
   fChain->SetBranchAddress("mu_preor_phiLayer1TGCHits", &mu_preor_phiLayer1TGCHits, &b_mu_preor_phiLayer1TGCHits);
   fChain->SetBranchAddress("mu_preor_phiLayer2TGCHits", &mu_preor_phiLayer2TGCHits, &b_mu_preor_phiLayer2TGCHits);
   fChain->SetBranchAddress("mu_preor_phiLayer3TGCHits", &mu_preor_phiLayer3TGCHits, &b_mu_preor_phiLayer3TGCHits);
   fChain->SetBranchAddress("mu_preor_phiLayer4TGCHits", &mu_preor_phiLayer4TGCHits, &b_mu_preor_phiLayer4TGCHits);
   fChain->SetBranchAddress("mu_preor_etaLayer1TGCHits", &mu_preor_etaLayer1TGCHits, &b_mu_preor_etaLayer1TGCHits);
   fChain->SetBranchAddress("mu_preor_etaLayer2TGCHits", &mu_preor_etaLayer2TGCHits, &b_mu_preor_etaLayer2TGCHits);
   fChain->SetBranchAddress("mu_preor_etaLayer3TGCHits", &mu_preor_etaLayer3TGCHits, &b_mu_preor_etaLayer3TGCHits);
   fChain->SetBranchAddress("mu_preor_etaLayer4TGCHits", &mu_preor_etaLayer4TGCHits, &b_mu_preor_etaLayer4TGCHits);
   fChain->SetBranchAddress("mu_preor_phiLayer1TGCHoles", &mu_preor_phiLayer1TGCHoles, &b_mu_preor_phiLayer1TGCHoles);
   fChain->SetBranchAddress("mu_preor_phiLayer2TGCHoles", &mu_preor_phiLayer2TGCHoles, &b_mu_preor_phiLayer2TGCHoles);
   fChain->SetBranchAddress("mu_preor_phiLayer3TGCHoles", &mu_preor_phiLayer3TGCHoles, &b_mu_preor_phiLayer3TGCHoles);
   fChain->SetBranchAddress("mu_preor_phiLayer4TGCHoles", &mu_preor_phiLayer4TGCHoles, &b_mu_preor_phiLayer4TGCHoles);
   fChain->SetBranchAddress("mu_preor_etaLayer1TGCHoles", &mu_preor_etaLayer1TGCHoles, &b_mu_preor_etaLayer1TGCHoles);
   fChain->SetBranchAddress("mu_preor_etaLayer2TGCHoles", &mu_preor_etaLayer2TGCHoles, &b_mu_preor_etaLayer2TGCHoles);
   fChain->SetBranchAddress("mu_preor_etaLayer3TGCHoles", &mu_preor_etaLayer3TGCHoles, &b_mu_preor_etaLayer3TGCHoles);
   fChain->SetBranchAddress("mu_preor_etaLayer4TGCHoles", &mu_preor_etaLayer4TGCHoles, &b_mu_preor_etaLayer4TGCHoles);
   fChain->SetBranchAddress("mu_preor_segmentDeltaEta", &mu_preor_segmentDeltaEta, &b_mu_preor_segmentDeltaEta);
   fChain->SetBranchAddress("mu_preor_segmentDeltaPhi", &mu_preor_segmentDeltaPhi, &b_mu_preor_segmentDeltaPhi);
   fChain->SetBranchAddress("mu_preor_segmentChi2OverDoF", &mu_preor_segmentChi2OverDoF, &b_mu_preor_segmentChi2OverDoF);
   fChain->SetBranchAddress("mu_preor_t0", &mu_preor_t0, &b_mu_preor_t0);
   fChain->SetBranchAddress("mu_preor_beta", &mu_preor_beta, &b_mu_preor_beta);
   fChain->SetBranchAddress("mu_preor_meanDeltaADCCountsMDT", &mu_preor_meanDeltaADCCountsMDT, &b_mu_preor_meanDeltaADCCountsMDT);
   fChain->SetBranchAddress("mu_preor_msOuterMatchDOF", &mu_preor_msOuterMatchDOF, &b_mu_preor_msOuterMatchDOF);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_SF", &el_SF, &b_el_SF);
   fChain->SetBranchAddress("el_SF_iso", &el_SF_iso, &b_el_SF_iso);
   fChain->SetBranchAddress("el_SF_trigger", &el_SF_trigger, &b_el_SF_trigger);
   fChain->SetBranchAddress("el_SF_tot", &el_SF_tot, &b_el_SF_tot);
   fChain->SetBranchAddress("el_SF_tot_isotight", &el_SF_tot_isotight, &b_el_SF_tot_isotight);
   fChain->SetBranchAddress("el_isotool_pass_loosetrackonly", &el_isotool_pass_loosetrackonly, &b_el_isotool_pass_loosetrackonly);
   fChain->SetBranchAddress("el_isotool_pass_fixedcuttighttrackonly", &el_isotool_pass_fixedcuttighttrackonly, &b_el_isotool_pass_fixedcuttighttrackonly);
   fChain->SetBranchAddress("el_isotool_pass_fixedcuttight", &el_isotool_pass_fixedcuttight, &b_el_isotool_pass_fixedcuttight);
   fChain->SetBranchAddress("el_m", &el_m, &b_el_m);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_isGoodOQ", &el_isGoodOQ, &b_el_isGoodOQ);
   fChain->SetBranchAddress("el_passLHID", &el_passLHID, &b_el_passLHID);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_id_pt", &el_id_pt, &b_el_id_pt);
   fChain->SetBranchAddress("el_id_eta", &el_id_eta, &b_el_id_eta);
   fChain->SetBranchAddress("el_id_phi", &el_id_phi, &b_el_id_phi);
   fChain->SetBranchAddress("el_id_m", &el_id_m, &b_el_id_m);
   fChain->SetBranchAddress("el_cl_pt", &el_cl_pt, &b_el_cl_pt);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_cl_etaBE2", &el_cl_etaBE2, &b_el_cl_etaBE2);
   fChain->SetBranchAddress("el_cl_phi", &el_cl_phi, &b_el_cl_phi);
   fChain->SetBranchAddress("el_cl_m", &el_cl_m, &b_el_cl_m);
   fChain->SetBranchAddress("el_ptcone20", &el_ptcone20, &b_el_ptcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_etcone20", &el_etcone20, &b_el_etcone20);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptcone30", &el_ptcone30, &b_el_ptcone30);
   fChain->SetBranchAddress("el_ptvarcone30", &el_ptvarcone30, &b_el_ptvarcone30);
   fChain->SetBranchAddress("el_etcone30", &el_etcone30, &b_el_etcone30);
   fChain->SetBranchAddress("el_topoetcone30", &el_topoetcone30, &b_el_topoetcone30);
   fChain->SetBranchAddress("el_ptcone40", &el_ptcone40, &b_el_ptcone40);
   fChain->SetBranchAddress("el_ptvarcone40", &el_ptvarcone40, &b_el_ptvarcone40);
   fChain->SetBranchAddress("el_etcone40", &el_etcone40, &b_el_etcone40);
   fChain->SetBranchAddress("el_topoetcone40", &el_topoetcone40, &b_el_topoetcone40);
   fChain->SetBranchAddress("el_d0", &el_d0, &b_el_d0);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_z0", &el_z0, &b_el_z0);
   fChain->SetBranchAddress("el_z0sig", &el_z0sig, &b_el_z0sig);
   fChain->SetBranchAddress("el_demaxs1", &el_demaxs1, &b_el_demaxs1);
   fChain->SetBranchAddress("el_fside", &el_fside, &b_el_fside);
   fChain->SetBranchAddress("el_weta2", &el_weta2, &b_el_weta2);
   fChain->SetBranchAddress("el_ws3", &el_ws3, &b_el_ws3);
   fChain->SetBranchAddress("el_eratio", &el_eratio, &b_el_eratio);
   fChain->SetBranchAddress("el_reta", &el_reta, &b_el_reta);
   fChain->SetBranchAddress("el_rphi", &el_rphi, &b_el_rphi);
   fChain->SetBranchAddress("el_time_cl", &el_time_cl, &b_el_time_cl);
   fChain->SetBranchAddress("el_time_maxEcell", &el_time_maxEcell, &b_el_time_maxEcell);
   fChain->SetBranchAddress("el_truth_pt", &el_truth_pt, &b_el_truth_pt);
   fChain->SetBranchAddress("el_truth_eta", &el_truth_eta, &b_el_truth_eta);
   fChain->SetBranchAddress("el_truth_phi", &el_truth_phi, &b_el_truth_phi);
   fChain->SetBranchAddress("el_truth_E", &el_truth_E, &b_el_truth_E);
   fChain->SetBranchAddress("el_author", &el_author, &b_el_author);
   fChain->SetBranchAddress("el_isConv", &el_isConv, &b_el_isConv);
   fChain->SetBranchAddress("el_passChID", &el_passChID, &b_el_passChID);
   fChain->SetBranchAddress("el_ecisBDT", &el_ecisBDT, &b_el_ecisBDT);
   fChain->SetBranchAddress("el_truth_matched", &el_truth_matched, &b_el_truth_matched);
   fChain->SetBranchAddress("el_truth_mothertype", &el_truth_mothertype, &b_el_truth_mothertype);
   fChain->SetBranchAddress("el_truth_status", &el_truth_status, &b_el_truth_status);
   fChain->SetBranchAddress("el_truth_type", &el_truth_type, &b_el_truth_type);
   fChain->SetBranchAddress("el_truth_typebkg", &el_truth_typebkg, &b_el_truth_typebkg);
   fChain->SetBranchAddress("el_truth_origin", &el_truth_origin, &b_el_truth_origin);
   fChain->SetBranchAddress("el_truth_originbkg", &el_truth_originbkg, &b_el_truth_originbkg);
   fChain->SetBranchAddress("el_isotool_pass_loose", &el_isotool_pass_loose, &b_el_isotool_pass_loose);
   fChain->SetBranchAddress("el_isotool_pass_gradient", &el_isotool_pass_gradient, &b_el_isotool_pass_gradient);
   fChain->SetBranchAddress("el_isotool_pass_gradientloose", &el_isotool_pass_gradientloose, &b_el_isotool_pass_gradientloose);
   fChain->SetBranchAddress("el_met_nomuon_dphi", &el_met_nomuon_dphi, &b_el_met_nomuon_dphi);
   fChain->SetBranchAddress("el_met_wmuon_dphi", &el_met_wmuon_dphi, &b_el_met_wmuon_dphi);
   fChain->SetBranchAddress("el_met_noelectron_dphi", &el_met_noelectron_dphi, &b_el_met_noelectron_dphi);
   fChain->SetBranchAddress("el_preor_pt", &el_preor_pt, &b_el_preor_pt);
   fChain->SetBranchAddress("el_preor_eta", &el_preor_eta, &b_el_preor_eta);
   fChain->SetBranchAddress("el_preor_phi", &el_preor_phi, &b_el_preor_phi);
   fChain->SetBranchAddress("el_preor_SF", &el_preor_SF, &b_el_preor_SF);
   fChain->SetBranchAddress("el_preor_SF_iso", &el_preor_SF_iso, &b_el_preor_SF_iso);
   fChain->SetBranchAddress("el_preor_SF_trigger", &el_preor_SF_trigger, &b_el_preor_SF_trigger);
   fChain->SetBranchAddress("el_preor_SF_tot", &el_preor_SF_tot, &b_el_preor_SF_tot);
   fChain->SetBranchAddress("el_preor_SF_tot_isotight", &el_preor_SF_tot_isotight, &b_el_preor_SF_tot_isotight);
   fChain->SetBranchAddress("el_preor_isotool_pass_loosetrackonly", &el_preor_isotool_pass_loosetrackonly, &b_el_preor_isotool_pass_loosetrackonly);
   fChain->SetBranchAddress("el_preor_isotool_pass_fixedcuttighttrackonly", &el_preor_isotool_pass_fixedcuttighttrackonly, &b_el_preor_isotool_pass_fixedcuttighttrackonly);
   fChain->SetBranchAddress("el_preor_isotool_pass_fixedcuttight", &el_preor_isotool_pass_fixedcuttight, &b_el_preor_isotool_pass_fixedcuttight);
   fChain->SetBranchAddress("el_preor_m", &el_preor_m, &b_el_preor_m);
   fChain->SetBranchAddress("el_preor_e", &el_preor_e, &b_el_preor_e);
   fChain->SetBranchAddress("el_preor_isGoodOQ", &el_preor_isGoodOQ, &b_el_preor_isGoodOQ);
   fChain->SetBranchAddress("el_preor_passLHID", &el_preor_passLHID, &b_el_preor_passLHID);
   fChain->SetBranchAddress("el_preor_charge", &el_preor_charge, &b_el_preor_charge);
   fChain->SetBranchAddress("el_preor_id_pt", &el_preor_id_pt, &b_el_preor_id_pt);
   fChain->SetBranchAddress("el_preor_id_eta", &el_preor_id_eta, &b_el_preor_id_eta);
   fChain->SetBranchAddress("el_preor_id_phi", &el_preor_id_phi, &b_el_preor_id_phi);
   fChain->SetBranchAddress("el_preor_id_m", &el_preor_id_m, &b_el_preor_id_m);
   fChain->SetBranchAddress("el_preor_cl_pt", &el_preor_cl_pt, &b_el_preor_cl_pt);
   fChain->SetBranchAddress("el_preor_cl_eta", &el_preor_cl_eta, &b_el_preor_cl_eta);
   fChain->SetBranchAddress("el_preor_cl_etaBE2", &el_preor_cl_etaBE2, &b_el_preor_cl_etaBE2);
   fChain->SetBranchAddress("el_preor_cl_phi", &el_preor_cl_phi, &b_el_preor_cl_phi);
   fChain->SetBranchAddress("el_preor_cl_m", &el_preor_cl_m, &b_el_preor_cl_m);
   fChain->SetBranchAddress("el_preor_ptcone20", &el_preor_ptcone20, &b_el_preor_ptcone20);
   fChain->SetBranchAddress("el_preor_ptvarcone20", &el_preor_ptvarcone20, &b_el_preor_ptvarcone20);
   fChain->SetBranchAddress("el_preor_etcone20", &el_preor_etcone20, &b_el_preor_etcone20);
   fChain->SetBranchAddress("el_preor_topoetcone20", &el_preor_topoetcone20, &b_el_preor_topoetcone20);
   fChain->SetBranchAddress("el_preor_ptcone30", &el_preor_ptcone30, &b_el_preor_ptcone30);
   fChain->SetBranchAddress("el_preor_ptvarcone30", &el_preor_ptvarcone30, &b_el_preor_ptvarcone30);
   fChain->SetBranchAddress("el_preor_etcone30", &el_preor_etcone30, &b_el_preor_etcone30);
   fChain->SetBranchAddress("el_preor_topoetcone30", &el_preor_topoetcone30, &b_el_preor_topoetcone30);
   fChain->SetBranchAddress("el_preor_ptcone40", &el_preor_ptcone40, &b_el_preor_ptcone40);
   fChain->SetBranchAddress("el_preor_ptvarcone40", &el_preor_ptvarcone40, &b_el_preor_ptvarcone40);
   fChain->SetBranchAddress("el_preor_etcone40", &el_preor_etcone40, &b_el_preor_etcone40);
   fChain->SetBranchAddress("el_preor_topoetcone40", &el_preor_topoetcone40, &b_el_preor_topoetcone40);
   fChain->SetBranchAddress("el_preor_d0", &el_preor_d0, &b_el_preor_d0);
   fChain->SetBranchAddress("el_preor_d0sig", &el_preor_d0sig, &b_el_preor_d0sig);
   fChain->SetBranchAddress("el_preor_z0", &el_preor_z0, &b_el_preor_z0);
   fChain->SetBranchAddress("el_preor_z0sig", &el_preor_z0sig, &b_el_preor_z0sig);
   fChain->SetBranchAddress("el_preor_demaxs1", &el_preor_demaxs1, &b_el_preor_demaxs1);
   fChain->SetBranchAddress("el_preor_fside", &el_preor_fside, &b_el_preor_fside);
   fChain->SetBranchAddress("el_preor_weta2", &el_preor_weta2, &b_el_preor_weta2);
   fChain->SetBranchAddress("el_preor_ws3", &el_preor_ws3, &b_el_preor_ws3);
   fChain->SetBranchAddress("el_preor_eratio", &el_preor_eratio, &b_el_preor_eratio);
   fChain->SetBranchAddress("el_preor_reta", &el_preor_reta, &b_el_preor_reta);
   fChain->SetBranchAddress("el_preor_rphi", &el_preor_rphi, &b_el_preor_rphi);
   fChain->SetBranchAddress("el_preor_time_cl", &el_preor_time_cl, &b_el_preor_time_cl);
   fChain->SetBranchAddress("el_preor_time_maxEcell", &el_preor_time_maxEcell, &b_el_preor_time_maxEcell);
   fChain->SetBranchAddress("el_preor_truth_pt", &el_preor_truth_pt, &b_el_preor_truth_pt);
   fChain->SetBranchAddress("el_preor_truth_eta", &el_preor_truth_eta, &b_el_preor_truth_eta);
   fChain->SetBranchAddress("el_preor_truth_phi", &el_preor_truth_phi, &b_el_preor_truth_phi);
   fChain->SetBranchAddress("el_preor_truth_E", &el_preor_truth_E, &b_el_preor_truth_E);
   fChain->SetBranchAddress("el_preor_author", &el_preor_author, &b_el_preor_author);
   fChain->SetBranchAddress("el_preor_isConv", &el_preor_isConv, &b_el_preor_isConv);
   fChain->SetBranchAddress("el_preor_passChID", &el_preor_passChID, &b_el_preor_passChID);
   fChain->SetBranchAddress("el_preor_ecisBDT", &el_preor_ecisBDT, &b_el_preor_ecisBDT);
   fChain->SetBranchAddress("el_preor_truth_matched", &el_preor_truth_matched, &b_el_preor_truth_matched);
   fChain->SetBranchAddress("el_preor_truth_mothertype", &el_preor_truth_mothertype, &b_el_preor_truth_mothertype);
   fChain->SetBranchAddress("el_preor_truth_status", &el_preor_truth_status, &b_el_preor_truth_status);
   fChain->SetBranchAddress("el_preor_truth_type", &el_preor_truth_type, &b_el_preor_truth_type);
   fChain->SetBranchAddress("el_preor_truth_typebkg", &el_preor_truth_typebkg, &b_el_preor_truth_typebkg);
   fChain->SetBranchAddress("el_preor_truth_origin", &el_preor_truth_origin, &b_el_preor_truth_origin);
   fChain->SetBranchAddress("el_preor_truth_originbkg", &el_preor_truth_originbkg, &b_el_preor_truth_originbkg);
   fChain->SetBranchAddress("el_preor_isotool_pass_loose", &el_preor_isotool_pass_loose, &b_el_preor_isotool_pass_loose);
   fChain->SetBranchAddress("el_preor_isotool_pass_gradient", &el_preor_isotool_pass_gradient, &b_el_preor_isotool_pass_gradient);
   fChain->SetBranchAddress("el_preor_isotool_pass_gradientloose", &el_preor_isotool_pass_gradientloose, &b_el_preor_isotool_pass_gradientloose);
   fChain->SetBranchAddress("el_preor_met_nomuon_dphi", &el_preor_met_nomuon_dphi, &b_el_preor_met_nomuon_dphi);
   fChain->SetBranchAddress("el_preor_met_wmuon_dphi", &el_preor_met_wmuon_dphi, &b_el_preor_met_wmuon_dphi);
   fChain->SetBranchAddress("el_preor_met_noelectron_dphi", &el_preor_met_noelectron_dphi, &b_el_preor_met_noelectron_dphi);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_fmax", &jet_fmax, &b_jet_fmax);
   fChain->SetBranchAddress("jet_fch", &jet_fch, &b_jet_fch);
   fChain->SetBranchAddress("jet_MV2c00_discriminant", &jet_MV2c00_discriminant, &b_jet_MV2c00_discriminant);
   fChain->SetBranchAddress("jet_MV2c10_discriminant", &jet_MV2c10_discriminant, &b_jet_MV2c10_discriminant);
   fChain->SetBranchAddress("jet_MV2c20_discriminant", &jet_MV2c20_discriminant, &b_jet_MV2c20_discriminant);
   fChain->SetBranchAddress("jet_isbjet", &jet_isbjet, &b_jet_isbjet);
   fChain->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID, &b_jet_PartonTruthLabelID);
   fChain->SetBranchAddress("jet_ConeTruthLabelID", &jet_ConeTruthLabelID, &b_jet_ConeTruthLabelID);
   fChain->SetBranchAddress("jet_met_nomuon_dphi", &jet_met_nomuon_dphi, &b_jet_met_nomuon_dphi);
   fChain->SetBranchAddress("jet_met_wmuon_dphi", &jet_met_wmuon_dphi, &b_jet_met_wmuon_dphi);
   fChain->SetBranchAddress("jet_met_noelectron_dphi", &jet_met_noelectron_dphi, &b_jet_met_noelectron_dphi);
   fChain->SetBranchAddress("jet_met_nophoton_dphi", &jet_met_nophoton_dphi, &b_jet_met_nophoton_dphi);
   fChain->SetBranchAddress("jet_weight", &jet_weight, &b_jet_weight);
   fChain->SetBranchAddress("jet_raw_pt", &jet_raw_pt, &b_jet_raw_pt);
   fChain->SetBranchAddress("jet_raw_eta", &jet_raw_eta, &b_jet_raw_eta);
   fChain->SetBranchAddress("jet_raw_phi", &jet_raw_phi, &b_jet_raw_phi);
   fChain->SetBranchAddress("jet_raw_m", &jet_raw_m, &b_jet_raw_m);
   fChain->SetBranchAddress("jet_timing", &jet_timing, &b_jet_timing);
   fChain->SetBranchAddress("jet_emfrac", &jet_emfrac, &b_jet_emfrac);
   fChain->SetBranchAddress("jet_hecf", &jet_hecf, &b_jet_hecf);
   fChain->SetBranchAddress("jet_hecq", &jet_hecq, &b_jet_hecq);
   fChain->SetBranchAddress("jet_larq", &jet_larq, &b_jet_larq);
   fChain->SetBranchAddress("jet_avglarq", &jet_avglarq, &b_jet_avglarq);
   fChain->SetBranchAddress("jet_negE", &jet_negE, &b_jet_negE);
   fChain->SetBranchAddress("jet_lambda", &jet_lambda, &b_jet_lambda);
   fChain->SetBranchAddress("jet_lambda2", &jet_lambda2, &b_jet_lambda2);
   fChain->SetBranchAddress("jet_jvtxf", &jet_jvtxf, &b_jet_jvtxf);
   fChain->SetBranchAddress("jet_fmaxi", &jet_fmaxi, &b_jet_fmaxi);
   fChain->SetBranchAddress("jet_isbjet_loose", &jet_isbjet_loose, &b_jet_isbjet_loose);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_cleaning", &jet_cleaning, &b_jet_cleaning);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_B", &jet_TruthLabelDeltaR_B, &b_jet_TruthLabelDeltaR_B);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_C", &jet_TruthLabelDeltaR_C, &b_jet_TruthLabelDeltaR_C);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_T", &jet_TruthLabelDeltaR_T, &b_jet_TruthLabelDeltaR_T);
   fChain->SetBranchAddress("jet_preor_pt", &jet_preor_pt, &b_jet_preor_pt);
   fChain->SetBranchAddress("jet_preor_eta", &jet_preor_eta, &b_jet_preor_eta);
   fChain->SetBranchAddress("jet_preor_phi", &jet_preor_phi, &b_jet_preor_phi);
   fChain->SetBranchAddress("jet_preor_m", &jet_preor_m, &b_jet_preor_m);
   fChain->SetBranchAddress("jet_preor_fmax", &jet_preor_fmax, &b_jet_preor_fmax);
   fChain->SetBranchAddress("jet_preor_fch", &jet_preor_fch, &b_jet_preor_fch);
   fChain->SetBranchAddress("jet_preor_MV2c00_discriminant", &jet_preor_MV2c00_discriminant, &b_jet_preor_MV2c00_discriminant);
   fChain->SetBranchAddress("jet_preor_MV2c10_discriminant", &jet_preor_MV2c10_discriminant, &b_jet_preor_MV2c10_discriminant);
   fChain->SetBranchAddress("jet_preor_MV2c20_discriminant", &jet_preor_MV2c20_discriminant, &b_jet_preor_MV2c20_discriminant);
   fChain->SetBranchAddress("jet_preor_isbjet", &jet_preor_isbjet, &b_jet_preor_isbjet);
   fChain->SetBranchAddress("jet_preor_PartonTruthLabelID", &jet_preor_PartonTruthLabelID, &b_jet_preor_PartonTruthLabelID);
   fChain->SetBranchAddress("jet_preor_ConeTruthLabelID", &jet_preor_ConeTruthLabelID, &b_jet_preor_ConeTruthLabelID);
   fChain->SetBranchAddress("jet_preor_met_nomuon_dphi", &jet_preor_met_nomuon_dphi, &b_jet_preor_met_nomuon_dphi);
   fChain->SetBranchAddress("jet_preor_met_wmuon_dphi", &jet_preor_met_wmuon_dphi, &b_jet_preor_met_wmuon_dphi);
   fChain->SetBranchAddress("jet_preor_met_noelectron_dphi", &jet_preor_met_noelectron_dphi, &b_jet_preor_met_noelectron_dphi);
   fChain->SetBranchAddress("jet_preor_met_nophoton_dphi", &jet_preor_met_nophoton_dphi, &b_jet_preor_met_nophoton_dphi);
   fChain->SetBranchAddress("jet_preor_weight", &jet_preor_weight, &b_jet_preor_weight);
   fChain->SetBranchAddress("jet_preor_raw_pt", &jet_preor_raw_pt, &b_jet_preor_raw_pt);
   fChain->SetBranchAddress("jet_preor_raw_eta", &jet_preor_raw_eta, &b_jet_preor_raw_eta);
   fChain->SetBranchAddress("jet_preor_raw_phi", &jet_preor_raw_phi, &b_jet_preor_raw_phi);
   fChain->SetBranchAddress("jet_preor_raw_m", &jet_preor_raw_m, &b_jet_preor_raw_m);
   fChain->SetBranchAddress("jet_preor_timing", &jet_preor_timing, &b_jet_preor_timing);
   fChain->SetBranchAddress("jet_preor_emfrac", &jet_preor_emfrac, &b_jet_preor_emfrac);
   fChain->SetBranchAddress("jet_preor_hecf", &jet_preor_hecf, &b_jet_preor_hecf);
   fChain->SetBranchAddress("jet_preor_hecq", &jet_preor_hecq, &b_jet_preor_hecq);
   fChain->SetBranchAddress("jet_preor_larq", &jet_preor_larq, &b_jet_preor_larq);
   fChain->SetBranchAddress("jet_preor_avglarq", &jet_preor_avglarq, &b_jet_preor_avglarq);
   fChain->SetBranchAddress("jet_preor_negE", &jet_preor_negE, &b_jet_preor_negE);
   fChain->SetBranchAddress("jet_preor_lambda", &jet_preor_lambda, &b_jet_preor_lambda);
   fChain->SetBranchAddress("jet_preor_lambda2", &jet_preor_lambda2, &b_jet_preor_lambda2);
   fChain->SetBranchAddress("jet_preor_jvtxf", &jet_preor_jvtxf, &b_jet_preor_jvtxf);
   fChain->SetBranchAddress("jet_preor_fmaxi", &jet_preor_fmaxi, &b_jet_preor_fmaxi);
   fChain->SetBranchAddress("jet_preor_isbjet_loose", &jet_preor_isbjet_loose, &b_jet_preor_isbjet_loose);
   fChain->SetBranchAddress("jet_preor_jvt", &jet_preor_jvt, &b_jet_preor_jvt);
   fChain->SetBranchAddress("jet_preor_cleaning", &jet_preor_cleaning, &b_jet_preor_cleaning);
   fChain->SetBranchAddress("jet_preor_TruthLabelDeltaR_B", &jet_preor_TruthLabelDeltaR_B, &b_jet_preor_TruthLabelDeltaR_B);
   fChain->SetBranchAddress("jet_preor_TruthLabelDeltaR_C", &jet_preor_TruthLabelDeltaR_C, &b_jet_preor_TruthLabelDeltaR_C);
   fChain->SetBranchAddress("jet_preor_TruthLabelDeltaR_T", &jet_preor_TruthLabelDeltaR_T, &b_jet_preor_TruthLabelDeltaR_T);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_SF", &ph_SF, &b_ph_SF);
   fChain->SetBranchAddress("ph_SF_iso", &ph_SF_iso, &b_ph_SF_iso);
   fChain->SetBranchAddress("ph_isotool_pass_fixedcuttightcaloonly", &ph_isotool_pass_fixedcuttightcaloonly, &b_ph_isotool_pass_fixedcuttightcaloonly);
   fChain->SetBranchAddress("ph_isotool_pass_fixedcuttight", &ph_isotool_pass_fixedcuttight, &b_ph_isotool_pass_fixedcuttight);
   fChain->SetBranchAddress("ph_isotool_pass_fixedcutloose", &ph_isotool_pass_fixedcutloose, &b_ph_isotool_pass_fixedcutloose);
   fChain->SetBranchAddress("ph_m", &ph_m, &b_ph_m);
   fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
   fChain->SetBranchAddress("ph_ptvarcone20", &ph_ptvarcone20, &b_ph_ptvarcone20);
   fChain->SetBranchAddress("ph_etcone20", &ph_etcone20, &b_ph_etcone20);
   fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
   fChain->SetBranchAddress("ph_ptcone30", &ph_ptcone30, &b_ph_ptcone30);
   fChain->SetBranchAddress("ph_ptvarcone30", &ph_ptvarcone30, &b_ph_ptvarcone30);
   fChain->SetBranchAddress("ph_etcone30", &ph_etcone30, &b_ph_etcone30);
   fChain->SetBranchAddress("ph_topoetcone30", &ph_topoetcone30, &b_ph_topoetcone30);
   fChain->SetBranchAddress("ph_ptcone40", &ph_ptcone40, &b_ph_ptcone40);
   fChain->SetBranchAddress("ph_ptvarcone40", &ph_ptvarcone40, &b_ph_ptvarcone40);
   fChain->SetBranchAddress("ph_etcone40", &ph_etcone40, &b_ph_etcone40);
   fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ph_isTight", &ph_isTight, &b_ph_isTight);
   fChain->SetBranchAddress("ph_isEM", &ph_isEM, &b_ph_isEM);
   fChain->SetBranchAddress("ph_OQ", &ph_OQ, &b_ph_OQ);
   fChain->SetBranchAddress("ph_Rphi", &ph_Rphi, &b_ph_Rphi);
   fChain->SetBranchAddress("ph_weta2", &ph_weta2, &b_ph_weta2);
   fChain->SetBranchAddress("ph_fracs1", &ph_fracs1, &b_ph_fracs1);
   fChain->SetBranchAddress("ph_weta1", &ph_weta1, &b_ph_weta1);
   fChain->SetBranchAddress("ph_emaxs1", &ph_emaxs1, &b_ph_emaxs1);
   fChain->SetBranchAddress("ph_f1", &ph_f1, &b_ph_f1);
   fChain->SetBranchAddress("ph_Reta", &ph_Reta, &b_ph_Reta);
   fChain->SetBranchAddress("ph_wtots1", &ph_wtots1, &b_ph_wtots1);
   fChain->SetBranchAddress("ph_Eratio", &ph_Eratio, &b_ph_Eratio);
   fChain->SetBranchAddress("ph_Rhad", &ph_Rhad, &b_ph_Rhad);
   fChain->SetBranchAddress("ph_Rhad1", &ph_Rhad1, &b_ph_Rhad1);
   fChain->SetBranchAddress("ph_e277", &ph_e277, &b_ph_e277);
   fChain->SetBranchAddress("ph_deltae", &ph_deltae, &b_ph_deltae);
   fChain->SetBranchAddress("ph_author", &ph_author, &b_ph_author);
   fChain->SetBranchAddress("ph_isConv", &ph_isConv, &b_ph_isConv);
   fChain->SetBranchAddress("ph_met_nomuon_dphi", &ph_met_nomuon_dphi, &b_ph_met_nomuon_dphi);
   fChain->SetBranchAddress("ph_met_wmuon_dphi", &ph_met_wmuon_dphi, &b_ph_met_wmuon_dphi);
   fChain->SetBranchAddress("ph_met_nophoton_dphi", &ph_met_nophoton_dphi, &b_ph_met_nophoton_dphi);
   fChain->SetBranchAddress("ph_preor_pt", &ph_preor_pt, &b_ph_preor_pt);
   fChain->SetBranchAddress("ph_preor_eta", &ph_preor_eta, &b_ph_preor_eta);
   fChain->SetBranchAddress("ph_preor_phi", &ph_preor_phi, &b_ph_preor_phi);
   fChain->SetBranchAddress("ph_preor_SF", &ph_preor_SF, &b_ph_preor_SF);
   fChain->SetBranchAddress("ph_preor_SF_iso", &ph_preor_SF_iso, &b_ph_preor_SF_iso);
   fChain->SetBranchAddress("ph_preor_isotool_pass_fixedcuttightcaloonly", &ph_preor_isotool_pass_fixedcuttightcaloonly, &b_ph_preor_isotool_pass_fixedcuttightcaloonly);
   fChain->SetBranchAddress("ph_preor_isotool_pass_fixedcuttight", &ph_preor_isotool_pass_fixedcuttight, &b_ph_preor_isotool_pass_fixedcuttight);
   fChain->SetBranchAddress("ph_preor_isotool_pass_fixedcutloose", &ph_preor_isotool_pass_fixedcutloose, &b_ph_preor_isotool_pass_fixedcutloose);
   fChain->SetBranchAddress("ph_preor_m", &ph_preor_m, &b_ph_preor_m);
   fChain->SetBranchAddress("ph_preor_ptcone20", &ph_preor_ptcone20, &b_ph_preor_ptcone20);
   fChain->SetBranchAddress("ph_preor_ptvarcone20", &ph_preor_ptvarcone20, &b_ph_preor_ptvarcone20);
   fChain->SetBranchAddress("ph_preor_etcone20", &ph_preor_etcone20, &b_ph_preor_etcone20);
   fChain->SetBranchAddress("ph_preor_topoetcone20", &ph_preor_topoetcone20, &b_ph_preor_topoetcone20);
   fChain->SetBranchAddress("ph_preor_ptcone30", &ph_preor_ptcone30, &b_ph_preor_ptcone30);
   fChain->SetBranchAddress("ph_preor_ptvarcone30", &ph_preor_ptvarcone30, &b_ph_preor_ptvarcone30);
   fChain->SetBranchAddress("ph_preor_etcone30", &ph_preor_etcone30, &b_ph_preor_etcone30);
   fChain->SetBranchAddress("ph_preor_topoetcone30", &ph_preor_topoetcone30, &b_ph_preor_topoetcone30);
   fChain->SetBranchAddress("ph_preor_ptcone40", &ph_preor_ptcone40, &b_ph_preor_ptcone40);
   fChain->SetBranchAddress("ph_preor_ptvarcone40", &ph_preor_ptvarcone40, &b_ph_preor_ptvarcone40);
   fChain->SetBranchAddress("ph_preor_etcone40", &ph_preor_etcone40, &b_ph_preor_etcone40);
   fChain->SetBranchAddress("ph_preor_topoetcone40", &ph_preor_topoetcone40, &b_ph_preor_topoetcone40);
   fChain->SetBranchAddress("ph_preor_isTight", &ph_preor_isTight, &b_ph_preor_isTight);
   fChain->SetBranchAddress("ph_preor_isEM", &ph_preor_isEM, &b_ph_preor_isEM);
   fChain->SetBranchAddress("ph_preor_OQ", &ph_preor_OQ, &b_ph_preor_OQ);
   fChain->SetBranchAddress("ph_preor_Rphi", &ph_preor_Rphi, &b_ph_preor_Rphi);
   fChain->SetBranchAddress("ph_preor_weta2", &ph_preor_weta2, &b_ph_preor_weta2);
   fChain->SetBranchAddress("ph_preor_fracs1", &ph_preor_fracs1, &b_ph_preor_fracs1);
   fChain->SetBranchAddress("ph_preor_weta1", &ph_preor_weta1, &b_ph_preor_weta1);
   fChain->SetBranchAddress("ph_preor_emaxs1", &ph_preor_emaxs1, &b_ph_preor_emaxs1);
   fChain->SetBranchAddress("ph_preor_f1", &ph_preor_f1, &b_ph_preor_f1);
   fChain->SetBranchAddress("ph_preor_Reta", &ph_preor_Reta, &b_ph_preor_Reta);
   fChain->SetBranchAddress("ph_preor_wtots1", &ph_preor_wtots1, &b_ph_preor_wtots1);
   fChain->SetBranchAddress("ph_preor_Eratio", &ph_preor_Eratio, &b_ph_preor_Eratio);
   fChain->SetBranchAddress("ph_preor_Rhad", &ph_preor_Rhad, &b_ph_preor_Rhad);
   fChain->SetBranchAddress("ph_preor_Rhad1", &ph_preor_Rhad1, &b_ph_preor_Rhad1);
   fChain->SetBranchAddress("ph_preor_e277", &ph_preor_e277, &b_ph_preor_e277);
   fChain->SetBranchAddress("ph_preor_deltae", &ph_preor_deltae, &b_ph_preor_deltae);
   fChain->SetBranchAddress("ph_preor_author", &ph_preor_author, &b_ph_preor_author);
   fChain->SetBranchAddress("ph_preor_isConv", &ph_preor_isConv, &b_ph_preor_isConv);
   fChain->SetBranchAddress("ph_preor_met_nomuon_dphi", &ph_preor_met_nomuon_dphi, &b_ph_preor_met_nomuon_dphi);
   fChain->SetBranchAddress("ph_preor_met_wmuon_dphi", &ph_preor_met_wmuon_dphi, &b_ph_preor_met_wmuon_dphi);
   fChain->SetBranchAddress("ph_preor_met_nophoton_dphi", &ph_preor_met_nophoton_dphi, &b_ph_preor_met_nophoton_dphi);
   fChain->SetBranchAddress("hem_n", &hem_n, &b_hem_n);
   fChain->SetBranchAddress("hem_pt", &hem_pt, &b_hem_pt);
   fChain->SetBranchAddress("hem_eta", &hem_eta, &b_hem_eta);
   fChain->SetBranchAddress("hem_phi", &hem_phi, &b_hem_phi);
   fChain->SetBranchAddress("hem_m", &hem_m, &b_hem_m);
   fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
   fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
   fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
   fChain->SetBranchAddress("tau_idtool_pass_loose", &tau_idtool_pass_loose, &b_tau_idtool_pass_loose);
   fChain->SetBranchAddress("tau_idtool_pass_medium", &tau_idtool_pass_medium, &b_tau_idtool_pass_medium);
   fChain->SetBranchAddress("tau_idtool_pass_tight", &tau_idtool_pass_tight, &b_tau_idtool_pass_tight);
   fChain->SetBranchAddress("tau_multiplicity", &tau_multiplicity, &b_tau_multiplicity);
   fChain->SetBranchAddress("tau_medium_multiplicity", &tau_medium_multiplicity, &b_tau_medium_multiplicity);
   fChain->SetBranchAddress("tau_tight_multiplicity", &tau_tight_multiplicity, &b_tau_tight_multiplicity);
   Notify();
}

Bool_t nominal::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void nominal::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t nominal::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef nominal_cxx
